<?php
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/propertyContentModel.php';

/* Fetching all values form Posted Form*/
$name            = cleanXSS(urldecode($_POST['name']));
$pType         = cleanXSS(urldecode($_POST['type']));
$title         = cleanXSS(urldecode($_POST['title']));
$status         = cleanXSS(urldecode($_POST['status']));
$property_type         = cleanXSS(urldecode($_POST['property_type']));
$short_desc         = cleanXSS(urldecode($_POST['short_desc']));
$long_desc          = cleanXSS(urldecode($_POST['long_desc']));
$website_url           = cleanXSS(urldecode($_POST['website_url']));
$latitude             = cleanXSS(urldecode($_POST['latitude']));
$longitude           = cleanXSS(urldecode($_POST['longitude']));
$imageUrl           = cleanXSS(urldecode($_POST['imageUrl']));
$imageUrl1           = cleanXSS(urldecode($_POST['imageUrl1']));
$pdfUrl2           = cleanXSS(urldecode($_POST['pdfUrl2']));
$id         = cleanXSS(urldecode($_POST['id']));
$p_id         = cleanXSS(urldecode($_POST['p_id']));
$isFeatured         = cleanXSS(urldecode($_POST['isFeatured']));
$file = $_FILES['pdf-url'];


/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/luxuryResidencesImages/";
$targetDirPDF                  = "../uploads/luxuryResidencesImages/";
$checkArr1                  = array();
$returnArr                  = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */

    if ($_POST['method'] == "update") {

      if($file["error"] == 0 ){
          if(end(explode(".",basename($file["name"]))) == "pdf"){
              //9999999

              if($file["size"] < 9999999) {
                  $checkArr2 = array();
                  $fileName = uniqid() . "." . end(explode(".", basename($file["name"])));
                  //$fileName = basename($file["name"]);
                  $targetFilePDF = $targetDirPDF . $fileName;
                  $uploadStatusPDF = uploadPDF($file, $type, $targetFilePDF);
                  if (noError($uploadStatusPDF)) {
                      $checkArr2["success"] = "Successfully uploaded pdf file";
                      $checkArr2["PDFfile"] = "luxuryResidencesImages/" . $fileName;
                  } else {
                      $checkArr2["error"] = $uploadStatusPDF["errMsg"];
                  }
              }else{
                  $checkArr2["error"] = "Please upload file up to size 10 MB only";
              }
          }else{
              $checkArr2["error"] = "Please submit pdf file only!!!";
          }

      }


        /* To upload Color and Black white image */

        foreach ($_FILES as $key => $file) {
            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'luxuryResidencesImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;
            }
        }

        /* Inserting Or Updating DB */
        if (!isset($checkArr1['LuxuryResidences-feature-image']['name']) || empty($checkArr1['LuxuryResidences-feature-image']['name'])) {
                    $image = $imageUrl;
        } else {
                    $image = $checkArr1['LuxuryResidences-feature-image']['name'];
        }

        /* Inserting Or Updating DB */
        if (!isset($checkArr1['LuxuryResidences-feature-image1']['name']) || empty($checkArr1['LuxuryResidences-feature-image1']['name'])) {
                    $enquieryImage = $imageUrl;
        } else {
                    $enquieryImage = $checkArr1['LuxuryResidences-feature-image1']['name'];
        }



        if (!isset( $checkArr1['LuxuryResidences-feature-image']['error'])) {
            $updateLuxuryResidencesDetails = updateproperty_details($id,$p_id,NULL	,$image,$name,$location,$title,$pType,$status,$property_type,NULL,$short_desc,$long_desc,$website_url,$checkArr2['PDFfile'],$latitude,$longitude,$enquieryImage,$isFeatured,$conn);
            /* Deleting Previous image if updated new Image*/
            if (noError($updateLuxuryResidencesDetails)) {
                if (isset($checkArr1['LuxuryResidences-feature-image']['name'])) {
                    unlink('../uploads/'.$imageUrl);
                }
                $returnArr = $updateLuxuryResidencesDetails;
            }
        } else {

            $returnArr = setErrorStack($returnArr, 17);
        }

    } elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removeAbout = removeProperty($id,$conn);
        if (noError($removeAbout)) {
            $returnArr = $removeAbout;
        } else {
            $returnArr = $removeAbout;
        }
    }
}
echo json_encode($returnArr);
?>
