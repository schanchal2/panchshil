<?php

//prepare for request
//start session
session_start();


//include necessary files
require_once '../utilities/config.php'; 
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/enquiryContentModel.php';
/* Fetching all values */


$enquiryArray = array();
$enquiryArray['propertyName']		= cleanXSS(urldecode($_POST['propertyName']));
$enquiryArray['name']				= cleanXSS(urldecode($_POST['name']));
$enquiryArray['email']				= cleanXSS(urldecode($_POST['email']));
$enquiryArray['mobileNumber']		= cleanXSS(urldecode($_POST['mobileNumber']));
$enquiryArray['contactDate']		= cleanXSS(urldecode($_POST['contactDate']));
$enquiryArray['contactTime']		= cleanXSS(urldecode($_POST['contactTime']));



// /* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
	$conn = $conn["conn"];

	if($_POST['method'] == "delete"){
			$id = cleanXSS(urldecode($_POST['id']));
			$removeEnquiryDetails = removeEnquiryDetails($id,$conn);
		if (noError($removeEnquiryDetails)) {
			$returnArr = $removeEnquiryDetails;
		} else {
			$returnArr = $removeEnquiryDetails;
		}

	}elseif($_POST['method'] == "update"){
			$id = cleanXSS(urldecode($_POST['id']));
			$updateEnquiryDetails = updateEnquiryDetails($id,$enquiryArray,$conn);
			if(noError($updateEnquiryDetails)) {
				$returnArr = $updateEnquiryDetails;
			}else{
				$returnArr = $updateEnquiryDetails;
			}
	}

}
 echo json_encode($returnArr);

?>