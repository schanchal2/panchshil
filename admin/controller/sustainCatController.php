<?php

/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/aboutContentModel.php';
//echo json_decode($_POST);die;
/* Fetching all values form Posted Form*/
$id                        = cleanXSS(urldecode($_POST['id']));
$sustainUrlId              = cleanXSS(urldecode($_POST['sustainids']));
$sustain_cat_image1        = cleanXSS(urldecode($_POST['sustain_cat_image1']));
$sustain_cat_long_url      = cleanXSS(urldecode($_POST['sustain_cat_long_url']));
$sustain_cat_sign_image1   = cleanXSS(urldecode($_POST['sustain_cat_sign_image1']));
$name                      = cleanXSS(urldecode($_POST['sustain_cat_name']));
$designation               = cleanXSS(urldecode($_POST['sustain_cat_designation']));

// printArr($_POST);die;

/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/SustainCatImages/";
$checkArr1                  = array();
$returnArr                  = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */
    if ($_POST['method'] == "update") {

        /* To upload Color and Black white image */
        foreach ($_FILES as $key => $file) {

            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'SustainCatImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;
                //printArr($checkArr);
            }
        }
        /* Inserting Or Updating Business Details in DB */
        if (!isset($checkArr1['sustain_cat_image']['name']) || empty($checkArr1['sustain_cat_image']['name'])) {
                    $sustain_cat_image = $sustain_cat_image1;

        } else {
                    $sustain_cat_image = $checkArr1['sustain_cat_image']['name'];
        }
        if (!isset($checkArr1['sustain_cat_sign_image']['name']) || empty($checkArr1['sustain_cat_sign_image']['name'])) {
                    $sign_image_url = $sustain_cat_sign_image1;

        } else {
                    $sign_image_url = $checkArr1['sustain_cat_sign_image']['name'];
        }



        if (!isset( $checkArr1['sustain_cat_sign_image']['error'])) {

            $updateSustainCatDetails = updateSustainCatDetails($id,$sustainUrlId,$status,$sustain_cat_image,$sustain_cat_long_url,$sign_image_url,$name,$designation,$conn);

            //printArr($updateSustainCatDetails); 

            /* Deleting Previous image if updated new Image*/
            if (noError($updateSustainCatDetails)) {
                if (isset($checkArr1['sustain_cat_image']['name'])) {
                    unlink('../uploads/'.$sustain_cat_image1);
                }
                if (isset($checkArr1['sustain_cat_sign_image']['name'])) {
                    unlink('../uploads/'.$sustain_cat_sign_image1);
                }
                $returnArr = $updateSustainCatDetails;
            }
        } else {
            $returnArr = setErrorStack($returnArr, 17);
        }

    } elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removesustaincat = removesustaincat($id,$conn);
        if (noError($removesustaincat)) {
            $returnArr = $removesustaincat;
        } else {
            $returnArr = $removesustaincat;
        }
    }
}
echo json_encode($returnArr);
?>
