<?php
/*banner Controller Created by kavita*/
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/amenitiesContentModel.php';

/* Fetching all values form Posted Form*/
$id                         = cleanXSS(urldecode($_POST['id']));
$o_id                        = cleanXSS(urldecode($_POST['o_id']));
$p_id                        = cleanXSS(urldecode($_POST['p_id']));
$imageUrl                   = cleanXSS(urldecode($_POST['imageUrl']));
$imageUrl1                  = cleanXSS(urldecode($_POST['imageUrl1']));
$title                 = cleanXSS(urldecode($_POST['title']));
$aType                 = cleanXSS(urldecode($_POST['type']));

/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/bannerImages/";
$checkArr1                  = array();
$returnArr                  = array();
/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */
    if ($_POST['method'] == "update") {

        /* To upload Color and Black white image */
        foreach ($_FILES as $key => $file) {

            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'bannerImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;

            }
        }


        /* Inserting Or Updating Client Details in DB */
        if (!isset($checkArr1['banner-feature-image']['name']) || empty($checkArr1['banner-feature-image']['name'])) {
            $image = $imageUrl;
        } else {
            $image = $checkArr1['banner-feature-image']['name'];
        }

        if (!isset($checkArr1['banner-feature-image1']['name']) || empty($checkArr1['banner-feature-image1']['name'])) {
            $image1 = $imageUrl1;
        } else {
            $image1 = $checkArr1['banner-feature-image1']['name'];
        }

        if (!isset( $checkArr1['banner-feature-image']['error']) && !isset( $checkArr1['banner-feature-image1']['error'])) {
            $updateClientDetails = updateAmenitiesDetails($id,$o_id,$p_id,$image,$image1,$aType,$title,$conn);
            /* Deleting image if updated */
            if (noError($updateClientDetails)) {
                if (isset($checkArr1['banner-feature-image']['name']) || !empty($checkArr1['banner-feature-image']['name'])) {
                    unlink('../uploads/'.$clientColorImageUrl);
                }
                if (isset($checkArr1['banner-feature-image1']['name']) || !empty($checkArr1['banner-feature-image1']['name'])) {
                    unlink('../uploads/'.$clientBlackImageUrl);
                }
                $returnArr = $updateClientDetails;
            } else {
                $returnArr = $updateClientDetails;
            }
        } else {
            $errMsg = $checkArr1['banner-feature-image']['name']." AND ".$checkArr1['banner-feature-image1']['name'];
            $returnArr = setErrorStack($returnArr, 17, $errMsg);
        }

    }
    elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removebanner = removeAmenities($id,$conn);
        if (noError($removebanner)) {
            $returnArr = $removebanner;
        } else {
            $returnArr = $removebanner;
        }
    }
}
echo json_encode($returnArr);
?>
