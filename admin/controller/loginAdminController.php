<?php

session_start();
require_once '../utilities/config.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../utilities/authUtils.php';

$returnArr = array();
$userName = $_POST['username'];

$pass = $_POST['password'];

$pass = encryptPassword($pass, $adminSalt);
// echo $pass; die();


if ( $adminUserName == $userName && $adminPassword == $pass ) {
	$returnArr = setErrorStack($returnArr,-1,"Success Authenticating admin username and password");
} else {
	$returnArr = setErrorStack($returnArr,5,"Invalid username or password!!!");
}


if (noError($returnArr)) {
	$_SESSION['email'] = $userName;
	//redirecting to Dashbord of Admin
	print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."views';\", 000);");
    print("</script>");
    die;
} else {
	$_SESSION["error"] = $returnArr["errMsg"];
    //redirect back to root/index
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}



?>