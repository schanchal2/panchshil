<?php
/*banner Controller Created by kavita*/
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/typeContentModel.php';

/* Fetching all values form Posted Form*/
$id                         = cleanXSS(urldecode($_POST['id']));
$o_id                        = cleanXSS(urldecode($_POST['o_id']));
$p_id                        = cleanXSS(urldecode($_POST['p_id']));
$description                  = cleanXSS(urldecode($_POST['description']));
$pagename                  = cleanXSS(urldecode($_POST['pagename']));

/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/bannerImages/";
$checkArr1                  = array();
$returnArr                  = array();
/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */
    if ($_POST['method'] == "update") {
            $updateClientDetails = updateTypeDetails($id,$p_id,$pagename,$description,$conn);
            /* Deleting image if updated */
            if (noError($updateClientDetails)) {
                $returnArr = $updateClientDetails;
            } else {
                $returnArr = $updateClientDetails;
            }
        }  elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removeGallery = removeGallery($id,$conn);
        if (noError($removeGallery)) {
            $returnArr = $removeGallery;
        } else {
            $returnArr = $removeGallery;
        }
    }
}
echo json_encode($returnArr);
?>
