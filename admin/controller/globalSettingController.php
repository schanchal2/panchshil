<?php

//prepare for request
//start session
session_start();


//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php'; 
require_once '../utilities/errorMap.php';
require_once '../model/globalSettingModel.php';
/* Fetching all values */

$returnArr = array();
$globalSettingArray = array();

$globalSettingArray['facebookLink']			= cleanXSS(urldecode($_POST['facebookLink']));
$globalSettingArray['twitterLink'] 			= cleanXSS(urldecode($_POST['twitterLink']));
$globalSettingArray['linkedinLink']    		= cleanXSS(urldecode($_POST['linkedinLink'])); 
$globalSettingArray['googleLink']      		= cleanXSS(urldecode($_POST['googleLink']));
$globalSettingArray['youtubeLink']      	= cleanXSS(urldecode($_POST['youtubeLink']));
//$globalSettingArray['aboutPanchashilLink']    = cleanXSS(urldecode($_POST['aboutPanchashilLink']));
$globalSettingArray['aboutDescription']      = cleanXSS(urldecode($_POST['aboutDescription']));

// /* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
	$conn = $conn["conn"];
	if($_POST['method'] == "update"){
			$Id = cleanXSS(urldecode($_POST['id']));
			$updateGlobalContent = updateGlobalContent($Id,$globalSettingArray,$conn);
			if(noError($updateGlobalContent)) {
				$returnArr = $updateGlobalContent;
			}else{
				$returnArr = $updateGlobalContent;
			}
	}elseif($_POST['method'] == "delete"){
			$Id = cleanXSS(urldecode($_POST['id']));
			$removeGlobalSetting = removeGlobalSetting($Id,$conn);
			if(noError($removeGlobalSetting)) {
				$returnArr = $removeGlobalSetting;
			}else{
				$returnArr = $removeGlobalSetting;
			}
	}
}
 echo json_encode($returnArr);

?>