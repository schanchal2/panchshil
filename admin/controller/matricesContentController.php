<?php
/*Start Session to display error on view page */
session_start();


/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once "../model/matricesContentModel.php";

/* Fetching all values */
$matircesId 			= cleanXSS(urldecode($_POST['matricesId']));
$matircesText 			= cleanXSS(urldecode($_POST['image-title']));
$no_Of_Count    		= cleanXSS(urldecode($_POST['no-of-count'])); 
$matricsImageUrl 		= cleanXSS(urldecode($_POST['imageUrl']));

// /* Declairing target folder to upload image and returning array */
$targetDir 			= "../uploads/matricsImages/";
$checkArr1 			= array();
$returnArr 			= array();

// /* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
	$conn = $conn["conn"];

	/*	@ Checking Method Type 
		@ if method is update then Add new header content
		@ Else delete Content 
	*/
	if ($_POST['method'] == "update") {

		/* To upload Feature image */	
		foreach ($_FILES as $key => $file) {			
			if ($file['error'] == 0) {
				$checkArr = array();
				list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

				/* To check error in File */
				if (!isset($checkArr['error'])) {					
					$fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
					$targetFile = $targetDir . $fileName;

					/* To check folder Existance */
					$folderExist = folderPresenceCheck($targetDir);
					if (noError($folderExist)) {
						/* To Upload Image in Folder */
						$uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
						if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
							$checkArr['success'] 	= $uploadStatus['errMsg'];
							$checkArr['name'] 		= 'matricsImages/'.$fileName;
							$checkArr['errCode'] 	= $uploadStatus['errCode'];
						} else {
							$checkArr['error'] 		= $uploadStatus['errMsg'];
							$checkArr['name'] 		= $file['name'];
						}
					} else {
							$checkArr['error'] 		= "Directed Folder Mismatched";
							$checkArr['name'] 		= $file['name'];
					}
							
				}
				$checkArr1[$key] = $checkArr;
			}
		}
		

		/* Inserting Or Updating Header Content in DB */
		if (!isset($checkArr1['img-logo']['name']) || empty($checkArr1['img-logo']['name'])) {
			$matrics_logo = $matricsImageUrl;
		} else {
			$matrics_logo = $checkArr1['img-logo']['name'];			
		}		
		//$featureImage = $checkArr1['feature-image']['name'];
		
		if (!isset( $checkArr1['img-logo']['error'])) {
			$updateMatricsContent = updateMatricesContent($matircesId,$matrics_logo,$matircesText,$no_Of_Count,$conn);
			
			if (noError($updateMatricsContent)) {
				if (isset($checkArr1['img-logo']['name']) || !empty($checkArr1['img-logo']['name'])) {
					unlink('../uploads/'.$matricsImageUrl);
				}
				$returnArr = $updateMatricsContent;
			} else {
				$returnArr = $updateMatricsContent;
			}
		} else {
			$errMsg = $checkArr1['img-logo']['name'];
			$returnArr = setErrorStack($returnArr, 17, $errMsg);
		}
	}elseif($_POST['method'] == "delete"){
			$matircesId = cleanXSS(urldecode($_POST['matricsId']));
			$removeMatrics = removeMatrics($matircesId,$conn);
		if (noError($removeMatrics)) {
			$returnArr = $removeMatrics;
		} else {
			$returnArr = $removeMatrics;
		}

	}
} 

 echo json_encode($returnArr);

?>