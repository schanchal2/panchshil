<?php
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/businessDetailsModel.php';

/* Fetching all values form Posted Form*/
$businessId                 = cleanXSS(urldecode($_POST['Id']));
$businessStatus             = cleanXSS(urldecode($_POST['business-status']));
$businessBigImageUrl        = cleanXSS(urldecode($_POST['business-big-image-url']));
$businessFeatureImageUrl    = cleanXSS(urldecode($_POST['business-feature-image-url']));
$businessName               = cleanXSS(urldecode($_POST['business-name']));
$businessShortDesc          = cleanXSS(urldecode($_POST['business-short-desc']));
$businessFullDesc           = cleanXSS(urldecode($_POST['business-full-desc']));
$page_type                   = cleanXSS(urldecode($_POST['page_type']));


/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/businessImages/";
$checkArr1                  = array();
$returnArr                  = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type 
        @ if method is update then Add new Business Details
        @ Else delete Business 
    */
    if ($_POST['method'] == "update") {
             
        /* To upload Color and Black white image */        
        foreach ($_FILES as $key => $file) {  
            
            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {                   
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'businessImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }                                        
                }
                $checkArr1[$key] = $checkArr;

            }
        }

       
        /* Inserting Or Updating Business Details in DB */
        if (!isset($checkArr1['business-big-image']['name']) || empty($checkArr1['business-big-image']['name'])) {
            $businessBigImage = $businessBigImageUrl;
        } else {
            $businessBigImage = $checkArr1['business-big-image']['name'];            
        }
        if (!isset($checkArr1['business-feature-image']['name']) || empty($checkArr1['business-feature-image']['name'])) {
            $businessFeatureImage = $businessFeatureImageUrl;
        } else {
            $businessFeatureImage = $checkArr1['business-feature-image']['name'];            
        }
                
        if (!isset( $checkArr1['business-big-image']['error']) && !isset( $checkArr1['business-feature-image']['error'])) {            
            $updateBusinessDetails = updateBusinessDetails($businessId, $businessName, $businessStatus, $businessBigImage, $businessFeatureImage, $businessShortDesc, $businessFullDesc, $page_type,$conn);
            /* Deleting Previous image if updated new Image*/
            if (noError($updateBusinessDetails)) {
                if (isset($checkArr1['business-big-image']['name']) || !empty($checkArr1['business-big-image']['name'])) {
                    unlink('../uploads/'.$businessBigImageUrl);
                }
                if (isset($checkArr1['business-feature-image']['name']) || !empty($checkArr1['business-feature-image']['name'])) {
                    unlink('../uploads/'.$businessFeatureImageUrl);
                }
                $returnArr = $updateBusinessDetails;
            } else {
                $returnArr = $updateBusinessDetails;
            }
        } else {
            $errMsg = $checkArr1['business-big-image']['name']." AND ".$checkArr1['business-feature-image']['name'];
            $returnArr = setErrorStack($returnArr, 17, $errMsg);
        }

    } elseif ($_POST['method'] == "delete") {
        $DeleteBusinessId = cleanXSS(urldecode($_POST['DeleteBusinessId']));
        $removeBusiness = removeBusiness($DeleteBusinessId,$conn);
        if (noError($removeBusiness)) {
            $returnArr = $removeBusiness;
        } else {
            $returnArr = $removeBusiness;
        }
    }
}

echo json_encode($returnArr);

?>