<?php
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/leadershipContentModel.php';

/* Fetching all values form Posted Form*/
$imageUrl           = cleanXSS(urldecode($_POST['ImageUrl']));
$id           = cleanXSS(urldecode($_POST['Id']));
$name            = cleanXSS(urldecode($_POST['name']));
$description      = cleanXSS(urldecode($_POST['description']));
$facebook          = cleanXSS(urldecode($_POST['facebook']));
$twitter   = cleanXSS(urldecode($_POST['twitter']));
$linkedin   = cleanXSS(urldecode($_POST['linkedin']));
$abt_id   = cleanXSS(urldecode($_POST['abt_id']));
$designation   = cleanXSS(urldecode($_POST['designation']));



/* Declairing target folder to upload image and returning array */
$targetDir           = "../uploads/leadershipImages/";
$checkArr1           = array();
$returnArr           = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Client Details
        @ Else delete Client
    */
    if ($_POST['method'] == "update") {

        /* To upload Color and Black white image */
        foreach ($_FILES as $key => $file) {
            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'leadershipImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }

                }
                $checkArr1[$key] = $checkArr;
            }
        }


        /* Inserting Or Updating Client Details in DB */
        if (!isset($checkArr1['leadershipFeatureImage']['name']) || empty($checkArr1['leadershipFeatureImage']['name'])) {
            $leadershipColorImage = $imageUrl;
        } else {
            $leadershipColorImage = $checkArr1['leadershipFeatureImage']['name'];
        }


        if (!isset( $checkArr1['leadershipFeatureImage']['error'])) {
            $updateGrowthDetails = updateLeadershipDetails($abt_id,$id,$leadershipColorImage,$name,$designation,$description,$facebook,$twitter,$linkedin,$conn);


            /* Deleting image if updated */

            if (noError($updateGrowthDetails)) {
                if (isset($checkArr1['leadershipFeatureImage']['name']) || !empty($checkArr1['leadershipFeatureImage']['name'])) {
                    unlink('../uploads/'.$imageUrl);
                }
                $returnArr = $updateGrowthDetails;
            } else {
                $returnArr = $updateGrowthDetails;
            }
        } else {
            $errMsg = $checkArr1['leadershipFeatureImage']['name'];
            $returnArr = setErrorStack($returnArr, 17, $errMsg);
        }

    } elseif ($_POST['method'] == "delete") {
        $DeleteLeadeshipId = cleanXSS(urldecode($_POST['deleteLeadership']));
        $removeLeadeship = removeLeadership($DeleteLeadeshipId,$conn);
        if (noError($removeLeadeship)) {
            $returnArr = $removeLeadeship;
        } else {
            $returnArr = $removeLeadeship;
        }
    }
}

echo json_encode($returnArr);

?>
