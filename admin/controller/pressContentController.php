<?php
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/pressContentModel.php';

/* Fetching all values form Posted Form*/
$pressId            = cleanXSS(urldecode($_POST['Id']));
$pressTitle         = cleanXSS(urldecode($_POST['press-title']));
$pressDescription   = cleanXSS(urldecode($_POST['press-description']));
$sourceName         = cleanXSS(urldecode($_POST['source-name']));
$pressDate          = cleanXSS(urldecode($_POST['press-date']));
$ImageUrl           = cleanXSS(urldecode($_POST['imageUrl']));
$pdfUrl             = cleanXSS(urldecode($_POST['pdfUrl']));
$videoUrl           = cleanXSS(urldecode($_POST['VideoUrl']));
$linkUrl            = cleanXSS(urldecode($_POST['LinkUrl']));

/* Declairing target folder to upload image and returning array */
$targetedDir        = "../uploads/pressContents/";
$targetPdf          = "../uploads/pressContents/pressPdf";
$targetImages       = "../uploads/pressContents/pressImages";


$checkArr1          = array();
$returnArr          = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type 
        @ if method is update then Add new Client Details
        @ Else delete Client 
    */
    if ($_POST['method'] == "update") {
             
        /* To upload Color and Black white image */        
        foreach ($_FILES as $key => $file) {  
            
            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {   
                    $folderExist = folderPresenceCheck($targetedDir);
                    if (noError($folderExist)) {
                        $fileExc = basename($file['name']);
                        switch ($fileExc) {
                            case ($fileExc == 'pdf'):
                                $targetDir = $targetPdf;
                                $targetFolder = "pressPdf";
                                break;
                            case ($fileExc == 'jpg' || $fileExc == 'png' $fileExc == 'jpeg' || $fileExc == 'gif' || $fileExc == 'svg' || $fileExc == 'tiff' || $fileExc == 'bmp'):
                                $targetDir = $targetImages;
                                $targetFolder = "pressImages";
                                break;                            
                            default:
                                $targetDir = $targetedDir;
                                $targetFolder = "pressContents";
                                break;
                        }
                        $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                        $targetFile = $targetDir . $fileName; 

                        /* To check folder Existance */
                        $folderExist = folderPresenceCheck($targetDir);
                        if (noError($folderExist)) {
                            /* To Upload Image in Folder */
                            $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                            if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                                $checkArr['success']    = $uploadStatus['errMsg'];
                                $checkArr['name']       = $targetFolder.'/'.$fileName;//Targeted Folder with file name
                                $checkArr['errCode']    = $uploadStatus['errCode'];
                            } else {
                                $checkArr['error']      = $uploadStatus['errMsg'];
                                $checkArr['name']       = $file['name'];
                            }
                        } else {
                            $checkArr['error']      = "Directed Folder Mismatched";
                            $checkArr['name']       = $file['name'];
                        }
                    }                                  
                }
                $checkArr1[$key] = $checkArr;
            }
        }

       
        /* Inserting Or Updating Client Details in DB */
        if (!isset($checkArr1['client-colour-photo']['name']) || empty($checkArr1['client-colour-photo']['name'])) {
            $clientColorImage = $clientColorImageUrl;
        } else {
            $clientColorImage = $checkArr1['client-colour-photo']['name'];            
        }
        if (!isset($checkArr1['client-black-photo']['name']) || empty($checkArr1['client-black-photo']['name'])) {
            $clientGreyImage = $clientBlackImageUrl;
        } else {
            $clientGreyImage = $checkArr1['client-black-photo']['name'];            
        }
                
        if (!isset( $checkArr1['client-colour-photo']['error']) && !isset( $checkArr1['client-black-photo']['error'])) {
            $updateClientDetails = updateClientDetails($clientId,$clientName,$clientColorImage,$clientGreyImage,$services,$clientUrl,$conn);
            /* Deleting image if updated */
            if (noError($updateClientDetails)) {
                if (isset($checkArr1['client-colour-photo']['name']) || !empty($checkArr1['client-colour-photo']['name'])) {
                    unlink('../uploads/'.$clientColorImageUrl);
                }
                if (isset($checkArr1['client-black-photo']['name']) || !empty($checkArr1['client-black-photo']['name'])) {
                    unlink('../uploads/'.$clientBlackImageUrl);
                }
                $returnArr = $updateClientDetails;
            } else {
                $returnArr = $updateClientDetails;
            }
        } else {
            $errMsg = $checkArr1['client-colour-photo']['name']." AND ".$checkArr1['client-black-photo']['name'];
            $returnArr = setErrorStack($returnArr, 17, $errMsg);
        }

    } elseif ($_POST['method'] == "delete") {
        $DeleteClientId = cleanXSS(urldecode($_POST['DeleteClientId']));
        $removeClient = removeClient($DeleteClientId,$conn);
        if (noError($removeClient)) {
            $returnArr = $removeClient;
        } else {
            $returnArr = $removeClient;
        }
    }
}

echo json_encode($returnArr);

?>