<?php
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/clientDetailsModel.php';

/* Fetching all values form Posted Form*/
$clientId               = cleanXSS(urldecode($_POST['clientid']));
$clientName             = cleanXSS(urldecode($_POST['client-name']));
$clientColorImageUrl    = cleanXSS(urldecode($_POST['clientColorImageUrl']));
$services               = cleanXSS(urldecode($_POST['client-services']));
$clientUrl              = cleanXSS(urldecode($_POST['client-url']));
$clienUrlId             = cleanXSS(urldecode($_POST['clientUrlId']));
$clientServicesBelong   = cleanXSS(urldecode($_POST['client-services-belong']));


/* Declairing target folder to upload image and returning array */
$targetDir              = "../uploads/clientImages/";
$checkArr1              = array();
$returnArr              = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type 
        @ if method is update then Add new Client Details
        @ Else delete Client 
    */
    if ($_POST['method'] == "update") {
             
        /* To upload Color and Black white image */        
        foreach ($_FILES as $key => $file) {  
            
            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {                   
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'clientImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }                                        
                }
                $checkArr1[$key] = $checkArr;

            }
        }
        

       
        /* Inserting Or Updating Client Details in DB */
        if (!isset($checkArr1['client-colour-photo']['name']) || empty($checkArr1['client-colour-photo']['name'])) {
            $clientColorImage = $clientColorImageUrl;
        } else {
            $clientColorImage = $checkArr1['client-colour-photo']['name'];            
        }
                
        if (!isset( $checkArr1['client-colour-photo']['error'])) {
            $updateClientDetails = updateClientDetails($clientId, $clienUrlId, $clientName, $clientColorImage, $services, $clientServicesBelong, $clientUrl, $conn);
            /* Deleting image if updated */
            if (noError($updateClientDetails)) {
                if (isset($checkArr1['client-colour-photo']['name']) || !empty($checkArr1['client-colour-photo']['name'])) {
                    unlink('../uploads/'.$clientColorImageUrl);
                }
                $returnArr = $updateClientDetails;
            } else {
                $returnArr = $updateClientDetails;
            }
        } else {
            $errMsg = $checkArr1['client-colour-photo']['name'];
            $returnArr = setErrorStack($returnArr, 17, $errMsg);
        }

    } elseif ($_POST['method'] == "delete") {
        $DeleteClientId     = cleanXSS(urldecode($_POST['DeleteClientId']));
        $DeleteClientUrlId  = cleanXSS(urldecode($_POST['DeleteClientUrlId']));

        $removeClient       = removeClient($DeleteClientId, $DeleteClientUrlId, $conn);
        if (noError($removeClient)) {
            $returnArr = $removeClient;
        } else {
            $returnArr = $removeClient;
        }
    }
}

echo json_encode($returnArr);

?>