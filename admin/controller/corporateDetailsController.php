<?php

/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/corporateDetailsModel.php';

/* Fetching all values form Posted Form*/
$id                         = cleanXSS(urldecode($_POST['id']));
$corporateUrlId             = cleanXSS(urldecode($_POST['corporateUrlId']));
$status                     = cleanXSS(urldecode($_POST['corporateStatus']));
$imageUrl                   = cleanXSS(urldecode($_POST['imageUrl']));
$name                       = cleanXSS(urldecode($_POST['corporate-name']));
$shortDesc                  = cleanXSS(urldecode($_POST['corporate-shortDesc']));
$longDesc                   = cleanXSS(urldecode($_POST['corporate-longDesc']));
$pagetype                   = cleanXSS(urldecode($_POST['pagetype']));
//echo json_encode($_POST);die;

/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/corporateImages/";
$checkArr1                  = array();
$returnArr                  = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
    @ if method is update then Add new Business Details
    @ Else delete Business
     */
    if ($_POST['method'] == "update") {
        /* To upload Color and Black white image */
        foreach ($_FILES as $key => $file) {
            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'corporateImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;

            }
        }
        // printArr($checkArr1);
        /* Inserting Or Updating DB */
        if (!isset($checkArr1['corporate-image']['name']) || empty($checkArr1['corporate-image']['name'])) {
            $image = $imageUrl;
        } else {
            $image = $checkArr1['corporate-image']['name'];
        }
        if (!isset( $checkArr1['corporate-image']['error'])) {
            $updateCorporateDetails = updateCorporateDetails($id, $corporateUrlId, $status, $image, $name, $shortDesc, $longDesc, $pagetype, $conn);
            /* Deleting Previous image if updated new Image*/
            if (noError($updateCorporateDetails)) {
                if (isset($checkArr1['corporate-image']['name'])) {
                    unlink('../uploads/'.$imageUrl);
                }
                $returnArr = $updateCorporateDetails;
            }
        } else {

            $returnArr = setErrorStack($returnArr, 17);
        }

    } elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $corporateDeleteId = cleanXSS(urldecode($_POST['deleteCorporateUrlId']));
        $removeCompanyProfile = removeCompanyProfile($id, $corporateDeleteId, $conn);
        if (noError($removeCompanyProfile)) {
            $returnArr = $removeCompanyProfile;
        } else {
            $returnArr = $removeCompanyProfile;
        }
    }
}
echo json_encode($returnArr);
?>