<?php

/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/forwordModel.php';

/* Fetching all values form Posted Form*/
$id                 = cleanXSS(urldecode($_POST['id']));
$forword_image_url  = cleanXSS(urldecode($_POST['forword_image_url']));
$long_url           = cleanXSS(urldecode($_POST['long_url']));
$sign_image_url     = cleanXSS(urldecode($_POST['sign_image_url']));
$name               = cleanXSS(urldecode($_POST['name']));
$designation        = cleanXSS(urldecode($_POST['designation']));
$forword_id         = cleanXSS(urldecode($_POST['forword_id']));

/* Declairing target folder to upload image and returning array */
$targetDir          = "../uploads/forwordImages/";
$checkArr1          = array();
$returnArr          = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */
    if ($_POST['method'] == "update") {

        /* To upload Color and Black white image */
        foreach ($_FILES as $key => $file) {

            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'forwordImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;
            }
        }
        /* Inserting Or Updating Business Details in DB */
        if (!isset($checkArr1['forword_image']['name']) || empty($checkArr1['forword_image']['name'])) {
                    $image = $forword_image_url;

        } else {
                    $image = $checkArr1['forword_image']['name'];
        }
        if (!isset($checkArr1['sign_image']['name']) || empty($checkArr1['sign_image']['name'])) {
                    $sign_image = $sign_image_url;

        } else {
                    $sign_image = $checkArr1['sign_image']['name'];
        }

        if (!isset( $checkArr1['sign_image']['error'])) {
            $updateForwordDetails = updateForwordDetails($id,$forword_id,$image,$long_url,$sign_image,$name,$designation,$conn);
            /* Deleting Previous image if updated new Image*/
            if (noError($updateForwordDetails)) {
                if (isset($checkArr1['forword_image']['name'])) {
                    unlink('../uploads/'.$forword_image_url);
                }
                if (isset($checkArr1['sign_image']['name'])) {
                    unlink('../uploads/'.$sign_image_url);
                }
                $returnArr = $updateForwordDetails;
            }
        } else {
            $returnArr = setErrorStack($returnArr, 17);
        }

    } elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removeforword = removeforword($id,$conn);
        if (noError($removeforword)) {
            $returnArr = $removeforword;
        } else {
            $returnArr = $removeforword;
        }
    }
}
echo json_encode($returnArr);
?>
