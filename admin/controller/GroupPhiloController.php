<?php

/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/aboutContentModel.php';

/* Fetching all values form Posted Form*/
$id          = cleanXSS(urldecode($_POST['id']));
$group_image       = cleanXSS(urldecode($_POST['image']));
$long_url    = cleanXSS(urldecode($_POST['long_url']));
$group_philo_image_url  = cleanXSS(urldecode($_POST['group_philo_image']));
$name        = cleanXSS(urldecode($_POST['name']));
$designation = cleanXSS(urldecode($_POST['designation']));

/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/GroupPhilo/";
$checkArr1                  = array();
$returnArr                  = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */
    if ($_POST['method'] == "update") {

        /* To upload Color and Black white image */
        foreach ($_FILES as $key => $file) {

            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'GroupPhilo/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;
            }
        }
        /* Inserting Or Updating Business Details in DB */
        if (!isset($checkArr1['group_image']['name']) || empty($checkArr1['group_image']['name'])) {
                    $image = $group_image;

        } else {
                    $image = $checkArr1['group_image']['name'];
        }
        if (!isset($checkArr1['group_philo_image']['name']) || empty($checkArr1['group_philo_image']['name'])) {
                    $group_philo_image = $group_philo_image_url;

        } else {
                    $group_philo_image = $checkArr1['group_philo_image']['name'];
        }

        if (!isset( $checkArr1['group_philo_image']['error'])) {
            $updateGroupPhilo = updateGroupPhilo($id,$image,$long_url,$group_philo_image,$name,$designation,$conn);
            /* Deleting Previous image if updated new Image*/
            if (noError($updateGroupPhilo)) {
                if (isset($checkArr1['group_image']['name'])) {
                    unlink('../uploads/'.$group_image);
                }
                if (isset($checkArr1['group_philo_image']['name'])) {
                    unlink('../uploads/'.$group_philo_image_url);
                }
                $returnArr = $updateGroupPhilo;
            }
        } else {
            $returnArr = setErrorStack($returnArr, 17);
        }

    } elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removeGroupPhilo = removeGroupPhilo($id,$conn);
        if (noError($removeGroupPhilo)) {
            $returnArr = $removeGroupPhilo;
        } else {
            $returnArr = $removeGroupPhilo;
        }
    }
}
echo json_encode($returnArr);
?>
