<?php

/**
 * function name: noError();
 * parameters: $resArray(array)
 * purpose: to check success of failure of the function.
 */
function noError($resArray) {
    $noError = false;
    if($resArray["errCode"] == -1){
        $noError = true;
    }
    return $noError;
}

/**
 * function name: getErrorStack();
 * parameters: $resArray(array)
 * purpose: to get full error stack
 */
function getErrorStack($resArray){
    $returnArr = array();
    $errStack = $resArray["errCode"];
    foreach($errStack as $errCode){
        echo "<br>Error code: ".$errCode." Error msg: ".getErrMsg($errCode);
    }
}

/**
 * function name: printArr();
 * parameters: $returnArr(array)
 * purpose: to display array in proper format
 */
function printArr($returnArr){
    echo "<pre>";
    print_r($returnArr);
    echo "</pre>";
}

/**
 * function name: cleanXSS();
 * parameters: $string(string)
 * purpose: to upload images
 */
function cleanXSS($string){
    return htmlspecialchars($string,ENT_QUOTES,'UTF-8');
}

/**
 * function name: uploadImage();
 * parameters: $file(array),$fileWidth(number),$fileHeight(number),$fileType(string)
 * purpose: to upload images
 */
function uploadImage($file,$fileWidth,$fileHeight,$fileType,$targetFile){
    $returnArr = array();
    $check = getimagesize($file["tmp_name"]);
    if($check !== false) {
        if(!file_exists($targetFile)){
            if(move_uploaded_file($file["tmp_name"], $targetFile)){
                $errMsg = "Successfully uploaded file ".$file["name"];
                $returnArr = setErrorStack($returnArr, -1, $errMsg);
            }else{
                $returnArr = setErrorStack($returnArr, 11);
            }
        }else{
            $returnArr = setErrorStack($returnArr, 8);
        }

    }else{
        $returnArr = setErrorStack($returnArr, 10);
    }

    return $returnArr;
}

/**
 * function name: uploadImage();
 * parameters: $file(array),$fileWidth(number),$fileHeight(number),$fileType(string)
 * purpose: to upload images
 */
function uploadPDF($file,$fileType,$targetFile){
    $returnArr = array();
    if(!file_exists($targetFile)){
        if(move_uploaded_file($file["tmp_name"], $targetFile)){
            $errMsg = "Successfully uploaded file ".$file["name"];
            $returnArr = setErrorStack($returnArr, -1, $errMsg);
        }else{
            $returnArr = setErrorStack($returnArr, 11);
        }
    }else{
        $returnArr = setErrorStack($returnArr, 8);
    }
    return $returnArr;
}

/**
 * function name: folderPresenceCheck();
 * parameters: $dir(string)
 * purpose: to Check folder Existance and if not then create it
 */
function folderPresenceCheck ($dir) {
    $returnArr = array();
    $dir = rtrim($dir,'/');    
    if (!file_exists($dir)) {        
        $makeDirectory = @mkdir($dir, 0777, true);
        if($makeDirectory) {
            $errMsg = "Successfully Created Folder";
            $returnArr = setErrorStack($returnArr, -1, $errMsg);
        } else {
            $returnArr = setErrorStack($returnArr, 43);
        }
    } else {
        $errMsg = "Folder Already Exist";
        $returnArr = setErrorStack($returnArr, -1, $errMsg);
    }
    return $returnArr;
}
?>