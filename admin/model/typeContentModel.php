<?php

/*Created By Kavita*/
/**************************************************************************************************
    @ function name: updateTestimonialDetails();
    @ purpose: to Add or update Testimonial Details to Database
 *************************************************************************************************/

function updateTypeDetails($id,$p_id,$pagename,$name,$conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }
    if (!isset($id) || empty($id)) {
        $query = "INSERT INTO `typeDetails` (`id`,`p_id`, `pagename`, `description`, `created`) VALUES (NULL,'".cleanQueryParameter($p_id,$conn)."', '".cleanQueryParameter($pagename,$conn)."', '".cleanQueryParameter($name,$conn)."', CURRENT_TIMESTAMP);";
  }
  else
   {
    # code...
     $query="UPDATE `clientTestimonial` SET `name` = '".cleanQueryParameter($name,$conn)."', `image` = '".cleanQueryParameter($imageUrl,$conn)."', `desigantion` = '".cleanQueryParameter($designation,$conn)."', `description` = '".cleanQueryParameter($longDesc,$conn)."' WHERE id = '".cleanQueryParameter($id,$conn)."';";
  }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $errMsg = "Successfully updated mediaDetails content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
      $returnArr = setErrorStack($queryResult, 35);
    }

    return $returnArr;
}

/***************************************************************************************************
    @ function name: getTestimonialDetails();

    @ purpose: to fetch Testimonial Details
 **************************************************************************************************/

function getTypeDetails($pagename,$conn){
    $query = "SELECT * FROM typeDetails WHERE pagename  ='".cleanQueryParameter($pagename,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Page Name content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}


//
//
// /***************************************************************************************************
//     @ function name: getTestimonialDetailsById();
//
//     @ purpose: to fetch Testimonial Details By ID
//  **************************************************************************************************/
//
// function getTestimonialDetailsById($id,$conn){
//   $query = "SELECT * FROM clientTestimonial WHERE award_id ='".cleanQueryParameter($id,$conn)."'";
//     $queryResult = runQuery($query,$conn);
//     if(noError($queryResult)){
//         $res = array();
//         while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
//             $res["response"][] = $row;
//         $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Client testimonial content",$res);
//     }else{
//         $returnArr = setErrorStack($queryResult,28);
//     }
//     return $returnArr;
// }
//
//
//
//
// /***************************************************************************************************
//     @ function name: getTestimonialDetailsByTestId();
//
//     @ purpose: to fetch Testimonial Details By ID
//  **************************************************************************************************/
//
// function getTestimonialDetailsByTestId($id,$conn){
//   $query = "SELECT * FROM clientTestimonial WHERE id ='".cleanQueryParameter($id,$conn)."'";
//     $queryResult = runQuery($query,$conn);
//     if(noError($queryResult)){
//         $res = array();
//         while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
//             $res["response"][] = $row;
//         $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Client testimonial content",$res);
//     }else{
//         $returnArr = setErrorStack($queryResult,28);
//     }
//     return $returnArr;
// }
//
//
// /**************************************************************************************************
//     @ Function Name : removeTestimonial()
//     @ Purpose : Deleteing Testimonial Details from database and Image from folder
// ***************************************************************************************************/
//
//
//
//   function removeTestimonial($Id,$conn) {
//       $returnArr = array();
//         $query = "SELECT * FROM clientTestimonial WHERE id ='".cleanQueryParameter($Id,$conn)."'";
//       $queryResult = runQuery($query, $conn);
//       $res = array();
//       while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
//           $res["response"][] = $row;
//       }
//
//         $query1 = "DELETE FROM clientTestimonial WHERE id = '".cleanQueryParameter($Id,$conn)."'";
//       $queryResult1 = runQuery($query1,$conn);
//       if (noError($queryResult1)) {
//           unlink("../uploads/".$res["response"][0]["image"]);
//           $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Award Details");
//       } else {
//           $returnArr = setErrorStack($queryResult1,36);
//       }
//       return $returnArr;
//   }
?>
