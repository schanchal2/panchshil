<?php

/**************************************************************************************************
    @ function name: updateClientDetails();
    @ parameters: $clientId(integer[Auto]),$clientName(String),$clientColorImage(String),$clientGreyImage(String),$services(String),$clientUrl(string),$conn(string)
    @ purpose: to Add or update Client Details to Database
 *************************************************************************************************/
function updateBusinessDetails($BusinessId, $businessName, $businessStatus, $businessBigImage, $businessFeatureImage, $businessShortDesc, $businessFullDesc, $page_type,$conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($businessStatus != 'Inactive') {
        $businessStatus = 'Active';
    }
    if ( !empty($businessBigImage) && !empty($businessFeatureImage) ) {

        /* checking for Add new data or Update Existin Data */
        if (!isset($BusinessId) || empty($BusinessId)) {                          
            $query = "INSERT INTO business_details (business_name, business_status, business_big_image, business_feature_image, business_short_desc, business_full_desc,pagename) VALUE ('".cleanQueryParameter($businessName,$conn)."','".cleanQueryParameter($businessStatus,$conn)."','".cleanQueryParameter($businessBigImage,$conn)."','".cleanQueryParameter($businessFeatureImage,$conn)."','".cleanQueryParameter($businessShortDesc,$conn)."','".cleanQueryParameter($businessFullDesc,$conn)."','".cleanQueryParameter($page_type,$conn)."')";                
        } else { 
            $query = "UPDATE business_details SET business_name='".cleanQueryParameter($businessName,$conn)."', business_status='".cleanQueryParameter($businessStatus,$conn)."',business_big_image='".cleanQueryParameter($businessBigImage,$conn)."',business_feature_image='".cleanQueryParameter($businessFeatureImage,$conn)."',business_short_desc='".cleanQueryParameter($businessShortDesc,$conn)."',business_full_desc='".cleanQueryParameter($businessFullDesc,$conn)."' WHERE id='".cleanQueryParameter($BusinessId,$conn)."'";       
        }
        $queryResult = runQuery($query,$conn);
        if(noError($queryResult)){
            $errMsg = "Successfully updated Business content";
            $returnArr = setErrorStack($queryResult, -1, $errMsg);
        }else{
            $returnArr = setErrorStack($queryResult, 35);
        }  
    } 
    else {
        $returnArr = setErrorStack($queryResult, 21);
    }
    return $returnArr;    
}

/***************************************************************************************************
    @ function name: getClientDetails();
    @ parameters: $clientId(Integer),$conn(string)
    @ purpose: to fetch Client Details
 **************************************************************************************************/
function getBusinessDetails($businessId,$require,$conn){

    if (!isset($businessId) || empty($businessId)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM business_details";
        } else {
            $query = "SELECT * FROM business_details WHERE business_status='Active'";
        }        
    } else {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM business_details WHERE id ='".cleanQueryParameter($businessId,$conn)."'";
        } else {
            $query = "SELECT * FROM business_details WHERE id ='".cleanQueryParameter($businessId,$conn)."' AND business_status='Active'";
        }        
    }   

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched business_details content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

/**************************************************************************************************
    @ Function Name : removeClient()
    @ Parameters : $businessId(Integer),$conn(string)
    @ Purpose : Deleteing CLient Details from database and Image from folder 
***************************************************************************************************/
function removeBusiness($DeleteBusinessId,$conn) {
    $returnArr = array();

    $query = "SELECT * FROM business_details WHERE id ='".cleanQueryParameter($DeleteBusinessId,$conn)."'";
    $queryResult = runQuery($query, $conn);
    $res = array();
    while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
        $res["response"][] = $row;
    }

    if ($res["response"][0]["business_status"] == "Active") {
        $status = "Inactive";
    } else {
        $status = "Active";
    }

    $query1 = "UPDATE business_details SET business_status='".cleanQueryParameter($status,$conn)."' WHERE id='".cleanQueryParameter($DeleteBusinessId,$conn)."'";   
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Business Details");
    } else {
        $returnArr = setErrorStack($queryResult1,36);
    }
    return $returnArr;
}

?>