<?php

/*Created By Kavita*/
/**************************************************************************************************
    @ function name: updatePressDetails();
    @ purpose: to Add or update Press Details to Database
 *************************************************************************************************/

function updateFeatureDetails($id,$o_id,$p_id,$countPos,$name,$galaryImagesArray,$conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }
    if (!isset($id) || empty($id)) {

        $query = "INSERT INTO `feature_specification_details` (`id`, `o_id`,`p_id`,`count_id`, `image`, `description`, `created`) VALUES (NULL, '".cleanQueryParameter($o_id,$conn)."','".cleanQueryParameter($p_id,$conn)."','".cleanQueryParameter($countPos,$conn)."', '".cleanQueryParameter($galaryImagesArray,$conn)."', '".cleanQueryParameter($name,$conn)."', CURRENT_TIMESTAMP);";

    } else {
$query = "UPDATE `banner_details` SET `banner1` = '".cleanQueryParameter($imageUrl,$conn)."', `banner2` = '".cleanQueryParameter($imageUrl1,$conn)."', `banner3` = '".cleanQueryParameter($imageUrl2,$conn)."', `banner4` = '".cleanQueryParameter($imageUrl3,$conn)."', `banner5` = '".cleanQueryParameter($imageUrl4,$conn)."' WHERE `id` = '".cleanQueryParameter($id,$conn)."';";
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $errMsg = "Successfully updated Press  content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
      $returnArr = setErrorStack($queryResult, 35);
    }

    return $returnArr;
}



/***************************************************************************************************
    @ function name: getfeatureSpecificationDetailsById();

    @ purpose: to fetch Gallery Details By Id
 **************************************************************************************************/

function getfeatureSpecificationDetailsById($id,$require,$conn){
    if (!empty($id) && empty($require)) {
   $query = "SELECT * FROM feature_specification_details WHERE p_id ='".cleanQueryParameter($id,$conn)."' ORDER BY `id` DESC ";
}
else {
   $query = "SELECT * FROM feature_specification_details WHERE p_id ='".cleanQueryParameter($id,$conn)."' ";
}
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Gallery content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}



/***************************************************************************************************
    @ function name: getFeatureSpecificationDetailsByUniqId();

    @ purpose: to fetch Feature Specification Details By Id
 **************************************************************************************************/

function getFeatureSpecificationDetailsByUniqId($id,$conn){
  $query = "SELECT * FROM feature_specification_details WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Feature Specification content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}


/***************************************************************************************************
    @ function name: updateFeatureSpecificationInfoById();

    @ purpose: to fetch Feature Specification Details By Id
 **************************************************************************************************/

function updateFeatureSpecificationInfoById($id,$imageUrl,$description,$conn)
{
      $query = "UPDATE `feature_specification_details` SET `image` = '".cleanQueryParameter($imageUrl,$conn)."', `description` = '".cleanQueryParameter($description,$conn)."' WHERE `id` = '".cleanQueryParameter($id,$conn)."';";
      $queryResult = runQuery($query,$conn);
      if(noError($queryResult)){
      $errMsg = "Successfully updated Feature Specification Content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
      }else{
      $returnArr = setErrorStack($queryResult, 35);
      }
      return $returnArr;
}


/**************************************************************************************************
    @ Function Name : removeFeatureSpecification()
    @ Purpose : Deleteing Feature Specification from database and Image from folder
***************************************************************************************************/



  function removeFeatureSpecification($Id,$conn) {
            $returnArr = array();
            $query = "SELECT * FROM feature_specification_details WHERE id ='".cleanQueryParameter($Id,$conn)."'";
            $queryResult = runQuery($query, $conn);
            $res = array();
            while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
            $res["response"][] = $row;
            }
            $query1 = "DELETE FROM feature_specification_details WHERE id = '".cleanQueryParameter($Id,$conn)."'";
            $queryResult1 = runQuery($query1,$conn);
            if (noError($queryResult1)) {
            unlink("../uploads/".$res["response"][0]["image"]);
            $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Feature Specification Details");
            } else {
            $returnArr = setErrorStack($queryResult1,36);
            }
            return $returnArr;
  }
?>
