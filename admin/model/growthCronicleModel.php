<?php

/**************************************************************************************************
    @ function name: updateClientDetails();
    @ parameters: $clientId(integer[Auto]),$clientName(String),$clientColorImage(String),$clientGreyImage(String),$services(String),$clientUrl(string),$conn(string)
    @ purpose: to Add or update Client Details to Database
 *************************************************************************************************/
function updateGrowthDetails($growthId,$growthUrlId,$growthColorImage,$growthYear,$growthDescription,$conn) {
    $returnArr = array();

    /* Checking Image is present or not */
    if ( !empty($growthColorImage)) {

        /* checking for Add new data or Update Existin Data commenting test*/


        if (empty($growthId)) {
            $query = "INSERT INTO growth_details (growthUrlId,growthImage, growthYear,growthDescription) VALUE ('".cleanQueryParameter($growthUrlId,$conn)."','".cleanQueryParameter($growthColorImage,$conn)."','".cleanQueryParameter($growthYear,$conn)."','".cleanQueryParameter($growthDescription,$conn)."')";
        } else {
            $query = "UPDATE growth_details SET growthUrlId='".cleanQueryParameter($growthUrlId,$conn)."', growthImage='".cleanQueryParameter($growthColorImage,$conn)."', growthYear='".cleanQueryParameter($growthYear,$conn)."',growthDescription='".cleanQueryParameter($growthDescription,$conn)."' WHERE id='".cleanQueryParameter($growthId,$conn)."'";
        }
        $queryResult = runQuery($query,$conn);
        if(noError($queryResult)){
            $errMsg = "Successfully updated growth_details content";
            $returnArr = setErrorStack($queryResult, -1, $errMsg);
        }else{
            $returnArr = setErrorStack($queryResult, 35);
        }
    }
    else {
        $returnArr = setErrorStack($queryResult, 21);
    }
    return $returnArr;
}

/***************************************************************************************************
    @ function name: getClientDetails();
    @ parameters: $clientId(Integer),$conn(string)
    @ purpose: to fetch Client Details
 **************************************************************************************************/
function getGrowthDetails($growthId,$growthUrlId,$conn){

    if (!isset($growthId) || empty($growthId)) {
        $query = "SELECT * FROM growth_details Where growthUrlId='".cleanQueryParameter($growthUrlId,$conn)."'";
    } else {
        $query = "SELECT * FROM growth_details WHERE id ='".cleanQueryParameter($growthId,$conn)."' AND growthUrlId='".cleanQueryParameter($growthUrlId,$conn)."'";
    }

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched growth_details content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

/**************************************************************************************************
    @ Function Name : removeClient()
    @ Parameters : $clientId(Integer),$conn(string)
    @ Purpose : Deleteing CLient Details from database and Image from folder
***************************************************************************************************/
function removeGrowth($DeleteGrowthId,$DeleteGrowthUrlId,$conn) {
    $returnArr = array();
    $query = "SELECT * FROM growth_details WHERE id ='".cleanQueryParameter($DeleteGrowthId,$conn)."' AND growthUrlId='".cleanQueryParameter($DeleteGrowthUrlId,$conn)."'";
    $queryResult = runQuery($query, $conn);
    $res = array();
    while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
        $res["response"][] = $row;
    }

    $query1 = "DELETE FROM growth_details WHERE id = '".cleanQueryParameter($DeleteGrowthId,$conn)."'AND growthUrlId='".cleanQueryParameter($DeleteGrowthUrlId,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
        unlink("../uploads/".$res["response"][0]["growthImage"]);

        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Growth Details");
    } else {
        $returnArr = setErrorStack($queryResult1,36);
    }
    return $returnArr;
}

/***************************************************************************************************
  @ function name: getCorporateDetails();

  @ purpose: to fetch CorporateDetails
**************************************************************************************************/
  function getGrowthCorporateDetails($id,$CorporateUrlId,$require,$conn){

    if (!isset($id) || empty($id)) {
      if (!isset($require) || empty($require)) {
        $query = "SELECT * FROM corporateDetails WHERE corporateUrlId='".cleanQueryParameter($CorporateUrlId,$conn)."'";
      } else {
        $query = "SELECT * FROM corporateDetails WHERE status='Active' AND corporateUrlId='".cleanQueryParameter($CorporateUrlId,$conn)."'";
      }
    } else {
      if (!isset($require) || empty($require)) {
       $query = "SELECT * FROM corporateDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND corporateUrlId='".cleanQueryParameter($CorporateUrlId,$conn)."'";
      } else {
      $query = "SELECT * FROM corporateDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active' AND corporateUrlId='".cleanQueryParameter($CorporateUrlId,$conn)."'";
      }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $res = array();
      while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
        $res["response"][] = $row;
      $returnArr = setErrorStack($queryResult,-1,"Successfully fetched aboutDetails content",$res);
    }else{
      $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
  }

?>
