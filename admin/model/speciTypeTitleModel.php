<?php

/*Created By Kavita*/
/**************************************************************************************************
    @ function name: updateSpeciTitleDetails();
    @ purpose: to Add or update Press Details to Database
 *************************************************************************************************/

  function updateSpeciTitleDetails($id,$o_id,$p_id,$imageUrl,$imageUrl1,$type,$area,$conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }
    if (!isset($id) || empty($id)) {
        $query = "INSERT INTO `floor_plan_property_details` (`id`, `o_id`,`p_id`, `floor_img`, `property_img`, `type`, `area`, `created`) VALUES (NULL,'".cleanQueryParameter($o_id,$conn)."', '".cleanQueryParameter($p_id,$conn)."','".cleanQueryParameter($imageUrl,$conn)."', '".cleanQueryParameter($imageUrl1,$conn)."', '".cleanQueryParameter($type,$conn)."', '".cleanQueryParameter($area,$conn)."', CURRENT_TIMESTAMP);";
    } else {
       $query = "UPDATE `floor_plan_property_details` SET `floor_img` = '".cleanQueryParameter($imageUrl,$conn)."', `property_img` = '".cleanQueryParameter($imageUrl1,$conn)."', `area` = '".cleanQueryParameter($area,$conn)."', `type` = '".cleanQueryParameter($type,$conn)."' WHERE `id` = '".cleanQueryParameter($id,$conn)."';";
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $errMsg = "Successfully updated Amenities  content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
      $returnArr = setErrorStack($queryResult, 35);
    }

    return $returnArr;
}

/***************************************************************************************************
    @ function name: getSpeciTitleDetails();

  @ purpose: to fetch Floor Plan Details By  Id
 **************************************************************************************************/

function getSpeciTitleDetails($id,$conn){
     $query = "SELECT * FROM floor_plan_property_details WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Floor Plan content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}



/***************************************************************************************************
    @ function name: getSpeciTitleDetailsById();

    @ purpose: to fetch Floor Plan Details By property Id
 **************************************************************************************************/

function getSpeciTitleDetailsById($id,$conn){
    $query = "SELECT * FROM speciTitle WHERE p_id ='".cleanQueryParameter($id,$conn)."' ORDER BY `id` DESC ";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Floor Plan content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}


/**************************************************************************************************
    @ Function Name : removeSpeciTitle()
    @ Purpose : Deleteing Floor Plan Details from database and Image from folder
***************************************************************************************************/



  function removeSpeciTitle($Id,$conn) {
      $returnArr = array();
       $query = "SELECT * FROM floor_plan_property_details WHERE id ='".cleanQueryParameter($Id,$conn)."'";
      $queryResult = runQuery($query, $conn);
      $res = array();
      while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
          $res["response"][] = $row;
      }
       $query1 = "DELETE FROM floor_plan_property_details WHERE id = '".cleanQueryParameter($Id,$conn)."'";
      $queryResult1 = runQuery($query1,$conn);
      if (noError($queryResult1)) {
          unlink("../uploads/".$res["response"][0]["property_img"]);
          unlink("../uploads/".$res["response"][0]["floor_img"]);
          $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Floor Plan content");
      } else {
          $returnArr = setErrorStack($queryResult1,36);
      }
      return $returnArr;
  }
?>
