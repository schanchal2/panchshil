<?php

/**************************************************************************************************
    @ function name: updateClientDetails();
    @ parameters: $clientId(integer[Auto]),$clientName(String),$clientColorImage(String),$clientGreyImage(String),$services(String),$clientUrl(string),$conn(string)
    @ purpose: to Add or update Client Details to Database
 *************************************************************************************************/
function updateBusinessSubCatDetails($businessId,$businessCatId,$id,$businessSubCatName, $businessSubCatNumber, $businessSubCatEmail, $businessSubCatAddress, $conn){
   // printArr(func_get_args());
    $returnArr = array();


    /* checking for Add new data or Update Existin Data */
    if (!isset($id) || empty($id)) { 
        
         $query = "INSERT INTO business_sub_category_details (business_id, business_cat_id, sub_cat_name, sub_cat_number, sub_cat_email, sub_cat_address) VALUE ('".cleanQueryParameter($businessId,$conn)."','".cleanQueryParameter($businessCatId,$conn)."','".cleanQueryParameter($businessSubCatName,$conn)."','".cleanQueryParameter($businessSubCatNumber,$conn)."','".cleanQueryParameter($businessSubCatEmail,$conn)."','".cleanQueryParameter($businessSubCatAddress,$conn)."')";  
                     
    } else { 
        $query = "UPDATE business_sub_category_details SET sub_cat_name='".cleanQueryParameter($businessSubCatName,$conn)."', sub_cat_number='".cleanQueryParameter($businessSubCatNumber,$conn)."', sub_cat_email='".cleanQueryParameter($businessSubCatEmail,$conn)."', sub_cat_address='".cleanQueryParameter($businessSubCatAddress,$conn)."' WHERE id ='".cleanQueryParameter($id,$conn)."'";       
    }
    //printArr($query);
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $errMsg = "Successfully updated Business content";
        $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
        if (empty($errMsg)) {
            $returnArr = setErrorStack($queryResult, 35);
        } else {
            $returnArr = setErrorStack($queryResult, 1, $errMsg);
        }
        
    } 

    return $returnArr;    
}

/***************************************************************************************************
    @ function name: getClientDetails();
    @ parameters: $clientId(Integer),$conn(string)
    @ purpose: to fetch Client Details
 **************************************************************************************************/
function getBusinessSubCatDetails($businessId,$businessCatId,$require, $conn){

    if (!isset($businessCatId) || empty($businessCatId)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM business_sub_category_details";
        } else {
            $query = "SELECT * FROM business_sub_category_details WHERE `business_id` = '".cleanQueryParameter($businessId,$conn)."' ";
        }
    } else {
        if (!isset($require) || empty($require)) {
             $query = "SELECT * FROM business_sub_category_details WHERE `business_id` = '".cleanQueryParameter($businessId,$conn)."' AND `business_cat_id` = '".cleanQueryParameter($businessCatId,$conn)."' ";
        } else {
              $query = "SELECT * FROM `business_sub_category_details` WHERE `business_id` = '".cleanQueryParameter($businessId,$conn)."' AND `business_cat_id` = '".cleanQueryParameter($businessCatId,$conn)."' ";
        }
    }
   //printArr($query);
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched BusinessSubCategory content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

/**************************************************************************************************
    @ Function Name : removeClient()
    @ Parameters : $businessId(Integer),$conn(string)
    @ Purpose : Deleteing CLient Details from database and Image from folder 
***************************************************************************************************/
function removeBusinessSubCat($id,$conn) {
      $returnArr = array();
      //$status = "";
     $query = "SELECT * FROM business_sub_category_details WHERE id ='".cleanQueryParameter($id,$conn)."'";
      $queryResult = runQuery($query, $conn);
      $res = array();
      while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
          $res["response"][] = $row;
      }
      $query1 = "DELETE from business_sub_category_details WHERE id ='".cleanQueryParameter($id,$conn)."'";
      $queryResult1 = runQuery($query1,$conn);
      if (noError($queryResult1)) {
        // unlink("../uploads/".$res["response"][0]["image"]);
        // unlink("../uploads/".$res["response"][0]["sign_image"]);
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Business Sub Category Details");
      } else {
          $returnArr = setErrorStack($queryResult1,36);
      }
      return $returnArr;
    }



?>