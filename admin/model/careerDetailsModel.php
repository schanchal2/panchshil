<?php
/**************************************************************************************************
    @ function name: updateCareerDetails();
    @ purpose: to Add or update Career Details to Database
 *************************************************************************************************/
function updateCareerDetails($id, $careerImage, $career_description, $careerImage1, $career_name, $career_designation,$conn){
    $returnArr = array();
  //  echo $id;

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }

    //if ( !empty($image)) {

        /* checking for Add new data or Update Existin Data */
        if (!isset($id) || empty($id)) {

          $query = "INSERT INTO career (image, career_description, career_image1,career_name,career_designation,pagename) VALUE ('".cleanQueryParameter($careerImage,$conn)."','".cleanQueryParameter($career_description,$conn)."','".cleanQueryParameter($careerImage1,$conn)."','".cleanQueryParameter($career_name,$conn)."','".cleanQueryParameter($career_designation,$conn)."','".cleanQueryParameter('career_category.php',$conn)."')";
        } else {

           $query = "UPDATE career SET image='".cleanQueryParameter($careerImage,$conn)."', career_description='".cleanQueryParameter($career_description,$conn)."',career_image1='".cleanQueryParameter($careerImage1,$conn)."',career_name='".cleanQueryParameter($career_name,$conn)."',career_designation='".cleanQueryParameter($career_designation,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
        }
        $queryResult = runQuery($query,$conn);

        if(noError($queryResult)){
            $errMsg = "Successfully updated Career content";
            $returnArr = setErrorStack($queryResult, -1, $errMsg);
        }else{
            $returnArr = setErrorStack($queryResult, 35);
        }
    //}
    // else {
    //     $returnArr = setErrorStack($queryResult, 21);
    // }
    return $returnArr;
}

  /***************************************************************************************************
      @ function name: getCareerDetails();

      @ purpose: to fetch CareerDetails
   **************************************************************************************************/
  function getCareerDetails($id,$require,$conn){

      if (!isset($id) || empty($id)) {
          if (!isset($require) || empty($require)) {
             $query = "SELECT * FROM career";
          } else {
              $query = "SELECT * FROM career WHERE status='Active'";
          }
      } else {
          if (!isset($require) || empty($require)) {
               $query = "SELECT * FROM career WHERE id ='".cleanQueryParameter($id,$conn)."'";
          } else {
                $query = "SELECT * FROM career WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
          }
      }
      $queryResult = runQuery($query,$conn);
      
      if(noError($queryResult)){
          $res = array();
          while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))

              $res["response"][] = $row;

          $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Career content",$res);
      }else{
          $returnArr = setErrorStack($queryResult,28);
      }

      return $returnArr;
  }
  /**************************************************************************************************
      @ Function Name : removeCareer()
      @ Purpose : Deleteing about Details from database and Image from folder
  ***************************************************************************************************/
  function removeCareer($id,$conn) {
    $returnArr = array();
    //$status = "";
   $query = "SELECT * FROM career WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query, $conn);
    $res = array();
    while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
        $res["response"][] = $row;
    }

    if ($res["response"][0]["status"] == "Active") {
        $status = "Inactive";
    } else {
        $status = "Active";
    }
        //DELETE from forword WHERE `id` = 2
        $query1 = "UPDATE career SET status='".cleanQueryParameter($status,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
         $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Career Details");
    } else {
        $returnArr = setErrorStack($queryResult1,36);
    }
    return $returnArr;
  }

  /**************************************************************************************************
    @ function name: updateCareerDetails();
    @ purpose: to Add or update Career Details to Database
 *************************************************************************************************/
function updateCareerCatDetails($id,$career_cat_desc,$career_cat_name,$career_cat_design,$image,$career_id,$conn){
    $returnArr = array();
  //  echo $id;

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }

    //if ( !empty($image)) {

        /* checking for Add new data or Update Existin Data */
        if (!isset($id) || empty($id)) {

          $query = "INSERT INTO career_cat (career_id,image, career_cat_desc, career_cat_name,career_cat_design) VALUE ('".cleanQueryParameter($career_id,$conn)."','".cleanQueryParameter($image,$conn)."','".cleanQueryParameter($career_cat_desc,$conn)."','".cleanQueryParameter($career_cat_name,$conn)."','".cleanQueryParameter($career_cat_design,$conn)."')";
        } else {

           $query = "UPDATE career_cat SET image='".cleanQueryParameter($image,$conn)."', career_id='".cleanQueryParameter($career_id,$conn)."',career_cat_desc='".cleanQueryParameter($career_cat_desc,$conn)."',career_cat_name='".cleanQueryParameter($career_cat_name,$conn)."',career_cat_design='".cleanQueryParameter($career_cat_design,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
        }
        $queryResult = runQuery($query,$conn);

        if(noError($queryResult)){
            $errMsg = "Successfully updated Career Category content";
            $returnArr = setErrorStack($queryResult, -1, $errMsg);
        }else{
            $returnArr = setErrorStack($queryResult, 35);
        }
    //}
    // else {
    //     $returnArr = setErrorStack($queryResult, 21);
    // }
    return $returnArr;
}

  /***************************************************************************************************
      @ function name: getCareerDetails();

      @ purpose: to fetch CareerDetails
   **************************************************************************************************/
  function getCareerCatDetails($career_id,$require,$conn){

      if (!isset($career_id) || empty($career_id)) {
          if (!isset($require) || empty($require)) {
             $query = "SELECT * FROM career_cat";
          } else {
              $query = "SELECT * FROM career_cat WHERE status='Active'";
          }
      } else {
          if (!isset($require) || empty($require)) {
               $query = "SELECT * FROM career_cat WHERE career_id ='".cleanQueryParameter($career_id,$conn)."'";
          } else {
                $query = "SELECT * FROM career_cat WHERE career_id ='".cleanQueryParameter($career_id,$conn)."' AND status='Active'";
          }
      }
      //printArr($query);
      $queryResult = runQuery($query,$conn);
      
      if(noError($queryResult)){
          $res = array();
          while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))

              $res["response"][] = $row;

          $returnArr = setErrorStack($queryResult,-1,"Successfully fetched CareerCat content",$res);
      }else{
          $returnArr = setErrorStack($queryResult,28);
      }

      return $returnArr;
  }
  /**************************************************************************************************
      @ Function Name : removeCareer()
      @ Purpose : Deleteing about Details from database and Image from folder
  ***************************************************************************************************/
  function removeCareerCat($id,$conn) {
    $returnArr = array();
    //$status = "";
   $query = "SELECT * FROM career_cat WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query, $conn);
    $res = array();
    while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
        $res["response"][] = $row;
    }

    if ($res["response"][0]["status"] == "Active") {
        $status = "Inactive";
    } else {
        $status = "Active";
    }
        //DELETE from forword WHERE `id` = 2
        $query1 = "DELETE from career_cat WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
         $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Career Details");
    } else {
        $returnArr = setErrorStack($queryResult1,36);
    }
    return $returnArr;
  }

?>
