
<?php


/**************************************************************************************************
    @ function name: updateAboutDetails();
    @ purpose: to Add or update about Details to Database
 *************************************************************************************************/

function updateAboutDetails($id,$status,$image,$boxImage,$name,$shortDesc,$longDesc,$page_type,$conn){

    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }
    //if ( !empty($image)) {

    /* checking for Add new data or Update Existin Data */
    if (!isset($id) || empty($id)) {

   $query = "INSERT INTO aboutDetails (status,image,boxImage, name,shortDesc,longDesc,pagename) VALUE ('".cleanQueryParameter($status,$conn)."','".cleanQueryParameter($image,$conn)."','".cleanQueryParameter($boxImage,$conn)."','".cleanQueryParameter($name,$conn)."','".cleanQueryParameter($shortDesc,$conn)."','".cleanQueryParameter($longDesc,$conn)."','".cleanQueryParameter($page_type,$conn)."')";

    } else {
  $query = "UPDATE aboutDetails SET status='".cleanQueryParameter($status,$conn)."', image='".cleanQueryParameter($image,$conn)."',boxImage='".cleanQueryParameter($boxImage,$conn)."',name='".cleanQueryParameter($name,$conn)."',shortDesc='".cleanQueryParameter($shortDesc,$conn)."',longDesc='".cleanQueryParameter($longDesc,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
    }
    $queryResult = runQuery($query,$conn);

    if(noError($queryResult)){
      $errMsg = "Successfully updated about content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
      $returnArr = setErrorStack($queryResult, 35);
    }

    return $returnArr;
}

/***************************************************************************************************
    @ function name: getAboutDetails();

    @ purpose: to fetch AboutDetails
 **************************************************************************************************/
function getAboutDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM aboutDetails";
        } else {
            $query = "SELECT * FROM aboutDetails WHERE status='Active'";
        }
    } else {
        if (!isset($require) || empty($require)) {
             $query = "SELECT * FROM aboutDetails WHERE id ='".cleanQueryParameter($id,$conn)."'";
        } else {
              $query = "SELECT * FROM aboutDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched aboutDetails content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}
/**************************************************************************************************
    @ Function Name : removeClient()
    @ Purpose : Deleteing about Details from database and Image from folder
***************************************************************************************************/
function removeAbout($id,$conn) {
    $returnArr = array();
    //$status = "";
    $query = "SELECT * FROM aboutDetails WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query, $conn);
    $res = array();
    while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
        $res["response"][] = $row;
    }

    if ($res["response"][0]["status"] == "Active") {
        $status = "Inactive";
    } else {
        $status = "Active";
    }

   $query1 = "UPDATE aboutDetails SET status='".cleanQueryParameter($status,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
         $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted about Details");
    } else {
        $returnArr = setErrorStack($queryResult1,36);
    }
    return $returnArr;
  }



function getLeaderDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM aboutDetails where pagename = 'manageLeadership.php'";
        }
        else
        {
           $query = "SELECT * FROM aboutDetails where pagename = 'manageLeadership.php'";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched aboutDetails content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

    /**************************************************************************************************
    @ function name: updateSustainDetails();
    @ purpose: to Add or update about Details to Database
     *************************************************************************************************/
    function updateSustainDetails($id,$sustainUrlId,$status,$image,$name,$shortDesc,$longDesc,$conn){
        $returnArr = array();

        /* Checking Image is present or not */
        if ($status != 'Inactive') {
            $status = 'Active';
        }
        //if ( !empty($image)) {

        /* checking for Add new data or Update Existin Data */
        if (!isset($id) || empty($id)) {
            $query = "INSERT INTO sustainDetails (status,sustainid,image, name,shortDesc,longDesc,pagename) VALUE ('".cleanQueryParameter($status,$conn)."',".cleanQueryParameter($sustainUrlId,$conn).",'".cleanQueryParameter($image,$conn)."','".cleanQueryParameter($name,$conn)."','".cleanQueryParameter($shortDesc,$conn)."','".cleanQueryParameter($longDesc,$conn)."','".cleanQueryParameter('manageSustainCat.php',$conn)."')";
        } else {
            $query = "UPDATE sustainDetails SET status='".cleanQueryParameter($status,$conn)."', image='".cleanQueryParameter($image,$conn)."',sustainid='".cleanQueryParameter($sustainUrlId,$conn)."',name='".cleanQueryParameter($name,$conn)."',shortDesc='".cleanQueryParameter($shortDesc,$conn)."',longDesc='".cleanQueryParameter($longDesc,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
        }
        $queryResult = runQuery($query,$conn);
        if(noError($queryResult)){
            $errMsg = "Successfully updated Sustain content";
            $returnArr = setErrorStack($queryResult, -1, $errMsg);
        }else{
            $returnArr = setErrorStack($queryResult, 35);
        }

        return $returnArr;
    }

    /***************************************************************************************************
        @ function name: getSustainDetails();

        @ purpose: to fetch SustainDetails
     **************************************************************************************************/
    function getSustainDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
           $query = "SELECT * FROM sustainDetails";
        } else {
          $query = "SELECT * FROM sustainDetails WHERE status='Active'";
        }
    } else {
        if (!isset($require) || empty($require)) {
          $query = "SELECT * FROM sustainDetails WHERE id ='".cleanQueryParameter($id,$conn)."'";
        } else {
          $query = "SELECT * FROM sustainDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Sustainability content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
    }

    /**************************************************************************************************
        @ Function Name : removeSustain()
        @ Purpose : Deleteing about Details from database and Image from folder
    ***************************************************************************************************/
    function removeSustain($id,$conn) {
        $returnArr = array();
        //$status = "";
        $query = "SELECT * FROM sustainDetails WHERE id ='".cleanQueryParameter($id,$conn)."'";
        $queryResult = runQuery($query, $conn);
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
            $res["response"][] = $row;
        }

        if ($res["response"][0]["status"] == "Active") {
            $status = "Inactive";
        } else {
            $status = "Active";
        }

        $query1 = "UPDATE sustainDetails SET status='".cleanQueryParameter($status,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
        $queryResult1 = runQuery($query1,$conn);
        if (noError($queryResult1)) {
            $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted about Details");
        } else {
            $returnArr = setErrorStack($queryResult1,36);
        }
        return $returnArr;
    }




/**************************************************************************************************
              @ function name: updateGroupPhilo();
              @ purpose: to Add or update about Details to Database
           *************************************************************************************************/
          function updateGroupPhilo($id,$image,$long_url,$group_philo_image,$name,$designation,$conn){
              $returnArr = array();
            //  echo $id;

              /* Checking Image is present or not */
              if ($status != 'Inactive') {
                  $status = 'Active';
              }
              //if ( !empty($image)) {

                  /* checking for Add new data or Update Existin Data */
                  if (!isset($id) || empty($id)) {

                      $query = "INSERT INTO group_philo (image, long_url, group_philo_image,name,designation) VALUE ('".cleanQueryParameter($image,$conn)."','".cleanQueryParameter($long_url,$conn)."','".cleanQueryParameter($group_philo_image,$conn)."','".cleanQueryParameter($name,$conn)."','".cleanQueryParameter($designation,$conn)."')";
                  } else {

                      $query = "UPDATE group_philo SET image='".cleanQueryParameter($image,$conn)."', long_url='".cleanQueryParameter($long_url,$conn)."',group_philo_image='".cleanQueryParameter($group_philo_image,$conn)."',name='".cleanQueryParameter($name,$conn)."',designation='".cleanQueryParameter($designation,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
                  }
                  $queryResult = runQuery($query,$conn);

                  if(noError($queryResult)){
                      $errMsg = "Successfully updated Forword content";
                      $returnArr = setErrorStack($queryResult, -1, $errMsg);
                  }else{
                      $returnArr = setErrorStack($queryResult, 35);
                  }
              //}
              // else {
              //     $returnArr = setErrorStack($queryResult, 21);
              // }
              return $returnArr;
          }

            /***************************************************************************************************
                @ function name: getGroupPhilo();

                @ purpose: to fetch getGroupPhilo
             **************************************************************************************************/
            function getGroupPhilo($id,$require,$conn){

                if (!isset($id) || empty($id)) {
                    if (!isset($require) || empty($require)) {
                       $query = "SELECT * FROM group_philo";
                    } else {
                        $query = "SELECT * FROM group_philo WHERE status='Active'";
                    }
                } else {
                    if (!isset($require) || empty($require)) {
                         $query = "SELECT * FROM group_philo WHERE id ='".cleanQueryParameter($id,$conn)."'";
                    } else {
                          $query = "SELECT * FROM group_philo WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
                    }
                }
                $queryResult = runQuery($query,$conn);
                if(noError($queryResult)){
                    $res = array();
                    while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
                        $res["response"][] = $row;
                    $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Company Profile",$res);
                }else{
                    $returnArr = setErrorStack($queryResult,28);
                }
                return $returnArr;
            }
            /***************************  ***********************************************************************
                @ Function Name : removeGroupPhilo()
                @ Purpose : Deleteing about Details from database and Image from folder
            ***************************************************************************************************/
            function removeGroupPhilo($id,$conn) {
              $returnArr = array();
              //$status = "";
             $query = "SELECT * FROM group_philo WHERE id ='".cleanQueryParameter($id,$conn)."'";
              $queryResult = runQuery($query, $conn);
              $res = array();
              while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
                  $res["response"][] = $row;
              }

              if ($res["response"][0]["status"] == "Active") {
                  $status = "Inactive";
              } else {
                  $status = "Active";
              }
                  //DELETE from forword WHERE `id` = 2
                  $query1 = "DELETE from group_philo WHERE id ='".cleanQueryParameter($id,$conn)."'";
              $queryResult1 = runQuery($query1,$conn);
              if (noError($queryResult1)) {
                   $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted forword Details");
              } else {
                  $returnArr = setErrorStack($queryResult1,36);
              }
              return $returnArr;
            }

        /**************************************************************************************************
        @ function name: updateSustainCatDetails();
        @ purpose: to Add or update about Details to Database
         *************************************************************************************************/
        function updateSustainCatDetails($id,$sustainUrlId,$status,$sustain_cat_image,$sustain_cat_long_url,$sign_image_url,$name,$designation,$conn){
            $returnArr = array();

            /* Checking Image is present or not */
            if ($status != 'Inactive') {
                $status = 'Active';
            }
            //if ( !empty($image)) {

            /* checking for Add new data or Update Existin Data */
            if (!isset($id) || empty($id)) {

                $query = "INSERT INTO sustain_cat (status,sustainCatId,sustain_cat_image, sustain_cat_long_url,sustain_cat_sign_image,sustain_cat_name,sustain_cat_designation) VALUE ('".cleanQueryParameter($status,$conn)."','".cleanQueryParameter($sustainUrlId,$conn)."','".cleanQueryParameter($sustain_cat_image,$conn)."','".cleanQueryParameter($sustain_cat_long_url,$conn)."','".cleanQueryParameter($sign_image_url,$conn)."','".cleanQueryParameter($name,$conn)."','".cleanQueryParameter($designation,$conn)."')";
            } else {


                $query = "UPDATE sustain_cat SET status='".cleanQueryParameter($status,$conn)."', sustainCatId='".cleanQueryParameter($sustainUrlId,$conn)."', sustain_cat_image='".cleanQueryParameter($sustain_cat_image,$conn)."',sustain_cat_long_url='".cleanQueryParameter($sustain_cat_long_url,$conn)."',sustain_cat_sign_image='".cleanQueryParameter($sign_image_url,$conn)."',sustain_cat_name='".cleanQueryParameter($name,$conn)."',sustain_cat_designation='".cleanQueryParameter($designation,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
            }
            $queryResult = runQuery($query,$conn);
            // printArr($queryResult);
            if(noError($queryResult)){
                $errMsg = "Successfully updated Sustain content";
                $returnArr = setErrorStack($queryResult, -1, $errMsg);
            }else{
                $returnArr = setErrorStack($queryResult, 35);
            }

            return $returnArr;
        }

        /***************************************************************************************************
        @ function name: getSustainCatDetails();

        @ purpose: to fetch SustainCatDetails
         **************************************************************************************************/
        function getSustainCatDetails($sustainCatId,$require,$conn){

            if (!isset($sustainCatId) || empty($sustainCatId)) {
                if (!isset($require) || empty($require)) {
                    $query = "SELECT * FROM sustain_cat";
                } else {
                    $query = "SELECT * FROM sustain_cat WHERE status='Active'";
                }
            } else {
                if (!isset($require) || empty($require)) {
                    $query = "SELECT * FROM sustain_cat WHERE sustainCatId ='".cleanQueryParameter($sustainCatId,$conn)."'";
                } else {
                    $query = "SELECT * FROM sustain_cat WHERE sustainCatId ='".cleanQueryParameter($sustainCatId,$conn)."' AND status='Active'";
                }
            }
            $queryResult = runQuery($query,$conn);
            if(noError($queryResult)){
                $res = array();
                while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
                    $res["response"][] = $row;
                $returnArr = setErrorStack($queryResult,-1,"Successfully fetched SustainCatDetails",$res);
            }else{
                $returnArr = setErrorStack($queryResult,28);
            }
            return $returnArr;
        }

        function removesustaincat($id,$conn) {
            $returnArr = array();
            //$status = "";
            $query = "SELECT * FROM sustain_cat WHERE id ='".cleanQueryParameter($id,$conn)."'";
            $queryResult = runQuery($query, $conn);
            $res = array();
            while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
                $res["response"][] = $row;
            }

            if ($res["response"][0]["status"] == "Active") {
                $status = "Inactive";
            } else {
                $status = "Active";
            }
            //DELETE from forword WHERE `id` = 2
            $query1 = "DELETE from sustain_cat WHERE id ='".cleanQueryParameter($id,$conn)."'";
            $queryResult1 = runQuery($query1,$conn);
            if (noError($queryResult1)) {
                $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted sustain_cat Details");
            } else {
                $returnArr = setErrorStack($queryResult1,36);
            }
            return $returnArr;
        }


        function getAboutSustainDetails($id,$require,$conn){

            if (!isset($id) || empty($id)) {
                if (!isset($require) || empty($require)) {
                    $query = "SELECT * FROM aboutDetails";
                } else {
                    $query = "SELECT * FROM aboutDetails WHERE status='Active'";
                }
            } else {
                if (!isset($require) || empty($require)) {
                    $query = "SELECT * FROM aboutDetails WHERE id ='".cleanQueryParameter($id,$conn)."'";
                } else {
                    $query = "SELECT * FROM aboutDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
                }
            }
            $queryResult = runQuery($query,$conn);
            if(noError($queryResult)){
                $res = array();
                while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
                    $res["response"][] = $row;
                $returnArr = setErrorStack($queryResult,-1,"Successfully fetched aboutDetails content",$res);
            }else{
                $returnArr = setErrorStack($queryResult,28);
            }
            return $returnArr;
        }

?>
