<?php

/**
 * function name: getContact_Detail();
 * parameters:$conn(object)
 * purpose: to retrieve record form contact detail table;
 */
function getAllContact_Detail($conn){

    $returnArr = array();
    $getResult = array();

    $query = "select * from contacts ";

    $execQuery = runQuery($query, $conn);
    if(noError($execQuery)){
        while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $res["response"][] = $row;
        }
        $returnArr = setErrorStack($execQuery, -1, "Successfully fetched record contacts details" ,$res);
    }else{
        $returnArr = setErrorStack($execQuery, 25, "Error: Retrieving contacts detail");
    }

    return $returnArr;
}


function getContact_Detail($contactid,$conn){

    $query = "SELECT * FROM contacts WHERE id ='".cleanQueryParameter($contactid,$conn)."'";

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched header content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,13);
    }
    return $returnArr;
}


/**
 * function name: addupdatecontact1();
 * parameters: $id(id), $location(location), $phoneno1(phoneno1),$phoneno2(phoneno2),$phoneno3(phoneno3),$phoneno4(phoneno4),$conn(object)
 * purpose: to add contact by contacts id details;
 */


function addupdatecontact1($id,$location,$address,$phoneno1,$phoneno2,$phoneno3,$phoneno4,$fax,$conn){

    $returnArr = array();

    /* checking to Add new data or Update Existin Data */
    if ($id=="undefined") {
                
         $query = "INSERT INTO contacts (location,address,phoneno1,phoneno2,phoneno3,phoneno4,fax) VALUE ('".cleanQueryParameter($location,$conn)."','".cleanQueryParameter($address,$conn)."','".cleanQueryParameter($phoneno1,$conn)."','".cleanQueryParameter($phoneno2,$conn)."','".cleanQueryParameter($phoneno3,$conn)."','".cleanQueryParameter($phoneno4,$conn)."','".cleanQueryParameter($fax,$conn)."')";
                         
             } 

     else 
     { 
         $query = "UPDATE contacts SET location='".cleanQueryParameter($location,$conn)."', address='".cleanQueryParameter($address,$conn)."',phoneno1='".cleanQueryParameter($phoneno1,$conn)."', phoneno2='".cleanQueryParameter($phoneno2,$conn)."',phoneno3='".cleanQueryParameter($phoneno3,$conn)."',phoneno4='".cleanQueryParameter($phoneno4,$conn)."',fax='".cleanQueryParameter($fax,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";     
    }
  

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $errMsg = "Successfully updated Contact Details";
        $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
        $returnArr = setErrorStack($queryResult, 12);
    }
    return $returnArr;
}

//remove the contact record in removeconatct1 function
function removeContact1($id,$conn) {
    $returnArr = array();  
    $query1 = "DELETE FROM contacts WHERE id = '".cleanQueryParameter($id,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted contact Details");
    } else {
        $returnArr = setErrorStack($queryResult1,36);
    }
    return $returnArr;
}
?>