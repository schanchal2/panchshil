<?php

/*Created By Kavita*/
/**************************************************************************************************
    @ function name: updatePressDetails();
    @ purpose: to Add or update Press Details to Database
 *************************************************************************************************/

function updatePressDetails($id,$media_id,$imageUrl,$imageUrl1,$name,$sourcename,$longDesc,$press_date,$pdf_url,$video_url,$link_url,$press_status,$media_type,$conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }
    if (!isset($id) || empty($id)) {

      $query = " INSERT INTO `pressDetails` (`id`, `name`, `media_id`, `image1`, `image2`, `date`, `source_name`, `you_tube_url`, `url`, `pdf`, `descrption`,`media_type`, `status`, `created`) VALUES (NULL ,'".cleanQueryParameter($name,$conn)."', '".cleanQueryParameter($media_id,$conn)."','".cleanQueryParameter($imageUrl,$conn)."',  '".cleanQueryParameter($imageUrl1,$conn)."', '".cleanQueryParameter($press_date,$conn)."', '".cleanQueryParameter($sourcename,$conn)."',  '".cleanQueryParameter($video_url,$conn)."',  '".cleanQueryParameter($link_url,$conn)."', '".cleanQueryParameter($pdf_url,$conn)."','".cleanQueryParameter($longDesc,$conn)."','".cleanQueryParameter($media_type,$conn)."','Active', CURRENT_TIMESTAMP);";

    } else {
$query = "UPDATE `pressDetails` SET `name` = '".cleanQueryParameter($name,$conn)."', `image1` = '".cleanQueryParameter($imageUrl,$conn)."', `image2` = '".cleanQueryParameter($imageUrl1,$conn)."', `date` = '".cleanQueryParameter($press_date,$conn)."', `source_name` = '".cleanQueryParameter($sourcename,$conn)."', `you_tube_url` = '".cleanQueryParameter($video_url,$conn)."', `url` = '".cleanQueryParameter($link_url,$conn)."', `pdf` = '".cleanQueryParameter($pdf_url,$conn)."', `descrption` = '".cleanQueryParameter($longDesc,$conn)."', `status` = '".cleanQueryParameter($press_status,$conn)."' WHERE `pressDetails`.`id` = '".cleanQueryParameter($id,$conn)."';";
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $errMsg = "Successfully updated Press  content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
      $returnArr = setErrorStack($queryResult, 35);
    }

    return $returnArr;
}

/***************************************************************************************************
    @ function name: getPressDetails();

    @ purpose: to fetch Press Details
 **************************************************************************************************/

function getPressDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM pressDetails";
        } else {
            $query = "SELECT * FROM pressDetails ";
        }
    } else {
        if (!isset($require) || empty($require)) {
              $query = "SELECT * FROM pressDetails WHERE id ='".cleanQueryParameter($id,$conn)."'";
        } else {
               $query = "SELECT * FROM pressDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Press content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}



/***************************************************************************************************
    @ function name: getPressDetailsById();

    @ purpose: to fetch Press Details By Id
 **************************************************************************************************/

function getPressDetailsById($id,$conn){
  $query = "SELECT * FROM pressDetails WHERE media_id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Press content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}


/**************************************************************************************************
    @ Function Name : removePress()
    @ Purpose : Deleteing Press Details from database and Image from folder
***************************************************************************************************/



  function removePress($Id,$conn) {
      $returnArr = array();
       $query = "SELECT * FROM pressDetails WHERE id ='".cleanQueryParameter($Id,$conn)."'";
      $queryResult = runQuery($query, $conn);
      $res = array();
      while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
          $res["response"][] = $row;
      }

       $query1 = "DELETE FROM pressDetails WHERE id = '".cleanQueryParameter($Id,$conn)."'";
      $queryResult1 = runQuery($query1,$conn);
      if (noError($queryResult1)) {
          unlink("../uploads/".$res["response"][0]["image1"]);
          unlink("../uploads/".$res["response"][0]["image2"]);
          unlink("../uploads/".$res["response"][0]["pdf"]);
          $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Press content");
      } else {
          $returnArr = setErrorStack($queryResult1,36);
      }
      return $returnArr;
  }
?>
