<?php

/**************************************************************************************************
@ function name: updateCompanyProfile();
@ purpose: to Add or update about Details to Database
 *************************************************************************************************/
function updateCompanyProfile($id, $companyForwrdUrlId, $image, $long_url, $company_profile_image, $name, $designation, $conn){
    $returnArr = array();
    //  echo $id;
    //if ( !empty($image)) {

    /* checking for Add new data or Update Existin Data */
    if (!isset($id) || empty($id)) {

        $query = "INSERT INTO company_profile (compProfileUrlId, image, long_url, company_profile_image,name,designation) VALUE ('".cleanQueryParameter($companyForwrdUrlId,$conn)."','".cleanQueryParameter($image,$conn)."','".cleanQueryParameter($long_url,$conn)."','".cleanQueryParameter($company_profile_image,$conn)."','".cleanQueryParameter($name,$conn)."','".cleanQueryParameter($designation,$conn)."')";
    } else {

        $query = "UPDATE company_profile SET compProfileUrlId='".cleanQueryParameter($companyForwrdUrlId,$conn)."', image='".cleanQueryParameter($image,$conn)."', long_url='".cleanQueryParameter($long_url,$conn)."',company_profile_image='".cleanQueryParameter($company_profile_image,$conn)."',name='".cleanQueryParameter($name,$conn)."',designation='".cleanQueryParameter($designation,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
    }
    $queryResult = runQuery($query,$conn);
    print_r($queryResult);

    if(noError($queryResult)){
        $errMsg = "Successfully updated Forword content";
        $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
        $returnArr = setErrorStack($queryResult, 35);
    }
    //}
    // else {
    //     $returnArr = setErrorStack($queryResult, 21);
    // }
    return $returnArr;
}

/***************************************************************************************************
@ function name: getCompanyProfile();

@ purpose: to fetch getCompanyProfile
 **************************************************************************************************/
//function getCompanyProfile($id,$conn){
//
//    if (!isset($id) || empty($id)) {
//        if (!isset($require) || empty($require)) {
//            $query = "SELECT * FROM company_profile";
//        } else {
//            $query = "SELECT * FROM company_profile WHERE status='Active'";
//        }
//    } else {
//        if (!isset($require) || empty($require)) {
//            $query = "SELECT * FROM company_profile WHERE compProfileUrlId ='".cleanQueryParameter($id,$conn)."'";
//        } else {
//            $query = "SELECT * FROM company_profile WHERE compProfileUrlId ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
//        }
//    }
//    $queryResult = runQuery($query,$conn);
//    if(noError($queryResult)){
//        $res = array();
//        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
//            $res["response"][] = $row;
//        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Company Profile",$res);
//    }else{
//        $returnArr = setErrorStack($queryResult,28);
//    }
//    return $returnArr;
//}
/***************************  ***********************************************************************
@ Function Name : removeCompanyProfile()
@ Purpose : Deleteing about Details from database and Image from folder
 ***************************************************************************************************/
function removeCompanyProfile($id,$conn) {
    $returnArr = array();
    //$status = "";
    $query = "SELECT * FROM company_profile WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query, $conn);
    $res = array();
    while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
        $res["response"][] = $row;
    }

    $query1 = "DELETE from company_profile WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
        unlink("../uploads/".$res["response"][0]["image"]);
        unlink("../uploads/".$res["response"][0]["company_profile_image"]);
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted forword Details");
    } else {
        $returnArr = setErrorStack($queryResult1,36);
    }
    return $returnArr;
}

/***************************************************************************************************
@ function name: getCorporateDetails();

@ purpose: to fetch CorporateDetails
 **************************************************************************************************/
function getCorporateCompanyDetails($id,$CorporateUrlId,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM corporateDetails WHERE corporateUrlId='".cleanQueryParameter($CorporateUrlId,$conn)."'";
        } else {
            $query = "SELECT * FROM corporateDetails WHERE status='Active' AND corporateUrlId='".cleanQueryParameter($CorporateUrlId,$conn)."'";
        }
    } else {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM corporateDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND corporateUrlId='".cleanQueryParameter($CorporateUrlId,$conn)."'";
        } else {
            $query = "SELECT * FROM corporateDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active' AND corporateUrlId='".cleanQueryParameter($CorporateUrlId,$conn)."'";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched aboutDetails content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

?>