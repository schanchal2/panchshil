<?php

/***************************************************************************************************
    @ function name: sendMail();
    @ parameters: $contactArray(Array),$conn(string)
    @ purpose: to Send Email on contact us Form submit.
 **************************************************************************************************/
function sendMail($contactArray, $conn) {

    /* Global Email Addreess */
    global $mailId;
    $returnArr = array();

    /* Creating email Body for sending */
    $body       = "Hello Admin of Panchshil, You have new Contact Form<br/><br/><br/>";
    $body      .= "<table border = '1'>";
    $body      .= "<tbody>";
    $body      .= "<tr><th style = 'padding:10px;'>Reason </th><td style = 'padding:10px;'>".$contactArray['reason']."</td></tr>";
    $body      .= "<tr><th style = 'padding:10px;'>Name </th><td style = 'padding:10px;'>".$contactArray['firstName']." ".$contactArray['lastName']."</td></tr>";    
    $body      .= "<tr><th style = 'padding:10px;'>Phone No </th><td style = 'padding:10px;'>".$contactArray['phone']."</td></tr>";
    $body      .= "<tr><th style = 'padding:10px;'>E-Mail</th><td style = 'padding:10px;'>".$contactArray['email']."</td></tr>";
    $body      .= "<tr><th style = 'padding:10px;'>Address</th><td style = 'padding:10px;'>".$contactArray['address']."</td></tr>";
    $body      .= "<tr><th style = 'padding:10px;'>Country</th><td style = 'padding:10px;'>".$contactArray['country']."</td></tr>";
    $body      .= "<tr><th style = 'padding:10px;'>City</th><td style = 'padding:10px;'>".$contactArray['city']."</td></tr>";
    $body      .= "<tr><th style = 'padding:10px;'>Profession</th><td style = 'padding:10px;'>".$contactArray['profession']."</td></tr>";
    $body      .= "<tr><th style = 'padding:10px;'>Company Name</th><td style = 'padding:10px;'>".$contactArray['company_name']."</td></tr>";
    $body      .= "<tr><th style = 'padding:10px;'>Designation</th><td style = 'padding:10px;'>".$contactArray['designation']."</td></tr>";
    $body      .= "<tr><th style = 'padding:10px;'>Comment</th><td style = 'padding:10px;'>".$contactArray['comment']."</td></tr>";
    $body      .= "</tbody>";
    $body      .= "</table>"; 

    /* Email Credentials */
    $from       = $contactArray['email'];
    $SenderName = " ".$contactArray['firstName']." ".$contactArray['lastName']." ";
    $subject    = "Panchshil Contact Request";

    /* Creating Header to send Email */
    $headers    = '';
    $headers    = 'MIME-Version: 1.0' . "\r\n";
    $headers   .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers   .= 'From: '.$SenderName.' <'.$from.'>' . "\r\n";

    /* Calling Mail Function to send Email */
    $mailVerify = @mail($mailId, $subject, $body, $headers);

    /* Verifying email is Sent or not */
    if ($mailVerify){
        $returnArr = setErrorStack($queryResult,-1,"Successfully Sent Mail");
    } else {
        $returnArr = setErrorStack($queryResult,1,"Failed Sent Mail");
    }
    return $returnArr;
}
    

function sendSMS($msg = "",$mobileNum = ""){
    define('SEND_SMS_URL', 'http://onlinesms.in/api/sendValidSMSdataUrl.php?');
    define('SMS_URL_LOGIN','9320027660');
    define('SMS_URL_PWD','tagepuguz');
    define('SMS_URL_SNDRID','OPTINS');

    $ch = curl_init();
    $url = SEND_SMS_URL;
    $login = SMS_URL_LOGIN;
    $pwd = SMS_URL_PWD;
    $sndID =SMS_URL_SNDRID;
   // $mobileNum = "+91".$mobileNum;
    //$mobileNum = "91".$mobileNum;
    $message=urlencode($msg);
    $urlnew=$url."login=".$login."&pword=".$pwd."&msg=".$message."&senderid=".$sndID."&mobnum=".$mobileNum;
    curl_setopt($ch, CURLOPT_URL, $urlnew);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    //curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    //curl_setopt($ch, CURLOPT_POSTFIELDS, "login=".$login."&pword=".$pwd."&senderid=".$sndID."&mobnum=".$mobileNum."&msg=".$msg);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "login=".$login."&pword=".$pwd."&msg=".$message."&senderid=".$sndID."&mobnum=".$mobileNum);
    //$info = curl_getinfo($ch);
    //print_r($info);
    $buffer = curl_exec($ch);
    //print_r($buffer);
    if(empty($buffer))
    {
      //echo " buffer is empty ";
    }
    else
    { //echo $buffer;

    }
    curl_close($ch);
    return $buffer;
}




function notifyCustomer($contactArray,$conn) {

    /* Global Email Addreess */
    global $mailId;
    $returnArr   = array();

    /* Creating email Body for sending */
    $body        = "Hello ".$contactArray['firstName']." ".$contactArray['lastName'].",<br/><br/>";
    $body       .= "Thank You For Showing Intrest In Panchshil Bussiness Group.<br/><br/>We will Contact You As Soon As Possible.<br/><br/>Regards,<br/><br/>";
    $body       .= "<b>Panchshil Business Group, Pune</b><br/>Address: Tech Park One,<br/>Tower E,<br/>191 Yerwada,<br/>Pune - 411 006<br/>INDIA<br/>Tel : +91(20) 66473200";

    /* Email Credentials */
    $from        = $mailId;
    $SenderName  = "Panchshil Business Group";
    $subject     = "Panchashil Notification";
    $to          = $contactArray['email'];

    /* Creating Header to send Email */
    $headers     = '';
    $headers     = 'MIME-Version: 1.0' . "\r\n";
    $headers    .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers    .= 'From: '.$SenderName.' <'.$from.'>' . "\r\n";

    /* Calling Mail Function to send Email */
    $customerMailVerify = @mail($to, $subject, $body, $headers);

    $smsMsg = "Hello ".$contactArray['firstName']." ".$contactArray['lastName']." Thank You For Showing Intrest In Panchshil Bussiness Group. We will Contact You as Soon as possible.";
    $customerSMSSending = sendSMS($smsMsg,$contactArray['phone']);
    if ($customerMailVerify){
        $returnArr = setErrorStack($queryResult,-1,"Successfully Sent Mail");
    } else {
        $returnArr = setErrorStack($queryResult,1,"Failed To Send Mail");
    }
    return $returnArr;

}


/***************************************************************************************************
    @ function name: addContactUsDetails();
    @ parameters: $contactArray(Array),$conn(string)
    @ purpose: to Insert Contact us Form Details.
 **************************************************************************************************/
function addContactUsDetails($contactArray,$conn){
    $returnArr = array();
    $resCountry = array();
   
    if (!empty($contactArray["country"])){
        $queryCountry = "select name from countries where id=".cleanQueryParameter($contactArray["country"],$conn);
        $queryCountryResult = runQuery($queryCountry,$conn);
        if(noError($queryCountryResult)){
            while ($rowCountry = mysqli_fetch_assoc($queryCountryResult["dbResource"]))
                $resCountry["response"][] = $rowCountry;
        } 
        
    } 

    $query = "INSERT INTO contact_us_form (reason,firstName,lastName,phone,email,address,country,city,profession,company_name,designation,comment) VALUE ('".cleanQueryParameter($contactArray["reason"],$conn)."','".cleanQueryParameter($contactArray["firstName"],$conn)."','".cleanQueryParameter($contactArray["lastName"],$conn)."','".cleanQueryParameter($contactArray["phone"],$conn)."','".cleanQueryParameter($contactArray["email"],$conn)."','".cleanQueryParameter($contactArray["address"],$conn)."','".cleanQueryParameter($resCountry["response"][0]["name"],$conn)."','".cleanQueryParameter($contactArray["city"],$conn)."','".cleanQueryParameter($contactArray["profession"],$conn)."','".cleanQueryParameter($contactArray["company_name"],$conn)."','".cleanQueryParameter($contactArray["designation"],$conn)."','".cleanQueryParameter($contactArray["comment"],$conn)."')";  
    
    $contactArray['country']   =   $resCountry["response"][0]["name"];
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $sendEmail = sendMail($contactArray, $conn);
        $notifyCustomer = notifyCustomer($contactArray,$conn);
        if (noError($sendEmail) && noError($notifyCustomer)) {
            $errMsg = "SuccessFully Added Contact US Details";
            $returnArr = setErrorStack($queryResult, -1, $errMsg);
        } else {
            $returnArr = setErrorStack($queryResult,24);
        }
        
    }else{
        $returnArr = setErrorStack($queryResult,24);
    }
    return $returnArr;
}

/***************************************************************************************************
    @ function name: getContactUSDetails();
    @ parameters: $contactFormId(Integer),$conn(string)
    @ purpose: to fetch ContactUs form Details.
 **************************************************************************************************/
function getContactUSDetails($contactFormId,$conn){

	if ( !isset($contactFormId) || empty($contactFormId)){
		$query = "SELECT * FROM contact_us_form";
	} else {
		$query = "SELECT * FROM contact_us_form WHERE id ='".cleanQueryParameter($contactFormId,$conn)."'";
	}   

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched contact us content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,18);
    }
    return $returnArr;
}


/**************************************************************************************************
    @ Function Name : removeHeader()
    @ Parameters : $contactFormId(Integer),$conn(string)
    @ Purpose : Delete Contact Us form Details from database
***************************************************************************************************/
function removeContactUSDetails($contactFormId,$conn) {
    $returnArr = array();
    
    $query1 = "DELETE FROM contact_us_form WHERE id = '".cleanQueryParameter($contactFormId,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
       
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted contact us Details");
    } else {
        $returnArr = setErrorStack($queryResult1,35);
    }
    return $returnArr;
}




?>