<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/alliancesContentModel.php';

$id = urldecode($_GET["id"]);
$uploadDir = "../uploads/alliancesImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getAlliancesDetails = getAlliancesDetails($id,NULL,$conn);

    if(noError($getAlliancesDetails)){
        $getAlliancesDetails = $getAlliancesDetails["response"];

    }else{
        $returnArr = $getAlliancesDetails;
    }

}else{
    $returnArr = $conn;
    exit;
}
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if ($id=="undefined"){ ?>
	<h4 class="modal-title">Add New Alliances</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Alliances</h4>
		<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="aboutForm" enctype = "multipart/form-data">
		<input type="hidden" name="id" id="id" value="<?php echo $getAlliancesDetails[0]['id']; ?>">
		<input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getAlliancesDetails[0]['box_image']; ?>">
    	<input type="hidden" name="imageUrl1" id="imageUrl1" value="<?php echo $getAlliancesDetails[0]['feature_image']; ?>">
		<input type="hidden" name="method" value="update">
		<div class="form-group">
			<label for="feature-image">Box Image : </label>
			<input type="file" id = "alliances-feature-image" class="form-control" name="alliances-feature-image" onchange="aboutPreview('#alliances-feature-image', '#alliances-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="alliances-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getAlliancesDetails[0]["box_image"]; ?>'); <?php if (empty($getAlliancesDetails[0]['box_image']) || !isset($getAlliancesDetails[0]['box_image'])) { echo 'display: none'; }?>;"></div>
		</div>

    <div class="form-group">
			<label for="feature-image">Feature Image : </label>
			<input type="file" id = "alliances-feature-image1" class="form-control" name="alliances-feature-image1" onchange="aboutPreview('#alliances-feature-image1', '#alliances-image-preview1',null,'<?php echo $uploadDir; ?>');">
			<div id="alliances-image-preview1" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getAlliancesDetails[0]["feature_image"]; ?>'); <?php if (empty($getAlliancesDetails[0]['feature_image']) || !isset($getAlliancesDetails[0]['feature_image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="about-name">Name</label>
			<input type="text" class="form-control" id="alliances-name" name="alliances-name" value="<?php echo $getAlliancesDetails[0]['name']; ?>" maxlength="100">
		</div>

		<div class="form-group">
			<label for="alliances-services">Short Description</label>
			<textarea class="form-control" rows="3" id="alliances-shortDesc" name="alliances-shortDesc" maxlength="1000" ><?php echo $getAlliancesDetails[0]['short_desc']; ?></textarea>
		</div>
		<div class="form-group">
			<label for="alliances-services">long Description</label>
			<textarea class="form-control" rows="3" id="alliances-longDesc" name="alliances-longDesc" value="" maxlength="10000"><?php echo $getAlliancesDetails[0]['long_desc']; ?></textarea>
		</div>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdatAlliancesDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
