<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/awardContentModel.php';

$id = urldecode($_GET["id"]);
//echo $id;
$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getPressDetails = getAwardDetails($id,NULL,$conn);

    if(noError($getPressDetails)){
        $getPressDetails = $getPressDetails["response"];

    }else{
        $returnArr = $getPressDetails;
    }

}else{
    $returnArr = $conn;
    exit;
}

?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
  <div class="col-lg-3 blue-button-1" onclick="opentestimonial('<?php echo $id;?>');">Add Testimonial</div>
	<h4 class="modal-title">Update Award</h4>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
  	<div class="modal-err1" style="color:red;"></div>
	<form id="pressForm" enctype = "multipart/form-data">
      <input type="hidden" name="press_status" id="press_status" value="<?php echo $getPressDetails[0]['status']; ?>">
  <input type="hidden" name="id" id="id" value="<?php echo $getPressDetails[0]['id']; ?>">
  <input type="hidden" name="media_id" id="media_id" value="<?php echo $id; ?>">
  <input type="hidden" name="media_id1" id="media_id1" value="<?php echo $getPressDetails[0]['media_id']; ?>">
  <input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getPressDetails[0]['left_img']; ?>">
  <input type="hidden" name="imageUrl1" id="imageUrl1" value="<?php echo $getPressDetails[0]['right_img']; ?>">
  <input type="hidden" name="imageUrl2" id="imageUrl2" value="<?php echo $getPressDetails[0]['logo_img']; ?>">
	<input type="hidden" name="method" value="update">

		<div class="form-group">
			<label for="feature-image">Left Image : </label>
			<input type="file" id = "press-feature-image" class="form-control" name="press-feature-image" onchange="pressPreview('#press-feature-image', '#press-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="press-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getPressDetails[0]["left_img"]; ?>'); <?php if (empty($getPressDetails[0]['left_img']) || !isset($getPressDetails[0]['left_img'])) { echo 'display: none'; }?>;"></div>
		</div>

    <div class="form-group">
      <label for="feature-image">Right Image : </label>
      <input type="file" id = "press-feature-image1" class="form-control" name="press-feature-image1" onchange="pressPreview('#press-feature-image1', '#press-image-preview1',null,'<?php echo $uploadDir; ?>');">
      <div id="press-image-preview1" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getPressDetails[0]["right_img"]; ?>'); <?php if (empty($getPressDetails[0]['right_img']) || !isset($getPressDetails[0]['right_img'])) { echo 'display: none'; }?>;"></div>
    </div>

    <div class="form-group">
      <label for="feature-image">Logo Image : </label>
      <input type="file" id = "press-feature-image2" class="form-control" name="press-feature-image2" onchange="pressPreview('#press-feature-image2', '#press-image-preview2',null,'<?php echo $uploadDir; ?>');">
      <div id="press-image-preview2" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getPressDetails[0]["logo_img"]; ?>'); <?php if (empty($getPressDetails[0]['logo_img']) || !isset($getPressDetails[0]['logo_img'])) { echo 'display: none'; }?>;"></div>
    </div>


		<div class="form-group">
			<label for="about-name">Award Name</label>
			<input type="text" class="form-control" id="press-name" maxlength="100" name="press-name" value="<?php echo $getPressDetails[0]['name']; ?>" >
		</div>
		<div class="form-group">
			<label for="about-services">Award Title</label>
			<textarea class="form-control" rows="3" id="press-source-name" maxlength="200" name="press-source-name" ><?php echo $getPressDetails[0]['title']; ?></textarea>
		</div>
		<div class="form-group">
			<label for="about-services"> Description</label>
			<textarea class="form-control" rows="3" id="press-longDesc" maxlength="10000" name="press-longDesc" value=""><?php echo $getPressDetails[0]['address']; ?></textarea>
		</div>

	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdatePressDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>

<script>
   function updateViewMediaDialog(id){
     //alert(id);
        $( ".modal-content" ).load("updateAwardContent.php?id="+id);
        $("#myModal").modal();
    }

    /* To show preview of uploaded Image */
    function pressPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        if (!files.length || !window.FileReader) return;
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();

            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdatePressDetails() {
        $('.btn-success').prop('disable',true);
        var media_id                 = $('#media_id').val();
        //alert(media_id);
        var media_id1                 = $('#media_id1').val();
      //  alert(media_id1);
        var name                 = $('#press-name').val();
        var shortDesc            = $('#press-shortDesc').val();
        var longDesc             = $('#press-longDesc').val();

        if (name == '' || shortDesc == '' || longDesc == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        }

        else {
            var form = $('#pressForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/awardController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                  //  alert(data);
                  //  console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Media Details !!!');
                        $("#load-content").load("manageMediaContent.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Press Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    /* To remove about Details */
    function removePress(id) {
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/pressController.php",
            data:{
                id:id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Press Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Press Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageMediaContent.php");
            },
            error: function(data) {

                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting About Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);

            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmChangesRemodal.php?id="+id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removePress(id);
        $('#myModal .close').click();
    }

    function opentestimonial(id){
  //  alert(id);
        $("#load-content").load("manageTestimonialContent.php?id="+id);
    }

</script>
