<?php
/*Start session*/
session_start();

/* Including all necessary files and model */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once "../model/headerContentModel.php";

$pageId = cleanXSS(urldecode($_GET['pageId']));

/* Checking Session is active or not */
if (!isset($_SESSION['email']) || empty($_SESSION['email'])) {
	/* Session is not active redirecting to login page */
	print('<script>');
	print('var t = setTimeout(\'window.location="'.$rootUrl.'";\', 000);');
	print('</script>');
	die();
}

/* Declairing all necessary variables */
$returnArr = array();
$uploadDir = "../uploads/headerImages/";

/* Establishing connection to database */
$conn = createDbConnection($serverName, $dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
	$conn = $conn['conn'];
	if (isset($pageId) || !empty($pageId)) {
		$headerInfo = getHomeHeaderContent($pageId,$conn);
		if (noError($headerInfo)) {
			$headerInfo = $headerInfo['response'][0];
		} else {
			$returnArr = $headerInfo;
		}
	} 
} else {
	$returnArr = $conn;
	exit;
}
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if ($pageId=="undefined"){ ?>
	<h4 class="modal-title">Add New Header Content</h4>	
<?php }else{?>
		<h4 class="modal-title">Update Header Content</h4>	
	<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="homePageContent" action="<?php echo $rootUrl; ?>controller/headerContentController.php" method = "post" enctype = "multipart/form-data">		
		<input type="hidden" name="feature-image-url" value="<?php echo $headerInfo['image']; ?>">
		<input type="hidden" name="pageId" value="<?php echo $headerInfo['id']; ?>">
		<input type="hidden" name="method" value="update">
		<input type="hidden" name="pageName" value="home">
		<div class="form-group">
			<label for="feature-image"> Feature Image : </label>			
			<input type="file" id = "feature-image" class="form-control" name="feature-image" onchange="headerPreview('#feature-image', '#feature-img-preview',null,'<?php echo $uploadDir; ?>');">
		</div>
		<div id="feature-img-preview" class="feature-image featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$headerInfo["image"]; ?>'); <?php if (empty($headerInfo['image']) || !isset($headerInfo['image'])) { echo 'display: none'; }?>"></div>
		<div class="form-group">
			<label for="top-desc-title">Top Description Title:</label>
			<input type="text" class="form-control" id="top-desc-title" name="top-desc-title" value="<?php echo $headerInfo['title']; ?>" maxlength="100">
		</div>
		<div class="form-group">
			<label for="top-desc">Top Description:</label>
			<textarea class="form-control" rows="3" id="top-desc" name="top-desc" maxlength="1000"><?php echo $headerInfo['description'];  ?></textarea>
		</div>
		<div class="form-group">
			<label for="know-more">Know More URL :</label>
			<input type="text" class="form-control" id="know-more" name="know-more" value="<?php echo $headerInfo['know_more_link']; ?>" maxlength="1000">
		</div>
	</form>	
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateHomeHeaderContent('<?php echo $headerInfo[0]['id']?>');">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>	
</div>