<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/growthCronicleModel.php';

$growthId = urldecode($_GET["growthId"]);
$growthUrlId = urldecode($_GET['growthUrlId']);
$uploadDir = "../uploads/growthImages/";
$returnArr = array();
//echo $id;

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $growthInfo = getGrowthDetails($growthId,$growthUrlId,$conn);
    if(noError($growthInfo)){
        $growthInfo = $growthInfo["response"];
    }else{
        $returnArr = $growthInfo;
    }

}else{
    $returnArr = $conn;
    exit;
}
// printArr($growthInfo);
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if ($growthId=="undefined"){ ?>
	<h4 class="modal-title">Add New Growth Chronicle Details</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Growth Chronicle Details</h4>
		<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="growth-form" action="" method = "post" enctype = "multipart/form-data">
		<input type="hidden" id = "growthId" class="" name="growthId" value='<?php echo $growthInfo[0]["id"]; ?>'>

		<input type="hidden" name="growthUrlId" id="growthUrlId" value="<?php if (!isset($growthInfo[0]['growthUrlId']) || empty($growthInfo[0]['growthUrlId']) || $growthInfo[0]['growthUrlId'] == 0) { echo $growthUrlId; } else { echo $growthInfo[0]['growthUrlId']; }?>">

		<input type="hidden" id = "growthImageUrl" class="" name="growthImageUrl" value="<?php echo $growthInfo[0]['growthImage']; ?>">
		<input type="hidden" id = "method" class="" name="method" value="update">

		<div class="form-group">
			<label for="feature-image"> Image : </label>
			<input type="file" id = "growthFeatureImage" class="form-control" name="growthFeatureImage" onchange="growthPreview('#growthFeatureImage', '#growthColorImage',null,'<?php echo $uploadDir; ?>');">
			<div id="growthColorImage" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$growthInfo[0]["growthImage"]; ?>'); <?php if (empty($growthInfo[0]['growthImage']) || !isset($growthInfo[0]['growthImage'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label>Year:</label>
			<input type="text" pattern="[0-9-+]{4,9}" class="form-control" id="growthYear" name="growthYear" value="<?php echo $growthInfo[0]["growthYear"]; ?>" >
		</div>
		<div class="form-group">
			<label>Description:</label>
			<textarea class="form-control" id="description1" name="description1" maxlength="10000" rows = "4"><?php echo $growthInfo[0]["growthDescription"]; ?></textarea>
		</div>
	</form>
	<div class="modal-footer">

		<button type="button" class="btn btn-success" onclick="addUpdateGrowthDetails()">Submit</button>
		<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
	</div>
</div>
