<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/mediaContentModel.php';

$id = urldecode($_GET["id"]);
$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getMediaDetails = getMediaDetails($id,NULL,$conn);

    if(noError($getMediaDetails)){
        $getMediaDetails = $getMediaDetails["response"];

    }else{
        $returnArr = $getMediaDetails;
    }

}else{
    $returnArr = $conn;
    exit;
}
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if ($id=="undefined"){ ?>
	<h4 class="modal-title">Add New Media</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Media</h4>
		<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="aboutForm" enctype = "multipart/form-data">
		<input type="hidden" name="id" id="id" value="<?php echo $getMediaDetails[0]['id']; ?>">
		<input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getMediaDetails[0]['box_image']; ?>">
    	<input type="hidden" name="imageUrl1" id="imageUrl1" value="<?php echo $getMediaDetails[0]['feature_image']; ?>">
		<input type="hidden" name="aboutStatus" id="aboutStatus" value="<?php echo $getMediaDetails[0]['status']; ?>">
		<input type="hidden" name="method" value="update">
		<div class="form-group">
			<label for="feature-image">Box Image : </label>
			<input type="file" id = "media-feature-image" class="form-control" name="media-feature-image" onchange="aboutPreview('#media-feature-image', '#media-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="media-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getMediaDetails[0]["box_image"]; ?>'); <?php if (empty($getMediaDetails[0]['box_image']) || !isset($getMediaDetails[0]['box_image'])) { echo 'display: none'; }?>;"></div>
		</div>

    <div class="form-group">
			<label for="feature-image">Feature Image : </label>
			<input type="file" id = "media-feature-image1" class="form-control" name="media-feature-image1" onchange="aboutPreview('#media-feature-image1', '#media-image-preview1',null,'<?php echo $uploadDir; ?>');">
			<div id="media-image-preview1" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getMediaDetails[0]["feature_image"]; ?>'); <?php if (empty($getMediaDetails[0]['feature_image']) || !isset($getMediaDetails[0]['feature_image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="about-name">Name</label>
			<input type="text" class="form-control" id="featured-name" maxlength="100"  name="featured-name" value="<?php echo $getMediaDetails[0]['name']; ?>" >
		</div>

    <div class="form-group">
      <label for="about-name">Select Type</label>
    </div>


    <select class="form-control droper" name="page_type" id="page_type" style="">
              <option value = "NULL">Please Select</option>
              <option value = "managePressContent.php">Press</option>
              <option value = "updateAwardContent.php">Awards</option>
          </select>




		<div class="form-group">
			<label for="about-services">Short Description</label>
			<textarea class="form-control" rows="3" id="featured-shortDesc" maxlength="1000" name="featured-shortDesc" ><?php echo $getMediaDetails[0]['short_desc']; ?></textarea>
		</div>
		<div class="form-group">
			<label for="about-services">long Description</label>
			<textarea class="form-control" rows="3" id="featured-longDesc" maxlength="10000" name="featured-longDesc" value=""><?php echo $getMediaDetails[0]['long_desc']; ?></textarea>
		</div>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdatMediaDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
