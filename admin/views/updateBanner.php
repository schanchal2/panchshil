<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/bannerContentModel.php';

$id = urldecode($_GET["id"]);
//echo $id;
$b_id = urldecode($_GET["b_id"]);
//echo $b_id;
$p_id=urldecode($_GET["p_id"]);
$method= urldecode($_GET["method"]);

$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    if($method!="insert"){
    $getBannerDetails = getBannerDetails($b_id,NULL,$conn);
    if(noError($getBannerDetails)){
        $getBannerDetails = $getBannerDetails["response"];
    }else{
        $returnArr = $getBannerDetails;
    }
  }

}else{
    $returnArr = $conn;
    exit;
}
//printArr($getBannerDetails);
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if($method=="insert"){ ?>
	<h4 class="modal-title">Add New Banner</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Banner</h4>
		<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
  	<div class="modal-err1" style="color:red;"></div>
	<form id="bannerForm" enctype = "multipart/form-data">
      <input type="hidden" name="press_status" id="press_status" value="<?php echo $getBannerDetails[0]['status']; ?>">
  <input type="hidden" name="id" id="id" value="<?php echo $getBannerDetails[0]['id']; ?>">
  <input type="hidden" name="o_id" id="o_id" value="<?php echo $b_id; ?>">
  <input type="hidden" name="o_id1" id="o_id1" value="<?php echo $getBannerDetails[0]['o_id']; ?>">
  <input type="hidden" name="p_id" id="p_id" value="<?php echo $p_id; ?>">
  <input type="hidden" name="p_id1" id="p_id1" value="<?php echo $getBannerDetails[0]['p_id']; ?>">
  <input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getBannerDetails[0]['banner1']; ?>">
  <input type="hidden" name="imageUrl1" id="imageUrl1" value="<?php echo $getBannerDetails[0]['banner2']; ?>">
  <input type="hidden" name="imageUrl2" id="imageUrl2" value="<?php echo $getBannerDetails[0]['banner3']; ?>">
  <input type="hidden" name="imageUrl3" id="imageUrl3" value="<?php echo $getBannerDetails[0]['banner4']; ?>">
  <input type="hidden" name="imageUrl4" id="imageUrl4" value="<?php echo $getBannerDetails[0]['banner5']; ?>">
	<input type="hidden" name="method" value="update">

		<div class="form-group">
			<label for="feature-image">Banner 1 : </label>
			<input type="file" id = "banner-feature-image" class="form-control" name="banner-feature-image" onchange="bannerPreview('#banner-feature-image', '#banner-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="banner-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner1"]; ?>'); <?php if (empty($getBannerDetails[0]['banner1']) || !isset($getBannerDetails[0]['banner1'])) { echo 'display: none'; }?>;"></div>
		</div>

    <div class="form-group">
      <label for="feature-image">Banner 2 : </label>
      <input type="file" id = "banner-feature-image1" class="form-control" name="banner-feature-image1" onchange="bannerPreview('#banner-feature-image1', '#banner-image-preview1',null,'<?php echo $uploadDir; ?>');">
      <div id="banner-image-preview1" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner2"]; ?>'); <?php if (empty($getBannerDetails[0]['banner2']) || !isset($getBannerDetails[0]['banner2'])) { echo 'display: none'; }?>;"></div>
    </div>

    <div class="form-group">
      <label for="feature-image">Banner 3 :</label>
      <input type="file" id = "banner-feature-image2" class="form-control" name="banner-feature-image2" onchange="bannerPreview('#banner-feature-image2', '#banner-image-preview2',null,'<?php echo $uploadDir; ?>');">
      <div id="banner-image-preview2" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner3"]; ?>'); <?php if (empty($getBannerDetails[0]['banner3']) || !isset($getBannerDetails[0]['banner3'])) { echo 'display: none'; }?>;"></div>
    </div>

    <div class="form-group">
      <label for="feature-image">Banner 4 : </label>
      <input type="file" id = "banner-feature-image3" class="form-control" name="banner-feature-image3" onchange="bannerPreview('#banner-feature-image3', '#banner-image-preview3',null,'<?php echo $uploadDir; ?>');">
      <div id="banner-image-preview3" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner4"]; ?>'); <?php if (empty($getBannerDetails[0]['banner4']) || !isset($getBannerDetails[0]['banner4'])) { echo 'display: none'; }?>;"></div>
    </div>

    <div class="form-group">
      <label for="feature-image">Banner 4 :</label>
      <input type="file" id = "banner-feature-image4" class="form-control" name="banner-feature-image4" onchange="bannerPreview('#banner-feature-image4', '#banner-image-preview4',null,'<?php echo $uploadDir; ?>');">
      <div id="banner-image-preview4" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner5"]; ?>'); <?php if (empty($getBannerDetails[0]['banner5']) || !isset($getBannerDetails[0]['banner5'])) { echo 'display: none'; }?>;"></div>
    </div>

	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateBannerDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
