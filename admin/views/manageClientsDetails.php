<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/clientDetailsModel.php';

$returnArr      = array();
$clientUrlId    = cleanXSS(urldecode($_GET['id']));
$pageName       = cleanXSS(urldecode($_GET['pageName']));
//echo $clientUrlId;

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];    
    $clientInfo = getClientDetails($clientId, $clientUrlId, $conn);
    echo "<pre>"; print_r($clientInfo); echo "</pre>";
    if(noError($clientInfo)){
        $clientInfo = $clientInfo["response"];
    }else{
        $returnArr = $clientInfo;
    }
}else{
    $returnArr = $conn;
    exit;
}
?>

<div class="col-lg-12 main-container-1">
    <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
       <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage <?php echo ucfirst(strtolower($pageName)); ?> Details</p>
        </div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <input type="hidden" name="client-Url-Id" id="client-Url-Id" value="<?php echo $clientUrlId; ?>">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;"> 
                        <th width="20%">Image</th>                        
                        <th width="25%">Name</th>
                        <th width="25%">Services</th>
                        <th width="10%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                          <div class="col-lg-3 blue-button-1" onclick="openEditDialog('-1','<?php echo $clientUrlId;?>');">Add New Client</div>
                    <?php 
                        if (empty($clientInfo)) {
                            echo "<tr><td colspan = '5' style = 'text-align:center;'>No Data Found</td></tr>";
                        }
                        for ($i = 0; $i < count($clientInfo); $i++) {
                    ?>

                        <tr>                                                       
                            <td><div id="clientColorImage" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$clientInfo[$i]["clientColorImage"]; ?>');"></div>
                            </td>
                            
                            <td>                            
                            <p class = "clientDisplay"><?php echo $clientInfo[$i]['clientName']; ?></p>                            
                            </td>
                            <td>                               
                                <p class = "clientDisplay"><?php echo $clientInfo[$i]['clientServices']; ?></p>                                
                            </td>
                            <td>
                                <a href="#" onclick="openEditDialog('<?php echo $clientInfo[$i]['clientId'];?>','<?php echo $clientInfo[$i]['clientUrlId'];?>');">Edit</a><br>
                                <a href="#" onclick="confirmRemoveData('<?php echo $clientInfo[$i]['clientId'];?>','<?php echo $clientInfo[$i]['clientUrlId'];?>')">Delete</a>
                            </td>   
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
             
           <!-- <a href="#" onclick="openEditDialog();">Add Team</a>-->
        </div>  
</div>
</div>
<script>
    $(document).ready(function() {
        //$('#example1').DataTable();
    } );

    /* TO Display Remodal on click of Add or Update Clients details */
   function openEditDialog(id,urlId){

    //alert(id);
    //alert(urlId);


        $(".modal-content").load("updateClientsDetails.php?clientId="+id+"&clientUrlId="+urlId);
        $("#myModal").modal();
    }
    var pageName            = '<?php echo $pageName; ?>';
    /* To show preview of uploaded Image */
    function clientPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        /* Checking Is any File selected */
        if (!files.length || !window.FileReader) return;

        /* Check Uploadede file is Image Only */
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();
            /* set image data as background of div */
            reader.onloadend = function(){ 
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }
    
    /* TO add and update Clients details */
    function addUpdateClientsDetails() {
        $('.btn-success').prop('disable',true);
        var clientUrlId     = $('#client-Url-Id').val();
        var clientName      = $('#client-name').val();
        var clientUrl       = $('#client-url').val();
        var clientServices  = $('#client-services').val();
        if (clientName == '' || clientUrl == '' || clientServices == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#clientfrom')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/clientsDetailsController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Client Details !!!');
                        $("#load-content").load("manageClientsDetails.php?id="+clientUrlId+"&pageName="+pageName);
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Client Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }                
    }

    /* To remove Client Details */
    function removeClient(id) {
        var clientUrlId     = $('#client-Url-Id').val();        
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/clientsDetailsController.php",
            data:{
                DeleteClientId:id, DeleteClientUrlId:clientUrlId, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Client Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Client Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageClientsDetails.php?id="+clientUrlId+"&pageName="+pageName);
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Client Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);
            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmRemoveData.php?id="+id);
        $("#myModal").modal(); 
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeClient(id);
        $('#myModal .close').click();
    }
</script>

