<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/awardContentModel.php';

$returnArr = array();

$media_id=$_GET['id'];
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);


if(noError($conn)){
    $conn = $conn["conn"];
    $getAwardDetails = getAwardDetailsById($media_id,$conn);
    if(noError($getAwardDetails)){
        $getAwardDetails = $getAwardDetails["response"];

    }else{
        $returnArr = $getAwardDetails;
    }

}else{
    $returnArr = $conn;
    exit;
}

//printArr($getAwardDetails);

?>
<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
      	 <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage Award Of <?php echo " "; echo $media_id;?></div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="updateViewMediaDialog('<?php echo $media_id; ?>');">Add Award</div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px; ">
                        <th width="30%" style="text-align:center;">Image</th>
                        <th width="25%" style="text-align:center;">Title</th>
                        <th width="25%" style="text-align:center;">Name</th>
                          <th width="25%" style="text-align:center;">Description</th>
                        <th width="5%" style="text-align:center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (empty($getAwardDetails)) {
                                echo "<tr><td colspan = '6' style = 'text-align:center;'>No Data Found</td></tr>";
                            }
                            for ($i = 0; $i < count($getAwardDetails); $i++) {
                        ?>
                        <tr>



                            <td>
                                <div id="image" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getAwardDetails[$i]["left_img"]; ?>');"></div>
                            </td>
                            <td>
                                 <p class="businessDisplay" id="name" name="name"><?php echo $getAwardDetails[$i]['title']; ?></p>
                            </td>
                            <td>
                                 <p class="businessDisplay" id="source_name" name="source_name"><?php echo $getAwardDetails[$i]['name']; ?></p>
                            </td>

                            <td>
                                 <p class="businessDisplay" id="date" name="date"><?php echo $getAwardDetails[$i]['address']; ?></p>
                            </td>
                            <td>
                                <a href="#" onclick="updateViewMediaDialog(<?php echo $getAwardDetails[$i]['id']; ?>);">Edit</a><br>
                                <a href="#" onclick="confirmRemoveData(<?php echo $getAwardDetails[$i]['id']; ?>);">
                                    <span style="font-size: 16px;font-weight: bold;  ">Delete
                                    </span>
                                </a><br>
                                  <a href="#" onclick='opentestimonial(<?php echo $getAwardDetails[$i]['id'];?>);'>Manage</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</div>
<script>
   function updateViewMediaDialog(id){
     //alert(id);
        $( ".modal-content" ).load("updateAwardContent.php?id="+id);
        $("#myModal").modal();
    }

    /* To show preview of uploaded Image */
    function pressPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        if (!files.length || !window.FileReader) return;
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();

            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdatePressDetails() {
        $('.btn-success').prop('disable',true);
        var media_id                 = $('#media_id').val();
        //alert(media_id);
        var media_id1                 = $('#media_id1').val();
      //  alert(media_id1);
        var name                 = $('#press-name').val();
        var shortDesc            = $('#press-shortDesc').val();
        var longDesc             = $('#press-longDesc').val();

        if (name == '' || shortDesc == '' || longDesc == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        }

        else {
            var form = $('#pressForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/awardController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                  //  alert(data);
                  //  console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Media Details !!!');
                        if(media_id && media_id1)
                        {
                            $("#load-content").load("manageAwardContent.php?id="+media_id1);
                        }
                        else {
                              $("#load-content").load("manageAwardContent.php?id="+media_id);
                        }
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Press Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    /* To remove about Details */
    function removePress(id) {
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/pressController.php",
            data:{
                id:id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Press Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Press Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageMediaContent.php");
            },
            error: function(data) {

                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting About Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);

            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmChangesRemodal.php?id="+id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removePress(id);
        $('#myModal .close').click();
    }

    function opentestimonial(id){
    //alert(id);
        $("#load-content").load("managetestimonialContent.php?id="+id);
    }

</script>
