<?php

//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/aboutContentModel.php';

$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
// printArr($conn);
if(noError($conn)){
    $conn = $conn["conn"];
    $getAboutDetails = getAboutDetails(NULL,NULL,$conn);
    if(noError($getAboutDetails)){
        $getAboutDetails = $getAboutDetails["response"];

    }else{
        $returnArr = $getAboutDetails;
    }

}else{
    $returnArr = $conn;
    exit;
}

// printArr($getAboutDetails);

?>
<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
      	 <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage About</div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="updateViewAboutDialog();">Add About</div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;">
                         <th width="25%">Image</th>
                         <th width="25%">Name</th>
                         <th width="25%">Status</th>
                         <th width="25%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (empty($getAboutDetails)) {
                                echo "<tr><td colspan = '4' style = 'text-align:center;'>No Data Found</td></tr>";
                            }
                            for ($i = 0; $i < count($getAboutDetails); $i++) {
                        ?>
                        <tr>
                            <td>
                                <div id="image" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$getAboutDetails[$i]["image"]; ?>');"></div>
                            </td>
                            <td>
                                 <p class="businessDisplay" id="Name" name="Name"><?php echo $getAboutDetails[$i]['name']; ?></p>
                            </td>
                            <td>
                                 <span style="font-size: 16px;font-weight: bold; <?php if($getAboutDetails[$i]['status'] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?> "><?php echo strtoupper($getAboutDetails[$i]['status']); ?></span>
                            </td>
                            <td>
                                <a href="#" onclick="updateViewAboutDialog(<?php echo $getAboutDetails[$i]['id']; ?>);">Edit</a><br>
                                <a href="#" onclick="confirmRemoveData(<?php echo $getAboutDetails[$i]['id']; ?>);">
                                    <span style="font-size: 16px;font-weight: bold;  ">
                                        <?php if($getAboutDetails[$i]["status"] == "Active"){ echo "Inactivate/Delete";}else{echo "Activate";} ?>
                                    </span>
                                </a><br>


                                <a href="#" onclick="openViewAboutDialog('<?php echo $getAboutDetails[$i]["pagename"];?>','<?php echo $getAboutDetails[$i]["id"];?>','<?php echo $getAboutDetails[$i]["name"];?>');">Manage</a><br>

                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</div>
<script>
   function updateViewAboutDialog(id){
        $( ".modal-content" ).load("updateAbout.php?id="+id);
        $("#myModal").modal();
    }

    /* To show preview of uploaded Image */
    function aboutPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        if (!files.length || !window.FileReader) return;
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();

            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdateAboutDetails() {
        $('.btn-success').prop('disable',true);
      //  alert(1);
        var name                 = $('#about-name').val();
        var shortDesc            = $('#about-shortDesc').val();
        var longDesc             = $('#about-longDesc').val();
        if (name == '' || shortDesc == '' || longDesc == '') {
          //alert(1);
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#aboutForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/aboutController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated about Details !!!');
                        $("#load-content").load("manageAboutContent.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating About Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    /* To remove about Details */
    function removeAbout(id) {
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/aboutController.php",
            data:{
                id:id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted About Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete About Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageAboutContent.php");
            },
            error: function(data) {

                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting About Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);

            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        //alert(id);
        $(".modal-content").load("confirmChangesRemodal.php?id="+id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeAbout(id);
        $('#myModal .close').click();
    }

    function openViewAboutDialog(pagename,id,name){

      console.log(pagename);
        $("#load-content").load(pagename+"?id="+id+"&pageName="+encodeURIComponent(name));

    }

</script>
