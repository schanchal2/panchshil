<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/testimonialContentModel.php';
require_once '../model/pageNameContentModel.php';

$returnArr = array();

$media_id=$_GET['id'];
//echo $media_id;
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);


if(noError($conn)){
    $conn = $conn["conn"];
    $getTestimonialDetails = getTestimonialDetailsById($media_id,$conn);
    if(noError($getTestimonialDetails)){
        $getTestimonialDetails = $getTestimonialDetails["response"];

    }else{
        $returnArr = $getTestimonialDetails;
    }

    $pageInfo = getPagenameForTestimonial('awardDetails',$media_id,$conn);
    if(noError($pageInfo)){
        $pageInfo = $pageInfo["response"][0];
    }else{
        $returnArr = $pageInfo;
    }

}else{
    $returnArr = $conn;
    exit;
}

//printArr($pageInfo);

?>
<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
      	 <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;"><?php echo strtoupper($pageInfo['name']); ?> </div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="updateViewMediaDialog('insert',<?php echo $media_id; ?>);">AddClient Testimonial </div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px; ">
                        <th width="30%" style="text-align:center;">Name</th>
                        <th width="30%" style="text-align:center;">Designation</th>
                          <th width="30%" style="text-align:center;">Description</th>
                        <th width="10%" style="text-align:center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (empty($getTestimonialDetails)) {
                                echo "<tr><td colspan = '6' style = 'text-align:center;'>No Data Found</td></tr>";
                            }
                            for ($i = 0; $i < count($getTestimonialDetails); $i++) {
                        ?>
                        <tr>
                            <td>
                                 <p class="businessDisplay" id="name" name="name"><?php echo $getTestimonialDetails[$i]['name']; ?></p>
                            </td>
                            <td>
                                 <p class="businessDisplay" id="source_name" name="source_name"><?php echo $getTestimonialDetails[$i]['desigantion']; ?></p>
                            </td>

                            <td>
                                 <p class="businessDisplay" id="date" name="date"><?php echo $getTestimonialDetails[$i]['description']; ?></p>
                            </td>
                            <td>
                                <a href="#" onclick="updateViewMediaDialog('update',<?php echo $getTestimonialDetails[$i]['id']; ?>);">Edit</a><br>
                                <a href="#" onclick="confirmRemoveData(<?php echo $getTestimonialDetails[$i]['id']; ?>,<?php echo $getTestimonialDetails[$i]['award_id']; ?>);">
                                    <span style="font-size: 16px;font-weight: bold;  ">Delete
                                    </span>
                                </a><br>

                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</div>
<script>
   function updateViewMediaDialog(method,award_id){
        //alert(method);
      //  alert(award_id);
        $( ".modal-content" ).load("updateTestimonialContent.php?award_id="+award_id+'&method='+method);
        $("#myModal").modal();
    }

    /* To show preview of uploaded Image */
    function pressPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        if (!files.length || !window.FileReader) return;
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();

            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdateTestimonialDetails() {
        $('.btn-success').prop('disable',true);
        var award_id                 = $('#award_id').val();
          var award_id1                 = $('#award_id1').val();
        var name                 = $('#press-name').val();
        var shortDesc            = $('#press-shortDesc').val();
        var longDesc             = $('#press-longDesc').val();

        if (name == '' || shortDesc == '' || longDesc == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        }

        else {
            var form = $('#pressForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/testimonialController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                  //  alert(data);
                  //  console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Testimonial Details !!!');
                        if(award_id && award_id1)
                        {
                          $("#load-content").load("manageTestimonialContent.php?id="+award_id1);
                        }
                        else {
                              $("#load-content").load("manageTestimonialContent.php?id="+award_id);
                        }
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Testimonial Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    /* To remove about Details */
    function removeTestimonial(id,award_id) {
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/testimonialController.php",
            data:{
                id:id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Testimonial Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Testimonial Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageTestimonialContent.php?id="+award_id);
            },
            error: function(data) {

                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Testimonial Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);

            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id,award_id) {
        $(".modal-content").load("confirmChangesRemodalById.php?id="+id+'&award_id='+award_id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id,award_id) {
        removeTestimonial(id,award_id);
        $('#myModal .close').click();
    }

    function opentestimonial(){
        $("#load-content").load("managetestimonialContent.php");
    }

</script>
