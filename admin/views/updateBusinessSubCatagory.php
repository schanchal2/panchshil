<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/businessSubCatagoryModel.php';

$businessId = cleanXSS(urldecode($_GET["businessId"]));
$businessCatId = cleanXSS(urldecode($_GET["businessCatId"]));
$businessSubCatId = cleanXSS(urldecode($_GET["businessSubCatId"]));
// $uploadDir = "../uploads/businessImages/";
$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];    
    $businessSubCatInfo = getBusinessSubCatDetails($businessSubCatId, $businessCatId, NULL, $conn);
    if(noError($businessSubCatInfo)){
        $businessSubCatInfo = $businessSubCatInfo["response"];                    
    }else{
        $returnArr = $businessSubCatInfo;
    }
}else{
    $returnArr = $conn;
    exit;
}
// printArr($businessSubCatInfo);
?>


<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Add subcatagories Information</h4>		
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="businessSubCatForm" enctype = "multipart/form-data">		
        <input type="hidden" name="businessId" id="businessId" value="<?php echo $businessId; ?>">
        <input type="hidden" name="businessCatId" id="businessCatId" value="<?php echo $businessCatId; ?>">
        <input type="hidden" name="businessSubCatId" id="businessSubCatId" value="<?php echo $businessSubCatInfo[0]['id']; ?>">
        <input type="hidden" name="businessSubCatStatus" id="businessSubCatStatus" value="<?php echo $businessSubCatInfo[0]['sub_cat_status']; ?>">
		<input type="hidden" name="method" value="update">
		<div class="form-group">

			<label for="businessSubCat-name">Name</label>
			<input type="text" class="form-control" id="businessSubCat-name" name="businessSubCat-name" value="<?php echo $businessSubCatInfo[0]['sub_cat_name']; ?>" >
		</div>
		<div class="form-group">
			<label for="businessSubCat-number">Number</label>
			<input type="text" class="form-control" id="businessSubCat-number" name="businessSubCat-number" value="<?php echo $businessSubCatInfo[0]['sub_cat_number']; ?>" maxlength="10">
		</div>
		<div class="form-group">
			<label for="businessSubCat-email">Email</label>
			<input type="text" class="form-control" id="businessSubCat-email" name="businessSubCat-email" value="<?php echo $businessSubCatInfo[0]['sub_cat_email']; ?>" >
		</div>
		<div class="form-group">
			<label for="businessSubCat-address">Address</label>
			<textarea class="form-control" rows="3" id="businessSubCat-address" name="businessSubCat-address"><?php echo $businessSubCatInfo[0]['sub_cat_address']; ?></textarea>
		</div>
	</form>	
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateBusinessSubCatDetails();">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>	
</div>