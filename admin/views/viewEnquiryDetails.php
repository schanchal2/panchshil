<?php
//prepare for request
//start session
session_start();


//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/enquiryContentModel.php';

$returnArr = array();
$Id = $_GET['id'];
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $enquiryContent = getEnquiryDetails($Id,$conn);
    if(noError($enquiryContent)){
        $enquiryContent = $enquiryContent["response"];

    }else{
        $returnArr = $enquiryContent;
    }
}else{
    $returnArr = $conn;
    exit;
}
maxlength="1000" <?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/floorPlanContentModel.php';
$id = urldecode($_GET["id"]);
$b_id = urldecode($_GET["b_id"]);
$p_id=urldecode($_GET["p_id"]);
$method= urldecode($_GET["method"]);
$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    if($method!="insert"){
    $getFloorPlanDetails = getFloorPlanDetails($b_id,$conn);
    if(noError($getFloorPlanDetails)){
        $getFloorPlanDetails = $getFloorPlanDetails["response"];
    }else{
        $returnArr = $getFloorPlanDetails;
    }
  }

}else{
    $returnArr = $conn;
    exit;
}
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if($method=="insert"){ ?>
	<h4 class="modal-title">Add Floor Plan</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Floor Plan</h4>
		<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
  	<div class="modal-err1" style="color:red;"></div>
	<form id="floorPlanForm" enctype = "multipart/form-data">
      <input type="hidden" name="press_status" id="press_status" value="<?php echo $getFloorPlanDetails[0]['status']; ?>">
  <input type="hidden" name="id" id="id" value="<?php echo $getFloorPlanDetails[0]['id']; ?>">
  <input type="hidden" name="o_id" id="o_id" value="<?php echo $b_id; ?>">
  <input type="hidden" name="o_id1" id="o_id1" value="<?php echo $getFloorPlanDetails[0]['o_id']; ?>">
  <input type="hidden" name="p_id" id="p_id" value="<?php echo $p_id; ?>">
  <input type="hidden" name="p_id1" id="p_id1" value="<?php echo $getFloorPlanDetails[0]['p_id']; ?>">
  <input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getFloorPlanDetails[0]['floor_img']; ?>">
  <input type="hidden" name="imageUrl1" id="imageUrl1" value="<?php echo $getFloorPlanDetails[0]['property_img']; ?>">
	<input type="hidden" name="method" value="update">

		<div class="form-group">
			<label for="feature-image">Floor Image: </label>
			<input type="file" id = "banner-feature-image" class="form-control" name="banner-feature-image" onchange="bannerPreview('#banner-feature-image', '#banner-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="banner-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getFloorPlanDetails[0]["floor_img"]; ?>'); <?php if (empty($getFloorPlanDetails[0]['floor_img']) || !isset($getFloorPlanDetails[0]['floor_img'])) { echo 'display: none'; }?>;"></div>
		</div>

    <div class="form-group">
      <label for="feature-image">Property Image: </label>
      <input type="file" id = "banner-feature-image1" class="form-control" name="banner-feature-image1" onchange="bannerPreview('#banner-feature-image1', '#banner-image-preview1',null,'<?php echo $uploadDir; ?>');">
      <div id="banner-image-preview1" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getFloorPlanDetails[0]["property_img"]; ?>'); <?php if (empty($getFloorPlanDetails[0]['property_img']) || !isset($getFloorPlanDetails[0]['property_img'])) { echo 'display: none'; }?>;"></div>
    </div>
    <div class="form-group">
			<label for="corporate-services">Type</label>
			<textarea class="form-control" rows="3" id="type" maxlength="100" name="type" ><?php echo $getFloorPlanDetails[0]['type']; ?></textarea>
		</div>
    <div class="form-group">
			<label for="corporate-services">Area</label>
			<textarea class="form-control" rows="3" id="area" maxlength="100" name="area" ><?php echo $getFloorPlanDetails[0]['area']; ?></textarea>
		</div>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateFloorPlanDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>

?>


<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">View Enquiry Details</h4>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<?php for($i=0; $i < count($enquiryContent); $i++) { ?>
	<form id="enquiry-form" action="" method = "post" enctype = "multipart/form-data">
		<input type="hidden" class="form-control" name="method" value="update">
		<input type="hidden" class="form-control" name="id" value="<?php echo $enquiryContent[$i]['id'];?>">
		<div class="form-group">
			<label>Property Name:</label>
			<input type="text" class="form-control" id="propertyName" name="propertyName" value="<?php echo $enquiryContent[$i]["propertyName"];?>" readonly>
		</div>
		<div class="form-group">
			<label>Name:</label>
			<input type="text" class="form-control" id="name" name="name" value="<?php echo $enquiryContent[$i]["name"];?>" readonly>
		</div>
		<div class="form-group">
			<label>Email:</label>
			<input type="text" class="form-control" id="email" name="email" value="<?php echo $enquiryContent[$i]["email"];?>" readonly>
		</div>
		<div class="form-group">
			<label>Mobile Number:</label>
			<input type="text" class="form-control" maxlength= "10" id="mobileNumber" name="mobileNumber" value="<?php echo $enquiryContent[$i]["mobileNumber"];?>">
		</div>
		<div class="form-group">
			<label>Date:</label>
			<input type="date" class="form-control" id="contactDate"  name="contactDate" value="<?php echo $enquiryContent[$i]["contactDate"];?>">
		</div>
		<div class="form-group">
			<label>Time:</label>
			<input type="time" class="form-control" id="contactTime" name="contactTime" value="<?php echo $enquiryContent[$i]["contactTime"];?>">
		</div>
	</form>
	<?php } ?>
	<div class="modal-footer">
		<button type="button" class="btn btn-success" onclick="addUpdateEnquiryContent(<?php echo $enquiryContent[$i]['id']; ?>);">Update</button>
		<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
	</div>
</div>
