<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/matrixSpecificationContentModel.php';

$id = urldecode($_GET["id"]);
//echo $id;
$b_id = urldecode($_GET["b_id"]);
$p_id = urldecode($_GET["p_id"]);
//echo $b_id;
$method= urldecode($_GET["method"]);

$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    if($method!="insert"){
    $getBannerDetails = getBannerDetails($b_id,NULL,$conn);
    if(noError($getBannerDetails)){
        $getBannerDetails = $getBannerDetails["response"];
    }else{
        $returnArr = $getBannerDetails;
    }
  }

}else{
    $returnArr = $conn;
    exit;
}
//printArr($getBannerDetails);
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if($method=="insert"){ ?>
	<h4 class="modal-title">Add New Matrix Specification</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Matrix Specification</h4>
		<?php }?>
</div>
  <form name="bannerForm" id="bannerForm">
    	<input type="hidden" name="method" value="update">
      <input type="hidden" name="o_id" id="o_id" value="<?php echo $b_id?>">
      <input type="hidden" name="p_id" id="p_id" value="<?php echo $p_id?>">
    <div class="table-responsive">
      <table class="table table-bordered" id="dynamic_field">
        <tr>
          <td><input type="text" maxlength="100" name="name[]" placeholder=" Description" class="form-control" /></td>
          <td><input type="text" maxlength="100" name="unitTitle[]" placeholder=" Unit Title" class="form-control" /></td>
          <td><input type="text" pattern="[0-9]{10}" name="unitValue[]" placeholder=" Unit Value" onkeypress="return isNumberKey(event)" class="form-control" /></td>
          <td>  <input name="file" type="file" id="file"/></td>
          <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>
        </tr>
      </table>
    </div>
  </form>

<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateBannerDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
</form>
<script>
 $(document).ready(function(){
      var i=1;
      $('#add').click(function(){
           i++;
           $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" maxlength="100" name="name[]" placeholder="Enter Description" class="form-control" /></td><td><input type="text" name="unitTitle[]" maxlength="100"  placeholder="Unit Title" class="form-control" /></td><td><input type="text" name="unitValue[]" pattern="[0-9]{10}" placeholder="Unit Value" onkeypress="return isNumberKey(event)" class="form-control" /></td><td> <input name="file'+i+'" type="file"  id="file"/></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
      });
      $(document).on('click', '.btn_remove', function(){
           var button_id = $(this).attr("id");
           $('#row'+button_id+'').remove();
      });

 });
 </script>
 <SCRIPT language=Javascript>
       <!--maxlength="300"
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode > 31 && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
         </SCRIPT>
