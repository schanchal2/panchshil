<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/leadershipContentModel.php';

$leadershipId = urldecode($_GET["leadershipId"]);
//echo $leadershipId;
$leadId = urldecode($_GET["ledership_id"]);
//echo $leadId;
$method= urldecode($_GET["method"]);
//echo $method;
$pagename= urldecode($_GET["pagename"]);


$uploadDir = "../uploads/leadershipImages/";
$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    if($method!="insert"){
    $leadershipInfo = getLeadershipDetails($leadId,NULL,$conn);
    if(noError($leadershipInfo)){
        $leadershipInfo = $leadershipInfo["response"];
    }else{
        $returnArr = $leadershipInfo;
    }
}
}else{
    $returnArr = $conn;
    exit;
}
// printArr($leadershipInfo);
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<?php if($method=="insert"){ ?>
	<h4 class="modal-title">Add New Leadership</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Leadership</h4>
		<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="leadership-form" action="" method = "post" enctype = "multipart/form-data">
    <input type="hidden" id = "Id" class="" name="Id" value='<?php echo $leadershipInfo[0]["id"]; ?>'>
		<input type="hidden" id = "pagename" class="" name="pagename" value='<?php echo $pagename ?>'>
		<input type="hidden" id = "ImageUrl" class="" name="ImageUrl" value="<?php echo $leadershipInfo[0]['image']; ?>">
    <input type="hidden" id = "abt_id" class="" name="abt_id" value="<?php echo $leadId; ?>">
    <input type="hidden" id = "abt_id1" class="" name="abt_id1" value="<?php echo $leadershipInfo[0]['about_id']; ?>">
		<input type="hidden" id = "method" class="" name="method" value="update">
		<div class="form-group">
			<label for="feature-image"> Image : </label>
			<input type="file" id = "leadershipFeatureImage" class="form-control" name="leadershipFeatureImage" onchange="growthPreview('#leadershipFeatureImage', '#growthColorImage',null,'<?php echo $uploadDir; ?>');">
			<div id="growthColorImage" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$leadershipInfo[0]["image"]; ?>'); <?php if (empty($leadershipInfo[0]['image']) || !isset($leadershipInfo[0]['image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label>name:</label>
			<input type="text"  class="form-control" id="name" name="name" value="<?php echo $leadershipInfo[0]["name"]; ?>"  maxlength="100">
		</div>
    <div class="form-group">
			<label>Designation:</label>
			<input type="text"  class="form-control" id="designation" name="designation" value="<?php echo $leadershipInfo[0]["designation"]; ?>"  maxlength="200">
		</div>
		<div class="form-group">
			<label>Description:</label>
			<textarea class="form-control" id="description" name="description" maxlength="10000" rows = "4"><?php echo $leadershipInfo[0]["description"]; ?></textarea>
		</div>
    <div class="form-group">
			<label>Facebook:</label>
			<input type="text"  class="form-control" id="facebook" maxlength="200" name="facebook" maxlength="100" value="<?php echo $leadershipInfo[0]["facebook"]; ?>" >
		</div>
    <div class="form-group">
			<label>Twitter:</label>
			<input type="text"  class="form-control" id="twitter" maxlength="200" name="twitter" value="<?php echo $leadershipInfo[0]["twitter"]; ?>" >
		</div>
    <div class="form-group">
			<label>Linked In:</label>
			<input type="text"  class="form-control" id="linkedin" maxlength="200" name="linkedin" value="<?php echo $leadershipInfo[0]["linkedin"]; ?>" >
		</div>
	</form>
	<div class="modal-footer">

		<button type="button" class="btn btn-success" onclick="addUpdateLeadershipDetails()">Submit</button>
		<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
	</div>
</div>
