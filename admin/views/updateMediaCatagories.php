<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Add Media Subcatagories Content</h4>	
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="pressForm" action="" method = "post" enctype = "multipart/form-data">
		<input type="hidden" name="Id" value="">
		<input type="hidden" name="imageUrl" value="">
		<input type="hidden" name="pdfUrl" value="">
		<input type="hidden" name="VideoUrl" value="">
		<input type="hidden" name="LinkUrl" value="">
		<input type="hidden" name="method" value="update">

		<div class="form-group">
			<label for="is-feature"> Is Featured : </label><br>
			<input class="press-radio" type="radio" name="featured-press" value="No" /  checked > <span style="margin:0 15px 0 3px; color:red;">NO</span>
			 <input class="press-radio" type="radio" name="featured-press" value="yes"/> <span style="margin:0 15px 0 3px; color:green;">Yes</span>  			
		</div>
		<div class="form-group">
			<label for="image-logo"> Image  : </label>
			<input type="file" id = "img-logo" class="form-control" name="img-logo" onchange="">			
		</div>
		<div id="press-img-preview" class="press-image featureImages" style=""></div>
		<div class="form-group">
			<label for="image-title">Title:</label>
			<input type="text" class="form-control" id="press-title" name="press-title" value="" >
		</div>
		<div class="form-group">
			<label for="description">Description:</label>
			<textarea class="form-control" id="press-description" name="press-description" rows = '4' value=""></textarea>
		</div>
		<div class="form-group">
			<label for="source-name">Source name:</label>
			<input type="text" class="form-control" id="source-name" name="source-name" value="">
		</div>
		<div class="form-group">
			<label for="date">Date:</label>
			<input type="date" class="form-control" id="press-date" name="press-date" value="">
		</div>
		<div class="form-group">
			<label for="pdf-url">Add Pdf</label>
			<input type="file" class="form-control" id="pdf-url" name="pdf-url" value="">
		</div>
		<div class="form-group">
			<label for="video-url">Video Url:</label>
			<input type="text" class="form-control" id="video-url" name="video-url" value="">
		</div>
		<div class="form-group">
			<label for="share-link-url">Link Url:</label>
			<input type="text" class="form-control" id="link-url" name="link-url" value="">
		</div>		
	</form>	
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdatePressDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>	
</div>