<?php

//start session
session_start();

require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/companyProfileModel.php';

$returnArr = array();
$companyUrlId       = cleanXSS(urldecode($_GET['id']));
$pageName           = cleanXSS(urldecode($_GET['pageName']));
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active redirect to the login page

    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getCompanyProfile = getCompanyProfile($companyUrlId,$conn);
    if(noError($getCompanyProfile)){
        $getCompanyProfile = $getCompanyProfile["response"];
    }else{
        $returnArr = $getCompanyProfile;
    }
}else{
    $returnArr = $conn;
    exit;
}

?>

<div class="col-lg-12 main-container-1">
    <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
            <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;"> Manage <?php echo ucfirst(strtolower($pageName)); ?> Details</p>
        </div>
        <div class="modal-err" style="color: red"></div>
        <div class="col-lg-12 redirect">

            <form id="companyform" enctype = "multipart/form-data">
                <input type="hidden" name="id" id="id" value="<?php echo $getCompanyProfile[0]['id']; ?>">
                <input type="hidden" name="image" value="<?php echo $getCompanyProfile[0]['image']; ?>">
                <input type="hidden" name="company_profile_image_url" value="<?php echo $getCompanyProfile[0]['company_profile_image']; ?>">
                <input type="hidden" name="method" value="update">
                <input type="hidden" name="compProfileId" id = "compProfileId" value="<?php echo $companyUrlId; ?>">


                <div class="form-group">
                    <label for="feature-image">Image : </label>
                    <input type="file" id = "company_image" class="form-control" name="company_image" onchange="companyProfilePreview('#company_image', '#image-preview',null,'<?php echo $uploadDir; ?>');">
                    <div id="image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getCompanyProfile[0]["image"]; ?>'); <?php if (empty($getCompanyProfile[0]['image']) || !isset($getCompanyProfile[0]['image'])) { echo 'display: none'; }?>;"></div>
                </div>

                <div class="form-group">
                    <label for="feature-image">Bottom Image : </label>
                    <input type="file" id = "company_profile_image" class="form-control" name="company_profile_image" onchange="companyProfilePreview('#company_profile_image', '#sign-image-preview',null,'<?php echo $uploadDir; ?>');">
                    <div id="sign-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getCompanyProfile[0]["company_profile_image"]; ?>'); <?php if (empty($getCompanyProfile[0]['company_profile_image']) || !isset($getCompanyProfile[0]['company_profile_image'])) { echo 'display: none'; }?>;"></div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="top-desc">Name:</label>
                    <input type="text" class="form-control" id="name" name="name" maxlength="100" value = '<?php echo $getCompanyProfile[0]["name"]; ?>'>
                </div>
                <div class="form-group">
                    <label for="top-desc-title">Description:</label>
                    <textarea class="form-control" rows="5" id="long_url" name="long_url" maxlength="10000"><?php echo $getCompanyProfile[0]["long_url"]; ?></textarea>
                </div>
                <div class="form-group">
                    <label for="know-more">Footer Title :</label>
                    <input type="text" class="form-control" id="designation" name="designation" value="<?php echo $getCompanyProfile[0]["designation"]; ?>" maxlength="100">
                </div>
            </form>
            <div class="col-lg-6"></div>
            <div class="col-lg-3 blue-button-1" onclick="addUpdateCompanyProfile()">Submit</div>
            <div class="col-lg-3 blue-button-1" id="clear" onclick ="confirmRemoveData(<?php echo $getCompanyProfile[0]['id']; ?>);">Clear</div>
        </div>
    </div>
</div>
<script>


    /* TO Display Remodal on click of Add or Update about details */

    var pageName    = '<?php echo $pageName; ?>';
    /* TO add and update Clients details */
    function addUpdateCompanyProfile(id) {
        $('.blue-button-1').prop('disable',true);

        var UrlForrward = $('#compProfileId').val();
        var long_url    = $('#long_url').val();
        var name        = $('#name').val();
        var designation = $('#designation').val();
        if (long_url == '' || name == '' || designation == '') {
            $('.blue-button-1').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#companyform')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/companyProfileController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Company Profile !!!');
                        $("#load-content").load("manageCompanyProfile.php?id="+UrlForrward+"&pageName="+encodeURIComponent(pageName));
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.blue-button-1').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Company Profile!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    //to remove campus details by campus id
    function removeCompanyProfile(id){
        var UrlForrward = $('#compProfileId').val();
        $.ajax({
            type: "POST",
            url: '../controller/companyProfileController.php',
            data: {
                id:id,
                method: "delete"
            },
            dataType: 'json',
            success: function (data) {
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text('Successfully deleted Company Profile!!!');
                    setTimeout(function() { $(".alert-success").hide(); }, 5000);
                }else{
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Company Profile");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageCompanyProfile.php?id="+UrlForrward+"&pageName="+encodeURIComponent(pageName));
            },
            error: function (data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error in Company Profile!!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);
            }
        });
    }

    /* To show preview of uploaded Image */
    function companyProfilePreview(id,pre_id,nextSlider,uploadDir) {

        var files =  $(id)[0].files;
        var img = new Image();

        /* Checking Is any File selected */
        if (!files.length || !window.FileReader) return;

        /* Check Uploadede file is Image Only */
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();
            /* set image data as background of div */
            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#my_logo"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_logo').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_logo').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        //alert(id);
        $(".modal-content").load("confirmRemoveData.php?id="+id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeCompanyProfile(id);
        $('#myModal .close').click();
    }
</script>