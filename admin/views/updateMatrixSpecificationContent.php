<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/matrixSpecificationContentModel.php';

$id = urldecode($_GET["id"]);
$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getMatrixSpecificationDetailsByUniqId = getMatrixSpecificationDetailsByUniqId($id,$conn);
    if(noError($getMatrixSpecificationDetailsByUniqId)){
        $getMatrixSpecificationDetailsByUniqId = $getMatrixSpecificationDetailsByUniqId["response"];
    }else{
        $returnArr = $getMatrixSpecificationDetailsByUniqId;
    }
}else{
    $returnArr = $conn;
    exit;
}
//printArr($getMatrixSpecificationDetailsByUniqId);
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Update Matrix Specification</h4>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
  	<div class="modal-err1" style="color:red;"></div>
	<form id="galleryForm" enctype = "multipart/form-data">
      <input type="hidden" name="id" id="id" value="<?php echo $getMatrixSpecificationDetailsByUniqId[0]['id']; ?>">
  <input type="hidden" name="o_id" id="o_id" value="<?php echo $getMatrixSpecificationDetailsByUniqId[0]['o_id'] ?>"; >
  <input type="hidden" name="p_id" id="p_id" value="<?php echo $getMatrixSpecificationDetailsByUniqId[0]['p_id']; ?>">
  <input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getMatrixSpecificationDetailsByUniqId[0]['image']; ?>">
	<input type="hidden" name="method" value="update">
		<div class="form-group">
			<label for="feature-image">Image: </label>
			<input type="file" id = "gallery-feature-image" class="form-control" name="gallery-feature-image" onchange="bannerPreview('#gallery-feature-image', '#gallery-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="gallery-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getMatrixSpecificationDetailsByUniqId[0]["image"]; ?>'); <?php if (empty($getMatrixSpecificationDetailsByUniqId[0]['image']) || !isset($getMatrixSpecificationDetailsByUniqId[0]['image'])) { echo 'display: none'; }?>;"></div>
		</div>
    <div class="form-group">
			<label for="corporate-services">Title :</label>
      <input type="text"  class="form-control" id="title" name="title" value="<?php echo $getMatrixSpecificationDetailsByUniqId[0]["title"]; ?>" >
		</div>
    <div class="form-group">
      <label for="corporate-services">Unit Title :</label>
      <input type="text"  class="form-control" id="uTitle" name="uTitle" value="<?php echo $getMatrixSpecificationDetailsByUniqId[0]["unitTitle"]; ?>" >
    </div>
    <div class="form-group">
      <label for="corporate-services">Unit Value :</label>
      <input type="text"  class="form-control" id="uValue" name="uValue" value="<?php echo $getMatrixSpecificationDetailsByUniqId[0]["unitValue"]; ?>" >
    </div>

	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="UpdateMatrixSpecificationDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
