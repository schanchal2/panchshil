<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/mediaContentModel.php';

$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
// printArr($conn);
if(noError($conn)){
    $conn = $conn["conn"];
    $getMediaDetails = getMediaDetails(NULL,NULL,$conn);
    if(noError($getMediaDetails)){
        $getMediaDetails = $getMediaDetails["response"];

    }else{
        $returnArr = $getMediaDetails;
    }

}else{
    $returnArr = $conn;
    exit;
}

// printArr($getMediaDetails);

?>
<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
      	 <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage Media</div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="updateViewMediaDialog();">Add Media</div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px; ">
                        <th width="30%" style="text-align:center;">Image</th>
                          <th width="30%" style="text-align:center;">Feature</th>
                        <th width="25%" style="text-align:center;">Name</th>
                        <th width="25%" style="text-align:center;">Description</th>
                        <th width="10%" style="text-align:center;">Status</th>
                        <th width="5%" style="text-align:center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (empty($getMediaDetails)) {
                                echo "<tr><td colspan = '6' style = 'text-align:center;'>No Data Found</td></tr>";
                            }
                            for ($i = 0; $i < count($getMediaDetails); $i++) {
                        ?>
                        <tr>


                          <td>
                              <div id="image" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getMediaDetails[$i]["feature_image"]; ?>');"></div>
                          </td>
                            <td>
                                <div id="image" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getMediaDetails[$i]["box_image"]; ?>');"></div>
                            </td>
                            <td>
                                 <p class="businessDisplay" id="Name" name="Name"><?php echo $getMediaDetails[$i]['name']; ?></p>
                            </td>
                            <td>
                            <div style="height: 130px;width: 100px;">
                            <p class="pressDisplay" id="" name="" style="max-width: 170px; max-height: 128px;"><?php echo $getMediaDetails[$i]['long_desc']; ?></p>
                            <div style="height: 130px;">
                            </div>
                        </td>
                            <td>
                                 <span style="font-size: 16px;font-weight: bold; <?php if($getMediaDetails[$i]['status'] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?> "><?php echo strtoupper($getMediaDetails[$i]['status']); ?></span>
                            </td>
                            <td>
                              <a href="#" onclick="updateViewMediaDialog(<?php echo $getMediaDetails[$i]['id']; ?>);">Edit</a><br>
                              <a href="#" onclick="confirmRemoveData(<?php echo $getMediaDetails[$i]['id']; ?>);">
                              <span style="font-size: 16px;font-weight: bold;  ">
                              <?php if($getMediaDetails[$i]["status"] == "Active"){ echo "Inactivate/Delete";}else{echo "Activate";} ?>
                              </span>
                              </a><br>
                              <a href="#" onclick='openViewMediaDialog("<?php echo $getMediaDetails[$i]['media_type']; ?>",<?php echo $getMediaDetails[$i]['id'];?>);'>Manage</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</div>
<script>
   function updateViewMediaDialog(id){
        $( ".modal-content" ).load("updateMediaContent.php?id="+id);
        $("#myModal").modal();
    }

    /* To show preview of uploaded Image */
    function aboutPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        if (!files.length || !window.FileReader) return;
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();

            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdatMediaDetails() {
        $('.btn-success').prop('disable',true);
      // alert(1);
      var page_type=$('#page_type').is(":checked");
    //  alert(page_type);
    //  return;
        var name                 = $('#featured-name').val();
        var shortDesc            = $('#featured-shortDesc').val();
        var longDesc             = $('#featured-longDesc').val();
        if (name == '' || shortDesc == '' || longDesc == '' &&  page_type=='') {
        //  alert(1);
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#aboutForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/mediaController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                  //  alert(data);
                  //  console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated about Details !!!');
                        $("#load-content").load("manageMediaContent.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating About Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    /* To remove about Details */
    function removeAbout(id) {
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/mediaController.php",
            data:{
                id:id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted About Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete About Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageMediaContent.php");
            },
            error: function(data) {

                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting About Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);

            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmChangesRemodal.php?id="+id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeAbout(id);
        $('#myModal .close').click();
    }

    function openViewMediaDialog(pagename,id){
        $("#load-content").load(pagename+'?&id='+id);
    }

</script>
