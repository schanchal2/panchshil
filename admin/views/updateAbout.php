<?php
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/aboutContentModel.php';

$id = urldecode($_GET["id"]);
$uploadDir = "../uploads/aboutImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getAboutDetails = getAboutDetails($id,NULL,$conn);

    if(noError($getAboutDetails)){
        $getAboutDetails = $getAboutDetails["response"];

    }else{
        $returnArr = $getAboutDetails;
    }

}else{
    $returnArr = $conn;
    exit;
}
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Add New About Page </h4>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>

	<form id="aboutForm" enctype = "multipart/form-data">
		<input type="hidden" name="id" id="id" value="<?php echo $getAboutDetails[0]['id']; ?>">
		<input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getAboutDetails[0]['image']; ?>">
		<input type="hidden" name="boximageUrl" id="boximageUrl" value="<?php echo $getAboutDetails[0]['boxImage']; ?>">
		<input type="hidden" name="aboutStatus" id="aboutStatus" value="<?php echo $getAboutDetails[0]['status']; ?>">
		<input type="hidden" name="method" value="update">
		<div class="form-group">
			<label for="feature-image">Feature Image : </label>
			<input type="file" id = "about-feature-image" class="form-control" name="about-feature-image" onchange="aboutPreview('#about-feature-image', '#about-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="about-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getAboutDetails[0]["image"]; ?>'); <?php if (empty($getAboutDetails[0]['image']) || !isset($getAboutDetails[0]['image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="box-image">Box Image : </label>
			<input type="file" id = "about-box-image" class="form-control" name="about-box-image" onchange="aboutPreview('#about-box-image', '#about-box-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="about-box-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getAboutDetails[0]["boxImage"]; ?>'); <?php if (empty($getAboutDetails[0]['boxImage']) || !isset($getAboutDetails[0]['boxImage'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="about-name">Name</label>
			<input type="text" class="form-control" id="about-name" maxlength="100" name="about-name" value="<?php echo $getAboutDetails[0]['name']; ?>" >
		</div>
		<?php if (empty($getAboutDetails[0]['id']) || !isset($getAboutDetails[0]['id'])) { ?>
    	<div class="form-group">
			<label for="about-name">Please Select</label>
			<select class="form-control droper" name="page_type" id="page_type" style="">
				<option value = "NULL">Please Select</option>
				<option value = "manageCorporateContent.php">Co-operate Profile</option>
				<option value = "manageSustainContent.php">Sustainablity</option>
				<option value = "manageClientsDetails.php">Client</option>
				<option value = "manageLeadership.php">leadership</option>
        	</select>
		</div>
        <?php }?>



		<div class="form-group">
			<label for="about-services">Short Description</label>
			<textarea class="form-control" rows="3" id="about-shortDesc" maxlength="1000" name="about-shortDesc" ><?php echo $getAboutDetails[0]['shortDesc']; ?></textarea>
		</div>
		<div class="form-group">
			<label for="about-services">long Description</label>
			<textarea class="form-control" rows="3" id="about-longDesc" maxlength="10000" name="about-longDesc" value=""><?php echo $getAboutDetails[0]['longDesc']; ?></textarea>

		</div>
	</form>

</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateAboutDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
