<?php
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/aboutContentModel.php';

$id = urldecode($_GET["id"]);
$uploadDir = "../uploads/forwordImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    //$featureType = "home";
    $getforwordDetails = getforwordDetails($id,NULL,$conn);
      //printArr($getforwordDetails);
    if(noError($getforwordDetails)){

            $getforwordDetails = $getforwordDetails["response"];
    }else{
            $returnArr = $getforwordDetails;
    }
}else{
            $returnArr = $conn;
            exit;
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<?php if ($id=="undefined") {?>
	<h4 class="modal-title">Add New Forword</h4>
	<?php } else{?>
	   <h4 class="modal-title">Update Forword </h4>
	<?php }
	?>
</div>

<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="forwordform" enctype = "multipart/form-data">
		<input type="hidden" name="id" id="id" value="<?php echo $getforwordDetails[0]['id']; ?>">
		<input type="hidden" name="forword_image" value="<?php echo $getforwordDetails[0]['forword_image']; ?>">
    <input type="hidden" name="sign_image" value="<?php echo $getforwordDetails[0]['sign_image']; ?>">
		<input type="hidden" name="method" value="update">
		<div class="form-group">
			<label for="feature-image">Image : </label>
			<input type="file" id = "forword_image" class="form-control" name="forword_image" onchange="forwordPreview('#forword_image', '#forword-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="forword-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getforwordDetails[0]["image"]; ?>'); <?php if (empty($getforwordDetails[0]['image']) || !isset($getforwordDetails[0]['image'])) { echo 'display: none'; }?>;"></div>
		</div>
    <div class="form-group">
			<label for="feature-image">Signature Image : </label>
			<input type="file" id = "sign_image" class="form-control" name="sign_image" onchange="forwordPreview('#sign_image', '#sign-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="sign-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getforwordDetails[0]["sign_image"]; ?>'); <?php if (empty($getforwordDetails[0]['sign_image']) || !isset($getforwordDetails[0]['sign_image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="about-name">Description</label>
			<input type="text" class="form-control" id="long_url" name="long_url" value="<?php echo $getforwordDetails[0]['long_url']; ?>" >
		</div>

		<div class="form-group">
			<label for="about-services">Name</label>
		<input type="text" class="form-control" id="name" name="name" value="<?php echo $getforwordDetails[0]['name']; ?>" >
		</div>
		<div class="form-group">
			<label for="about-services">Designation</label>
      <input type="text" class="form-control" id="designation" name="designation" value="<?php echo $getforwordDetails[0]['designation']; ?>" >


				</div>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateForwordDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
