
<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/propertyContentModel.php';

$id = urldecode($_GET["id"]);
$property_id = urldecode($_GET["property_id"]);
$method= urldecode($_GET["method"]);

$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    if($method!="insert"){
    $getPropertyDetails = getPropertyDetails($property_id,NULL,$conn);
    if(noError($getPropertyDetails)){
        $getPropertyDetails = $getPropertyDetails["response"];
    }else{
        $returnArr = $getPropertyDetails;
    }
  }

}else{
    $returnArr = $conn;
    exit;
}
//printArr($getPropertyDetails);
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if($method=="insert"){ ?>
	<h4 class="modal-title">Add New Luxury Residences Details</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Luxury Residences</h4>
		<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
  	<div class="modal-err1" style="color:red;"></div>
	<form id="LuxuryResidencesForm" enctype = "multipart/form-data">
      <input type="hidden" name="press_status" id="press_status" value="<?php echo $getPropertyDetails[0]['status']; ?>">
  <input type="hidden" name="id" id="id" value="<?php echo $getPropertyDetails[0]['id']; ?>">
  <input type="hidden" name="p_id" id="p_id" value="<?php echo $property_id; ?>">
  <input type="hidden" name="p_id1" id="p_id1" value="<?php echo $getPropertyDetails[0]['category_id']; ?>">
  <input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getPropertyDetails[0]['image']; ?>">
  <input type="hidden" name="$imageUrl1" id="$imageUrl1" value="<?php echo $getPropertyDetails[0]['enquiry_image']; ?>">
    <input type="hidden" name="pdfUrl2" id="pdfUrl2" value="<?php echo $getPropertyDetails[0]['pdf']; ?>">
      <input type="hidden" name="isFeatured" id="isFeatured" value="<?php echo $getPropertyDetails[0]['isFeatured']; ?>">
	<input type="hidden" name="method" value="update">

  <div class="form-group">
    <label for="feature-image">Box Image : </label>
    <input type="file" id = "LuxuryResidences-feature-image" class="form-control" name="LuxuryResidences-feature-image" onchange="luxuryPreview('#LuxuryResidences-feature-image', '#LuxuryResidences-image-preview',null,'<?php echo $uploadDir; ?>');">
    <div id="LuxuryResidences-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getPropertyDetails[0]["image"]; ?>'); <?php if (empty($getPropertyDetails[0]['image']) || !isset($getPropertyDetails[0]['image'])) { echo 'display: none'; }?>;"></div>
  </div>

  <div class="form-group">
    <label for="feature-image">Box Image : </label>
    <input type="file" id = "LuxuryResidences-feature-image1" class="form-control" name="LuxuryResidences-feature-image1" onchange="luxuryPreview('#LuxuryResidences-feature-image1', '#LuxuryResidences-image-preview1',null,'<?php echo $uploadDir; ?>');">
    <div id="LuxuryResidences-image-preview1" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getPropertyDetails[0]["enquiry_image"]; ?>'); <?php if (empty($getPropertyDetails[0]['enquiry_image']) || !isset($getPropertyDetails[0]['enquiry_image'])) { echo 'display: none'; }?>;"></div>
  </div>


		<div class="form-group">
			<label for="about-name">Name : </label>
			<input type="text" class="form-control" id="name" name="name" value="<?php echo $getPropertyDetails[0]['name']; ?>" maxlength="100" >
		</div>
		<div class="form-group">
			<label for="about-services">Type : </label>
		<input type="text" class="form-control" id="type" name="type" value="<?php echo $getPropertyDetails[0]['type']; ?>" maxlength="100" >
		</div>
    <div class="form-group">
			<label for="about-services">	Property Status : </label>
		<input type="text" class="form-control" id="status" name="status" value="<?php echo $getPropertyDetails[0]['status']; ?>"  maxlength="100">
		</div>
    <div class="form-group">
			<label for="about-services">Property Type : </label>
		<input type="text" class="form-control" id="property_type" name="property_type" value="<?php echo $getPropertyDetails[0]['property_type']; ?>" maxlength="100" >
		</div>
		<div class="form-group">
			<label for="about-services"> Headline : </label>
		<input type="text" class="form-control" id="title" name="title" value="<?php echo $getPropertyDetails[0]['title']; ?>" maxlength="100" >
		</div>
    <div class="form-group">
      <label for="about-services"> Short Description : </label>
      <textarea class="form-control" rows="3" id="short_desc" name="short_desc" maxlength="1000" value=""><?php echo $getPropertyDetails[0]['short_desc']; ?></textarea>
    </div>
    <div class="form-group">
			<label for="about-services"> Long Description : </label>
			<textarea class="form-control" rows="3" id="long_desc" name="long_desc" maxlength="10000" value=""><?php echo $getPropertyDetails[0]['long_desc']; ?></textarea>
		</div>

    <div class="form-group"  id="video_url_id">
      <label for="video-url">Website Url:</label>
      <input type="text" class="form-control" id="website_url" name="website_url" maxlength="300" value="<?php echo $getPropertyDetails[0]['website_url']; ?>">
    </div>
    <div class="form-group" id="pdf_url_id">
      <label for="pdf-url">Add Pdf : </label>
      <input type="file" class="form-control" id="pdf-url" name="pdf-url" value="<?php echo $getPressDetails[0]['pdf']; ?>">
    </div>
    <div class="form-group">
      <label for="about-services"> Latitude : </label>
    <input type="text" class="form-control" id="latitude" name="latitude" value="<?php echo $getPropertyDetails[0]['latitude']; ?>" maxlength="100" >
    </div>
    <div class="form-group">
      <label for="about-services"> Longitude : </label>
    <input type="text" class="form-control" id="longitude" name="longitude" value="<?php echo $getPropertyDetails[0]['longitude']; ?>" maxlength="100" >
    </div>

	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateLuxuryResidencesDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
