<?php
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/aboutContentModel.php';

$sustainid = $_GET['sustainid'];
//echo $sustainid;

$name = $_GET['name'];
//echo $name;

$method = $_GET['method'];

$uploadDir = "../uploads/sustainImages/";
$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    if($method!="insert")
    {
    $getSustainDetails = getSustainDetails($sustainid,NULL,$conn); }
   // printArr($getSustainDetails);

    if(noError($getSustainDetails)){
        $getSustainDetails = $getSustainDetails["response"];

    }else{
        $returnArr = $getSustainDetails;
    }

}else{
    $returnArr = $conn;
    exit;
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<?php if ($method=="insert") {?>
	<h4 class="modal-title">Add New sustain Page </h4>
	<?php } else{?>
	   <h4 class="modal-title">Update sustain Page  </h4>
	<?php }
	?>
</div>

<div class="modal-body">

	<div class="modal-err" style="color:red;"></div>
	<form id="sustainFrom" enctype = "multipart/form-data">
		<input type="hidden" name="id" id="id" value="<?php echo $getSustainDetails[0]['id']; ?>">
		<input type="hidden" name="sustainidUrl" id="sustainidUrl" value='<?php echo $sustainid; ?>'>
		<input type="hidden" name="nameUrl" id="nameUrl" value='<?php echo $name; ?>'>
		<input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getSustainDetails[0]['image']; ?>">
		<input type="hidden" name="sustainStatus" id="sustainStatus" value="<?php echo $getSustainDetails[0]['status']; ?>">
		<input type="hidden" name="method" value="update">
		<div class="form-group">
			<label for="feature-image">Image : </label>
			<input type="file" id = "sustain-feature-image" class="form-control" name="sustain-feature-image" onchange="sustainPreview('#sustain-feature-image', '#sustain-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="sustain-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getSustainDetails[0]["image"]; ?>'); <?php if (empty($getSustainDetails[0]['image']) || !isset($getSustainDetails[0]['image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="sustain-name">Name</label>
			<input type="text" class="form-control" id="sustain-name" maxlength="100" name="sustain-name" value="<?php echo $getSustainDetails[0]['name']; ?>" >
		</div>
		<div class="form-group">
			<label for="sustain-services">Short Description</label>
			<textarea class="form-control" rows="3" id="sustain-shortDesc" maxlength="1000" name="sustain-shortDesc" ><?php echo $getSustainDetails[0]['shortDesc']; ?></textarea>
		</div>
		<div class="form-group">
			<label for="sustain-services">long Description</label>
			<textarea class="form-control" rows="3" id="sustain-longDesc" maxlength="10000" name="sustain-longDesc" value=""><?php echo $getSustainDetails[0]['longDesc']; ?></textarea>
		</div>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick='addUpdateSustainDetails();'>Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
