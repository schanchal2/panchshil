<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/businessDetailsModel.php';

$businessId = urldecode($_GET["businessId"]);
$uploadDir = "../uploads/businessImages/";
$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $businessInfo = getBusinessDetails($businessId,NULL,$conn);
    if(noError($businessInfo)){
        $businessInfo = $businessInfo["response"];
    }else{
        $returnArr = $businessInfo;
    }

}else{
    $returnArr = $conn;
    exit;
}
// printArr($businessInfo);
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<?php if ($businessId=="undefined") {?>
	<h4 class="modal-title">Add New Business </h4>
	<?php } else{?>
	   <h4 class="modal-title">Update Business </h4>
	<?php }
	?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="businessFrom" enctype = "multipart/form-data">
        <input type="hidden" name="Id" id="Id" value="<?php echo $businessInfo[0]['id']; ?>">
        <input type="hidden" name="business-status" id="business-status" value="<?php echo $businessInfo[0]['business_status']; ?>">
        <input type="hidden" name="business-big-image-url" id="business-big-image-url" value="<?php echo $businessInfo[0]["business_big_image"]; ?>">
        <input type="hidden" name="business-feature-image-url" id="business-feature-image-url" value="<?php echo $businessInfo[0]["business_feature_image"]; ?>">
		<input type="hidden" name="method" value="update">
		<label for="business-status">Status : </label>	<span style="font-size: 16px;font-weight: bold; <?php if($businessInfo[0]["business_status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?> "><?php echo $businessInfo[0]["business_status"]; ?></span>

		<div class="form-group">
			<label for="feature-image">Big Image : </label>
			<input type="file" id = "business-big-image" class="form-control" name="business-big-image" onchange="businessPreview('#business-big-image', '#big-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="big-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$businessInfo[0]["business_big_image"]; ?>'); <?php if (empty($businessInfo[0]['business_big_image']) || !isset($businessInfo[0]['business_big_image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="feature-image">Feature Image : </label>
			<input type="file" id = "business-feature-image" class="form-control" name="business-feature-image" onchange="businessPreview('#business-feature-image', '#feature-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="feature-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$businessInfo[0]["business_feature_image"]; ?>'); <?php if (empty($businessInfo[0]['business_feature_image']) || !isset($businessInfo[0]['business_feature_image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="business-name">Name</label>
			<input type="text" class="form-control" id="business-name"  maxlength="100" name="business-name" value="<?php echo $businessInfo[0]['business_name']; ?>" >
		</div>

		<?php if (empty($getAboutDetails[0]['id']) || !isset($getAboutDetails[0]['id'])) { ?>
    	<div class="form-group">
			<label for="about-name">Please Select</label>
			<select class="form-control droper" name="page_type" id="page_type" style="">
				<option value = "NULL">Please Select</option>
				<option value = "manageOffering.php">Offerings</option>
				<option value = "manageServices.php">Services</option>
        	</select>
		</div>
        <?php }?>

		<div class="form-group">
			<label for="business-services">Short Description</label>
			<textarea class="form-control" rows="3" id="business-short-desc"  maxlength="1000" name="business-short-desc"><?php echo $businessInfo[0]["business_short_desc"]; ?></textarea>
		</div>
		<div class="form-group">
			<label for="business-services">Full Description</label>
			<textarea class="form-control" rows="3" id="business-full-desc"  maxlength="10000" name="business-full-desc"><?php echo $businessInfo[0]["business_full_desc"]; ?></textarea>
		</div>


	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateBusinessDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
