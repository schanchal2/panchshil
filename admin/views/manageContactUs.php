<?php
//prepare for request
//start session
session_start();


//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/contactUsFormModel.php';

$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $contactUsContent = getContactUSDetails(NULL,$conn);
    if(noError($contactUsContent)){   
        $contactUsContent = $contactUsContent["response"];
    }else{
        $returnArr = $contactUsContent;
    }
}else{
    $returnArr = $conn;
    exit;
}

?>


<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
      	 <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage Contact US</p>
        </div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;"> 
                        <th  width="45%">Name</th>
                        <th  width="45%">Reason</th>
                        <th width="10%">Action</th>
                    </tr>
                    </thead>
                    <tbody>  
                            <?php 
                                if (empty($contactUsContent)) {
                                    echo "<tr><td colspan = '5' style = 'text-align:center;'>No Data Found</td></tr>";
                                }

                                for($i=0; $i < count($contactUsContent); $i++) { 
                            ?>          
                        <tr>                                                                          
                             <td>
                                    <div id="" class="col-lg-3" style=""></div>
                                    <div style = "width: 100px;">
                                    <p class = "contUsDisplay" id="name" name="name" ><?php echo $contactUsContent[$i]["firstName"]." ".$contactUsContent[$i]["lastName"];?></p>
                                    <div style = "">
                            </td>
                            <td>
                                    <div id="" class="col-lg-3" style=""></div>
                                    <div style = "width: 100px;">
                                    <p class = "contUsDisplay" id="reason" name="reason" readonly><?php echo $contactUsContent[$i]["reason"];?></p>                             
                                
                            </td>
                            <td>
                                <a href="#" onclick="openViewDialog(<?php echo $contactUsContent[$i]["id"]; ?>);">View</a><br>
                                <a href="#" onclick="confirmRemoveData(<?php echo $contactUsContent[$i]["id"]; ?>);">Delete</a>
                            </td>  
                              
                        </tr>
                      <?php } ?> 
                    </tbody>
                </table>
            </div>
        </div>  
	</div>
</div>
<script>
  /* TO Display Remodal on click of view Clients details */
   function openViewDialog(id){
        $(".modal-content").load("viewContactDetails.php?contactId="+id);
        $("#myModal").modal();
    }

    //to remove campus details by campus id
    function removeContact(id){
        var id = id;
            $.ajax({
                type: "POST",
                url: '../controller/contactUsFormController.php',
                data: {
                    id:id,
                    method: "delete"
                },
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if (data.errCode == -1) {
                        $(".alert-success").show();
                        $("#success-msg").text('Successfully deleted matrics details!!!');
                        setTimeout(function() { $(".alert-success").hide(); }, 5000);
                    }else{
                        $(".alert-danger").show();
                        $("#error-msg").text("Failed to Delete matrics Content");
                        setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                    }          
                     $("#load-content").load("manageContactUs.php?id="+encodeURIComponent(id));
                },
                error: function (data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in deleting matrics details!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        
    }



 /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmRemoveData.php?id="+id);
        $("#myModal").modal(); 
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeContact(id);
        $('#myModal .close').click();
    }

    
</script>
