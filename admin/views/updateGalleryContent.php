<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/galleryContentModel.php';

$id = urldecode($_GET["id"]);
$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getGalleryDetailsByUniqId = getGalleryDetailsByUniqId($id,$conn);
    if(noError($getGalleryDetailsByUniqId)){
        $getGalleryDetailsByUniqId = $getGalleryDetailsByUniqId["response"];
    }else{
        $returnArr = $getGalleryDetailsByUniqId;
    }
}else{
    $returnArr = $conn;
    exit;
}
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if($method=="insert"){ ?>
	<h4 class="modal-title">Add New Gallery</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Gallery</h4>
		<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
  	<div class="modal-err1" style="color:red;"></div>
	<form id="galleryForm" enctype = "multipart/form-data">
      <input type="hidden" name="id" id="id" value="<?php echo $getGalleryDetailsByUniqId[0]['id']; ?>">
  <input type="hidden" name="o_id" id="o_id" value="<?php echo $getGalleryDetailsByUniqId[0]['o_id'] ?>"; >
  <input type="hidden" name="p_id" id="p_id" value="<?php echo $getGalleryDetailsByUniqId[0]['p_id']; ?>">
  <input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getGalleryDetailsByUniqId[0]['image']; ?>">
	<input type="hidden" name="method" value="update">
		<div class="form-group">
			<label for="feature-image">Image: </label>
			<input type="file" id = "gallery-feature-image" class="form-control" name="gallery-feature-image" onchange="bannerPreview('#gallery-feature-image', '#gallery-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="gallery-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getGalleryDetailsByUniqId[0]["image"]; ?>'); <?php if (empty($getGalleryDetailsByUniqId[0]['image']) || !isset($getGalleryDetailsByUniqId[0]['image'])) { echo 'display: none'; }?>;"></div>
		</div>
    <div class="form-group">
			<label for="corporate-services">Description :</label>
			<textarea class="form-control" rows="3" id="description" maxlength="10000" name="description" ><?php echo $getGalleryDetailsByUniqId[0]['description']; ?></textarea>
		</div>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="UpdateGalleryDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
