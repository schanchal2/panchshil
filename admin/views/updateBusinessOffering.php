<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/offeringDetailsModel.php';

$offeringId = cleanXSS(urldecode($_GET["id"]));
$businessId = cleanXSS(urldecode($_GET["businessId"]));
$uploadDir = "../uploads/offeringImages/";
$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
      if($method!="insert"){
   	$getofferingDetails = getofferingDetailsById($offeringId,$conn);
    if(noError($getofferingDetails)){
        $getofferingDetails = $getofferingDetails["response"];
    }else{
        $returnArr = $getofferingDetails;
    }
  }

}else{
    $returnArr = $conn;
    exit;
}
// printArr($getofferingDetails);
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Update offering Category</h4>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="offeringForm" enctype = "multipart/form-data">
        <input type="hidden" name="offeringId" id="offeringId" value="<?php echo $getofferingDetails[0]['id']; ?>">
        <input type="hidden" name="bussinessId" id="bussinessId" value="<?php echo $businessId; ?>">
        <input type="hidden" name="offering-category-status" id="offering-category-status" value="<?php echo $getofferingDetails[0]['status']; ?>">

        <input type="hidden" name="offering-categories-feature-image-url" id="offering-categories-feature-image-url" value="<?php echo $getofferingDetails[0]['feature_image']; ?>">

         <input type="hidden" name="offering-categories-footer-image-url" id="offering-categories-footer-image-url" value="<?php echo$getofferingDetails[0]['footer_image']; ?>">
        <input type="hidden" name="offering-categories-big-image-url" id="offering-categories-big-image-url" value="<?php echo $getofferingDetails[0]['big_image']; ?>">
		<input type="hidden" name="method" value="update">

		<?php if (!empty($getofferingDetails)) {?><label for="offering-category-status">Status : </label>	<span style="font-size: 16px;font-weight: bold; <?php if($getofferingDetails[0]["status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?> "><?php echo $getofferingDetails[0]["status"]; ?></span>

		<?php }?>

		<div class="form-group">
			<label for="feature-image">Feature Image : </label>
			<input type="file" id = "offering-categories-feature-image" class="form-control" name="offering-categories-feature-image" onchange="offeringCatPreview('#offering-categories-feature-image', '#offering-categories-feature-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="offering-categories-feature-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getofferingDetails[0]["feature_image"]; ?>'); <?php if (empty($getofferingDetails[0]['feature_image']) || !isset($getofferingDetails[0]['feature_image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="big-image">Big Image : </label>
			<input type="file" id = "offering-categories-big-image" class="form-control" name="offering-categories-big-image" onchange="offeringCatPreview('#offering-categories-big-image', '#offering-categories-big-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="offering-categories-big-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getofferingDetails[0]["big_image"]; ?>'); <?php if (empty($getofferingDetails[0]['big_image']) || !isset($getofferingDetails[0]['big_image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="footer-image">Footer Image : </label>
			<input type="file" id = "offering-categories-footer-image" class="form-control" name="offering-categories-footer-image" onchange="offeringCatPreview('#offering-categories-footer-image', '#offering-categories-footer-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="offering-categories-footer-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getofferingDetails[0]["footer_image"]; ?>'); <?php if (empty($getofferingDetails[0]['footer_image']) || !isset($getofferingDetails[0]['footer_image'])) { echo 'display: none'; }?>;"></div>
		</div>

		<div class="form-group">
			<label for="offering-name">Name</label>
			<input type="text" class="form-control" id="offering-name" name="offering-name" value="<?php echo $getofferingDetails[0]['name']; ?>" >
		</div>

    <div class="form-group">
      <label for="about-name">Select Type</label>
    </div>
    <select class="form-control droper" name="page_type" id="page_type" style="">
              <option value = "NULL">Please Select</option>
              <option value = "manageluxuryResidences.php">Luxury Residences</option>
              <option value = "manageCommercial.php">Commercial</option>
              <option value = "manageHospitality.php">Hospitality</option>
              <option value = "manageRetail.php">Retail</option>
          </select>


		<div class="form-group">
			<label for="offering-shortDesc">Short Description</label>
			<textarea class="form-control" rows="3" id="offering-shortDesc" name="offering-shortDesc"><?php echo $getofferingDetails[0]['shortDesc']; ?></textarea>
		</div>
		<div class="form-group">
			<label for="offering-longDesc">Long Description</label>
			<textarea class="form-control" rows="3" id="offering-longDesc" name="offering-longDesc"><?php echo $getofferingDetails[0]['longDesc']; ?></textarea>
		</div>



	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateofferingDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
