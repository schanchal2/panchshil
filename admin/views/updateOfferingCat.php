<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/businessCategoryDetailsModel.php';

$businessCatId = cleanXSS(urldecode($_GET["businessCatId"])); //echo $businessCatId;
$businessId = cleanXSS(urldecode($_GET["businessId"])); //echo $businessId;
$uploadDir = "../uploads/businessCategoryImages/";
$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];    
   	$businessCatInfo = getBusinessCatDetails($businessCatId,$businessId,NULL,$conn);
    if(noError($businessCatInfo)){
        $businessCatInfo = $businessCatInfo["response"];                    
    }else{
        $returnArr = $businessCatInfo;
    }

}else{
    $returnArr = $conn;
    exit;
}
// printArr($businessCatInfo);
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<?php if ($businessCatId=="undefined") {?>
	<h4 class="modal-title">Add New Business Category</h4>	
	<?php } else{?>
	   <h4 class="modal-title">Update Business Category</h4>	
	<?php }
	?>
</div>

<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="businessCatForm" enctype = "multipart/form-data">		
        <input type="hidden" name="catId" id="catId" value="<?php echo $businessCatInfo[0]['id']; ?>">
        <input type="hidden" name="bussinessId" id="bussinessId" value="<?php echo $businessId; ?>">
        <input type="hidden" name="business-category-status" id="business-category-status" value="<?php echo $businessCatInfo[0]['category_status']; ?>">
        <input type="hidden" name="business-categories-footer-image-url" id="business-categories-footer-image-url" value="<?php echo$businessCatInfo[0]['category_footer_image']; ?>">
        <input type="hidden" name="business-categories-big-image-url" id="business-categories-big-image-url" value="<?php echo $businessCatInfo[0]['category_big_image']; ?>">
        <input type="hidden" name="business-categories-feature-image-url" id="business-categories-feature-image-url" value="<?php echo $businessCatInfo[0]['category_feature_image']; ?>">
		<input type="hidden" name="method" value="update">

		<label for="business-category-status">Status : </label>	<span style="font-size: 16px;font-weight: bold; <?php if($businessCatInfo[0]["category_status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?> "><?php echo $businessCatInfo[0]["category_status"]; ?></span>

		<div class="form-group">
			<label for="feature-image">Feature Image : </label>			
			<input type="file" id = "business-categories-feature-image" class="form-control" name="business-categories-feature-image" onchange="businessCatPreview('#business-categories-feature-image', '#business-categories-feature-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="business-categories-feature-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$businessCatInfo[0]["category_feature_image"]; ?>'); <?php if (empty($businessCatInfo[0]['category_feature_image']) || !isset($businessCatInfo[0]['category_feature_image'])) { echo 'display: none'; }?>;"></div>
		</div>	
		
		<div class="form-group">
			<label for="client-name">Name</label>
			<input type="text" class="form-control" id="business-categories-name" name="business-categories-name" value="<?php echo $businessCatInfo[0]['category_name']; ?>" >
		</div>

		<?php if (empty($businessCatInfo[0]['id']) || !isset($businessCatInfo[0]['id'])) { ?>
    	<div class="form-group">
			<label for="about-name">Please Select Type</label>
			<select class="form-control droper" name="page_type" id="page_type" style="">
				<option value = "NULL">Please Select</option>
				<option value = "Gold">Gold</option>
				<option value = "Platinum">Platinum</option>
        	</select>
		</div>    	
        <?php }?>

		<div class="form-group">
			<label for="client-services">Property Type</label>
			<input type="text" class="form-control" id="business-categories-name" name="business-categories-name" value="<?php echo $businessCatInfo[0]['category_name']; ?>" >
		</div>

		<div class="form-group">
			<label for="client-services">Area Sq.Ft</label>
			<input type="text" class="form-control" id="business-categories-name" name="business-categories-name" value="<?php echo $businessCatInfo[0]['category_name']; ?>" >
		</div>

		<div class="form-group">
			<label for="client-services">Short Description</label>
			<input type="text" class="form-control" id="business-categories-name" name="business-categories-name" value="<?php echo $businessCatInfo[0]['category_name']; ?>" >
		</div>

		<div class="form-group">
			<label for="client-services">Long Description</label>
			<input type="text" class="form-control" id="business-categories-name" name="business-categories-name" value="<?php echo $businessCatInfo[0]['category_name']; ?>" >
		</div>

		<div class="form-group">
			<label for="client-services">Category Type</label>
			<input type="text" class="form-control" id="business-categories-name" name="business-categories-name" value="<?php echo $businessCatInfo[0]['category_name']; ?>" >
		</div>

		<div class="form-group">
			<label for="client-services">Visit Website</label>
			<input type="text" class="form-control" id="business-categories-name" name="business-categories-name" value="<?php echo $businessCatInfo[0]['category_name']; ?>" >
		</div>

		<div class="form-group">
			<label for="feature-image">Add PDF : </label>			
			<input type="file" id = "business-categories-feature-image" class="form-control" name="business-categories-feature-image" onchange="businessCatPreview('#business-categories-feature-image', '#business-categories-feature-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="business-categories-feature-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$businessCatInfo[0]["category_feature_image"]; ?>'); <?php if (empty($businessCatInfo[0]['category_feature_image']) || !isset($businessCatInfo[0]['category_feature_image'])) { echo 'display: none'; }?>;"></div>
		</div>

		<div class="form-group">
			<label for="client-services">Latitude</label>
			<input type="text" class="form-control" id="business-categories-name" name="business-categories-name" value="<?php echo $businessCatInfo[0]['category_name']; ?>" >
		</div>

		<div class="form-group">
			<label for="client-services">Longitude</label>
			<input type="text" class="form-control" id="business-categories-name" name="business-categories-name" value="<?php echo $businessCatInfo[0]['category_name']; ?>" >
		</div>

		<div class="form-group">
			<label for="feature-image">Enquiry Image : </label>			
			<input type="file" id = "business-categories-feature-image" class="form-control" name="business-categories-feature-image" onchange="businessCatPreview('#business-categories-feature-image', '#business-categories-feature-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="business-categories-feature-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$businessCatInfo[0]["category_feature_image"]; ?>'); <?php if (empty($businessCatInfo[0]['category_feature_image']) || !isset($businessCatInfo[0]['category_feature_image'])) { echo 'display: none'; }?>;"></div>
		</div>
		

	</form>	
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateBusinessCatDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>	
</div>