<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/galleryContentModel.php';

$pagename = urldecode($_GET["pagename"]);
//echo $pagename;

$p_id = urldecode($_GET["p_id"]);
//echo $p_id;
$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getGalleryDetailsByUniqId = getGalleryDetailsByUniqId($id,$conn);
    if(noError($getGalleryDetailsByUniqId)){
        $getGalleryDetailsByUniqId = $getGalleryDetailsByUniqId["response"];
    }else{
        $returnArr = $getGalleryDetailsByUniqId;
    }
}else{
    $returnArr = $conn;
    exit;
}
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if($method=="insert"){ ?>
	<h4 class="modal-title">Add New Type</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Type</h4>
		<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
  	<div class="modal-err1" style="color:red;"></div>
	<form id="typeForm" enctype = "multipart/form-data">
  <input type="hidden" name="id" id="id" value="<?php echo $getGalleryDetailsByUniqId[0]['id']; ?>">
    <input type="hidden" name="p_id" id="p_id" value="<?php echo $p_id; ?>">
    <input type="hidden" name="p_id1" id="p_id1" value="<?php echo $getGalleryDetailsByUniqId[0]['id']; ?>">
  <input type="hidden" name="pagename" id="pagename" value="<?php echo $pagename?>">
	<input type="hidden" name="method" value="update">
    <div class="form-group">
			<label for="corporate-services">Type :</label>
      <input type="text" name="description" id="description" class="form-control" // value="<?php echo $getGalleryDetailsByUniqId[0]['description']; ?>"  maxlength="100">
		</div>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="UpdateTypeDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
