<?php

session_start();

require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/professionModel.php';


$id = cleanXSS(urldecode($_GET['id']));
$returnArr = array();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active redirect to the login page

    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $professionContent = getProfessionContent($id,$conn);
    if(noError($professionContent)){
    	 $professionContent = $professionContent["response"];
    	 
    }else{
            $returnArr = $professionContent;
    }
}else{
            $returnArr = $conn;
            exit;
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Add Profession</h4>	
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="profession-content" action="" method = "post" enctype = "multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $professionContent[0]['id']; ?>">
        <input type="hidden" name="method" value="update">
		<div class="form-group">
			<label for="image-title">Profession:</label>
			<input type="text" class="form-control" id="profession-name" name="profession-name" value="<?php echo $professionContent[0]["title"] ?>" >
		</div>		
	</form>	
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateProfessionDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>	
</div>