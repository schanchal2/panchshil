<?php

//start session
session_start();

require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/forwordModel.php';

$returnArr = array();

$id       = cleanXSS(urldecode($_GET['id']));
$pageName = cleanXSS(urldecode($_GET['pageName']));
//echo $id;

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active redirect to the login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn              = $conn["conn"];
    //$featureType     = "home";
    $getforwordDetails = getforwordDetails($id,$conn);
      //printArr($getforwordDetails);
    if(noError($getforwordDetails)){
            $getforwordDetails = $getforwordDetails["response"];
    }else{
            $returnArr = $getforwordDetails;
    }
}else{
            $returnArr = $conn;
            exit;
}
?>

<div class="col-lg-12 main-container-1">
    <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
        <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;"> Manage <?php echo ucfirst(strtolower($pageName)); ?> Details</p>
        </div>
        <div class="modal-err" style="color: red"></div>
        <div class="col-lg-12 redirect">

            <form id="forwordform" enctype = "multipart/form-data">
          		<input type="hidden" name="id" id="id" value="<?php echo $getforwordDetails[0]['id']; ?>">
          		<input type="hidden" name="forword_image_url" value="<?php echo $getforwordDetails[0][' image']; ?>">
              <input type="hidden" name="sign_image_url" value="<?php echo $getforwordDetails[0]['sign_image']; ?>">
          		<input type="hidden" name="method" value="update">

              <input type="hidden" name="forword_id" id = "forword_id" value="<?php echo $id; ?>">


              <div class="form-group">
          			<label for="forword_image">Image : </label>
          			<input type="file" id = "forword_image" class="form-control" name="forword_image" onchange="forwordPreview('#forword_image', '#forword-image-preview',null,'<?php echo $uploadDir; ?>');">
          			<div id="forword-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getforwordDetails[0]["image"]; ?>'); <?php if (empty($getforwordDetails[0]['image']) || !isset($getforwordDetails[0]['image'])) { echo 'display: none'; }?>;"></div>
          		</div>

              <div class="form-group">
          			<label for="feature-image">Signature Image : </label>
          			<input type="file" id = "sign_image" class="form-control" name="sign_image" onchange="forwordPreview('#sign_image', '#sign-image-preview',null,'<?php echo $uploadDir; ?>');">
          			<div id="sign-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getforwordDetails[0]["sign_image"]; ?>'); <?php if (empty($getforwordDetails[0]['sign_image']) || !isset($getforwordDetails[0]['sign_image'])) { echo 'display: none'; }?>;"></div>
          		</div>

              <hr>
              
              <div class="form-group">
                  <label for="top-desc">Name:</label>
                  <input type="text" class="form-control" rows="3" id="name" name="name" maxlength="100" value = "<?php echo $getforwordDetails[0]["name"]; ?>">
              </div>
              <div class="form-group">
                  <label for="top-desc-title">Description:</label>
                  <textarea type="text" class="form-control" id="long_url" rows="5" name="long_url" maxlength="10000"><?php echo $getforwordDetails[0]["long_url"]; ?></textarea>
              </div>
              <div class="form-group">
                  <label for="know-more">Designation :</label>
                  <input type="text" class="form-control" id="designation" name="designation" value="<?php echo $getforwordDetails[0]["designation"]; ?>" maxlength="100">
              </div>
              </form>
              <div class="col-lg-6"></div>
              <div class="col-lg-3 blue-button-1" onclick="addUpdateForwordDetails()">Submit</div>
              <div class="col-lg-3 blue-button-1" id="clear" onclick ="confirmRemoveData(<?php echo $getforwordDetails[0]['id']; ?>);">Clear</div>

        </div>
    </div>
</div>
<script>


    /* TO Display Remodal on click of Add or Update about details */

    var pageName    = '<?php echo $pageName; ?>';

    /* TO add and update Clients details */
    function addUpdateForwordDetails(id) {
    //  alert(id);
        $('.btn-success').prop('disable',true);
        var UrlForrward = $('#forword_id').val();
        var long_url    = $('#long_url').val();
        var name        = $('#name').val();
        var designation = $('#designation').val();

        if (long_url == '' || name == '' || designation == '') {

            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {

          var form = $('#forwordform')[0];
          var formData = new FormData(form);

            //alert(JSON.stringify(formData));
          //  console.log(formdata);

            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/forwordController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                  //  alert(data);
                //   console.log(data);
                  //   alert(JSON.stringify(data));
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Forword Details !!!');
                        $("#load-content").load("manageForword.php?id="+UrlForrward+"&pageName="+encodeURIComponent(pageName));
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Forword content!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    //to remove campus details by campus id
    function removeforword(id){
        var UrlForrward = $('#forword_id').val();
            $.ajax({
                type: "POST",
                url: '../controller/forwordController.php',
                data: {
                    id:ID,
                    method: "delete"
                },
                dataType: 'json',
                success: function (data) {
                  //  console.log(data);

                    if (data.errCode == -1) {
                        $(".alert-success").show();
                        $("#success-msg").text('Successfully deleted Forword details!!!');
                        setTimeout(function() { $(".alert-success").hide(); }, 5000);
                    }else{
                        $(".alert-danger").show();
                        $("#error-msg").text("Failed to Delete Forword Content");
                        setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                    }
                     $("#load-content").load("manageForword.php?id="+UrlForrward+"&pageName="+encodeURIComponent(pageName));
                },
                error: function (data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in Forword details!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });

    }

    /* To show preview of uploaded Image */
    function forwordPreview(id,pre_id,nextSlider,uploadDir) {

        var files = $(id)[0].files;
        var img   = new Image();

        /* Checking Is any File selected */
        if (!files.length || !window.FileReader) return;

        /* Check Uploadede file is Image Only */
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();
            /* set image data as background of div */
            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#my_logo"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_logo').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_logo').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    function confirmRemoveData(id) {
        // alert(id);
        // alert(1);
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/forwordController.php",
            data:{
                id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Forword Content");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Forword Content");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageForword.php?id="+encodeURIComponent(id));
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Content !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);

            }
        });
    } 


    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeforword(id);
        $('#myModal .close').click();
    }
</script>
