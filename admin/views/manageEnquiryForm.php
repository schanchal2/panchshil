<?php
//prepare for request
//start session
session_start();


//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/enquiryContentModel.php';

$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $enquiryContent = getEnquiryDetails(NULL,$conn);

    if(noError($enquiryContent)){   
        $enquiryContent = $enquiryContent["response"];

    }else{
        $returnArr = $enquiryContent;
    }
}else{
    $returnArr = $conn;
    exit;
}

?>

<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
      	 <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage Enquiry Form</p>
        </div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;"> 
                        <th>Property Name</th>
                        <th>Name</th>
                        <th>Contact Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>  
                     <?php 
                        if (empty($enquiryContent)) {
                            echo "<tr><td colspan = '5' style = 'text-align:center;'>No Data Found</td></tr>";
                        }
                        for($i=0; $i < count($enquiryContent); $i++) { 

                    ?>                
                        <tr>                                                        
                            <td>
                            	 <div style="height: 130px;width: 100px;">
                                 <p class="enquiryDisplay" id="propertyName" name="propertyName"><?php echo $enquiryContent[$i]["propertyName"]?></p>
                                <div style="height: 130px;">
                                </div>
                            </td>
                            <td>
                            	<div style="height: 130px;width: 100px;">
                                 <p class="enquiryDisplay" id="name" name="name"><?php echo $enquiryContent[$i]["name"]?></p>
                                <div style="height: 130px;">
                            </td>
                             <td>
                               <div style="height: 130px;width: 100px;">
                                 <p class="enquiryDisplay" id="contactDate" name="contactDate"><?php echo $enquiryContent[$i]["contactDate"]?></p>
                                <div style="height: 130px;">
                            </td>
                            <td>
                                <a href="#" onclick="openViewEnquiryDialog(<?php echo $enquiryContent[$i]['id']?>);">View</a><br>
                                <a href="#" onclick="confirmRemoveData(<?php echo $enquiryContent[$i]['id']; ?>);">Delete</a>
                            </td>   
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>  
	</div>
</div>
<script>
  /* TO Display Remodal on click of view Clients details */
   function openViewEnquiryDialog(Id){
        $(".modal-content").load("viewEnquiryDetails.php?id="+Id);  
        $("#myModal").modal();
    }
     function removeEnquiryDetails(id){
        var id = id;
            $.ajax({
                type: "POST",
                url: '../controller/enquiryFormController.php',
                data: {
                    id:id,
                    method: "delete"
                },
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if (data.errCode == -1) {
                        $(".alert-success").show();
                        $("#success-msg").text('Successfully deleted Enquiry details!!!');
                        setTimeout(function() { $(".alert-success").hide(); }, 5000);
                    }else{
                        $(".alert-danger").show();
                        $("#error-msg").text("Failed to Delete Enquiry Content");
                        setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                    }          
                     $("#load-content").load("manageEnquiryForm.php?id="+encodeURIComponent(id));
                },
                error: function (data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in deleting Enquiry details!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        
    }

     /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmRemoveData.php?id="+id);
        $("#myModal").modal(); 
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeEnquiryDetails(id);
        $('#myModal .close').click();
    }

 function addUpdateEnquiryContent(Id) {  
            var propertyName     = $('#propertyName').val();
            var name             = $('#name').val();
            var email            = $('#email').val();          
            var mobileNumber     = $('#mobileNumber').val();
            var contactDate      = $('#contactDate').val();
            var contactTime      = $('#contactTime').val();  
            var form             = $('#enquiry-form')[0];           
            var formData         = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/enquiryFormController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Enquiry Content Details !!!');
                        $("#load-content").load("manageEnquiryForm.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.blue-button-1').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Enquiry Content content!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
    }

</script>
