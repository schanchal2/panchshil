<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/matrixSpecificationContentModel.php';
require_once '../model/pageNameContentModel.php';

$returnArr = array();

$o_id=$_GET['id'];
//echo $o_id;
$p_id=$_GET['p_id'];
//echo $p_id;
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getMatrixSpecificationDetails = getMatrixSpecificationDetailsById($p_id,$conn);
    if(noError($getMatrixSpecificationDetails)){
        $getMatrixSpecificationDetails = $getMatrixSpecificationDetails["response"];

    }else{
        $returnArr = $getMatrixSpecificationDetails;
    }

    $pageInfo = getPagename('offering',$o_id,$conn);
    if(noError($pageInfo)){
        $pageInfo = $pageInfo["response"][0];
    }else{
        $returnArr = $pageInfo;
    }

    $pageInfoForPropertyDetails = getPagename('property_details',$p_id,$conn);
    if(noError($pageInfoForPropertyDetails)){
        $pageInfoForPropertyDetails = $pageInfoForPropertyDetails["response"][0];
    }else{
        $returnArr = $pageInfoForPropertyDetails;
    }


}else{
    $returnArr = $conn;
    exit;
}



?>
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="#" onclick="openViewOfferingBuisiness();" >Business ></a>
  <a class="breadcrumb-item" href="#" onclick="openViewOffering(<?php echo $getPropertyDetails[$i]['id']; ?>);" >Offering</a>
  <a class="breadcrumb-item" href="#" onclick="openViewParticulerPage(<?php echo $o_id; ?>,'<?php echo $pageInfo[pagename]; ?>');" >><?php echo ucfirst($pageInfo[name]); ?></a>
  <a class="breadcrumb-item" href="#" >><?php echo ucfirst($pageInfoForPropertyDetails[name]); ?></a>
  <a class="breadcrumb-item" href="#" >><strong>Manage Matrix Specification</strong></a>
</nav>
<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
      	 <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;"> Manage Matrix Specification</div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="addMatrixSpecificationDialog('insert',<?php echo $o_id; ?>,<?php echo $p_id; ?>);">Add Matrix Specification</div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px; ">
                        <th width="15%" style="text-align:center;">Image</th>
                        <th width="15%" style="text-align:center;">Title</th>
                        <th width="15%" style="text-align:center;">Unit Title</th>
                        <th width="15%" style="text-align:center;">Unit Value</th>
                        <th width="25%" style="text-align:center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (empty($getMatrixSpecificationDetails)) {
                                echo "<tr><td colspan = '6' style = 'text-align:center;'>No Data Found</td></tr>";
                            }
                            for ($i = 0; $i < count($getMatrixSpecificationDetails); $i++) {
                        ?>
                        <tr>
                            <td>
                                <div id="image" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getMatrixSpecificationDetails[$i]["image"]; ?>');"></div>
                            </td>
                            <td>
                                <p class = "headerDisplay" id="Name" name="Name" ><?php echo $getMatrixSpecificationDetails[$i]['title']; ?></p>
                            </td>
                            <td>
                                <p class = "headerDisplay" id="Name" name="Name" ><?php echo $getMatrixSpecificationDetails[$i]['unitTitle']; ?></p>
                            </td>
                            <td>
                                <p class = "headerDisplay" id="Name" name="Name" ><?php echo $getMatrixSpecificationDetails[$i]['unitValue']; ?></p>
                            </td>

                            <td>
                                <a href="#" onclick="updateViewMatrixSpecificationDialog(<?php echo $getMatrixSpecificationDetails[$i]['id']; ?>);">Edit</a><br>
                                <a href="#" onclick="confirmRemoveData(<?php echo $getMatrixSpecificationDetails[$i]['id']; ?>,<?php echo $getMatrixSpecificationDetails[$i]['p_id']; ?>,<?php echo $getMatrixSpecificationDetails[$i]['o_id']; ?>);">
                                    <span style="font-size: 16px;font-weight: bold;  ">Delete
                                    </span>
                                </a><br>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</div>
<script>

   function addMatrixSpecificationDialog(method,b_id,p_id){
        $( ".modal-content" ).load("updateMatrixSpecification.php?b_id="+b_id+'&method='+method+'&p_id='+p_id);
        $("#myModal").modal();
    }

    function updateViewMatrixSpecificationDialog(id){
         $( ".modal-content" ).load("updateMatrixSpecificationContent.php?id="+id);
         $("#myModal").modal();
     }

     function openViewOfferingBuisiness(){
          $("#load-content").load("manageBusiness.php");
      }

    /* To show preview of uploaded Image */
    function bannerPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        if (!files.length || !window.FileReader) return;
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();

            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdateBannerDetails() {
        $('.btn-success').prop('disable',true);
        var p_id                 = $('#p_id').val();
          var o_id                 = $('#o_id').val();
            var form = $('#bannerForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/matrixSpecificationController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                  //  alert(data);
                  //  console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Gallery Details !!!');
                        $("#load-content").load("manageMatrixSpecification.php?p_id="+p_id+"&id="+o_id);
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Banner Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }


        /* TO add and update Clients details */
        function UpdateMatrixSpecificationDetails() {
            $('.btn-success').prop('disable',true);
            var p_id                 = $('#p_id').val();
            var o_id                 = $('#o_id').val();
                var form = $('#galleryForm')[0];
                var formData = new FormData(form);
                $.ajax({
                    type:'post',
                    dataType:'json',
                    url:'../controller/updateMatrixSpecificationController.php',
                    data:formData,
                    contentType:false,
                    processData:false,
                    async:false,
                    success: function(data) {
                      //  alert(data);
                      //  console.log(data);
                        if (data.errCode == -1) {
                            $('#myModal .close').click();
                            $('.alert-success').show();
                            $('#success-msg').text('Successfully Updated Matrix Specification Details !!!');
                            $("#load-content").load("manageMatrixSpecification.php?p_id="+p_id+"&id="+o_id);
                            setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                        } else {
                            $('.btn-success').prop('disable',false);
                            $('.modal-err').text(data.errMsg);
                        }
                    },
                    error: function(data) {
                        $(".alert-danger").show();
                        $("#error-msg").text('Error in updating Matrix Specification Details !!!');
                        setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                    }
                });
            }

            /* To remove about Details */
            function removeMatrixSpecification(id,p_id,o_id) {
                $.ajax({
                    type:"POST",
                    dataType:"json",
                    url:"../controller/updateMatrixSpecificationController.php",
                    data:{
                        id:id, method:"delete"
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.errCode == -1) {
                            $(".alert-success").show();
                            $("#success-msg").text("Successfully Deleted Matrix Specification Details !!!");
                            setTimeout( function() { $(".alert-success").hide(); }, 5000);
                        } else {
                            $(".alert-danger").show();
                            $("#error-msg").text("Failed to Delete Matrix Specification Details !!!");
                            setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                        }
                        $("#load-content").load("manageMatrixSpecification.php?p_id="+p_id+"&id="+o_id);
                    },
                    error: function(data) {
                        $(".alert-danger").show();
                        $("#error-msg").text('Error Deleting Press Details !!!');
                        setTimeout(function() { $(".alert-danger").hide(); }, 10000);

                    }
                });
            }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id,p_id,o_id) {
        $(".modal-content").load("confirmChangesRemodalByIdAndPid.php?id="+id+'&p_id='+p_id+'&o_id='+o_id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id,p_id,o_id) {
        removeMatrixSpecification(id,p_id,o_id);
        $('#myModal .close').click();
    }

    function openViewMediaDialog(pagename){
  // alert(pagename);
      //  $("#load-content").load(pagename);
    }

</script>
