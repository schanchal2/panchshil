<?php

/*
	@ Prepare Session Request
	@ Call Session Management
*/
	session_start();

/*
	@ Include all required or necessory files
*/
	require_once "utilities/config.php";

/*
	@ checking is session is active
*/
	$showLoginForm = true;
	if ( isset($_SESSION['email']) ) {
		/* Session is active so Show message */
		$showLoginForm = false;
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Panchshil Admin</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl.'/css/protectedAdmin.css'; ?>">
</head>
<body class = "bodyColor">
	<div class="container containt">
		<div class="col-md-12 formContainer">
			<div class="col-md-12 headline">
				Panchshil Admin
			</div>
			<!--To show Error message-->
			<div id = "err-msg" class = "errorMessage">
				<?php
					/*Check for error in session and display if exist then clear*/
					if ( isset($_SESSION['error'])) {
						echo ucwords($_SESSION['error']);
					}
					unset($_SESSION['error']);
				?>
			</div>
			<!--End of show error div-->
			<!--To display login form or redirection message -->
			<div class="col-md-12">
				<?php
					if ($showLoginForm) {
				?>
						<form id = "admin-login" action="controller/loginAdminController.php" method = "POST">
							<div class = "form-group">
								<lable for = "usr" class="boldText">Username : </lable>
								<input name = "username" type = "text" class = "form-control" id = "usr" required>
							</div>
							<div class = "form-group">
								<lable for = "pass" class="boldText">Password : </lable>
								<input name = "password" type = "password" class = "form-control" id = "pass" required>
							</div>
							<div class="col-md-6 row"> </div>

							<div class="col-md-6 login-button">

				                <input type="submit" class="col-md-3 blueButton" value="Login">

				                <div class="col-md-3 blueButton" id="clear">Clear</div>

			           		</div>
			               
						</form>
				<?php
					} else {
				?>
						<div class="col-md-12">
							You are already logged in. <a href="<?php echo $rootURL; ?>views">Go to Dashboard</a>
						</div>
				<?php
					}
				?>
			</div>
		<!--End of login form or redirection div-->
	</div>
</div>
</body>
</html>
<script type="text/javascript" src="../js/jquery-2.2.3.min.js"></script>
<script type="text/javascript">
	$('#clear').click(function(){
		$('#admin-login')[0].reset();
		$('err-msg').text('');
	});
</script>
