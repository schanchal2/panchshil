<?php
  include("../layout/header.php");
  require_once '../../admin/utilities/config.php';
  require_once "../../admin/utilities/dbUtils.php";
  require_once "../../admin/utilities/utilities.php";
  require_once "../../admin/utilities/errorMap.php";
  require_once "../../admin/model/headerContentModel.php";
  require_once "../../admin/model/mediaContentModel.php";


  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
    $conn = $conn["conn"];
    $headerInfo = getHeaderContent('media',$conn);
    if(noError($headerInfo)){
      $headerInfo = $headerInfo["response"];
    }else{
      $returnArr = $headerInfo;
    }

    $mediaInfo = getMediaDetails(NULL,'status',$conn);
   if(noError($mediaInfo)){
     $mediaInfo = $mediaInfo["response"];
   }else{
     $returnArr = $mediaInfo;
   }

   //printArr($mediaInfo);

  }else{
    $returnArr = $conn;
    exit;
  }
  $aboutPageNames = array(
    "updateAwardContent.php"  => "award/award.php",
    "managePressContent.php"  => "press/press.php" );
?>
<?php if(count($headerInfo) <= 0 ){  ?>
<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.$mediaFeatureImage;?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Media</a>
          </div>
          <h1>Media</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="media-section">
  <div class="container media-container">
    <div class="row">
      <div class="col-md-12 media-text">
        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </div>
    </div>
<?php } else { ?>
<?php for($i=0; $i < count( $headerInfo); $i++) { ?>
<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.'uploads/'.$headerInfo[$i]["image"];?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Media</a>
          </div>
          <h1><?php echo $headerInfo[$i]["page_name"];?></h1>
        </div>
      </div>
    </div>
  </div>
</section>
<?php } ?>
<section class="media-section">
  <div class="container media-container">
    <div class="row"><?php for($i=0; $i < count( $headerInfo); $i++) { ?>
      <div class="col-md-12 media-text">
        <p class="text-center"><?php echo $headerInfo[$i]["description"];?></p>
      </div>
    </div><?php }
      } ?>


    <div class="row">
      <div class="col-md-12" style="margin-bottom: 30px;">
        <div class="media-content">
          <img src="<?php echo $rootUrl.'uploads/'.$mediaInfo[0]["feature_image"];?>" class="img-responsive"/>
          <div class="media-content-text">
      			<div class="row">
      				<div class="col-md-10 col-sm-10 media-content-text-padding">
                <h2><?php echo $mediaInfo[0]["name"];?></h2>
                <p><?php echo $mediaInfo[0]["short_desc"];?></p>
                <p><?php echo $mediaInfo[0]["long_desc"];?></p>
      				</div>
      				<div class="col-md-2 col-sm-2 media-content-arrow">
                <?php if( $mediaInfo[0]["media_type"]=="updateAwardContent.php")
                {?>
      					<a href="../award/award.php?id=<?php echo $mediaInfo[0]["id"];?>" class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                <?php }
                else{ ?>
                	<a href="../press/press.php?id=<?php echo $mediaInfo[0]["id"];?>" class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                  <?php } ?>
      				</div>
      			</div>
      		</div>
        </div>
      </div>
    </div>

    <div class="row">

      <?php
      $tot= count($mediaInfo)-1;
      for($i=1; $i < count($mediaInfo); $i++) { ?>
      <div class="<?php if($i == $tot && $tot %2 != 0) echo "col-md-12"; else{ echo "col-md-6"; }?>">
        <div class="business-content">
          <img src="<?php echo $rootUrl.'uploads/'.$mediaInfo[$i]["box_image"];?>" class="img-responsive"/>
          <div class="business-content-text">
            <div class="row">
              <div class="col-md-10 col-sm-10 business-content-text-padding">
                <h2><?php echo $mediaInfo[$i]["name"];?></h2>
                <p class="block-with-text1">
                  <?php
                      foreach ($aboutPageNames as $key => $value) {
                        if ($key == $mediaInfo[$i]["media_type"]) {
                          $pageRedirection = "../".$value."?id=".$mediaInfo[$i]["id"];
                          break;
                        } else {
                          $pageRedirection = "#";
                        }
                      }
                  ?>
                </p>
              </div>
              <div class="col-md-2 col-sm-2 business-content-arrow">
                <a href="<?php echo $pageRedirection; ?>"  class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>

</section>

<?php include("../layout/footer.php");  ?>
