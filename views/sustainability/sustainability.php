
<?php
    include("../layout/header.php");

    require_once '../../admin/utilities/config.php';
    require_once "../../admin/utilities/dbUtils.php";
    require_once "../../admin/utilities/utilities.php";
    require_once "../../admin/utilities/errorMap.php";
    require_once "../../admin/model/headerContentModel.php";
    require_once "../../admin/model/aboutContentModel.php";
     
     $id = $_GET['id'];
    $returnArr = array();
    

    $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
    if(noError($conn)){
        $conn = $conn["conn"];
        $headerInfo = getAboutSustainDetails($id,'Sustainibility',$conn);
        //printArr($headerInfo);
        $getSustainDetails  = getSustainDetails(Null,'Sustainibility',$conn);
        //printArr($getSustainDetails);
        if(noError($getSustainDetails)){
            $getSustainDetails = $getSustainDetails["response"];
        }else{
            $returnArr = $getSustainDetails;
        }
        if(noError($headerInfo)){
            $headerInfo = $headerInfo["response"];
        }else{
            $returnArr = $headerInfo;
        }  
    }else{
            $returnArr = $conn;
            exit;
    }
    
?>

<!-- header-->
<!--banner starts here-->
<?php if(count($headerInfo) <= 0){  ?>
<div class="banner-business" style="background-image: url(<?php echo $rootUrl.'uploads/'.$headerInfo[0]['image'];?>);">
    <div class="container-fluid nopadding">
        <ol class="breadcrumb">
            <li><a href="../home/home.php">Home</a></li>
            <li><a href="../about/about.php">About</a></li>
            <li class="active">Sustainability</li>
        </ol>
    </div><h1>Sustainability</h1>
</div>


<section class="business-section">
    <div class="container business-container">
        <div class="row">
            <div class="col-md-12 business-text">
                <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </div>
<?php } else { ?>
    <?php for($i=0; $i < count($headerInfo); $i++) { ?>
            <div class="banner-business" style="background-image: url(<?php echo $rootUrl.'uploads/'.$headerInfo[0]['image'];?>);">
                <div class="container-fluid nopadding">
                    <ol class="breadcrumb">
                        <li><a href="../home/home.php">Home</a></li>
                        <li><a href="../about/about.php">About</a></li>
                        <li class="active">Sustainability</li>
                    </ol>
                </div><h1><?php echo $headerInfo[$i]["name"];?></h1>
            </div>
    <?php } ?>


        <section class="business-section">
            <div class="container business-container">
                <div class="row"><?php for($i=0; $i < count( $headerInfo); $i++) { ?>
                    <div class="col-md-12 business-text">
                        <p class="text-center"><?php echo $headerInfo[$i]["longDesc"];?></p>
                    </div>
                </div><?php }
                }
                ?>



                <div class="row">
                    <?php for($i=0; $i < count($getSustainDetails); $i++) { ?>
                        <div class="<?php if($i == count($getSustainDetails)-1 && count($getSustainDetails)%2 != 0) echo "col-md-12"; else{ echo "col-md-6"; }?>">
                            <div class="business-content">
                                <img src="<?php echo $rootUrl.'uploads/'.$getSustainDetails[$i]["image"];?>" class="img-responsive"/>
                                <div class="business-content-text">
                                    <div class="row">
                                        <div class="col-md-10 col-sm-10 business-content-text-padding">
                                            <h2><?php echo $getSustainDetails[$i]["name"];?></h2>
                                            <p class="block-with-text1">
                                                <?php
                                                $aboutCatDetails = $getSustainDetails[$i]["shortDesc"];
                                                $aboutCatDetails = trim($aboutCatDetails);
                                                $aboutCatLength  =  strlen($aboutCatDetails);
                                                if ($aboutCatLength <= 120 ) {
                                                    echo $aboutCatDetails;
                                                } else {
                                                    echo substr($aboutCatDetails,0,120);
                                                    echo "...";
                                                }

                                                // foreach ($sustainPageNames as $key => $value) {
                                                //   if ($key == $getSustainDetails[$i]["pagename"]) {
                                                //     $pageRedirection = "../".$value."?sustainUrlid=".$getSustainDetails[$i]["sustainUrlId"];
                                                //     break;
                                                //   } else {
                                                //     $pageRedirection = "#";
                                                //   }
                                                // }
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-md-2 col-sm-2 business-content-arrow">
                                            <a href="<?php echo "../inBusiness/inBusiness.php?id=".$getSustainDetails[$i]['id']."&urlId=".$getSustainDetails[$i]['sustainid']; ?>"  class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>


            </div><!-- container -->
        </section>

<?php include("../layout/footer.php");  ?>