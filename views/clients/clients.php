<?php

  include("../layout/header.php");
  require_once '../../admin/utilities/config.php';
  require_once "../../admin/utilities/dbUtils.php";
  require_once "../../admin/utilities/utilities.php";
  require_once "../../admin/utilities/errorMap.php";
  require_once "../../admin/model/clientDetailsModel.php";

  $id     = $_GET['id'];
$returnArr = array();


  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
    $conn = $conn["conn"];
      $headerInfo = getAboutClientDetails($id,'Client',$conn);
      $clientInfo = getClientDetails($clientId, $id, $conn);
    if(noError($clientInfo)){
      $clientInfo = $clientInfo["response"];
      $totalClient = count($clientInfo);

    }else{
      $returnArr = $clientInfo;
    }
      if(noError($headerInfo)){
          $headerInfo = $headerInfo["response"];
      }else{
          $returnArr = $headerInfo;
      }
  }else{
    $returnArr = $conn;
    exit;
  }
?>

<section class="hero-section hero-banner-img">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-section-col-padding">
                <div class="hero-banner-text" style="background-image: url(<?php echo $rootUrl.'uploads/'.$headerInfo[0]['image'];?>);" >
                    <div class="hero-banner-link">
                        <a href="#">Home</a> / <a href="#">About Panchshil</a> / <a href="#">Clients</a>
                    </div>
                    <h1>Clients</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!--content starts here-->

<section class="clients-section">
  <div class="container clients-container">
    <div class="row">
      <div class="col-md-12 clients-text">
        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
      </div>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1>List of Clients</h1><br/>
        <ul class="clients-list-nav">
          <li><a href="#">All</a></li>
          <li><a href="#">Built-to-Suit</a></li>
          <li><a href="#">Commercial</a></li>
          <li><a href="#">Commercial Leased</a></li>
        </ul>
      </div>
    </div>
    <br/><br/>


    <div id="client-append">
    </div>
    <div id="offset" style="display:none;">0</div>
    <?php if(count($clientInfo) <= 15){ ?>
      <div class="row">
        <div class="col-md-12 common-btn" id="viewAllClient">
          <a onclick="loadMoreClient();" class="btn btn-default clients-logo-btn">VIEW ALL CLIENTS</a>
        </div>
      </div>

    <?php }else{ ?>
      <div class="row">
        <div class="col-md-12 common-btn" id="viewAllClient">
          <a onclick="loadMoreClient();" class="btn btn-default clients-logo-btn">VIEW ALL CLIENTS</a>
        </div>
      </div>

    <?php } ?>

  </div>
</section>


<!--footer-->
<?php include("../layout/footer.php");  ?>
