<?php include("../layout/header.php");  ?>

<section class="nri-section bg-black" id="footer-bg-black">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 nri-path-col-padding">
        <div class="nri-path-link">
            <a href="#">Home</a> / <a href="#">NRI</a> / <a href="#">Details</a>
        </div>
      </div>
    </div>
  </div><!-- container-fluid end -->

  <div class="container">
    <div class="row nri-content">
      <div class="col-md-6">
        <img src="../../img/nrihomepage.jpg" class="img-responsive"/>
      </div>
      <div class="col-md-6 nri-text-white">
        <h1>Tilte goes here...</h1>
        <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
        <br/>
        <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
        <br/><br/>
        <a href="#" class="btn btn-default nri-btn-share"><i class="fa fa-share-alt" aria-hidden="true"></i> &nbsp; SHARE NOW</a>
      </div>
    </div>
  </div><!-- container -->

<!--===================
  Related News
=====================  -->
  <div class="container nri-related-news">
    <h3 class="nri-text-white didot-bold-font text-center">Related News</h3>
    <br/>
    <div class="nri-news-slider">
      <div class="nri-slide">
        <a href="#"><img src="../../img/updates_1.jpg" class="img-responsive" alt="Updates image"></a>
        <div class="row">
          <div class="col-sm-3 col-xs-4 NewsUpdate">
            <div>
              <p class="date didot-font nri-text-white">30</p>
              <p class="month nri-text-white">JULY</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 updates-content nri-news-content">
            <p>Panchshil unveils homes next to Mahalaxmi Race course</p>
          </div>
        </div>
      </div>
      <div class="nri-slide">
          <a href="#"><img src="../../img/updates_2.jpg" class="img-responsive" alt="Updates image"></a>
          <div class="row">
            <div class="col-sm-3 col-xs-4 NewsUpdate">
              <div>
                <p class="date didot-font nri-text-white">17</p>
                <p class="month nri-text-white">JULY</p>
              </div>
            </div>
            <div class="col-sm-9 col-xs-8 updates-content nri-news-content">
              <p>Panchshil unveils homes next to Mahalaxmi Race course</p>
            </div>
          </div>
      </div>
      <div class="nri-slide">
          <a href="#"><img src="../../img/updates_3.jpg" class="img-responsive" alt="Updates image"></a>
          <div class="row">
            <div class="col-sm-3 col-xs-4 NewsUpdate">
              <div>
                <p class="date didot-font nri-text-white">26</p>
                <p class="month nri-text-white">JULY</p>
              </div>
            </div>
            <div class="col-sm-9 col-xs-8 updates-content nri-news-content">
              <p>Panchshil unveils homes next to Mahalaxmi Race course</p>
            </div>
          </div>
      </div>
      <div class="nri-slide">
        <a href="#"><img src="../../img/updates_1.jpg" class="img-responsive" alt="Updates image"></a>
        <div class="row">
          <div class="col-sm-3 col-xs-4 NewsUpdate">
            <div>
              <p class="date didot-font nri-text-white">30</p>
              <p class="month nri-text-white">JULY</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 updates-content nri-news-content">
            <p>Panchshil unveils homes next to Mahalaxmi Race course</p>
          </div>
        </div>
      </div>
      <div class="nri-slide">
          <a href="#"><img src="../../img/updates_2.jpg" class="img-responsive" alt="Updates image"></a>
          <div class="row">
            <div class="col-sm-3 col-xs-4 NewsUpdate">
              <div>
                <p class="date didot-font nri-text-white">30</p>
                <p class="month nri-text-white">JULY</p>
              </div>
            </div>
            <div class="col-sm-9 col-xs-8 updates-content nri-news-content">
              <p>Panchshil unveils homes next to Mahalaxmi Race course</p>
            </div>
          </div>
      </div>
      <div class="nri-slide">
          <a href="#"><img src="../../img/updates_3.jpg" class="img-responsive" alt="Updates image"></a>
          <div class="row">
            <div class="col-sm-3 col-xs-4 NewsUpdate">
              <div>
                <p class="date didot-font nri-text-white">30</p>
                <p class="month nri-text-white">JULY</p>
              </div>
            </div>
            <div class="col-sm-9 col-xs-8 updates-content nri-news-content">
              <p>Panchshil unveils homes next to Mahalaxmi Race course</p>
            </div>
          </div>
      </div>
    </div>
  </div>
  <!--===================
    Related News end
  =====================  -->

  <div class="container-fluid nri-feachers">
		<div class="row">
			<div class="col-md-4 nri-feachers-col-padding">
				<div class="nri-feachers-overlay">
					<img src="../../img/world-family.jpg" class="img-responsive" alt="world family image">
					<div class="nri-feachers-overlay-text">
						<h2>feachers</h2>
						<br/>
						<a href="#" class="btn btn-default">VIEW DETAILS</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 nri-feachers-col-padding">
				<div class="nri-feachers-overlay">
					<img src="../../img/simpler.jpg" class="img-responsive" alt="world family image">
					<div class="nri-feachers-overlay-text">
						<h2>BENEFITS</h2>
						<br/>
						<a href="#" class="btn btn-default">VIEW DETAILS</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 nri-feachers-col-padding nri-login-white-bg text-center">
        <div class="nri-login-form">
          <p class="nri-login-title">NRI</p>
          <p class="nri-login-subtitle">LOGIN / SIGN UP</p>
          <br/><br/><br/>
          <form>
          <input type="text" class="form-control nri-input" placeholder="USERNAME / EMAIL ID"><br/>
          <input type="password" class="form-control nri-input" placeholder="PASSWORD"><br/>
          <p><a href="#" class="nri-forgot-pwd">Forgot Password?</a></p>
          <br/><br/>
          <a href="#" class="btn btn-default nri-login-btn"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>&nbsp;&nbsp; LOGIN / SIGN UP</a>
        </from>
        </div>
			</div>
		</div>
	</div>
</section>

<?php include("../layout/footer.php");  ?>
<script>
  $(document).ready(function(){
    var id = $('section').attr('id');
    if( id == "footer-bg-black"){
      $("footer").css("background-color", "#282828");
      $(".footer-quick-link h5 a, .footer-quick-link .fa-long-arrow-right, .contact h5, .office-location, .contact-no, .follow-us h5, .login-signup a, .location-map h5, .location-content select").css("color", "#fff");
      $(".location-content select").css("background-color", "#282828");
    }
  });
</script>
