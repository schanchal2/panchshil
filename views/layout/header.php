<?php
 	require_once '../../admin/utilities/config.php';
	require_once "../../admin/utilities/dbUtils.php";
	require_once "../../admin/utilities/utilities.php";
	require_once "../../admin/utilities/errorMap.php";
	require_once "../../admin/model/offeringDetailsModel.php";
    require_once "../../admin/model/businessDetailsModel.php";
    require_once "../../admin/model/businessCategoryDetailsModel.php";
    require_once "../../admin/model/alliancesContentModel.php";
    require_once "../../admin/model/aboutContentModel.php";
    require_once '../../admin/model/corporateDetailsModel.php';
    require_once '../../admin/model/careerDetailsModel.php';
    require_once '../../admin/model/mediaContentModel.php';
    //require_once '../../admin/model/companyProfileModel.php';
  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
    $conn = $conn["conn"];

    // @ code For getting all business Names
    $getBusinessDetails = getBusinessDetails(NULL,'Status',$conn);    
    if (noError($getBusinessDetails)) {      
      $businessAllDetails = $getBusinessDetails['response'];
    } else {
      $businessAllDetails = $getBusinessDetails['errMsg'];
    }
    // printArr($businessAllDetails);

    // @ code to retrive all About Details
    $getAboutDetails = getAboutDetails(NULL,'status',$conn);
    if (noError($getAboutDetails)) {      
      $aboutAllDetails = $getAboutDetails['response'];
    } else {
      $aboutAllDetails = $getAboutDetails['errMsg'];
    }    
    // printArr($aboutAllDetails);

    // @ code to retrive all Alliances Details
    $getAlliancesDetails = getAlliancesDetails(NULL,'active',$conn);
    if(noError($getAlliancesDetails)){
        $getAlliancesDetails = $getAlliancesDetails["response"];
    }else{
        $returnArr = $getAlliancesDetails;
    }
    // printArr($getAlliancesDetails);

    // @ code to retrive all Career Details
    $getCareerDetails = getCareerDetails(NULL,'static',$conn);
    if(noError($getCareerDetails)){
        $getCareerAllDetails = $getCareerDetails["response"];
    }else{
        $returnArr = $getCareerDetails;
    }
    // printArr($getCareerAllDetails);

    // @ code to retrive all Career Details
    $getMediaContaint = getMediaDetails(NULL,'static',$conn);
    if(noError($getMediaContaint)){
        $getMediaAllDetails = $getMediaContaint["response"];
    }else{
        $returnArr = $getMediaContaint;
    }
    // printArr($getMediaAllDetails);

  }else{
    $returnArr = $conn;
    exit;
  }
  ?>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" type="text/css" href="../../lib/bootstrap/css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../lib/jquery.bxslider/jquery.bxslider.css"/>
  <link rel='stylesheet' type='text/css' href='../../css/protected.css'/>

  <!-- Include Unite Gallery core files -->
  <link rel='stylesheet' type='text/css' href='../../lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='../../lib/unitegallery/themes/default/ug-theme-default.css'/>
  <link rel="stylesheet" type="text/css" href="../../css/app.css?<?php echo date('l jS \of F Y h:i:s A'); ?>"/>



  <!-- main menu with search -->
  <link rel="stylesheet" href="../../css/reset.css"> <!-- CSS reset -->
  <link rel="stylesheet" href="../../css/style.css"> <!-- Resource style -->
  <link rel="stylesheet" href="../../css/homepage.css">
  <!-- main menu with search -->

<!-- banner -->
    <!-- Add custom CSS here -->
    <link href="../../css/full-slider.css" rel="stylesheet">
    <!-- banner -->
</head>

  <header class="cd-main-header ">
      <a class="cd-logo" href="#0"><img src="../../img/logo.jpg" alt="Logo"></a>
      <ul class="cd-header-buttons">
          <li>
              <a class="cd-search-trigger" href="#cd-search">
                  <!-- Search -->
                  <span></span>
                  <!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
              </a>
          </li>
          <li>
              <a class="cd-nav-trigger" href="#cd-primary-nav">
                  <!-- Menu --><span></span></a>
          </li>
      </ul>
      <!-- cd-header-buttons -->
  </header>
  <main class="cd-main-content">
      <!-- your content here -->
  </main>
  <div class="cd-overlay"></div>
  <!--    <div class="cd-overlay"></div>-->
  <nav class="cd-nav">
      <ul id="cd-primary-nav" class="cd-primary-nav is-fixed">
          <li class="logo-css">
              <a href="../../index.php"> &nbsp;<img src="../../img/home_logo.png" class="margin-btm"></a>
          </li>

         <!--  @ code for Business Categories And Subcategories. -->
          <li class="has-children">
              <a href="../../views/business/business.php" class="ab">Business</a>
              <a class="mobileMenuNext" href="#"></a>
              <ul class="cd-secondary-nav is-hidden">
                  <li class="go-back"><a href="#0">Menu</a></li>
                    <?php 
                      for ($i = 0; $i < count($businessAllDetails); $i++) { 

                        if ($businessAllDetails[$i]['pagename'] == 'manageServices.php') {
                          $businessUri =  $baseUrl.'views/businessCatagories/businessCatagories.php?id='.$businessAllDetails[$i]['id'];
                          $getBusinessCategoryDetails = getBusinessCatDetails(NULL,$businessAllDetails[$i]['id'],'status',$conn);
                          // printArr($getBusinessCategoryDetails);

                        } else if ($businessAllDetails[$i]['pagename'] == 'manageOffering.php'){                     
                          $businessUri =  $baseUrl.'views/offering/offering.php?id='.$businessAllDetails[$i]['id'];
                          $getBusinessCategoryDetails = getofferingDetails($businessAllDetails[$i]['id'],'status',$conn);
                        }

                        if (noError($getBusinessCategoryDetails)) {
                          $getBusinessCategoryAll = $getBusinessCategoryDetails['response'];
                          // printArr($getBusinessCategoryAll);
                        } else {
                           $getBusinessCategoryAll = $getBusinessCategoryDetails['errMsg'];
                        }
                    ?>
                  <li class="has-children">
                  
                      <a href="<?php echo $businessUri; ?>"><?php echo $businessAllDetails[$i]['business_name']; ?></a>
                      <a class="mobileMenuNext" href="#"></a>
                      <ul class="is-hidden">
                          <li class="go-back"><a href="#0"><?php echo $businessAllDetails[$i]['business_name']; ?></a></li>
                          <?php                          
                            for ($j = 0; $j < count($getBusinessCategoryAll); $j++) {
                          ?>
                          <?php 

                          if ($businessAllDetails[$i]['pagename'] == 'manageServices.php') {
                            $BusinessCatpageName = $getBusinessCategoryAll[$j]['category_name'];
                            $pageId = $getBusinessCategoryAll[$j]['id'];
                            $BusinessPageCatUri = $baseUrl.'views/ProjectManagement/project_management.php?id='.$pageId.'&businessId='.$businessAllDetails[$i]['id'];
                          } else {
                            $BusinessCatpageName = $getBusinessCategoryAll[$j]['name'];
                            $pageId = $getBusinessCategoryAll[$j]['id'];

                            switch ($getBusinessCategoryAll[$j]['pagename']) {
                              case 'manageluxuryResidences.php':
                                $BusinessPageCatUri = $baseUrl.'views/LuxaryResidancy/luxeryresidancy.php?id='.$pageId;
                                break;
                              case 'manageCommercial.php':
                                $BusinessPageCatUri = $baseUrl.'views/Commercial/commercial.php?id='.$pageId;
                                break;
                              case 'manageRetail.php':
                                $BusinessPageCatUri = $baseUrl.'views/retail/retail.php?id='.$pageId;
                                break;
                              case 'manageHospitality.php':
                                $BusinessPageCatUri = $baseUrl.'views/Hospitality/hospitality.php?id='.$pageId;
                                break;                              
                            }
                          }
                            ?>
                          
                          <li><a href="<?php echo $BusinessPageCatUri; ?>"><?php echo $BusinessCatpageName;?></a></li>
                          <?php } ?>
                          <!-- Retail page not found -->
                      </ul>
                  </li>
                  <?php } ?>                  
              </ul>
          </li>

          <!-- @ code for About Page, its category and Subcategories -->
          <li class="has-children">
              <a href="../about/about.php" class="ab">About</a>
              <a class="mobileMenuNext" href="#"></a>
              <ul class="cd-secondary-nav is-hidden">
                  <li class="go-back"><a href="#0">Menu</a></li>
                  <?php 
                    for ($k = 0; $k < count($aboutAllDetails); $k++ ) {
                      switch ($aboutAllDetails[$k]['pagename']) {
                        case 'manageSustainContent.php':                          
                          $aboutUri = $baseUrl.'views/sustainability/sustainability.php?id='.$aboutAllDetails[$k]['id'];
                          $aboutCatDetails = getSustainDetails(NULL,'status',$conn);
                          break;
                        case 'manageClientsDetails.php':
                          $aboutCatDetails['response']="";
                          $aboutCatDetails['errCode']="-1"; 
                          $aboutCatDetails['errMsg']="";
                          $aboutUri = $baseUrl.'views/clients/clients.php?id='.$aboutAllDetails[$k]['id'];
                          break;
                        case 'manageLeadership.php':
                          $aboutCatDetails['response']="";
                          $aboutCatDetails['errCode']="-1"; 
                          $aboutCatDetails['errMsg']="";
                          $aboutUri = $baseUrl.'views/leadership/leadership.php?id='.$aboutAllDetails[$k]['id'];
                          break;
                        case 'manageCorporateContent.php':
                          $aboutUri = $baseUrl.'views/corporateProfile/corporateProfile.php?id='.$aboutAllDetails[$k]['id'];
                          $aboutCatDetails = getCorporateDetails(NULL,$aboutAllDetails[$k]['id'],'status',$conn);
                          break; 
                      }
                      if (noError($aboutCatDetails)) {
                        $aboutCatAllDetails = $aboutCatDetails['response'];
                      }
                      // printArr($aboutCatAllDetails);
                  ?>
                  <li class="has-children">
                      <a href="<?php echo $aboutUri; ?>"><?php echo $aboutAllDetails[$k]['name']; ?></a>
                      <a class="mobileMenuNext" href="#"></a>
                      <ul class="is-hidden">
                          <li class="go-back"><a href="#0">Corporate Profile</a></li>
                          <?php                             
                              for ($y = 0; $y < count($aboutCatAllDetails) ; $y++) {    
                                switch ($aboutCatAllDetails[$y]['pagename']) {
                                  case 'manageForword.php':                          
                                    $aboutCategoryUri = $baseUrl."views/foreward/foreward.php?id=".$aboutCatAllDetails[$y]['id']."&urlId=".$aboutAllDetails[$k]['id'];
                                  break;
                                  case 'manageGrowthChronicle.php':                          
                                    $aboutCategoryUri = $baseUrl."views/GrowthChronicle/growth_chronicle.php?id=".$aboutCatAllDetails[$y]['id']."&urlId=".$aboutAllDetails[$k]['id'];
                                  break;
                                  case 'manageCompanyProfile.php':                          
                                    $aboutCategoryUri = $baseUrl."views/companyProfile/companyProfile.php?id=".$aboutCatAllDetails[$y]['id']."&urlId=".$aboutAllDetails[$k]['id'];
                                  break;
                                  case 'manageSustainCat.php':                          
                                    $aboutCategoryUri = $baseUrl."views/inBusiness/inBusiness.php?id=".$aboutCatAllDetails[$y]['id']."&urlId=".$aboutAllDetails[$k]['id'];
                                  break;
                                }
                          ?>
                          <li><a href="<?php echo $aboutCategoryUri; ?>"><?php echo $aboutCatAllDetails[$y]['name'];?></a></li>
                          <?php 
                              }   
                          ?>
                          

                          <!-- missing in pdf -->
                      </ul>
                  </li>
                  <?php } ?>
                  <!-- <li class="has-children">
                      <a href="../../views/leadership/leadership.php">Leadership</a>
                      <a class="mobileMenuNext" href="#"></a>
                      <ul class="is-hidden">
                          <li class="go-back"><a href="#0">Leadership</a></li>
                          <li><a href="#">Atul Chordia</a></li>
                          <li><a href="#0">Abhay Chordia</a></li>
                          <li><a href="#0">Sagar Chordia</a></li>
                          <li><a href="#0">Anand Sanghvi</a></li>
                          <li><a href="#0">Vivek Rachh</a></li>
                      </ul>
                  </li>
                  <li class="">
                      <a href="#">Clients</a>
                  </li>
                  <li class="has-children">
                      <a href="#">Sustainability</a>
                      <a class="mobileMenuNext" href="#"></a>
                      <ul class="is-hidden">
                          <li class="go-back"><a href="#0">Sustainability</a></li>
                          <li><a href="../inBusiness/in-business.php">In Business</a></li>
                          <li><a href="../csr/csr.php">CSR</a></li>
                      </ul>
                  </li> -->
              </ul>
          </li>


          <li class="has-children">
              <a href="../../views/alliances.php" class="ab">Alliances</a>
              <a class="mobileMenuNext" href="#"></a>
              <ul class="cd-secondary-nav is-hidden">
                  <li class="go-back"><a href="#0">Menu</a></li>
                  <?php 
                    for ($l = 0; $l < count($getAlliancesDetails); $l++ ) {
                  ?>
                  <li class="">
                      <a href="<?php echo $baseUrl.'views/brand/brand.php?id='.$getAlliancesDetails[$l]['id']; ?>"><?php echo $getAlliancesDetails[$l]['name']; ?></a>
                  </li>
                  <?php } ?>
              </ul>
          </li>



          <li class="has-children">
              <a href="../../views/career/career.php" class="ab">Careers</a>
              <a class="mobileMenuNext" href="#"></a>
              <ul class="cd-secondary-nav is-hidden">
                  <li class="go-back"><a href="#0">Menu</a></li>
                  <?php 
                    for ($m = 0; $m < count($getCareerAllDetails); $m++ ) {
                  ?>
                  <li class="">
                      <a href="<?php echo $baseUrl.'views/inCareer/inCareer.php?id='.$getCareerAllDetails[$m]['id'].'&career_id='.$getCareerAllDetails[$m]['id']; ?>"><?php echo $getCareerAllDetails[$m]['career_name']; ?></a>
                  </li>
                  <?php } ?>
              </ul>
          </li>

          <li class="has-children">
              <a href="../../views/media/media.php" class="ab">Media</a>
              <a class="mobileMenuNext" href="#"></a>
              <ul class="cd-secondary-nav is-hidden">
                  <li class="go-back"><a href="#0">Menu</a></li>
                  <?php 
                    for ($n = 0; $n < count($getMediaAllDetails); $n++ ) {
                      if ($getMediaAllDetails[$n]['media_type'] == 'managePressContent.php') {
                        $mediaUri = $baseUrl."views/press/press.php?id=".$getMediaAllDetails[$n]['id'];
                      } else if ($getMediaAllDetails[$n]['media_type'] == 'updateAwardContent.php') {
                        $mediaUri = $baseUrl."views/award/award.php?id=".$getMediaAllDetails[$n]['id'];
                      } else {
                        $mediaUri = '#';
                      }
                  ?>
                  <li class="">
                      <a href="<?php echo $mediaUri; ?>"><?php echo $getMediaAllDetails[$n]['name'];?></a>
                      <ul class="is-hidden">
                          <li class="go-back"><a href="#0"><?php echo $getMediaAllDetails[$n]['name'];?></a></li>
                          <li><a href="<?php echo $mediaUri.'&type=PDF'?>">PDF</a></li>
                          <li><a href="<?php echo $mediaUri.'&type=VIDEO'?>">Video</a></li>
                      </ul>
                  </li>
                  <?php } ?>
                  <!-- <li class="">
                      <a href="../../views/award/award.php">Awards</a>
                      <ul class="is-hidden">
                          <li class="go-back"><a href="#0">Awards</a></li>
                          <li><a href="#">PDF</a></li>
                          <li><a href="#0">Video</a></li>
                      </ul>
                  </li> -->
              </ul>
          </li>
          <li><a href="../../views/contact/contact.php">contact</a></li>
      </ul>
      <!-- primary-nav -->
  </nav>
  <!-- cd-nav -->
  <!-- header-->
