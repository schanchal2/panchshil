<?php

//start session
session_start();

 require_once '../../admin/utilities/config.php';
require_once '../../admin/utilities/dbUtils.php';
require_once '../../admin/utilities/utilities.php';
require_once '../../admin/utilities/errorMap.php';
require_once '../../admin/model/globalSettingModel.php';
require_once '../../admin/model/Contact_detail_Model.php';
require_once '../../admin/model/businessCategoryDetailsModel.php';
require_once "../../admin/model/businessDetailsModel.php";


$returnArr = array();
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    //$featureType = "home";
    $globalContent = getGlobalSettingContent(NULL,$conn);
    $ContactDetail = getAllContact_Detail($conn);
    $businessCatInfo = getBusinessCatDetails(NULL,1,'status',$conn);
    
    $businessDetails = getBusinessDetails($id,'status',$conn);
    
    if(noError($businessDetails)){
    	 $businessDetails = $businessDetails["response"];
    	//printArr( $businessDetails);
    }else{
    		$returnArr =  $businessDetails;
    }
    if(noError($businessCatInfo)){
    	 $businessCatInfo = $businessCatInfo["response"];

    }else{
    		$returnArr =  $businessCatInfo;
    }
  	if(noError($ContactDetail)){
    		$ContactDetail = $ContactDetail["response"];

    }else{
    		$returnArr =  $ContactDetail;
    }
    if(noError($globalContent)){
              $globalContent =  $globalContent["response"];

    }else{
            $returnArr =  $globalContent;
    }

}else{
            $returnArr = $conn;
            exit;
}
?>
<footer>
  <div class="container footer">
	<!-- footer-quick-link -->
		<div class="row footer-quick-link-hr-border">
			<?php for($i=0; $i < 2 && $i < count($businessDetails); $i++) {
				?>
			<div class="col-md-2 col-sm-12">				
				<div class="footer-quick-link">					
					<h5><a href="../businessCatagories/businessCatagories.php?id=<?php echo $businessDetails[$i]['id'];?>"><?php echo $businessDetails[$i]['business_name']; ?></a></h5>
					<ul>
						<?php 
							$businessCatAllInfo = getBusinessCatDetails(NULL, $businessDetails[$i]['id'],'status', $conn);
							$businessCatAllInfo = $businessCatAllInfo['response'];
							
							for($j=0; $j<count($businessCatAllInfo); $j++) {
						?>
						<li><a href="../ProjectManagement/project_management.php?id=<?php echo $businessCatAllInfo[$j]['id'];?>&businessId=<?php echo $businessDetails[$i]['id']; ?>"><i class="fa fa-long-arrow-right" aria-hidden="true"></i><?php echo $businessCatAllInfo[$j]['category_name']; ?></a></li>
						
						<?php } ?>
					</ul>					
				</div>
				
			</div>
			<?php } ?>
			<div class="col-md-2 col-sm-12">
				<div class="footer-quick-link">
					<h5><a href="#">corporate profile</a></h5>
					<ul>
						<li><a href="../foreward/foreward.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> foreword</a></li>
						<li><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> company profile</a></li>
						<li><a href="../GrowthChronicle/growth_chronicle.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> growth chronicle</a></li>
						<li><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> group philosophy</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-2 col-sm-12">
				<div class="footer-quick-link">
					<h5><a href="../media/media.php">Media</a></h5>
					<ul>
						<li><a href="../press/press.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> press</a></li>
						<li><a href="../award/award.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Awards</a></li>
					</ul>
					<br/>
					<h5><a href="#">downloads</a></h5>
				</div>
			</div>
			<div class="col-md-2 col-sm-12">
				<div class="footer-quick-link">
					<h5><a href="#">career</a></h5>
					<ul>
						<li><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> HR align</a></li>
						<li><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> PEP</a></li>
					</ul>
					<br/>
					<h5><a href="../leadership/leadership.php">leadership</a></h5>
				</div>
			</div>
			<div class="col-md-2 col-sm-12">
				<div class="footer-quick-link">
					<h5><a href="#">sustainability</a></h5>
					<ul>
						<li><a href="../business/business.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> in business</a></li>
						<li><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> CSR</a></li>
					</ul>
					<br/>
					<h5><a href="../clients/clients.php">clients</a></h5>
				</div>
			</div>
		</div>
	<!-- footer-quick-link end -->
	<!-- contact, location map , follow us -->
		<div class="row">
			<div class="col-md-4 col-sm-12 contact">
				 <h5 class="footer-header-padding">CONTACT</h5>
				 <div class="row">
				 <?php for($i=0; $i < 2; $i++) { ?>
				 	<div class="col-sm-6">
				 		<p class="office-location"><?php echo $ContactDetail[$i]['location']; ?></p>
				 		<p class="office-address"><?php echo $ContactDetail[$i]['address']; ?></p>
				 	</div>
				 <?php } ?>
				 </div>
				 <br/><br/>
				 <div class="row">
				 <?php for($i=0; $i < 2; $i++) {?>
				 	<div class="col-sm-6 contact-no">
				 		<p>T :<?php echo $ContactDetail[$i]['phoneno1']; ?> </p>
				 		<p>F : <?php echo $ContactDetail[$i]['fax']; ?></p>
				 	</div>
				 <?php } ?>
				 </div>
			</div>
			<div class="col-md-4 col-sm-12 location-map">
				<h5 class="footer-header-padding">LOCATION ON MAP</h5>
				<div class="location-content">
					<span>Select Property: </span>
					<select class="location-dropdown-list">
						<option selected>Property Name</option>
						<option>Mumbai</option>
						<option>Pune</option>
					</select>
				</div>
				<img src="../../img/worldMap.png" class="img-responsive">
			</div>
			<div class="col-md-4 col-sm-12 follow-us">
				<h5 class="footer-header-padding">FOLLOW US ON</h5>
				<ul class="social-icon">
					<li><a href="<?php echo $globalContent[0]['facebookLink'];  ?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
					<li><a href="<?php echo $globalContent[0]['twitterLink'];  ?>" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
					<li><a href="<?php echo $globalContent[0]['linkedinLink']; ?>" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
					<li><a href="<?php echo $globalContent[0]['googleLink']; ?>" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
					<li><a href="<?php echo $globalContent[0]['youtubeLink']; ?>" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
				</ul>
				<br/>
				<div class="login-signup">
					<a href="#"><img src="../../img/login-signup.png"/> &nbsp; LOGIN / SIGN UP</a>
				</div>
			</div>
		</div>
	<!-- contact, location map , follow us end -->
	</div><!-- container -->
	<!-- footer bottom -->
	<?php if(count($businessCatInfo) < 4){?>
		<div class="container-fluid">
			<div class="row">
			    <div class="col-md-3 col-sm-6 col-xs-12 footer-bottom">
				    <div class="footer-bottom-img">
					  <img src="<?php echo $rootUrl.$footerImage1;?>">
					  <div class="footer-bottom-overlay">
						   <a href="#" class="btn btn-default"><span class="plus-icon"><i class="fa fa-plus" aria-hidden="true"></i></span><span class="btn-text">Residential</span></a>
					  </div>
				    </div>
			    </div>
		
			    <div class="col-md-3 col-sm-6 col-xs-12 footer-bottom">
				    <div class="footer-bottom-img">
					  <img src="<?php echo $rootUrl.$footerImage2;?>">
					  <div class="footer-bottom-overlay">
						   <a href="#" class="btn btn-default"><span class="plus-icon"><i class="fa fa-plus" aria-hidden="true"></i></span><span class="btn-text">Commercial</span></a>
					  </div>
				    </div>
			    </div>
		
			    <div class="col-md-3 col-sm-6 col-xs-12 footer-bottom">
				    <div class="footer-bottom-img">
					  <img src="<?php echo $rootUrl.$footerImage3;?>">
					  <div class="footer-bottom-overlay">
						   <a href="#" class="btn btn-default"><span class="plus-icon"><i class="fa fa-plus" aria-hidden="true"></i></span><span class="btn-text">Hospitality</span></a>
					  </div>
				    </div>
			    </div>
			
			    <div class="col-md-3 col-sm-6 col-xs-12 footer-bottom">
				    <div class="footer-bottom-img">
					  <img src="<?php echo $rootUrl.$footerImage4;?>">
					  <div class="footer-bottom-overlay">
						   <a href="#" class="btn btn-default"><span class="plus-icon"><i class="fa fa-plus" aria-hidden="true"></i></span><span class="btn-text">Retail</span></a>
					  </div>
				    </div>
			    </div>			
			</div>
		</div>
		<?php }else {
		?>
		<div class="container-fluid">
			<div class="row">
				<?php for($i=0; $i < 4; $i++){ ?>
					<div class="col-md-3 col-sm-6 col-xs-12 footer-bottom">
						<div class="footer-bottom-img">
							<img src="<?php echo $rootUrl.'uploads/'.$businessCatInfo[$i]["category_footer_image"]; ?>">
							<div class="footer-bottom-overlay">
								<a href="#" class="btn btn-default"><span class="plus-icon"><i class="fa fa-plus plus-icon-change" aria-hidden="true"></i></span><span class="btn-text"><?php echo $businessCatInfo[$i]['category_name']; ?></span></a>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>			
		</div>
		<?php } ?>
	<!-- container fluid -->
	<!-- footer bottom end -->
	<!-- copy right -->
	<div class="copy-right">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<p>Privacy Policy Terms & Conditions</p>
				</div>
				<div class="col-sm-6">
					<p class="pull-right">Site designed & maintained by <a href="#" style="color: #fff; text-decoration: none;">Neon Tree</a></p>
				</div>
			</div>
		</div>
	</div>
	<!-- copy right end -->
<!-- Footer end -->
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="../../lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../js/app.js"></script>
<script type="text/javascript" src="../../lib/jquery.bxslider/jquery.bxslider.min.js"></script>

<!-- Include Unite Gallery core files -->
<script src='../../lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='../../lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
    	 var data=$(this).find(".text").html();
    		$(".img-hover-text").hide().stop().html(data).fadeIn();

    	}, function() {
    		$(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
	   jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end
   });
</script>

<script type="text/javascript" src="../../js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
	$(function () {
	var filterList = {
		init: function () {
			$('#KeyAmenities').mixItUp({
				selectors: {
					target: '.KeyAmenities-portfolio',
					filter: '.KeyAmenities-list'
				},
				load: {
					filter: '.parking, .upper_deck, .lower_deck'
				}
			});
		}
	};
	filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end
</script>

</body>
</html>
