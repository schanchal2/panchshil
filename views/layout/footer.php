<?php

//start session
session_start();

 // require_once '../../admin/utilities/config.php';
// require_once '../../admin/utilities/dbUtils.php';
// require_once '../../admin/utilities/utilities.php';
// require_once '../../admin/utilities/errorMap.php';
require_once '../../admin/model/globalSettingModel.php';
require_once '../../admin/model/Contact_detail_Model.php';
require_once '../../admin/model/businessCategoryDetailsModel.php';
require_once '../../admin/model/propertyContentModel.php';
// require_once "../../admin/model/businessDetailsModel.php";

// printArr($aboutAllDetails);

$returnArr = array();
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    //$featureType = "home";
    $globalContent = getGlobalSettingContent(NULL,$conn);

    $ContactDetail = getAllContact_Detail($conn);

    $businessCatInfo = getBusinessCatDetails(NULL,1,'status',$conn);
    
    $businessDetailsFooter = $businessAllDetails;

    if(noError($businessCatInfo)){
       $businessCatInfo = $businessCatInfo["response"];
       // printArr( $businessCatInfo);
    }else{
        $returnArr =  $businessCatInfo;
    }

    if(noError($ContactDetail)){
        $ContactDetail = $ContactDetail["response"];
        // printArr( $ContactDetail);
    }else{
        $returnArr =  $ContactDetail;
    }

    if(noError($globalContent)){
              $globalContent =  $globalContent["response"];
              // printArr( $globalContent);
    }else{
            $returnArr =  $globalContent;
    }
}else{
            $returnArr = $conn;
            exit;
}
?>
<footer>
  <div class="container footer">
<div class="footer-auto-center">
<?php for($i=0; $i < 2 && $i < count($businessDetailsFooter); $i++) {

  $BusspageId = $businessDetailsFooter[$i]['id'];
  if ($businessDetailsFooter[$i]['pagename'] == "manageServices.php") {
    $BusinessPageCatUrlFooter = $baseUrl."views/businessCatagories/businessCatagories.php?id=".$BusspageId;
    $businessCatAllInfoFooter = getBusinessCatDetails(NULL,$businessAllDetails[$i]['id'],'status',$conn);
  } else {
    $BusinessPageCatUrlFooter = $baseUrl."views/offering/offering.php?id=".$BusspageId;
    $businessCatAllInfoFooter = getofferingDetails($businessAllDetails[$i]['id'],'status',$conn);
  }
  // $businessCatAllInfoFooter = getBusinessCatDetails(NULL, $businessDetailsFooter[$i]['id'],'status', $conn);
  $businessCatAllInfoFooter = $businessCatAllInfoFooter['response'];
        ?>
    <div class="block">
      <div class="footer-quick-link">     
             <h5><a href="<?php echo $BusinessPageCatUrlFooter;?>"><?php echo $businessDetailsFooter[$i]['business_name']; ?></a></h5>
             <ul>
            <?php 
            
              // printArr($businessCatAllInfoFooter);

              
              for($j=0; $j<count($businessCatAllInfoFooter); $j++) { 

                if ($businessCatAllInfoFooter[$j]['pagename'] == 'manageBusinessSubCat.php') {
                  $BusinessCatpageName = $businessCatAllInfoFooter[$j]['category_name'];
                  $CatpageId = $businessCatAllInfoFooter[$j]['id'];
                  $BusinessPageCatUri = $baseUrl.'views/ProjectManagement/project_management.php?id='.$CatpageId.'&businessId='.$businessCatAllInfoFooter[$j]['id'];
                } else {
                  $BusinessCatpageName = $businessCatAllInfoFooter[$j]['name'];
                  $CatpageId = $businessCatAllInfoFooter[$j]['id'];

                  switch ($businessCatAllInfoFooter[$j]['pagename']) {
                    case 'manageluxuryResidences.php':
                    $BusinessPageCatUri = $baseUrl.'views/LuxaryResidancy/luxeryresidancy.php?id='.$CatpageId;
                    break;
                    case 'manageCommercial.php':
                    $BusinessPageCatUri = $baseUrl.'views/Commercial/commercial.php?id='.$CatpageId;
                    break;
                    case 'manageRetail.php':
                    $BusinessPageCatUri = $baseUrl.'views/retail/retail.php?id='.$CatpageId;
                    break;
                    case 'manageHospitality.php':
                    $BusinessPageCatUri = $baseUrl.'views/Hospitality/hospitality.php?id='.$CatpageId;
                    break;                              
                  }
                }
            ?>
               <li><a href="<?php echo $BusinessPageCatUri;?>"><i class="fa fa-long-arrow-right" aria-hidden="true"></i><?php echo $BusinessCatpageName;?></a></li>
               <?php } ?>
              </ul>
           </div>

    </div>
    <?php }?>


<?php 

  // @ code to retrive all About Details
    $aboutAllDetailsFooter = $aboutAllDetails;
    
    // printArr($aboutAllDetailsFooter);
    $aboutCatDetailsFooter = array(); 
       
    for ($n=0; $n < count($aboutAllDetailsFooter) && $n < 1 ; $n++) { 
      $aboutUriFooter = "";    
      switch ($aboutAllDetailsFooter[$n]['pagename']) {
        case 'manageSustainContent.php':
        $aboutUriFooter = $baseUrl.'views/sustainability/sustainability.php?id='.$aboutAllDetailsFooter[$n]['id'];
        $aboutCatDetailsFooter = getSustainDetails(NULL,'status',$conn);
        break;
        case 'manageClientsDetails.php':
        $aboutCatDetailsFooter['response']="";
        $aboutCatDetailsFooter['errCode']="-1"; 
        $aboutCatDetailsFooter['errMsg']=""; 
        $aboutUriFooter = $baseUrl.'views/clients/clients.php?id='.$aboutAllDetailsFooter[$n]['id'];
        break;
        case 'manageLeadership.php':
        $aboutCatDetailsFooter['response']="";
        $aboutCatDetailsFooter['errCode']="-1"; 
        $aboutCatDetailsFooter['errMsg']="";                        
        $aboutUriFooter = $baseUrl.'views/leadership/leadership.php?id='.$aboutAllDetailsFooter[$n]['id'];
        break;
        case 'manageCorporateContent.php':                       
        $aboutUriFooter = $baseUrl.'views/corporateProfile/corporateProfile.php?id='.$aboutAllDetailsFooter[$n]['id'];
        $aboutCatDetailsFooter = getCorporateDetails(NULL,$aboutAllDetailsFooter[$n]['id'],'status',$conn);
        break; 
      }
      if (noError($aboutCatDetailsFooter) ) {
        $aboutCatAllDetailsFooter = $aboutCatDetailsFooter['response'];
      }
      // printArr($aboutCatAllDetailsFooter);
    
?>
    <div class="block"> 
      <div class="footer-quick-link">
        <h5>
          <a href="<?php echo $aboutUriFooter; ?>">
            <?php echo $aboutAllDetailsFooter[$n]['name'];?>
          </a>
        </h5>

        
        <ul>
        <?php
        for ($p=0; $p < count($aboutCatAllDetailsFooter) ; $p++) { 
          switch ($aboutCatAllDetailsFooter[$p]['pagename']) {
            case 'manageForword.php':                          
            $aboutCategoryFooterUri = $baseUrl."views/foreward/foreward.php?id=".$aboutCatAllDetailsFooter[$p]['id']."&urlId=".$aboutAllDetailsFooter[$n]['id'];
            break;
            case 'manageGrowthChronicle.php':                          
            $aboutCategoryFooterUri = $baseUrl."views/GrowthChronicle/growth_chronicle.php?id=".$aboutCatAllDetailsFooter[$p]['id']."&urlId=".$aboutAllDetailsFooter[$n]['id'];
            break;
            case 'manageCompanyProfile.php':                          
            $aboutCategoryFooterUri = $baseUrl."views/companyProfile/companyProfile.php?id=".$aboutCatAllDetailsFooter[$p]['id']."&urlId=".$aboutAllDetailsFooter[$n]['id'];
            break;
            case 'manageSustainCat.php':                          
            $aboutCategoryFooterUri = $baseUrl."views/inBusiness/inBusiness.php?id=".$aboutCatAllDetailsFooter[$p]['id']."&urlId=".$aboutAllDetailsFooter[$n]['id'];
            break;
          }
        ?>
          <li>
            <a href="<?php echo $aboutCategoryFooterUri;?>">
              <i class="fa fa-long-arrow-right" aria-hidden="true"></i><?php echo $aboutCatAllDetailsFooter[$p]['name'];?>
            </a>
          </li>
          <?php }?>
          
        </ul>
        
      </div>
    </div>
  <?php }?>


  


    <div class="block">
      <div class="footer-quick-link">
        <h5>
          <a href="../../views/media/media.php">Media</a>
        </h5>
        
        <ul>
          <?php 
            $mediaCOntaints = $getMediaAllDetails; 
            // printArr($mediaCOntaints);

            for ($q = 0; $q < count($mediaCOntaints); $q++) {
              if ($mediaCOntaints[$q]['media_type'] == 'managePressContent.php') {
                $mediaFooterUri = $baseUrl."views/press/press.php?id=".$mediaCOntaints[$q]['id'];
              } else if ($mediaCOntaints[$q]['media_type'] == 'updateAwardContent.php') {
                $mediaFooterUri = $baseUrl."views/award/award.php?id=".$mediaCOntaints[$q]['id'];
              } else {
                $mediaFooterUri = '#';
              }

          ?>
          <li>
            <a href="<?php echo $mediaFooterUri; ?>">
              <i class="fa fa-long-arrow-right" aria-hidden="true"></i><?php echo $mediaCOntaints[$q]['name'];?>
            </a>
          </li>
          <?php } ?>
          <!-- <li>
            <a href="../../views/award/award.php">
              <i class="fa fa-long-arrow-right" aria-hidden="true"></i>Awards
            </a>
          </li> -->
        </ul>
        <br>
        <h5>
          <a href="../../views/downloads/downloads.php">Downloads</a>
        </h5>
      </div>
    </div>


    <div class="block">
      <div class="footer-quick-link">
        <h5>
        <a href="../../views/career/career.php">career</a>
        </h5>
        <ul>
          <?php 
            for ($r = 0; $r < count($getCareerAllDetails); $r++ ) {
          ?>
          <li>
            <a href="<?php echo $baseUrl.'views/inCareer/inCareer.php?id='.$getCareerAllDetails[$r]['id'].'&career_id='.$getCareerAllDetails[$r]['id']; ?>">
              <i class="fa fa-long-arrow-right" aria-hidden="true"></i><?php echo $getCareerAllDetails[$r]['career_name']; ?>
            </a>
          </li>
          <?php } ?>
          <!-- <li>
            <a href="../../views/pep/pep.php">
              <i class="fa fa-long-arrow-right" aria-hidden="true"></i>PEP
            </a>
          </li> -->
        </ul>
        <br>
        <?php 
          for ($n; $n < count($aboutAllDetailsFooter) && $n < 2 ; $n++) { 
      $aboutUriFooter = "";

                      // echo $aboutAllDetailsFooter[$n]['pagename']."<br>";                  
      switch ($aboutAllDetailsFooter[$n]['pagename']) {
        case 'manageSustainContent.php':
                      
        $aboutUriFooter = $baseUrl.'views/sustainability/sustainability.php?id='.$aboutAllDetailsFooter[$n]['id'];
        $aboutCatDetailsFooter = getSustainDetails(NULL,'status',$conn);
        // printArr($aboutCatDetailsFooter);
        break;
        case 'manageClientsDetails.php':
        $aboutCatDetailsFooter['response']="";
        $aboutCatDetailsFooter['errCode']="-1"; 
        $aboutCatDetailsFooter['errMsg']="";  
        // unset($aboutCatDetailsFooter); 
                       
        $aboutUriFooter = $baseUrl.'views/clients/clients.php?id='.$aboutAllDetailsFooter[$n]['id'];
        // NULL; 
        // unset($aboutCatDetailsFooter);    
        break;
        case 'manageLeadership.php':
        $aboutCatDetailsFooter['response']="";
        $aboutCatDetailsFooter['errCode']="-1"; 
        $aboutCatDetailsFooter['errMsg']=""; 
                       
        $aboutUriFooter = $baseUrl.'views/leadership/leadership.php?id='.$aboutAllDetailsFooter[$n]['id'];
        // $aboutCatDetailsFooter = NULL;    
        // unset($aboutCatDetailsFooter); 
        break;
        case 'manageCorporateContent.php':
                       
        $aboutUriFooter = $baseUrl.'views/corporateProfile/corporateProfile.php?id='.$aboutAllDetailsFooter[$n]['id'];
        $aboutCatDetailsFooter = getCorporateDetails(NULL,$aboutAllDetailsFooter[$n]['id'],'status',$conn);
                          // printArr($aboutCatDetailsFooter);
        break; 
      }
      if (noError($aboutCatDetailsFooter) ) {
        $aboutCatAllDetailsFooter = $aboutCatDetailsFooter['response'];
      }
      // printArr($aboutCatAllDetailsFooter);
        ?>
        <h5>
        <a href="<?php echo $aboutUriFooter; ?>"><?php echo $aboutAllDetailsFooter[$n]['name'];?></a>
        </h5>
        <?php } ?>
      </div>
    </div>
    <div class="block">
      <div class="footer-quick-link">
      <?php 
          for ($n; $n < count($aboutAllDetailsFooter) && $n < ($n+2) ; $n++) { 
            $aboutUriFooter = "";              
            switch ($aboutAllDetailsFooter[$n]['pagename']) {
              case 'manageSustainContent.php':
                            
              $aboutUriFooter = $baseUrl.'views/sustainability/sustainability.php?id='.$aboutAllDetailsFooter[$n]['id'];
              $aboutCatDetailsFooter = getSustainDetails(NULL,'status',$conn);
              break;

              case 'manageClientsDetails.php':
              $aboutCatDetailsFooter['response']="";
              $aboutCatDetailsFooter['errCode']="-1"; 
              $aboutCatDetailsFooter['errMsg']="";                
              $aboutUriFooter = $baseUrl.'views/clients/clients.php?id='.$aboutAllDetailsFooter[$n]['id'];
              break;

              case 'manageLeadership.php':
              $aboutCatDetailsFooter['response']="";
              $aboutCatDetailsFooter['errCode']="-1"; 
              $aboutCatDetailsFooter['errMsg']="";                              
              $aboutUriFooter = $baseUrl.'views/leadership/leadership.php?id='.$aboutAllDetailsFooter[$n]['id'];
              break;

              case 'manageCorporateContent.php':                             
              $aboutUriFooter = $baseUrl.'views/corporateProfile/corporateProfile.php?id='.$aboutAllDetailsFooter[$n]['id'];
              $aboutCatDetailsFooter = getCorporateDetails(NULL,$aboutAllDetailsFooter[$n]['id'],'status',$conn);
              break; 
            }
            if (noError($aboutCatDetailsFooter) ) {
              $aboutCatAllDetailsFooter = $aboutCatDetailsFooter['response'];
            }
            // printArr($aboutCatAllDetailsFooter);
        ?>
        <h5><a href="<?php echo $aboutUriFooter; ?>"><?php echo $aboutAllDetailsFooter[$n]['name'];?></a></h5>
        <ul>
        <?php
          if (!empty($aboutCatAllDetailsFooter)) {
            for ($x = 0; $x < count($aboutCatAllDetailsFooter) ; $x++) { 
              switch ($aboutCatAllDetailsFooter[$x]['pagename']) {
                case 'manageForword.php':                          
                $aboutCategoryFooterUri = $baseUrl."views/foreward/foreward.php?id=".$aboutCatAllDetailsFooter[$x]['id']."&urlId=".$aboutAllDetailsFooter[$n]['id'];
                break;
                case 'manageGrowthChronicle.php':                          
                $aboutCategoryFooterUri = $baseUrl."views/GrowthChronicle/growth_chronicle.php?id=".$aboutCatAllDetailsFooter[$x]['id']."&urlId=".$aboutAllDetailsFooter[$n]['id'];
                break;
                case 'manageCompanyProfile.php':                          
                $aboutCategoryFooterUri = $baseUrl."views/companyProfile/companyProfile.php?id=".$aboutCatAllDetailsFooter[$x]['id']."&urlId=".$aboutAllDetailsFooter[$n]['id'];
                break;
                case 'manageSustainCat.php':                          
                $aboutCategoryFooterUri = $baseUrl."views/inBusiness/inBusiness.php?id=".$aboutCatAllDetailsFooter[$x]['id']."&urlId=".$aboutAllDetailsFooter[$n]['id'];
                break;
              }
        ?>
          <li><a href="<?php echo $aboutCategoryFooterUri; ?>"><i class="fa fa-long-arrow-right" aria-hidden="true"></i><?php echo $aboutCatAllDetailsFooter[$x]['name'];?></a></li>
        
        <?php 
            }
          }
        ?>
          
        </ul>
        <br>
        <?php 
          } 
        ?>
      </div>
    </div>
</div>


	<!-- footer-quick-link end -->
	<!-- contact, location map , follow us -->
<div class="row">
  <div class="container">
    <div class="col-md-4 col-sm-12 contact padding2">
      <h5 class="footer-header-padding">CONTACT</h5>
      <div class="row">
      <?php for($i=0; $i < 2; $i++) { ?>
        <div class="col-sm-6">
          <p class="office-location"><?php echo $ContactDetail[$i]['location']; ?></p>
          <p class="office-address"><?php echo $ContactDetail[$i]['address']; ?></p>
        </div>
      <?php } ?>
      </div>
      <br/><br/>
      <div class="row">
      <?php for($i=0; $i < 2; $i++) {?>
        <div class="col-sm-6 contact-no">
          <p>T :<?php echo $ContactDetail[$i]['phoneno1']; ?></p>
          <p>F : <?php echo $ContactDetail[$i]['fax']; ?></p>
        </div> 
      <?php } ?>       
      </div>
    </div>
    <div class="col-md-4 col-sm-12 location-map">
      <h5 class="footer-header-padding">LOCATION ON MAP</h5>
      <div class="location-content">
        <span>Select Property: </span>
        <select class="location-dropdown-list" onchange="locationonMap('<?php echo $baseUrl."views/contact/contact.php";?>')">
          <option selected>Property Name</option>
          <option>Mumbai</option>
          <option>Pune</option>
        </select>
      </div><br>
      <img src="../../img/worldMap.png" class="img-responsive">
    </div>
    <div class="col-md-4 col-sm-12 follow-us">
      <h5 class="footer-header-padding">FOLLOW US ON</h5>
      <ul class="social-icon">
        <li><a href="<?php echo $globalContent[0]['facebookLink'];  ?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
        <li><a href="<?php echo $globalContent[0]['twitterLink'];  ?>" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
        <li><a href="<?php echo $globalContent[0]['googleLink'];  ?>" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
        <li><a href="<?php echo $globalContent[0]['linkedinLink'];  ?>" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
        <li><a href="<?php echo $globalContent[0]['youtubeLink'];  ?>" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
      </ul>
      <br/><br/>
      <div>
        <img style="vertical-align:middle" src="../../img/login-signup.png">
        <span style="font-size:14px;"> LOGIN / SIGN UP</span>
      </div>
    </div>
  </div>
</div>
	<!-- contact, location map , follow us end -->
	</div><!-- container -->
	<!-- footer bottom -->
<?php $getBusinessOfferingId = getofferingDetailsByPageName('manageOffering.php','Staic',$conn);
// printArr($getBusinessOfferingId['response'][0]);

$GetOfferingDetails = getofferingDetails($getBusinessOfferingId['response'][0]['id'],'Static',$conn);
$GetOfferingDetails = $GetOfferingDetails['response'];
// printArr($GetOfferingDetails);

?>
  <div class="sliderside">
<?php

if (count($GetOfferingDetails) <= 0) {

?>
      <!-- ngRepeat: cat in categories -->
      <div class="flexside">
          <div class="imgslider">
              <img src="../../img/residential.jpg">
              <!-- <div class="black-overlay"></div> -->
          </div>
          <div class="relativetxt">
              <i class="educa caticons"></i>
              <!-- <p class="ng-binding">Residential</p> -->
              <div class="relativetxt__text">
                  <p class="relativetxt__text__heading"> Luxury  Residences </p>
                  <ul class="relativetxt__text__projectList">
                      <li> <a href="#"> - Trump Towers Pune     </a> </li>
                      <li> <a href="#"> - yoopune By Philippes Stark     </a> </li>
                      <li> <a href="#"> - YOOVILLAS     </a> </li>
                      <li> <a href="#"> - The Address     </a> </li>
                      <li> <a href="#"> - Panchshil Towers     </a> </li>
                      <li> <a href="#"> - EON Waterfront     </a> </li>
                  </ul>
                  <div class="clearfix"></div>
              </div>
          </div>
      </div>
      <!-- end ngRepeat: cat in categories -->
      <div class="flexside">
          <div class="imgslider">
              <img src="../../img/commercial.jpg">
              <!-- <div class="black-overlay"></div> -->
          </div>
          <div class="relativetxt">
              <i class="child caticons"></i>
              <!-- <p class="ng-binding">commercial</p> -->
              <div class="relativetxt__text ">
                  <p class="relativetxt__text__heading"> Commercial </p>
                  <ul class="relativetxt__text__projectList">
                      <li> <a href="#"> - Express Towers </a> </li>
                      <li> <a href="#"> - Word Trade Center Pune </a> </li>
                      <li> <a href="#"> - Futura </a> </li>
                      <li> <a href="#"> - EON Free Zone </a> </li>
                      <li> <a href="#"> - Cummins India Campus </a> </li>
                      <li> <a href="#"> - Suzlon One Earth </a> </li>
                  </ul>
                  <div class="clearfix"></div>
              </div>
          </div>
      </div>
      <!-- end ngRepeat: cat in categories -->
      <div class="flexside">
          <div class="imgslider">
              <img src="../../img/hospitality-f.jpg">
              <!-- <div class="black-overlay"></div> -->
          </div>
          <div class="relativetxt">
              <i class="healt caticons"></i>
              <!-- <p class="ng-binding">hospitality</p> -->
              <div class="relativetxt__text ">
                  <p class="relativetxt__text__heading"> Hospitality </p>
                  <ul class="relativetxt__text__projectList">
                      <li> <a href="#"> - The Ritz Carlton </a> </li>
                      <li> <a href="#"> - JW Marriot Pune </a> </li>
                      <li> <a href="#"> - Courtyard by Marriot Pune Hinjewadi </a> </li>
                      <li> <a href="#"> - DoubeTree by Hilton </a> </li>
                      <li> <a href="#"> - Marriot Suites </a> </li>
                      <li> <a href="#"> - Oakwood Residence  </a> </li>
                  </ul>
                  <div class="clearfix"></div>
              </div>
          </div>
      </div>
      <!-- end ngRepeat: cat in categories -->
      <div class="flexside">
          <div class="imgslider">
              <img src="../../img/retail.jpg">
              <!-- <div class="black-overlay"></div> -->
          </div>
          <div class="relativetxt">
              <i class="envir caticons"></i>
              <!-- <p class="ng-binding">retail</p> -->
              <div class="relativetxt__text ">
                  <p class="relativetxt__text__heading"> Retail </p>
                  <ul class="relativetxt__text__projectList">
                      <li> <a href="#"> - The Pavillion </a></li>
                      <li> <a href="#"> - Balewadi High Street </a></li>
                      <li> <a href="#"> - ICC High Street </a></li>

                  </ul>
                  <div class="clearfix"></div>
              </div>
          </div>
      </div>
       <?php } else {

    for ($w = 0; $w < count($GetOfferingDetails); $w++) {
      $getOfferingCatDetails = getPropertyDetailsById($GetOfferingDetails[$w]['id'],$conn);
      $getOfferingCatDetails = $getOfferingCatDetails['response'];
      // printArr($getOfferingCatDetails);
      // manageluxuryResidences.php
      // manageCommercial.php
      // manageHospitality.php
      // manageRetail.php

      

  ?>



      <!-- ngRepeat: cat in categories -->
      <div class="flexside">
          <div class="imgslider">
              <img src="<?php echo $rootUrl.'uploads/'.$GetOfferingDetails[$w]['footer_image']; ?>">
              <!-- <div class="black-overlay"></div> -->
          </div>
          <div class="relativetxt">
              <i class="educa caticons"></i>
              <!-- <p class="ng-binding">Residential</p> -->
              <div class="relativetxt__text">
                  <p class="relativetxt__text__heading"> <?php echo $GetOfferingDetails[$w]['name']; ?> </p>
                  <ul class="relativetxt__text__projectList">
                  <?php 
                    for ($s = 0; $s < count($getOfferingCatDetails) && $s <= 4; $s++) {
                      switch ($GetOfferingDetails[$w]['pagename']) {
                        case 'manageluxuryResidences.php':                          
                        $offeringCatPageUrl = $baseUrl."views/panchshil_tower/panchshil_towers.php?id=".$getOfferingCatDetails[$s]['id'];
                        break;
                        case 'manageCommercial.php':                          
                        $offeringCatPageUrl = $baseUrl."views/expresstowers/express-towers.php?id=".$getOfferingCatDetails[$s]['id'];
                        break;
                        case 'manageHospitality.php':                          
                        $offeringCatPageUrl = $baseUrl."views/the_ritz_carlton/the_ritz_carlton.php?id=".$getOfferingCatDetails[$s]['id'];
                        break;
                        case 'manageRetail.php':                          
                        $offeringCatPageUrl = $baseUrl."views/the_pavillion/the_pavillion.php?id=".$getOfferingCatDetails[$s]['id'];
                        break;
                      }

                  ?>
                      <li> <a href="<?php echo $offeringCatPageUrl;?>"> <?php echo $getOfferingCatDetails[$s]['name'];?>    </a> </li>
                  <?php }?>
                      
                  </ul>
                  <div class="clearfix"></div>
              </div>
          </div>
      </div>
  <?php 
    }
      }
  ?>
  </div>
 

			<!-- container fluid -->
	<!-- footer bottom end -->
	<!-- copy right -->
	<div class="copy-right">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<p>Privacy Policy Terms & Conditions</p>
				</div>
				<div class="col-sm-6">
					<p class="pull-right">Site designed & maintained by <a href="#" style="text-decoration:none;color:#a9a9a9;">Neon Tree</a></p>
				</div>
			</div>
		</div>
	</div>
	<!-- copy right end -->
<!-- Footer end -->
</footer>
<!-- search div starts -->
<div id="cd-search" class="cd-search">
	<form>
		<input type="search" placeholder="Type to search here">
	</form>
</div>
<!-- search div ends-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="../../lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../js/app.js"></script>
<script type="text/javascript" src="../../lib/jquery.bxslider/jquery.bxslider.min.js"></script>

<!-- main menu with search -->
<!-- <script src="js/main.js"></script> -->
<script src="../../js/modernizr.js"></script> <!-- Modernizr -->
<!-- <script src="js/footer.js"></script> -->

<!-- main menu with search -->



<!-- Include Unite Gallery core files -->
<script src='../../lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='../../lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<!-- Script to Activate the Carousel -->
<script>
$('.carousel').carousel({
    interval: 5000 //changes the speed
})
</script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
       var data=$(this).find(".text").html();
        $(".img-hover-text").hide().stop().html(data).fadeIn();

      }, function() {
        $(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
     jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end

  //FOoter Map Redirection

   });
</script>

<script type="text/javascript" src="../../js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
  $(function () {
  var filterList = {
    init: function () {
      $('#KeyAmenities').mixItUp({
        selectors: {
          target: '.KeyAmenities-portfolio',
          filter: '.KeyAmenities-list'
        },
        load: {
          filter: '.parking, .upper_deck, .lower_deck'
        }
      });
    }
  };
  filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end
function locationonMap(locate) {
  // alert(locate);
  // var city = $(this).val();
  window.location.href = locate;
}
</script>


<!-- main menu with search -->
<script src="../../js/main.js"></script>
<!-- main menu with search -->
<script src="../../js/footer.js"></script>