
<?php include("../layout/header.php");  ?>

<section class="hero-section hero-banner-img">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">REC</a>
          </div>
          <h1>REC</h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="rec-section">
  <div class="container rec-container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2 class="didot-font">Headline Goes here...</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </div>
    </div>
  </div><!-- container -->
  <br/></br/><br/>
  <!-- =========================== -->
  <div class="container-fluid nri-feachers">
    <div class="row">
      <div class="col-md-4 nri-feachers-col-padding">
        <div class="nri-feachers-overlay">
          <img src="../../img/world-family.jpg" class="img-responsive" alt="world family image">
          <div class="nri-feachers-overlay-text">
            <h2>feachers</h2>
            <br/>
            <a href="#" class="btn btn-default">VIEW DETAILS</a>
          </div>
        </div>
      </div>
      <div class="col-md-4 nri-feachers-col-padding">
        <div class="nri-feachers-overlay">
          <img src="../../img/simpler.jpg" class="img-responsive" alt="world family image">
          <div class="nri-feachers-overlay-text">
            <h2>BENEFITS</h2>
            <br/>
            <a href="#" class="btn btn-default">VIEW DETAILS</a>
          </div>
        </div>
      </div>
      <div class="col-md-4 nri-feachers-col-padding rec-login-bg text-center">
        <div class="rec-login-form">
          <br/><br/>
          <p class="nri-login-title">AGENT</p>
          <p class="nri-login-subtitle">LOGIN / SIGN UP</p>
          <br/><br/><br/>
          <form>
          <input type="text" class="form-control nri-input" placeholder="USERNAME / EMAIL ID"><br/>
          <input type="password" class="form-control nri-input" placeholder="PASSWORD"><br/>
          <p><a href="#" class="nri-forgot-pwd">Forgot Password?</a></p>
          <br/><br/>
          <a href="#" class="btn btn-default nri-login-btn"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>&nbsp;&nbsp; LOGIN / SIGN UP</a>
        </from>
        </div>
      </div>
    </div>
  </div>
  <br/></br/><br/>
  <!-- ============================== -->

  <!--====== News and Update =========  -->
  <div class="container">
    <div class="row">
      <h2 class="didot-font text-center">News & Updates</h2><br/>
      <div class="col-md-4">
        <a href="#"><img src="../../img/updates_1.jpg" class="img-responsive" alt="Updates image"></a>
        <div class="row">
          <div class="col-sm-3 col-xs-4 NewsUpdate">
            <div>
              <p class="date didot-font">30</p>
              <p class="month">JULY</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 updates-content">
            <p>Panchshil unveils homes next to Mahalaxmi Race course</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <a href="#"><img src="../../img/updates_2.jpg" class="img-responsive" alt="Updates image"></a>
        <div class="row">
          <div class="col-sm-3 col-xs-4 NewsUpdate">
            <div>
              <p class="date didot-font">17</p>
              <p class="month">JULY</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 updates-content">
            <p>Panchshil Properties records 14% growth in net profit.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <a href="#"><img src="../../img/updates_3.jpg" class="img-responsive" alt="Updates image"></a>
        <div class="row">
          <div class="col-sm-3 col-xs-4 NewsUpdate">
            <div>
              <p class="date didot-font">26</p>
              <p class="month">JUNE</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 updates-content">
            <p>Panchshil unveils homes next to Mahalaxmi Race course</p>
          </div>
        </div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-md-4">
        <a href="#"><img src="../../img/updates_1.jpg" class="img-responsive" alt="Updates image"></a>
        <div class="row">
          <div class="col-sm-3 col-xs-4 NewsUpdate">
            <div>
              <p class="date didot-font">30</p>
              <p class="month">JULY</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 updates-content">
            <p>Panchshil unveils homes next to Mahalaxmi Race course</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <a href="#"><img src="../../img/updates_2.jpg" class="img-responsive" alt="Updates image"></a>
        <div class="row">
          <div class="col-sm-3 col-xs-4 NewsUpdate">
            <div>
              <p class="date didot-font">17</p>
              <p class="month">JULY</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 updates-content">
            <p>Panchshil Properties records 14% growth in net profit.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <a href="#"><img src="../../img/updates_3.jpg" class="img-responsive" alt="Updates image"></a>
        <div class="row">
          <div class="col-sm-3 col-xs-4 NewsUpdate">
            <div>
              <p class="date didot-font">26</p>
              <p class="month">JUNE</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 updates-content">
            <p>Panchshil unveils homes next to Mahalaxmi Race course</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="updates-btn">
          <a href="#" class="btn btn-default">READ ALL UPDATES</a>
        </div>
      </div>
    </div>
  </div>
  <!--===== News and Update end =====  -->
</section>

<?php include("../layout/footer.php");  ?>
