<?php

	require_once '../../admin/utilities/config.php';
	require_once "../../admin/utilities/dbUtils.php";
	require_once "../../admin/utilities/utilities.php";
	require_once "../../admin/utilities/errorMap.php";
	$msg = cleanXSS(urldecode($_GET['msg']));
	$code = cleanXSS(urldecode($_GET['code']));

	if ($code == -1) {
		$color = "green";
		$callFunction = 'contactRedirection();';
	} else if ($code == 0) {
		$color = "black";
	} else {
		$color = "red";
	}

?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"></h4>
</div>
<div class="modal-body">
<p style="color:<?php echo $color; ?>; font-size: 20px; font-weight:bold; text-align:center;"><?php echo $msg; ?></p>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss = "modal" onclick="<?php echo $callFunction; ?>">OK</button>
</div>