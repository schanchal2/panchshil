
<?php  include("../layout/header.php");

  require_once '../../admin/utilities/config.php';
  require_once "../../admin/utilities/dbUtils.php";
  require_once "../../admin/utilities/utilities.php";
  require_once "../../admin/utilities/errorMap.php";
  require_once "../../admin/model/ReasonsModel.php";
  require_once '../../admin/model/Contact_detail_Model.php';
  require_once "../../admin/model/headerContentModel.php";



$returnArr = array();

$state = $_POST['state'];


//to get country
function getcountry($conn)
{


    $returnArr = array();
    $getResult = array();

    $query = "SELECT * FROM `countries`";

    $execQuery = runQuery($query, $conn);
    if(noError($execQuery)){
        while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $res["response"][] = $row;
            //print_r($res);
        }
        $returnArr = setErrorStack($execQuery, -1, "Successfully fetched record  countries details" ,$res);
    }else{
        $returnArr = setErrorStack($execQuery, 25, "Error: Retrieving contacts detail");
    }

    return $returnArr;
}
// to get profession
function getprofession($conn){

    $returnArr = array();
    $getResult = array();

    $query = "SELECT * from profession ";

    $execQuery = runQuery($query, $conn);
    if(noError($execQuery)){
        while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $res["response"][] = $row;
        }
        $returnArr = setErrorStack($execQuery, -1, "Successfully fetched record contacts details" ,$res);
    }else{
        $returnArr = setErrorStack($execQuery, 25, "Error: Retrieving contacts detail");
    }

    return $returnArr;
}

//end to get country
 $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
 if(noError($conn)){
     $conn = $conn["conn"];
     $ContactDetail = getAllContact_Detail($conn);
     $headerInfo    = getHeaderContent('contact',$conn);
     if(noError($headerInfo)){
        $headerInfo = $headerInfo["response"];
      }else{
          $returnArr =  $headerInfo;
      }
     if(noError($ContactDetail)){
        $ContactDetail = $ContactDetail["response"];
      }else{
          $returnArr =  $ContactDetail;
      }

      $getcoutry=getcountry($conn);
      if(noError($getcoutry)){
        $getcoutry = $getcoutry["response"];
      }else{
        $returnArr = $getcoutry;
      }
      $getprofession = getprofession($conn);
      if(noError($getprofession)){
        $getprofession = $getprofession["response"];
      }else{
         $returnArr = $getprofession;
      }
      $getreasons=getreasons($conn);
      if(noError($getreasons)){
        $getreasons = $getreasons["response"];
        //printArr($getreasons);
      }else{
        $returnArr = $getreasons;
      }
  }else{
    $returnArr = $conn;
    exit;
  }
?>
<?php if(count($headerInfo) <= 0 ){  ?>
<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.$businessFeatureImage;?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Contact</a>
          </div>
          <h1>Contact</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<?php }else {
for($i=0; $i < count($headerInfo); $i++) { ?>
<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.'uploads/'.$headerInfo[$i]["image"];?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Contact</a>
          </div>
          <h1><?php echo $headerInfo[$i]["page_name"];?></h1>
        </div>
      </div>
    </div>
  </div>
</section>
<?php }
  } ?>
<section class="contact-section">
  <div class="container contact-container">
    <div class="row">
     <?php for($i=0; $i<count($ContactDetail); $i++){ ?> 
      <div class="col-md-4 text-center">
        <div  class="contact-box">
          <i class="fa fa-map-marker fa-3x" aria-hidden="true"></i>
          <h2><?php echo $ContactDetail[$i]['location']; ?></h2>
          <p class="contact-grey-text"><?php echo $ContactDetail[$i]['phoneno1']; ?></p>
          <p class="contact-grey-text"><?php echo $ContactDetail[$i]['address']; ?> </p>
        </div>
      </div><?php } ?>
      <div class="col-md-4 text-center">
        <div  class="contact-box">
          <img src="../../img/project-management-icon.png"/>
          <h2>Pre-Sales Enquiry</h2>
          <p class="contact-grey-text">+91 22 66863939</p>
          <p class="contact-grey-text">name@gmail.com</p>
        </div>
      </div>
    </div><!-- row close -->

  <!-- contact form -->
    <div class="row contact-form">
      <div class="col-md-10 col-md-offset-1">
      <div id="errorMsg" style=" margin: 10px; text-align:center; font-size: 20px; font-weight; display:none;"></div>
        <h2 class="text-center">Enquiry Form</h2>
        <br/>

        <form id = "contactForm">
        <input type="hidden" name = "method" value="update">
          <div clas="row">
            <div class="form-group col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
              <select class="form-control" name = "reason" id  = "reason">
                <option selected disabled>Reason for Enquiry</option>
                <?php
                    foreach($getreasons as $getreasons){
                    ?>
                    <option value="<?php echo $getreasons["reasons"]; ?>"><?php echo $getreasons["reasons"]; ?></option>
                    <?php
                    }
                    ?>
              </select><br/>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="First Name" name = "firstName" id  = "firstName"><br/>
                <input type="text" class="form-control" placeholder="Last Name" name = "lastName" id  = "lastNAme"><br/>
                <input type="text" class="form-control" placeholder="Phone" name = "phone" id  = "phone" onchange="PhoneValidation(this);"><br/>
                <input type="email" class="form-control" placeholder="Email" name = "email" id  = "email" onchange="ValidateEmail(this)"><br/><br/>
                <select class="form-control" name = "profession-name" id  = "profession-name">
                <option selected disabled> Profession</option>
                <?php 
                  //$getprofession = getprofession($conn);
                  foreach($getprofession as $getprofession) { 
                ?>
                      <option value="<?php echo $getprofession["title"] ?>"><?php echo $getprofession["title"]; ?></option>
                <?php 
                    }
                ?> 
                </select><br/>
                <input type="text" class="form-control" placeholder="Company / Organzation" name = "company_name" id  = "company_name" ><br/>
                <input type="text" class="form-control" placeholder="Designation" name = "designation" id  = "designation"><br/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <textarea class="form-control textarea-address" placeholder="Address" name = "address" id  = "address"></textarea><br/>
                <select class="form-control" name = "country" id  = "country">
                   <option value="">Select country</option>
                    <?php
                    foreach($getcoutry as $getcoutry1) {
                    ?>
                    <option value="<?php echo $getcoutry1["country_id"]; ?>"><?php echo $getcoutry1["name"]; ?></option>
                    <?php
                    }
                    ?>

                </select><br/>
                  <select class="form-control" name="state" id="state"  >
                  <option value="">Select city</option>
                  <?php
                  foreach($getstate as $getstate1) {
                  ?>
                  <option value="<?php echo $getstate1["name"]; ?>" name="state"><?php echo $getstate1["name"]; ?></option>
                  <?php
                  }?>  </select><br/><br/>
                <textarea class="form-control textarea-comment" placeholder="Comments" name = "comment" id  = "comment"></textarea><br/>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6 text-right">
              <a href="#" class="btn btn-default btn-clear">CLEAR</a>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 text-left">
              <a class="btn btn-default btn-submit" onclick="addContactForm();">SUBMIT</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  <!-- contact form end -->
  <!-- location map -->
    <div class="row">
      <div class="col-md-12">
        <br/><br/>
        <h2 class="text-center">Locate Property on Map</h2><br/>
        <!-- <img src="../../img/map.jpg" class="img-responsive"/> -->
        <iframe width="1140" height="450" frameborder="0" scrolling="no" marginheight="0" src="https://maps.google.com/maps?q=bitstreet technology kurla, &t=p&z=14&ie=UTF8&iwloc=&output=embed" marginwidth="0"><a class="addmaps" href="http://www.embedgooglemap.net"id="get-map-data" mce_href="http://maps.google.com/maps/api/js?sensor=false">bitstreet technology kurla, </a><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></iframe>
      </div>
    </div>
  <!-- location map end -->
  </div><!-- container close -->
</section>

<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog" style="width: 480px;">
    <!-- Modal content-->
    <div class="modal-content"></div>
  </div>
</div>
<?php include("../layout/footer.php");  ?>
<script type="text/javascript">

function ContactRemodal(msg,code) {
  $( ".modal-content" ).load("contactFormErrorMsg.php?msg="+msg+"&code="+code);
  $('#myModal').modal();
}

  /* TO add Contact Form Details */
  function addContactForm() {
    $('.btn-submit').prop('disable',true);
    var reason              = $('#reason').val();
    var firstName           = $('#firstName').val();
    var lastName            = $('#lastName').val();
    var phone               = $('#phone').val();
    var email               = $('#email').val();
    var address             = $('#address').val();
    var country             = $('#country').val();
    var city                = $('#city').val();
    var profession          = $('#profession').val();
    var company_name        = $('#company_name').val();
    var designation         = $('#designation').val();
    var comment             = $('#comment').val();

    if (reason == '' || firstName == '' || lastName == '' || phone == '' || email == '' || country == '' || city == '' || profession == '' || company_name == '' || designation == '') {
      $('.btn-submit').prop('disable',false);
      ContactRemodal(encodeURIComponent('Please Enter All Fields !!!'),'1');
    } else {
      var form = $('#contactForm')[0];
      var formData = new FormData(form);
      $.ajax({
        type:'post',
        dataType:'json',
        url:'../../admin/controller/contactUsFormController.php',
        data:formData,
        contentType:false,
        processData:false,
        async:false,
        success: function(data) {
          //alert(data);
          console.log(data.errCode);
          if (data.errCode == -1) {
            ContactRemodal(encodeURIComponent('We will Contact You As Soon As Possible...!!!'),'-1');
          } else {
            ContactRemodal(encodeURIComponent('Incorrect Information Entered..!'),'0');
          }
        },
        error: function(data) {
          ContactRemodal(encodeURIComponent('Please Visit after Some Time...!'),'1');
        }
      });
    }
  }

  

/*******************city country dropdown***********************/

$(document).ready(function(){
    $('#country').on('change',function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                datatype:'html',
                url:'../../admin/controller/getAllcitycountry.php',
                data:{
                    country_id: countryID
                },
                success:function(data){
                    // console.log(data);
                    // $('#state').html(html);
                    $('#state').html(data);
                }
            });
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>');
        }
    });

    $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
               datatype:'html',
                url:'../../admin/controller/getAllcitycountry.php',
                data:{
                    state_id: stateID
                },
                success:function(data){
                    // console.log(data);
                    $('#city').html(data);
                }
            });
        }else{
            $('#city').html('<option value="">Select state first</option>');
        }
    });
});
function ValidateEmail(mail)
{
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {
    return (true)
  }
    alert("You have entered an invalid email address!")
    return (false)
}

var reg = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
function PhoneValidation(phoneNumber)
      {
        var OK = reg.exec(phoneNumber.value);
        if (!OK)
          window.alert("phone number isn't  valid");
        else
          window.alert("phone number is  valid");
      }

function contactRedirection() {
  // window.location('<?php echo $baseUrl."views/contact/contact.php"; ?>');
  $(location).attr('href', '<?php echo $baseUrl."views/contact/contact.php"; ?>');
}

</script>
<!--***********end city country dropdown***********************-->



