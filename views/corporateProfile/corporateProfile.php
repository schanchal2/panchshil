<?php 

    include("../layout/header.php");
    require_once '../../admin/utilities/config.php';
    require_once "../../admin/utilities/dbUtils.php";
    require_once "../../admin/utilities/utilities.php";
    require_once "../../admin/utilities/errorMap.php";
    require_once "../../admin/model/corporateDetailsModel.php";

    // echo "nikhil"; die();
    $id = $_GET['id'];
    $returnArr = array();

    $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
    if(noError($conn)){
      $conn = $conn["conn"];
      $corporateHeaderInfo 	= getAboutCorporateDetails($id,'status',$conn);
      $corporateDetailsInfo = getCorporateDetails(NULL,$id,'status',$conn);
      
        if(noError($corporateHeaderInfo)){
        	$corporateHeaderInfo = $corporateHeaderInfo["response"];  
        }else{
        	$returnArr = $corporateHeaderInfo;
       	} 

       	if(noError($corporateDetailsInfo)){
        	$corporateDetailsInfo = $corporateDetailsInfo["response"];
        }else{
        	$returnArr = $corporateDetailsInfo;
      	}    
      	// printArr($corporateHeaderInfo);  
    }else{
      $returnArr = $conn;
      exit;
    }    
    $corporatePageNames = array(
      "manageForword.php"         => "foreward/foreward.php",
      "manageGrowthChronicle.php" => "GrowthChronicle/growth_chronicle.php",
      "manageCompanyProfile.php"  => "companyProfile/companyProfile.php");
?>

<div class="banner-offerings" style="background-image: url(<?php echo $rootUrl.'uploads/'.$corporateHeaderInfo[0]['image'];?>);">
 <div class="container-fluid nopadding">
  <ol class="breadcrumb">
    <li><a href="../home/home.php">Home</a></li>
    <li><a href="../about/about.php">About</a></li>
    <li class="active">Corporate Profile</li>
  </ol>
</div><h1>Corporate Profile</h1>
</div>

<section class="offering-container-section">
  <?php for($i=0; $i < count($corporateDetailsInfo); $i++) { ?>
  <div class="container">
    <div class="row">
      <div class="col-md-12 offering-container-padding">
        <div class="offering-container">
          <img src="<?php echo $rootUrl.'uploads/'.$corporateDetailsInfo[$i]["image"];?>" class="img-responsive"/>
          <div class="offering-container-text">
            <div class="row">
              <div class="col-md-10 col-sm-10 offering-container-text-padding">
                <div class="offering-container-hover-title">
                  <span class="title"><?php echo $corporateDetailsInfo[$i]["name"];?></span> <span class="property-count"></span>
                </div>
                <p class="block-with-text">
                     <?php
                         $corporateDetails = $corporateDetailsInfo[$i]["shortDesc"];
                         $corporateDetails = trim($corporateDetails);
                         $corporateLength  =  strlen($corporateDetails);
                         if ($corporateLength <= 150 ) {
                              echo $corporateDetails;
                          } else {
                              echo substr($corporateDetails,0,150);
                              echo "...";
                          }
                          foreach ($corporatePageNames as $key => $value) {
                            if ($key == $corporateDetailsInfo[$i]["pagename"]) {
                              $pageRedirection = "../".$value."?id=".$corporateDetailsInfo[$i]["id"]."&urlId=".$corporateDetailsInfo[$i]["corporateUrlId"];
                              break;
                            } else {
                              $pageRedirection = "#";
                            }
                          }
                    ?>
                </p>
             </div>
              <div class="col-md-2 col-sm-2 offering-container-arrow">
                <a href="<?php echo $pageRedirection; ?>" class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
 </div>
<?php } ?>
</section>
<?php include("../layout/footer.php");  ?>
