<?php
 	require_once '../../admin/utilities/config.php';
	require_once "../../admin/utilities/dbUtils.php";
	require_once "../../admin/utilities/utilities.php";
	require_once "../../admin/utilities/errorMap.php";
  require_once "../../admin/model/propertyContentModel.php";
  require_once "../../admin/model/bannerContentModel.php";
  require_once "../../admin/model/galleryContentModel.php";
  require_once "../../admin/model/featureSpecificationContentModel.php";
  require_once "../../admin/model/matrixSpecificationContentModel.php";
  require_once "../../admin/model/speciTypeTitleModel.php";

  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
    $conn = $conn["conn"];
    $pagename="manageOffering.php";
    $id=cleanXSS($_GET['id']);


    $getPropertyDetails = getPropertyDetails($id,'NULL',$conn);
    if(noError($getPropertyDetails)){
        $getPropertyDetails = $getPropertyDetails["response"];
    }else{
        $returnArr = $getPropertyDetails;
    }

    $getBannerDetails = getBannerDetailsById($id,$conn);
    if(noError($getBannerDetails)){
        $getBannerDetails = $getBannerDetails["response"];
    }else{
        $returnArr = $getBannerDetails;
    }

    $getGalleryDetails = getGalleryDetailsById($id,$conn);
    if(noError($getGalleryDetails)){
        $getGalleryDetails = $getGalleryDetails["response"];
    }else{
        $returnArr = $getGalleryDetails;
    }

    $getFeatureDetails = getfeatureSpecificationDetailsById($id,'asc',$conn);
    if(noError($getFeatureDetails)){
        $getFeatureDetails = $getFeatureDetails["response"];
    }else{
        $returnArr = $getFeatureDetails;
    }


    $getMatrixDetails = getMatrixSpecificationDetailsById($id,$conn);
    if(noError($getMatrixDetails)){
        $getMatrixDetails = $getMatrixDetails["response"];
    }else{
        $returnArr = $getMatrixDetails;
    }

    $getSpeciTitleDetails = getSpeciTitleDetailsById($id,$conn);
    if(noError($getSpeciTitleDetails)){
        $getSpeciTitleDetails = $getSpeciTitleDetails["response"];
    }else{
        $returnArr = $getSpeciTitleDetails;
    }





  }else{
    $returnArr = $conn;
    exit;
  }
 //printArr($getSpeciTitleDetails);


  ?>
  <html>

  <body class="nav-is-fixed">
    <?php include '../layout/header.php' ?>
    <div style="height:120px;"></div>

  <!-- header-->

<section class="property-section">
	<div class="container-fluid">
	  <div class="row">
	    <div class="col-md-12 property-slider">
	      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
              <?php if(!empty($getBannerDetails[0]["banner1"])) {?>
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li><?php } ?>
              <?php if(!empty($getBannerDetails[0]["banner2"])) {?>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li><?php } ?>
                <?php if($getBannerDetails[0]["banner3"]) {?><li data-target="#carousel-example-generic" data-slide-to="2"></li><?php } ?>
                <?php if(!empty($getBannerDetails[0]["banner4"])) {?><li data-target="#carousel-example-generic" data-slide-to="3"></li><?php } ?>
                <?php if(!empty($getBannerDetails[0]["banner5"])) {?><li data-target="#carousel-example-generic" data-slide-to="4"></li><?php } ?>
          </ol>
          <div class="carousel-inner">

              <?php if(isset($getBannerDetails[0]["banner1"])) {?>
          	<div class="item active">
              <img src="<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner1"]; ?>" class="img-responsive" alt="First slide">
              <div class="carousel-caption">
                <p class="didot-font commercial-slider-title"><?php echo ucfirst($getPropertyDetails[0]["title"]);?></p>
								<!-- <p class="didot-font commercial-slider-subtitle">PUNE</p> -->
              </div>
            </div>
            <?php } ?>

            <?php if(isset($getBannerDetails[0]["banner2"])) {?>
            <div class="item ">
              <img src="<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner2"]; ?>" class="img-responsive" alt="First slide">
              <div class="carousel-caption">
                <p class="didot-font commercial-slider-title"><?php echo ucfirst($getPropertyDetails[0]["title"]);?></p>
                <!-- <p class="didot-font commercial-slider-subtitle">PUNE</p> -->
              </div>
            </div>
            <?php } ?>
            <?php if(isset($getBannerDetails[0]["banner3"])) {?>
            <div class="item ">
              <img src="<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner3"]; ?>" class="img-responsive" alt="First slide">
              <div class="carousel-caption">
                <p class="didot-font commercial-slider-title"><?php echo ucfirst($getPropertyDetails[0]["title"]);?></p>
                <!-- <p class="didot-font commercial-slider-subtitle">PUNE</p> -->
              </div>
            </div>
            <?php } ?>
            <?php if(isset($getBannerDetails[0]["banner4"])) {?>
            <div class="item ">
              <img src="<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner4"]; ?>" class="img-responsive" alt="First slide">
              <div class="carousel-caption">
                <p class="didot-font commercial-slider-title"><?php echo ucfirst($getPropertyDetails[0]["title"]);?></p>
                <!-- <p class="didot-font commercial-slider-subtitle">PUNE</p> -->
              </div>
            </div>
            <?php } ?>

            <?php if(isset($getBannerDetails[0]["banner5"])) {?>
            <div class="item ">
              <img src="<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner5"]; ?>" class="img-responsive" alt="First slide">
              <div class="carousel-caption">
                <p class="didot-font commercial-slider-title"><?php echo ucfirst($getPropertyDetails[0]["title"]);?></p>
                <!-- <p class="didot-font commercial-slider-subtitle">PUNE</p> -->
              </div>
            </div>
            <?php } ?>

            </div>
          </div>
	    	</div>
        <div class="property-path">
					<div class="property-path-link">
	            <a href="../home/home.php">Home</a> / <a href="../offering/offering.php">Offering</a> / <a href="../hospitality/hospitality.php">Hospitality</a> / <a href="#">The Ritz Carlton</a>
	        </div>
        </div>
	    </div>
	  </div>
	</div>
</section>

<section class="property-content-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="didot-font text-center"><img src="<?php echo $rootUrl."uploads/".$getPropertyDetails[0]["image"]; ?>"/></h2>
				<br/>
        	<h2 class="didot-font text-center"><?php echo $getPropertyDetails[0]["name"]?></h2>
        <div class="retail-inner-text">
<h5><?php echo $getPropertyDetails[0]["location"]?></h5>
        </div>
				<p class="text-center"><?php echo $getPropertyDetails[0]["long_desc"]?></p>
			</div>
		</div><!-- row end -->
		<br/>

		<div class="row">
			<div class="col-md-12 text-center">
				<ul class="property-details-link">

					<li><a href="" data-toggle="modal" data-target=".gallery-popup"><i class="fa fa-picture-o" aria-hidden="true"></i> &nbsp;VIEW GALLERY</a></li>
          <li><a href="http://<?php echo $getPropertyDetails[0]["website_url"];?>" target="_blank" ><i class="fa fa-external-link" aria-hidden="true"></i> &nbsp;VISIT WEBSITE</a></li>

				</ul>
			</div>
		</div><!-- row end -->
		<br/><br/>

	<!-- ============
		Gallery popup
	=================-->
  <div class="modal fade gallery-popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content gallery-modal-content">
			<!-- ======================== -->
			<div class="gallery-padding">
				<div id="gallery">
          <?php for($j=0; $j < count( $getGalleryDetails); $j++) { ?>
					<img alt="Preview Image 1"
					 src="<?php echo $rootUrl."uploads/".$getGalleryDetails[$j]["image"]; ?>"
					 data-image="<?php echo $rootUrl."uploads/".$getGalleryDetails[$j]["image"]; ?>"
					 data-description="<?php echo $getGalleryDetails[$j]["description"];?>"/>
           <?php } ?>
				</div>
			</div>
			<!-- ======================== -->
		</div>
		<p class="esc-close">ESC to close window</p>
	</div>
	</div>
	<!-- ============
		Gallery popup end
	=================-->

  <div class="row">
    <div class="col-md-12 property-img-circle">
      <div class="property-img-circle-slider">
          <?php for($j=0; $j < count( $getFeatureDetails); $j++) { ?>
        <div class="img-circle-slider">
          <a href="#"><img src="<?php echo $rootUrl."uploads/".$getFeatureDetails[$j]["image"]; ?>" class="img-responsive img-circle" alt=""></a>
          <div class="img-overlay-count"><p class="didot-font"><?php echo $getFeatureDetails[$j]["count_id"]; ?></p></div>
          <div class="text">
            <h2 class="didot-font"><?php echo $getFeatureDetails[$j]["count_id"]; ?></h2>
            <p><?php echo $getFeatureDetails[$j]["description"]; ?></p>
          </div>
        </div>
        <?php } ?>

      </div>
    </div>
  </div><!-- row end -->

  <div class="row">
    <div class="col-md-12 text-center img-hover-text">
      <h2 class="didot-font"><?php echo $getFeatureDetails[0]["count_id"]; ?></h2>
      <p><?php echo $getFeatureDetails[0]["description"]; ?></p>
    </div>
  </div><!-- row end -->
		</div><!-- row end -->
		<br/><br/>
		<hr class="hr-color"/>
    <!-- specifications -->


    <br/>


    <!-- specifications end -->
		<!-- Locate on Map -->
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center didot-bold-font">Locate on Map</h2>
				<br/>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.489456215352!2d73.88804085051835!3d18.551959587328852!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c11e391e00c7%3A0xb39bd584c3fe4cdf!2sRitz+Carlton!5e0!3m2!1sen!2sin!4v1485504428874" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
		<br/><br/>
		<!-- Locate on Map end -->
  </div><!-- container -->
</section>

<?php include '../../includes/footer.html' ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="../../lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../js/app.js"></script>
<script type="text/javascript" src="../../lib/jquery.bxslider/jquery.bxslider.min.js"></script>

<!-- Include Unite Gallery core files -->
<script src='../../lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='../../lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
    	 var data=$(this).find(".text").html();
    		$(".img-hover-text").hide().stop().html(data).fadeIn();

    	}, function() {
    		$(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
	   jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end
   });
</script>

<script type="text/javascript" src="../../js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
	$(function () {
	var filterList = {
		init: function () {
			$('#KeyAmenities').mixItUp({
				selectors: {
					target: '.KeyAmenities-portfolio',
					filter: '.KeyAmenities-list'
				},
				load: {
					filter: '.parking, .upper_deck, .lower_deck'
				}
			});
		}
	};
	filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end
</script>
<!-- main menu with search -->
<script src="../../js/main.js"></script>
<script src="../../js/footer.js"></script>
<!-- main menu with search -->

</body>
</html>
