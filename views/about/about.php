
<?php
    include("../layout/header.php");

    require_once '../../admin/utilities/config.php';
    require_once "../../admin/utilities/dbUtils.php";
    require_once "../../admin/utilities/utilities.php";
    require_once "../../admin/utilities/errorMap.php";
    require_once "../../admin/model/headerContentModel.php";
    require_once "../../admin/model/aboutContentModel.php";

  
    $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
    if(noError($conn)){
        $conn = $conn["conn"];
        $headerInfo = getHeaderContent('about panchshil',$conn);
        $aboutInfo  = getAboutDetails(NULL,'status',$conn);
        if(noError($aboutInfo)){
            $aboutInfo = $aboutInfo["response"];
        }else{
            $returnArr = $aboutInfo;
        }
        if(noError($headerInfo)){
            $headerInfo = $headerInfo["response"];
        }else{
            $returnArr = $headerInfo;
        }  
    }else{
            $returnArr = $conn;
            exit;
    }
    $aboutPageNames = array(
      "manageClientsDetails.php"    => "clients/clients.php",
      "manageCorporateContent.php"  => "corporateProfile/corporateProfile.php",
      "manageLeadership.php"        => "leadership/leadership.php",
      "manageSustainContent.php"    => "sustainability/sustainability.php" );
?>
<?php if(count($headerInfo) <= 0){  ?>
<div class="banner-about" style="background-image: url(<?php echo $rootUrl.$businessFeatureImage ?>);">
 <div class="container-fluid nopadding">
    <ol class="breadcrumb">
    <li><a href="../../views/home/home.php">Home</a></li>
    <li class="active">About</li>

    </ol>
</div><h1>About</h1>
</div>


<section class="business-section">
  <div class="container business-container">
    <div class="row">
      <div class="col-md-12 business-text">
        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </div>
    </div>
  <?php }else{ ?>
<?php for($i=0; $i < count($headerInfo); $i++) { ?>
<div class="banner-about" style="background-image: url(<?php echo $rootUrl.'uploads/'.$headerInfo[$i]["image"];?>);">
 <div class="container-fluid nopadding">
    <ol class="breadcrumb">
    <li><a href="../../views/home/home.php">Home</a></li>
    <li class="active">About</li>

    </ol>
</div><br><h1><?php echo $headerInfo[$i]["page_name"];?></h1>
</div>




<section class="business-section">
  <div class="container business-container">
    <div class="row">
      <div class="col-md-12 business-text">
        <p class="text-center"><?php echo $headerInfo[$i]["description"];?></p>
      </div>
    </div><?php } }
     ?>

    <div class="row">
      <?php for($i=0; $i < count($aboutInfo); $i++) { ?>
      <div class="<?php if($i == count($aboutInfo)-1 && count($aboutInfo)%2 != 0) echo "col-md-12"; else{ echo "col-md-6"; }?>">
        <div class="business-content">
          <img src="<?php echo $rootUrl.'uploads/'.$aboutInfo[$i]["image"];?>" class="img-responsive"/>
          <div class="business-content-text">
            <div class="row">
              <div class="col-md-10 col-sm-10 business-content-text-padding">
                <h2><?php echo $aboutInfo[$i]["name"];?></h2>
                <p class="block-with-text1">
                  <?php 
                  $aboutCatDetails = $aboutInfo[$i]["shortDesc"];
                  $aboutCatDetails = trim($aboutCatDetails);
                  $aboutCatLength  =  strlen($aboutCatDetails);
                      if ($aboutCatLength <= 120 ) {
                          echo $aboutCatDetails;
                        } else {
                          echo substr($aboutCatDetails,0,120);
                          echo "..."; 
                      }
                      foreach ($aboutPageNames as $key => $value) {                        
                        if ($key == $aboutInfo[$i]["pagename"]) {                          
                          $pageRedirection = "../".$value."?id=".$aboutInfo[$i]["id"];
                          break;
                        } else {                          
                          $pageRedirection = "#";
                        }
                      }
                  ?>
                </p> 
              </div>
              <div class="col-md-2 col-sm-2 business-content-arrow">
                <a href="<?php echo $pageRedirection; ?>"  class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>


  </div><!-- container -->
</section>
<?php include("../layout/footer.php");  ?>