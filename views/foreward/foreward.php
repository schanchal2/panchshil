
<?php 
    include("../layout/header.php"); 
    require_once '../../admin/utilities/config.php';
    require_once "../../admin/utilities/dbUtils.php";
    require_once "../../admin/utilities/utilities.php";
    require_once "../../admin/utilities/errorMap.php";
    require_once "../../admin/model/corporateDetailsModel.php"; 
    require_once "../../admin/model/forwordModel.php"; 

    $id = $_GET['id'];
    $urlId = $_GET['urlId'];
    $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
    if(noError($conn)){
        $conn = $conn["conn"];
        $forwordInfo = getforwordDetails($id,$conn);
        if(noError($forwordInfo)){
            $forwordInfo = $forwordInfo["response"];
        }else{
            $returnArr = $forwordInfo;
        }
        $corporateDetails = getCorporateDetails($id,$urlId,'status',$conn);
        if(noError($corporateDetails)){
            $corporateDetails = $corporateDetails["response"];

        }else{
            $returnArr = $corporateDetails;
        }

        
    }
    // printArr($corporateDetails);
    // printArr($forwordInfo);
?>

<!-- header-->

<!--banner starts here-->
<div class="banner-hralign" style="background-image: url(<?php echo $rootUrl.'uploads/'.$corporateDetails[0]["image"];?>);">
    <div class="container-fluid nopadding">
        <ol class="breadcrumb">
            <li><a href="../home/home.php">Home</a></li>
            <li><a href="../about/about.php">About</a></li>
            <li><a href="../corporateprofile/corporate-profile.php">Corporate Profile</a></li>
            <li class="active">Foreward</li>
        </ol>
    </div>
    <h1><?php echo ucfirst(strtolower($corporateDetails[0]["name"])); ?></h1>
</div>



<section class="business-section">
    <div class="container business-container">
        <div class="row">
            <?php if (!empty($forwordInfo[0]["image"])) { ?>
                <div class="col-md-3">
                    <img class = "img-responsive" src="<?php echo $rootUrl.'uploads/'.$forwordInfo[0]["image"];?>"/>
                </div>
            <?php }?>
            <div class="col-md-9 foreward-text">
                <p><?php echo $forwordInfo[0]["long_url"]; ?></p>
                <br/><br/><br/>
                <?php if (!empty($forwordInfo[0]["sign_image"])) { ?>
                    <img class = "img-responsive" src="<?php echo $rootUrl.'uploads/'.$forwordInfo[0]["sign_image"];?>"/>
                <?php }?>
                <h2 class="didot-font"><?php echo $forwordInfo[0]["name"]; ?></h2>
                <p><?php echo $forwordInfo[0]["designation"]; ?></p>
            </div>
        </div>
    </div><!-- container -->
</section>

<?php include("../layout/footer.php");  ?>
