
<?php
    include("../layout/header.php");

    require_once '../../admin/utilities/config.php';
    require_once "../../admin/utilities/dbUtils.php";
    require_once "../../admin/utilities/utilities.php";
    require_once "../../admin/utilities/errorMap.php";
    require_once "../../admin/model/headerContentModel.php";
    require_once "../../admin/model/careerDetailsModel.php";

  
    $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
    if(noError($conn)){
        $conn = $conn["conn"];
        $headerInfo = getHeaderContent('Career',$conn);
        $getCareerDetails  = getCareerDetails($id,'career',$conn);
        $getCareerCatDetails  = getCareerCatDetails($id,'career',$conn);
       // printArr($getCareerCatDetails );
        if(noError($getCareerDetails)){
            $getCareerDetails = $getCareerDetails["response"];
        }else{
            $returnArr = $getCareerDetails;
        }

        if(noError($getCareerCatDetails)){
            $getCareerCatDetails = $getCareerCatDetails["response"];
        }else{
            $returnArr = $getCareerCatDetails;
        }

        if(noError($headerInfo)){
            $headerInfo = $headerInfo["response"];
        }else{
            $returnArr = $headerInfo;
        }  
    }else{
            $returnArr = $conn;
            exit;
    }



?>
<?php if(count($headerInfo) <= 0){  ?>
<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.$businessFeatureImage ?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Career</a>
          </div>
          <h1>Career</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="business-section">
  <div class="container business-container">
    <div class="row">
      <div class="col-md-12 business-text">
        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </div>
    </div>
  <?php }else{ ?>
<?php for($i=0; $i < count($headerInfo); $i++) { ?>
<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.'uploads/'.$headerInfo[$i]["image"];?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">About</a>
          </div>
          <h1><?php echo $headerInfo[$i]["page_name"];?></h1>
        </div>
      </div>
    </div>
  </div>
</section>
<?php } ?>

<section class="business-section">
  <div class="container business-container">
    <div class="row"><?php for($i=0; $i < count( $headerInfo); $i++) { ?>
      <div class="col-md-12 business-text">
        <p class="text-center"><?php echo $headerInfo[$i]["description"];?></p>
      </div>
    </div><?php }
      }
     ?>



    <div class="row">
      <?php for($i=0; $i < count($getCareerDetails); $i++) { ?>
      <div class="<?php if($i == count($getCareerDetails)-1 && count($getCareerDetails)%2 != 0) echo "col-md-12"; else{ echo "col-md-6"; }?>">
        <div class="business-content">
          <img src="<?php echo $rootUrl.'uploads/'.$getCareerDetails[$i]["image"];?>" class="img-responsive"/>
          <div class="business-content-text">
            <div class="row">
              <div class="col-md-10 col-sm-10 business-content-text-padding">
                <h2><?php echo $getCareerDetails[$i]["career_name"];?></h2>
                <p class="block-with-text1">
                  <?php 
                  $careerCatDetails = $getCareerDetails[$i]["career_designation"];
                  $careerCatDetails = trim($careerCatDetails);
                  $aboutCatLength  =  strlen($careerCatDetails);
                      if ($aboutCatLength <= 120 ) {
                          echo $careerCatDetails;
                        } else {
                          echo substr($careerCatDetails,0,120);
                          echo "..."; 
                      }
                      // foreach ($careerPageNames as $key => $value) {                        
                      //   if ($key == $getCareerDetails[$i]["pagename"]) {                          
                      //     $pageRedirection = "../".$value."?id=".$getCareerDetails[$i]["id"]."&image=".$getCareerDetails[$i]["image"];
                      //     break;
                      //   } else {                          
                      //     $pageRedirection = "#";
                      //   }
                      // }
                  ?>
                </p> 
              </div>
              <div class="col-md-2 col-sm-2 business-content-arrow">
                <a href="<?php echo "../inCareer/inCareer.php?id=".$getCareerCatDetails[$i]['id']."&career_id=".$getCareerCatDetails[$i]['career_id'];?>"  class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>


  </div><!-- container -->
</section>
<?php include("../layout/footer.php");  ?>
