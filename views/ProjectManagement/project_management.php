<?php 

    include("../layout/header.php");
    require_once '../../admin/utilities/config.php';
    require_once "../../admin/utilities/dbUtils.php";
    require_once "../../admin/utilities/utilities.php";
    require_once "../../admin/utilities/errorMap.php";
    require_once "../../admin/model/headerContentModel.php";
    require_once "../../admin/model/businessDetailsModel.php";
    require_once "../../admin/model/businessSubCatagoryModel.php";
    require_once "../../admin/model/businessCategoryDetailsModel.php";
    require_once "../../admin/model/clientDetailsModel.php";

     $businessCatId = $_GET['id'];
     $BusinessId = cleanXSS(urldecode($_GET['businessId']));

    $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
    if(noError($conn)){
      $conn = $conn["conn"];
      $businessSubCatInfo = getBusinessSubCatDetails($BusinessId, $businessCatId, 'status', $conn);
      $businessCatInfo = getBusinessCatDetails($businessCatId, $BusinessId, 'status', $conn);
      $clientInfo = getClientDetails(NULL, $conn);
      if(noError($clientInfo)){
        $clientInfo = $clientInfo["response"];
      }else{
        $returnArr = $clientInfo;
      }
      if(noError($businessSubCatInfo)){
        $businessSubCatInfo = $businessSubCatInfo["response"];                    
      }else{
        $returnArr = $businessSubCatInfo;
      } 
      if(noError($businessCatInfo)){
        $businessCatInfo = $businessCatInfo["response"];  

      }else{
        $returnArr = $businessSubCatInfo;
      }   
    }else{
      $returnArr = $conn;
      exit;
    } 

   // printArr($businessSubCatInfo);
   // printArr($businessCatInfo);
    //die;

?>

<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.'uploads/'.$businessCatInfo[0]['category_feature_image'];?>)">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Business</a> / <a href="#">services</a> / <a href="#">Property Management</a>
          </div>
          <h1><?php echo $businessCatInfo[0]["category_name"];?></h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="project-management-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <p><?php echo $businessCatInfo[0]["category_full_desc"];?></p>
        <br/>
        <img class="img-responsive" style="margin: auto;" src="../../img/project-management-icon.png"/><br/>
        <p><b><?php echo $businessSubCatInfo[0]["sub_cat_name"];?></b></p>
        <p class="project-management-grey-text"><?php echo $businessSubCatInfo[0]["sub_cat_number"];?> &nbsp; | &nbsp; <?php echo $businessSubCatInfo[0]["sub_cat_email"];?></p>
        <p class="project-management-grey-text"><?php echo $businessSubCatInfo[0]["sub_cat_address"];?></p>
      </div>
    </div>
    <br/>
    <hr class="hr-color"/>

    <div class="row">
      <div class="col-md-12 text-center">
        <h1>Our Clients</h1>
        <br/><br/>
      </div>
    </div>
  </div>

  <div class="container project-management-client">
   <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    
     <!-- Indicators -->
     <!-- <ol class="carousel-indicators">
       <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
       <li data-target="#carousel-example-generic" data-slide-to="1"></li>
       <li data-target="#carousel-example-generic" data-slide-to="2"></li>
     </ol> -->
     <!-- Controls -->
       <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
      </a>
     <!-- Wrapper for slides -->    
  <div class="carousel-inner" role="listbox"> 
    <?php 
      for($j = 0; $j < count($clientInfo); $j++){ 
    ?> 
    <div class="item <?php if($j == 0){ echo "active"; }?>"> 
      <div class="row">
        <?php 
          for($i=$j; $i <= ($j+7) && $i < count($clientInfo); $i++) {
        ?>
            <div class="col-md-3 clients-logo">
              <center>
                <div class="clients-logo-bg">
                  <a href="#"><img class="img-responsive" src="<?php echo $rootUrl.'uploads/'.$clientInfo[$i]["clientColorImage"];?>"/></a>
                </div>                
              </center>
            </div>              
        <?php 
          } 
          $j = $j+7;
        ?>
      </div>  
    </div>
       <?php 
        }
       ?>       
  </div>


</section>


<?php include("../layout/footer.php");  ?>
