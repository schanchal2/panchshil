
<?php 
    include("../layout/header.php"); 
    require_once '../../admin/utilities/config.php';
    require_once "../../admin/utilities/dbUtils.php";
    require_once "../../admin/utilities/utilities.php";
    require_once "../../admin/utilities/errorMap.php";
    require_once "../../admin/model/aboutContentModel.php";
    // require_once "../../admin/model/forwordModel.php";
    

    $id = $_GET['id'];
    $urlId = $_GET['urlId'];
    
    $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
    if(noError($conn)){
        $conn = $conn["conn"];
        $getSustainCatDetails = getSustainCatDetails($id,NULL,$conn);
        //printArr($getSustainCatDetails);
        if(noError($getSustainCatDetails)){
            $getSustainCatDetails = $getSustainCatDetails["response"];
        }else{
            $returnArr = $getSustainCatDetails;
        }
        //$getSustainDetails = getSustainDetails($id,$urlId,$conn);
        $getSustainDetails = getSustainDetails($id,$urlId,$conn);
        if(noError($getSustainDetails)){
            $getSustainDetails = $getSustainDetails["response"];

        }else{
            $returnArr = $getSustainDetails;
        }

        
    }
      //printArr($getSustainDetails);
     // echo $getSustainDetails[0]['image'];
     // printArr($getSustainCatDetails);
    //die();
?>

<div class="banner-business" style="background-image: url(<?php echo $rootUrl.'uploads/'.$getSustainDetails[0]['image'];?>);">
 <div class="container-fluid nopadding">
  <ol class="breadcrumb">
    <li><a href="../home/home.php">Home</a></li>
    <li><a href="../about/about.php">About</a></li>
    <li><a href="../sustainability/sustainability.php">Sustainability</a></li>
    <li class="active"><?php echo $getSustainDetails[0]["name"]; ?></li>
  </ol>
</div><h1><?php echo $getSustainDetails[0]["name"]; ?></h1>
</div>

<section class="foreward-section">
  <div class="container foreward-container">
    <div class="row">
      <?php if (!empty($getSustainCatDetails[0]["sustain_cat_image"])) { ?>
      <div class="col-md-3">
        <img src="<?php echo $rootUrl.'uploads/'.$getSustainCatDetails[0]["sustain_cat_image"];?>"/>
      </div>
      <?php }?>
      <div class="col-md-9 foreward-text">
        <p><?php echo $getSustainCatDetails[0]["sustain_cat_long_url"]; ?></p>
        <br/><br/><br/>
        <?php if (!empty($getSustainCatDetails[0]["sustain_cat_sign_image"])) { ?>
        <img src="<?php echo $rootUrl.'uploads/'.$getSustainCatDetails[0]["sustain_cat_sign_image"];?>"/>
        <?php }?>
        <h2 class="didot-font"><?php echo $getSustainCatDetails[0]["sustain_cat_name"]; ?></h2>
        <p><?php echo $getSustainCatDetails[0]["sustain_cat_designation"]; ?></p>
      </div>
    </div>
  </div>
</section>



<?php include("../layout/footer.php");  ?>

