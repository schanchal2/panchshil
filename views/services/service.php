<?php include("../layout/header.php");  ?>

<section class="hero-section hero-banner-img">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Services</a>
          </div>
          <h1>Services</h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="service-container-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12 service-container-padding">
        <div class="service-container">
          <img src="../../img/services-banner1.jpg" class="img-responsive"/>
          <div class="service-container-text">
      			<div class="row">
      				<div class="col-md-10 col-sm-10 service-container-text-padding">
      					<h2>Property Management</h2>
      					<p>Ability	to	add	more	offerings	in	the	future.</p>
      				</div>
      				<div class="col-md-2 col-sm-2 service-container-arrow">
      					<a href="#" class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
      				</div>
      			</div>
      		</div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 service-container-padding">
        <div class="service-container">
          <img src="../../img/services-banner1.jpg" class="img-responsive"/>
          <div class="service-container-text">
      			<div class="row">
      				<div class="col-md-10 col-sm-10 service-container-text-padding">
      					<h2>Facility Management</h2>
      					<p>Ability	to	add	more	offerings	in	the	future.</p>
      				</div>
      				<div class="col-md-2 col-sm-2 service-container-arrow">
      					<a href="#" class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
      				</div>
      			</div>
      		</div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 service-container-padding">
        <div class="service-container">
          <img src="../../img/services-banner1.jpg" class="img-responsive"/>
          <div class="service-container-text">
      			<div class="row">
      				<div class="col-md-10 col-sm-10 service-container-text-padding">
      					<h2>Fit-Out Management</h2>
      					<p>Ability	to	add	more	offerings	in	the	future.</p>
      				</div>
      				<div class="col-md-2 col-sm-2 service-container-arrow">
      					<a href="#" class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
      				</div>
      			</div>
      		</div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 service-container-padding">
        <div class="service-container">
          <img src="../../img/services-banner1.jpg" class="img-responsive"/>
          <div class="service-container-text">
      			<div class="row">
      				<div class="col-md-10 col-sm-10 service-container-text-padding">
      					<h2>Brand Licensing</h2>
      					<p>Ability	to	add	more	offerings	in	the	future.</p>
      				</div>
      				<div class="col-md-2 col-sm-2 service-container-arrow">
      					<a href="#" class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
      				</div>
      			</div>
      		</div>
        </div>
      </div>
    </div>
  </div>
</section>


<?php include("../layout/footer.php");  ?>
