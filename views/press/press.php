<?php
  include("../layout/header.php");
  require_once '../../admin/utilities/config.php';
  require_once "../../admin/utilities/dbUtils.php";
  require_once "../../admin/utilities/utilities.php";
  require_once "../../admin/utilities/errorMap.php";
  require_once "../../admin/model/headerContentModel.php";
  require_once "../../admin/model/pressContentModel.php";
  require_once "../../admin/model/mediaContentModel.php";


$id= $_GET['id'];
    $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
    if(noError($conn)){
      $conn = $conn["conn"];
      $pressInfo = getPressDetailsById($id,$conn);
      if(noError($pressInfo)){
        $pressInfo = $pressInfo["response"];
      }else{
        $returnArr = $pressInfo;
      }

      $mediaInfo = getMediaDetailsById($id,$conn);
      if(noError($mediaInfo)){
        $mediaInfo = $mediaInfo["response"];
      }else{
        $returnArr = $mediaInfo;
      }

    }else{
      $returnArr = $conn;
      exit;
    }
$a= $mediaInfo[0]["feature_image"];

 ?>


<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl."uploads/".$a;?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Media</a> / <a href="#">Press</a>
          </div>
          <h1>Press</h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="press-section">
  <div class="container press-container">
    <div class="row press-row-br-bottom">
      <div class="col-md-7">
        <p><span class="press-date-filter didot-font">October, 2016</span></p>
      </div>
      <div class="col-md-3" style="padding: 15px 10px;">
        <span class="press-type">MEDIA TYPE</span>
        <select class="press-media-type">
          <option selected>ALL</option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
        </select>
      </div>
      <div class="col-md-2" style="padding: 15px 10px;">
        <span class="press-view-type">VIEW TYPE</span>
        <span class="press-view-icon">
        <a href="#"><i class="fa fa-th" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-th-list" aria-hidden="true"></i></span></a>
      </div>
    </div>
    <br/>

    <div class="row ">
      <?php for($i=0; $i < count( $pressInfo); $i++) {
         ?>
      <div class="col-md-3">
        <div class="press-img">
          <a href="#"><img src="<?php echo $rootUrl."uploads/".$pressInfo[$i]["image1"]; ?>" class="img-responsive" alt="Updates image"/></a>
          <div class="press-img-overlay">
              <?php   if($pressInfo[$i]["url"])  {?>
            <a href="<?php echo $pressInfo[$i]["url"];?>" target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a>
            <?php } else if($pressInfo[$i]["pdf"]) {?>
             <a data-toggle="modal" data-target="#pressModel" onclick="getRemodalPressContaints(<?php echo $pressInfo[$i]["id"] ?>);"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
             <?php } else if($pressInfo[$i]["you_tube_url"]){?>
             <a data-toggle="modal" data-target="#pressModel" onclick="getRemodalPressContaints(<?php echo $pressInfo[$i]["id"] ?>);"><i class="fa fa-film" aria-hidden="true"></i></a>
             <?php } ?>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 col-xs-4 date-filter">
            <div>
              <p class="date didot-font"><?php $time= strtotime($pressInfo[$i]["date"]);   $day = date('j',$time);   echo $day; ?></p>
              <p class="month didot-font"><?php $day1 = date('M',$time);   echo $day1; ?></p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 press-content">
            <p><b><?php echo $pressInfo[$i]["name"];?></b></p>
            <p class="source"><?php echo $pressInfo[$i]["source_name"];?></p>
          </div>
        </div>
      </div>
      <?php }
       ?>


    </div>
    <br/>
    <div class="row">
      <div class="col-md-12 text-center common-btn">
        <a class="btn btn-default btn-press" href="#">VIEW ALL</a>
      </div>
    </div>
  </div><!-- container -->
</section>


<div class="modal fade campus-expert-modal" id="pressModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body padding-none">
      </div>

    </div>
  </div>
</div>

<script>
  //To load modal of each Expert on Ajax
  function getRemodalPressContaints(id) {

    $('.modal-body').html("");
    $.ajax({
      type: "post",
      dataType: "html",
      url: "../../controller/pressRemodal.php",
      data:{
        id
      },
      before: function(){
        $('.modal-body').html("");
      },
      success: function(data) {
        $('.modal-body').html(data);
      },
      error: function() {
        alert('errored to Load');
      }
    });
  }
</script>

  <?php include("../layout/footer.php");  ?>
