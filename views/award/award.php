<?php
  include("../layout/header.php");
  require_once '../../admin/utilities/config.php';
  require_once "../../admin/utilities/dbUtils.php";
  require_once "../../admin/utilities/utilities.php";
  require_once "../../admin/utilities/errorMap.php";
  require_once "../../admin/model/headerContentModel.php";
  require_once "../../admin/model/awardContentModel.php";
  require_once "../../admin/model/testimonialContentModel.php";
  require_once "../../admin/model/mediaContentModel.php";


  $id=$_GET['id'];
  //echo $id;
  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
    $conn = $conn["conn"];
    $awardInfo = getAwardDetailsById($id,$conn);
    if(noError($awardInfo)){
      $awardInfo = $awardInfo["response"];
    }else{
      $returnArr = $awardInfo;
    }

    $testimonialInfo = getTestimonialDetails($id,NULL,$conn);
    if(noError($testimonialInfo)){
      $testimonialInfo = $testimonialInfo["response"];
    }else{
      $returnArr = $testimonialInfo;
    }

    $mediaInfo = getMediaAwardDetailsById($id,$conn);
    if(noError($mediaInfo)){
      $mediaInfo = $mediaInfo["response"];
    }else{
      $returnArr = $mediaInfo;
    }

  }else{
    $returnArr = $conn;
    exit;
  }
  $a= $mediaInfo[0]["feature_image"];
//printArr($mediaInfo);
  ?>
<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl."uploads/".$a;?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Media</a> / <a href="#">Awards & Recognition</a>
          </div>
          <h1><?php echo $mediaInfo[0]["short_desc"]?></h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="award-section">
  <div class="container award-container">

    <?php for($i=0; $i < count( $awardInfo); $i++) {

       ?>

    <div class="row award-row-content award-img-margin">
      <div class="col-md-4">
        <a href="#"><img src="<?php echo $rootUrl."uploads/".$awardInfo[$i]["left_img"]; ?>" class="img-responsive" /></a>
      </div>
      <div class="col-md-4 text-center">
        <a href="#"><img src="<?php echo $rootUrl."uploads/".$awardInfo[$i]["logo_img"]; ?>" class="img-responsive" /></a>
        <br/><br/><br/><br/>
        <div>
          <a href="#">
            <span class="award-title-size"><?php echo $awardInfo[$i]["title"];?></span><br/>

          </a>
        </div>
        <br/><br/><br/><br/>
        <div>
          <a href="#">
            <span class="award-title-small-size"><?php echo $awardInfo[$i]["name"];?></span><br/>

          </a>
        </div>
        <br/><br/><br/><br/>
        <div>
          <a href="#"><span class="award-subtitle-size"><?php echo $awardInfo[$i]["address"];?></span></a>
        </div>
      </div>
      <div class="col-md-4">
        <a href="#"><img src="<?php echo $rootUrl."uploads/".$awardInfo[$i]["right_img"]; ?>" class="img-responsive" /></a>
      </div>
    </div>

    <?php } ?>
  </div>

  <div class="container-fluid">
    <div class="row">
      <br/><br/>
      <h2 class="text-center didot-font">Client's Testimonials</h2>
      <div class="col-md-12" data-wow-delay="0.2s">
        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
          <!-- Carousel Slides / Quotes -->
          <div class="carousel-inner text-center">
              <!-- Quote 1 -->

              <div class="item active">
                  <blockquote>
                      <div class="row">
                          <div class="col-sm-8 col-sm-offset-2">
                              <p class="quote-color">"<?php echo $testimonialInfo[0]["description"];?>"</p>
                              <br/><br/>
                              <span><b><?php echo $testimonialInfo[0]["name"];?></b></span>
                              <small><?php echo $testimonialInfo[0]["desigantion"];?></small>
                          </div>
                      </div>
                  </blockquote>
              </div>
              <?php for($i=1; $i < count( $testimonialInfo); $i++) {

                 ?>
              <div class="item">
                 <blockquote>
                     <div class="row">
                         <div class="col-sm-8 col-sm-offset-2">
                             <p class="quote-color">"<?php echo $testimonialInfo[$i]["description"];?>"</p>
                             <br/><br/>
                             <span><b><?php echo $testimonialInfo[$i]["name"];?></b></span>
                             <small><?php echo $testimonialInfo[$i]["desigantion"];?></small>
                         </div>
                     </div>
                 </blockquote>
             </div>

              <?php } ?>
          </div>
            <!-- Carousel Buttons Next/Prev -->
            <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include("../layout/footer.php");  ?>
