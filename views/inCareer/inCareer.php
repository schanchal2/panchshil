
<?php 
    include("../layout/header.php"); 
    require_once '../../admin/utilities/config.php';
    require_once "../../admin/utilities/dbUtils.php";
    require_once "../../admin/utilities/utilities.php";
    require_once "../../admin/utilities/errorMap.php";
    require_once "../../admin/model/careerDetailsModel.php"; 
    // require_once "../../admin/model/forwordModel.php"; 

    
    $id = trim(urldecode($_GET['id']));
    $career_id = trim(urldecode($_GET['career_id']));

    $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
    if(noError($conn)){
        $conn = $conn["conn"];
        if($career_id!=''){  
        $getCareerCatDetails = getCareerCatDetails($career_id,'status',$conn);
        //printArr($getCareerCatDetails);
        if(noError($getCareerCatDetails)){
            $getCareerCatDetails = $getCareerCatDetails["response"];
        }else{
            $returnArr = $getCareerCatDetails;
        }
      }
      if($id!=''){
        $getCareerDetails = getCareerDetails($id,'status',$conn);
        if(noError($getCareerDetails)){
            $getCareerDetails = $getCareerDetails["response"];

        }else{
            $returnArr = $getCareerDetails;
        }
}
        
    }
    // printArr($getCareerDetails);
    // printArr($getCareerCatDetails);
    // die();
?>

<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.'uploads/'.$getCareerDetails[0]["image"];?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Career</a>
          </div>
          
          <h1><?php echo $getCareerDetails[0]["career_name"]; ?></h1>
         
        </div>
      </div>
    </div>
  </div>
</section>
 

<section class="foreward-section">
  <div class="container foreward-container">
    <div class="row">
      <?php if (!empty($getCareerCatDetails[0]["image"])) { ?>
      <div class="col-md-3">
        <img src="<?php echo $rootUrl.'uploads/'.$getCareerCatDetails[0]["image"];?>"/>
      </div>
      <?php }?>
      <div class="col-md-9 foreward-text">
        <p><?php echo $getCareerCatDetails[0]["career_cat_desc"]; ?></p>
        <br/><br/><br/>
        <h2 class="didot-font"><?php echo $getCareerCatDetails[0]["career_cat_name"]; ?></h2>
        <p><?php echo $getCareerCatDetails[0]["career_cat_design"]; ?></p>
      </div>
    </div>
  </div>
</section>



<?php include("../layout/footer.php");  ?>

