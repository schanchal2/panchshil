<?php

     require_once "../../admin/utilities/config.php";
     require_once "../../admin/utilities/dbUtils.php";
     require_once "../../admin/utilities/utilities.php";
     require_once "../../admin/utilities/errorMap.php";
     require_once "../../admin/model/leadershipContentModel.php";
     require_once "../../admin/model/aboutContentModel.php";  
   
     $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
     if(noError($conn)){
        $conn = $conn["conn"];
        $leadershipInfo = getLeadershipDetails(NULL,'status',$conn);
         if(noError($leadershipInfo)){
            $leadershipInfo = $leadershipInfo["response"];
        }else{
            $returnArr = $leadershipInfo;
        }
     }
     $i=$_GET["id"];      
?>  

<div class="container leadership-container">
   <div class="row leadership-bg-grey" id="chngeImg">
      <div class="col-md-3 bg-white">
        <img src="<?php echo $rootUrl.'uploads/'.$leadershipInfo[$i]["image"];?>" class="img-responsive"/>
      </div>
      <div class="col-md-9 leadership-info">
        <h2><?php echo $leadershipInfo[$i]["name"]; ?></h2>
        <p><?php echo $leadershipInfo[$i]["designation"]; ?></p>
        <ul class="leadership-social-icon">
          <!-- For Facebook -->
            <?php if(!isset($leadershipInfo[$i]['facebook']) || empty($leadershipInfo[$i]['facebook'])){  ?>
                     
            <?php }else{ ?>
                <li class="facebook-bg"><a target = '_blank' href="<?php echo $leadershipInfo[$i]["facebook"]; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <?php } ?>
          <!-- -->

          <!-- For Twitter -->
            <?php if(!isset($leadershipInfo[$i]['twitter']) || empty($leadershipInfo[$i]['twitter'])){  ?>
                     
            <?php }else{ ?>
              <li class="twitter-bg"><a target = '_blank' href="<?php echo $leadershipInfo[$i]["twitter"]; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <?php } ?>
          <!-- -->

          <!-- For Linked in -->
          <?php if(!isset($leadershipInfo[$i]['linkedin']) || empty($leadershipInfo[$i]['linkedin'])){  ?>
                     
            <?php }else{ ?>
            <li class="linkedin-bg"><a target = '_blank' href="<?php echo $leadershipInfo[$i]["linkedin"]; ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
          <?php } ?>
          <!-- -->
        </ul>
        <br/>
        <p><?php echo $leadershipInfo[$i]["description"]; ?></p>
      </div>
    </div>
    <br/><br/>
      
    <div class="row">
    <?php for($j=0; $j < count($leadershipInfo); $j++){
      if ($j != $i) {
      ?>
      <div class="col-md-3">
        <div class="leadership-img" id="<?php echo $j; ?>" style="background-image: url(<?php echo $rootUrl.'uploads/'.$leadershipInfo[$j]["image"];?>); background-repeat: no-repeat;" onclick ="chnge(this.id);"></div>
        <div class="leadership-name">
          <h3><?php echo $leadershipInfo[$j]["name"]; ?></h3>
          <p><?php echo $leadershipInfo[$j]["designation"]; ?></p>
           <ul class="leadership-social-icon">
            <?php if(!isset($leadershipInfo[$j]['facebook']) || empty($leadershipInfo[$j]['facebook'])){  ?>
                     
            <?php }else{ ?>
                <li class="facebook-bg"><a target = '_blank' href="<?php echo $leadershipInfo[$j]["facebook"]; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <?php } ?>
            <!-- -->
            <?php if(!isset($leadershipInfo[$j]['twitter']) || empty($leadershipInfo[$j]['twitter'])){  ?>
                     
            <?php }else{ ?>
              <li class="twitter-bg"><a target = '_blank' href="<?php echo $leadershipInfo[$j]["twitter"]; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <?php } ?>
            <!-- -->
            <?php if(!isset($leadershipInfo[$j]['linkedin']) || empty($leadershipInfo[$j]['linkedin'])){  ?>
                     
            <?php }else{ ?>
            <li class="linkedin-bg"><a target = '_blank' href="<?php echo $leadershipInfo[$j]["linkedin"]; ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
            <?php } ?>
            <!-- -->
          </ul>
        </div>
      </div>
    <?php 
        }
        } ?>
    </div>