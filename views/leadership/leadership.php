<?php 

      include("../layout/header.php");  
      require_once "../../admin/utilities/config.php";
      require_once "../../admin/utilities/dbUtils.php";
      require_once "../../admin/utilities/utilities.php";
      require_once "../../admin/utilities/errorMap.php"; 
      require_once "../../admin/model/leadershipContentModel.php";
      require_once "../../admin/model/aboutContentModel.php";  
    
      $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
      if(noError($conn)){
         $conn = $conn["conn"];
         $leadershipInfo = getLeadershipDetails(NULL,'status',$conn);
         $aboutDetails   = getLeaderDetails(NULL,'status',$conn);

         if(noError($aboutDetails)){
             $aboutDetails = $aboutDetails["response"];
         }else{
             $returnArr = $aboutDetails;
         }
         if(noError($leadershipInfo)){
             $leadershipInfo = $leadershipInfo["response"];
         }else{
             $returnArr = $leadershipInfo;
         }
      }
?>

<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.'uploads/'.$aboutDetails[0]["image"];?>)";>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Media</a> / <a href="#">Press</a>
          </div>
            <h1><?php echo $aboutDetails[0]["name"]; ?></h1>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="load" style="display:none"></div>

<section class="leadership-section">
  
</section>


<?php include("../layout/footer.php");  ?>
<script>
function chnge(id){
     $.ajax({ 
           type: "GET",
           url: 'chngeImg.php',
           data: {
                id:id,
           },
           dataType: 'html',
           beforeSend: function(){
                //$('.leadership-section').html("<img src='../../img/loader/loader2.gif' style='position: absolute; left:0px; top:50px;  z-index:9999'>");
                $('.load').show();
            }, 
           success: function(data){                
               $('.leadership-section').html(data);
               $('.load').hide();
            } 
                   
     });
}
$( document ).ready(function(){
  //alert("nikhil");
  var id1=0;
  $(".leadership-section").load('chngeImg.php?id='+id1);
});

</script>
<style>
.load{
  position :fixed;
  left: 0px;
  top:0px;
  height: 100%;
  width: 100%;
  z-index: 999999;
  background: url(../../img/loader/loader2.gif) center no-repeat;
}
</style>