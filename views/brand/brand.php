
<?php
    include("../layout/header.php");

  require_once '../../admin/utilities/config.php';
  require_once "../../admin/utilities/dbUtils.php";
  require_once "../../admin/utilities/utilities.php";
  require_once "../../admin/utilities/errorMap.php";
  require_once "../../admin/model/headerContentModel.php";
  require_once "../../admin/model/brandAndPartnerContentModel.php";

  $id=$_GET['id'];
  echo $id;
  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
    $conn = $conn["conn"];
    $headerInfo = getHeaderContent('alliances',$conn);
    $brandDetails = getBrandAndPartnerDetailsById($id,$conn);
      if(noError($headerInfo)){
        $headerInfo = $headerInfo["response"];
        }else{
        $returnArr = $headerInfo;
      }
      if(noError($brandDetails)){
          $brandDetails =  $brandDetails["response"];
      }else{
          $returnArr = $brandDetails;
      }
 } else{
    $returnArr = $conn;
    exit;
  }
//  printArr($brandDetails);
?>
<?php if(count($headerInfo) <= 0 ){  ?>
<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.$businessFeatureImage ?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Library </a>/ <a href="#">Brands </a>
          </div>
          <h1>Alliance</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="business-section">
  <div class="container business-container">
    <div class="row">
      <div class="col-md-12 business-text">
        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </div>
    </div>
  <?php }else{ ?>
<?php for($i=0; $i < count($headerInfo); $i++) { ?>
<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.'uploads/'.$headerInfo[$i]["image"];?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Business</a>
          </div>
          <h1><?php echo $headerInfo[$i]["page_name"];?></h1>
        </div>
      </div>
    </div>
  </div>
</section>
<?php } } ?>

<!--content starts here-->

<div class="container-fluid inner-5x innerTB">
<div class="container">

  <?php for($i=0; $i < count($brandDetails); $i++) { ?>
<div class="row inner-5x innerB">
<div class="logo-content innerAll-more"><img src="http://localhost/panchshil_local/img/leadership.png"></div>
<div class="desc-content innerAll-40"><span class="title"><?php echo $brandDetails[$i]["name"];?><br><?php echo $brandDetails[$i]["desigantion"];?></span><br/><br/><p><?php echo $brandDetails[$i]["description"];?> </p><br/><a href="#" class="hvr-bounce-to-right">view detail</a></div>
    </div>
<?php } ?>

</div>
</div>
<?php include("../layout/footer.php");  ?>
