<?php
 	require_once '../../admin/utilities/config.php';
	require_once "../../admin/utilities/dbUtils.php";
	require_once "../../admin/utilities/utilities.php";
	require_once "../../admin/utilities/errorMap.php";
	require_once "../../admin/model/offeringDetailsModel.php";
  require_once "../../admin/model/propertyContentModel.php";

  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
    $conn = $conn["conn"];
    $pagename="manageOffering.php";
    $id=cleanXSS($_GET['id']);
    $offeringDetailsInfo = getofferingDetailsByPageName($pagename,$id,$conn);
    if(noError($offeringDetailsInfo)){
        $offeringDetailsInfo = $offeringDetailsInfo["response"];
    }else{
        $returnArr = $offeringDetailsInfo;
    }
  }else{
    $returnArr = $conn;
    exit;
  }
  //printArr($offeringDetailsInfo);
  ?>
  <?php
  if(!empty($id)){ ?>
<html>

<body class="nav-is-fixed">
  <?php include '../layout/header.php' ?>
  <div style="height:120px;"></div>

  <!-- header-->
  <!--banner starts here-->
  <div class="banner-luxury">
   <div class="container-fluid nopadding">
  	<ol class="breadcrumb">
      <li><a href="../home/home.php">Home</a></li>
      <li><a href="../offering/offering.php">Offerings</a></li>
      <li class="active">Luxury Residence</li>

  	</ol>
  </div><h1>Luxury Residence</h1>
  <div class="container">
    <div class="row">
      <form class="residence-property-finder">
        <div class="form-group">
          <select class="form-control">
            <option disabled selected>LOCATION ALL</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
          </select>
        </div>
        <div class="form-group">
          <select class="form-control">
            <option disabled selected>PRICE ALL</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
          </select>
        </div>
        <div class="form-group">
          <select class="form-control">
            <option disabled selected>BHK ALL</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
          </select>
        </div>
        <div class="form-group">
          <select class="form-control">
            <option disabled selected>CARPET AREA -</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
          </select>
        </div>
        <a href="#" class="btn btn-default btn-apply-filter">APPLY FILTER</a><br/>
        <a href="#"class="btn-reset-selection"><i class="fa fa-repeat" aria-hidden="true"></i> RESET SELECTION</a>
      </form>
    </div>
  </div>
  </div>


<section>
  <div class="container residency-container">
    <div class="row">
      <div class="col-md-4">
        <p><span class="header-residency-count didot-font">29</span> <span class="header-residency-title">PROPERTIES FOUND</span></p>
      </div>
      <div class="col-md-3" style="padding:20px 10px;">
        <span class="header-residency-title">PROPERTY TYPE</span>
        <select class="residency-property-type">
          <option selected>ALL</option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
        </select>
      </div>
      <div class="col-md-3" style="padding:20px 10px;">
        <span class="header-residency-title">PROPERTY STATUS</span>
        <select class="residency-property-type">
          <option selected>ALL</option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
        </select>
      </div>
      <div class="col-md-2" style="padding:20px 10px;">
        <span class="header-residency-title">VIEW TYPE</span>
        <span class="view-type-icon">
        <a href="#"><i class="fa fa-th" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-th-list" aria-hidden="true"></i></span></a>
      </div>

    </div>
  </div>
</section>

<section class="residency-section">
  <div class="container residency-content">
    <div class="row">
      <div class="col-md-4 mg-top">
          <div class="row">
            <div class="col-md-12 platinum-bg">
              <div class="hovereffect">
                  <img class="img-responsive" src="../../img/project-residential-1.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <a class="info" href="../yoopune_by_philippes_stark/yoopune-by-philippes-stark.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="residency-img-buttom-text">
                    <h4>YooPune</h4>
                    <p><i class="fa fa-circle platinum" aria-hidden="true"></i> PLATINUM</p>
                    <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12 platinum-bg">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="../yoovillas/yoovillas.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>YooVillas</h4>
                    <p><i class="fa fa-circle platinum" aria-hidden="true"></i> PLATINUM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>


      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12 platinum-bg">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="../avantgarde/avant-garde.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Avant Garde</h4>
                    <p><i class="fa fa-circle platinum" aria-hidden="true"></i> PLATINUM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>


      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12 platinum-bg">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="../the_address/the-address.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>The Address</h4>
                    <p><i class="fa fa-circle platinum" aria-hidden="true"></i> PLATINUM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>


      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Podium Villa</h4>
                  <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="panchshil_towers.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Panchshil Towers</h4>
                  <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="eon-free-zone.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>EON Waterfront</h4>
                  <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>

      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="one-north.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>One North</h4>
                  <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="waterfront.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Waterfront</h4>
                  <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="casa-9.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Casa 9</h4>
                  <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="silver-woods.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>silver Woods</h4>
                  <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="forest_castles.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Forest Castles</h4>
                  <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="satellite-towers.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Satellite Towers</h4>
                  <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12 platinum-bg">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-3.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="trump-towers-pune.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Trump Tower, Pune</h4>
                  <p><i class="fa fa-circle platinum" aria-hidden="true"></i> PLATINUM</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-md-12 text-center common-btn">
        <br/>
        <a href="#" class="btn btn-default residency-loading-btn"> LOAD MORE PROPERTIES</a>
      </div>
    </div>

  </div>
</section>
<?php include '../layout/footer.php' ?>
<?php
}else{
		header('location:../home/home.php');
} ?>
