
<?php
    include("../layout/header.php");

  require_once '../../admin/utilities/config.php';
  require_once "../../admin/utilities/dbUtils.php";
  require_once "../../admin/utilities/utilities.php";
  require_once "../../admin/utilities/errorMap.php";
  require_once "../../admin/model/headerContentModel.php";
  require_once "../../admin/model/alliancesContentModel.php";
  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
    $conn = $conn["conn"];
    $headerInfo = getHeaderContent('alliances',$conn);
    $alliancesDetails = getAlliancesDetails(NULL,'status',$conn);
      if(noError($headerInfo)){
        $headerInfo = $headerInfo["response"];
        }else{
        $returnArr = $headerInfo;
      }
      if(noError($alliancesDetails)){
          $alliancesDetails =  $alliancesDetails["response"];
      }else{
          $returnArr = $alliancesDetails;
      }
 } else{
    $returnArr = $conn;
    exit;
  }
//  printArr($alliancesDetails);
?>
<?php if(count($headerInfo) <= 0 ){  ?>
<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.$businessFeatureImage ?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Alliance</a>
          </div>
          <h1>Alliance</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="business-section">
  <div class="container business-container">
    <div class="row">
      <div class="col-md-12 business-text">
        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </div>
    </div>
  <?php }else{ ?>
<?php for($i=0; $i < count($headerInfo); $i++) { ?>
<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.'uploads/'.$headerInfo[$i]["image"];?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Business</a>
          </div>
          <h1><?php echo $headerInfo[$i]["page_name"];?></h1>
        </div>
      </div>
    </div>
  </div>
</section>
<?php } ?>

<section class="business-section">
  <div class="container business-container">
    <div class="row"><?php for($i=0; $i < count( $headerInfo); $i++) { ?>
      <div class="col-md-12 business-text">
        <p class="text-center"><?php echo $headerInfo[$i]["description"];?></p>
      </div>
    </div><?php }
      }
     ?>
    <div class="row">
      <?php for($i=0; $i < count($alliancesDetails); $i++) { ?>
      <div class="<?php if($i == count($alliancesDetails)-1 && count($alliancesDetails)%2 != 0) echo "col-md-12"; else{ echo "col-md-6"; }?>">
        <div class="business-content">
          <img src="<?php echo $rootUrl.'uploads/'.$alliancesDetails[$i]["box_image"];?>" class="img-responsive"/>
          <div class="business-content-text">
            <div class="row">
              <div class="col-md-10 col-sm-10 business-content-text-padding">
                <h2><?php echo $alliancesDetails[$i]["name"];?></h2>
                <p class="block-with-text1">
                </p>
              </div>
              <div class="col-md-2 col-sm-2 business-content-arrow">
                <a href="../brand/brand.php?id=<?php echo $alliancesDetails[$i]['id'];?>"  class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>-
        </div>
      </div>
      <?php } ?>
    </div>
  </div><!-- container -->
</section>
<?php include("../layout/footer.php");  ?>
