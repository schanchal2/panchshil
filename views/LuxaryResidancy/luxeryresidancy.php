<?php include("../layout/header.php");  ?>

<section class="hero-section hero-banner-img">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">offering</a> / <a href="#">Residence</a>
          </div>
          <h1>Luxury  Residence</h1>
          <div class="container">
            <div class="row">
              <form class="residence-property-finder">
                <div class="form-group">
                  <select class="form-control">
                    <option disabled selected>LOCATION ALL</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                  </select>
                </div>
                <div class="form-group">
                  <select class="form-control">
                    <option disabled selected>PRICE ALL</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                  </select>
                </div>
                <div class="form-group">
                  <select class="form-control">
                    <option disabled selected>BHK ALL</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                  </select>
                </div>
                <div class="form-group">
                  <select class="form-control">
                    <option disabled selected>CARPET AREA -</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                  </select>
                </div>
                <a href="#" class="btn btn-default btn-apply-filter">APPLY FILTER</a><br/>
                <a href="#"class="btn-reset-selection"><i class="fa fa-repeat" aria-hidden="true"></i> RESET SELECTION</a>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container residency-container">
    <div class="row">
      <div class="col-md-7">
        <p><span class="header-residency-count">29</span> <span class="header-residency-title">PROPERTIES FOUND</span></p>
      </div>
      <div class="col-md-3" style="padding: 10px;">
        <span class="header-residency-title">PROPERTY TYPE</span>
        <select class="residency-property-type">
          <option selected>ALL</option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
        </select>
      </div>
      <div class="col-md-2" style="padding: 10px;">
        <span class="header-residency-title">VIEW TYPE</span>
        <span class="view-type-icon">
        <a href="#"><i class="fa fa-th" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-th-list" aria-hidden="true"></i></span></a>
      </div>
    </div>
  </div>
</section>

<section class="residency-section">
  <div class="container residency-content">
    <div class="row">
      <div class="col-md-4 mg-top">
          <div class="row">
            <div class="col-md-12">
              <div class="hovereffect">
                  <img class="img-responsive" src="../../img/project-residential-1.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <a class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="residency-img-buttom-text">
                    <h4>YooPune</h4>
                    <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD</p>
                    <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Podium Villa</h4>
                  <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-3.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Trump Tower, Pune</h4>
                  <p><i class="fa fa-circle platinum" aria-hidden="true"></i> PLATINUM</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-4 mg-top">
          <div class="row">
            <div class="col-md-12">
              <div class="hovereffect">
                  <img class="img-responsive" src="../../img/project-residential-1.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <a class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="residency-img-buttom-text">
                    <h4>YooPune</h4>
                    <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD</p>
                    <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Podium Villa</h4>
                  <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-3.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Trump Tower, Pune</h4>
                  <p><i class="fa fa-circle platinum" aria-hidden="true"></i> PLATINUM</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 mg-top">
          <div class="row">
            <div class="col-md-12">
              <div class="hovereffect">
                  <img class="img-responsive" src="../../img/project-residential-1.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <a class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="residency-img-buttom-text">
                    <h4>YooPune</h4>
                    <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD</p>
                    <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-2.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Podium Villa</h4>
                  <p><i class="fa fa-circle gold" aria-hidden="true"></i> GOLD</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="hovereffect">
                <img class="img-responsive" src="../../img/project-residential-3.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
                     <a class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                     <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="residency-img-buttom-text">
                  <h4>Trump Tower, Pune</h4>
                  <p><i class="fa fa-circle platinum" aria-hidden="true"></i> PLATINUM</p>
                  <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 text-center common-btn">
        <br/>
        <a href="#" class="btn btn-default residency-loading-btn"> LOAD MORE PROPERTIES</a>
      </div>
    </div>

  </div>
</section>


  <?php include("../layout/footer.php");  ?>
