<?php
 	require_once '../../admin/utilities/config.php';
	require_once "../../admin/utilities/dbUtils.php";
	require_once "../../admin/utilities/utilities.php";
	require_once "../../admin/utilities/errorMap.php";
  require_once "../../admin/model/propertyContentModel.php";
  	require_once "../../admin/model/offeringDetailsModel.php";

  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
    $conn = $conn["conn"];
    $pagename="manageOffering.php";
    $id=cleanXSS($_GET['id']);
    $getPropertyDetailsById = getPropertyDetailsById($id,$conn);
    if(noError($getPropertyDetailsById)){
        $getPropertyDetailsById = $getPropertyDetailsById["response"];
    }else{
        $returnArr = $getPropertyDetailsById;
    }

    $offeringDetailsInfo = getofferingDetailsById($id,$conn);
    if(noError($offeringDetailsInfo)){
        $offeringDetailsInfo = $offeringDetailsInfo["response"];
    }else{
        $returnArr = $offeringDetailsInfo;
    }


  }else{
    $returnArr = $conn;
    exit;
  }
  //printArr($offeringDetailsInfo);
  ?>
  <?php
  if(!empty($id)){ ?>
    <html>

    <body class="nav-is-fixed">
      <?php include '../layout/header.php' ?>
      <div style="height:120px;"></div>

  <!-- header-->
  <!--banner starts here-->
  <div class="banner-commercial" style="background-image: url(<?php echo $rootUrl."uploads/".$offeringDetailsInfo[0]["big_image"]; ?>);">
   <div class="container-fluid nopadding">
  	<ol class="breadcrumb">
      <li><a href="../home/home.php">Home</a></li>
      <li><a href="../about/about.php">Offerings</a></li>
      <li class="active">Commercial</li>

  	</ol>
  </div><h1>Commercial</h1>

  </div>


<section>
  <div class="container residency-container">
    <div class="row">
      <div class="col-md-4">
        <p><span class="header-residency-count didot-font">29</span> <span class="header-residency-title">PROPERTIES FOUND</span></p>
      </div>
    </div>
  </div>
</section>

<section class="residency-section">
  <div class="container residency-content">
    <div class="row">
        <?php for($j=0; $j < count( $getPropertyDetailsById); $j++) { ?>
      <div class="col-md-4 col-sm-6 col-xs-12 mg-top">
          <div class="row">
            <div class="col-md-12 commercial-bg">
              <div class="hovereffect">
                  <img class="img-responsive" src="<?php echo $rootUrl."uploads/".$getPropertyDetailsById[$j]["image"]; ?>" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <a class="info" href="../expresstowers/express-towers.php?id=<?php echo $getPropertyDetailsById[$j]["id"];?>"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="residency-img-buttom-text">
                    <h4><?php echo $getPropertyDetailsById[$j]['name']?></h4>
                    <p>
                      <p><i class="fa fa-circle platinum" aria-hidden="true"></i> <?php echo $getPropertyDetailsById[$j]['property_type']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> <?php echo $getPropertyDetailsById[$j]['status']?></p>

                    <p><?php echo $getPropertyDetailsById[$j]['area']?></p>
                  </div>
              </div>
          </div>
      </div>
      <?php } ?>
    </div>


    <div class="row">
      <div class="col-md-12 text-center common-btn">
        <br/>
        <a href="#" class="btn btn-default residency-loading-btn"> LOAD MORE PROPERTIES</a>
      </div>
    </div>

  </div>
</section>
<?php include '../layout/footer.php' ?>
<?php
}else{
		header('location:../home/home.php');
} ?>
