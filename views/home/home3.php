<?php
 	include("../layout/header.php");


	require_once "../../admin/model/headerContentModel.php";
	require_once "../../admin/model/matricesContentModel.php";
	require_once "../../admin/model/businessCategoryDetailsModel.php";

	$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
	if(noError($conn)){
		$conn = $conn["conn"];
		$headerInfo = getHeaderContent('home',$conn);
		$matricsInfo = getMatricsContent(NULL ,$conn);
		$businessCatInfo = getBusinessCatDetails(NULL,1,'status',$conn);

		if(noError($businessCatInfo)){
			$businessCatInfo = $businessCatInfo["response"];

		}else{
			$returnArr = $businessCatInfo;
		}

		if(noError($headerInfo)){
			$headerInfo = $headerInfo["response"];



		}else{
			$returnArr = $headerInfo;
		}

		if(noError($matricsInfo)){
			$matricsInfo =  $matricsInfo["response"];
		}else{
			$returnArr = $matricsInfo;
		}
	}else{
		$returnArr = $conn;
		exit;
	}
?>
<!-- Slider Section -->


<!-- banner starts here -->
<div class="bs-example" data-example-id="carousel-with-captions">
   <div class="carousel slide home-page-carousel" id="carousel-example-captions" data-ride="carousel">
      <!-- <ol class="carousel-indicators">
         <li data-target="#carousel-example-captions" data-slide-to="0" class=""></li>
         <li data-target="#carousel-example-captions" data-slide-to="1" class=""></li>
         <li data-target="#carousel-example-captions" data-slide-to="2" class="active"></li>
      </ol> -->
      <div class="carousel-inner" role="listbox">
         <div class="item active">
            <img alt="900x500" data-src="<?php echo $rootUrl.'uploads/'.$headerInfo[0]["image"];?>" src="<?php echo $rootUrl.'uploads/'.$headerInfo[0]["image"];?>" data-holder-rendered="true">
            <div class="carousel-caption">
               <h3>Home</h3>
               <p>We seek to touch your life with all luxuries you've dreamt of. Towards establishing all such endeavours we present an array of privileges for our esteemed members.</p>
<a class="btn btn-default" href="About">KNOW MORE</a><br/><br/><br/><br/>
            </div>
         </div>
         <div class="item">
            <img alt="900x500" data-src="<?php echo $rootUrl.'uploads/'.$headerInfo[0]["image"];?>" src="<?php echo $rootUrl.'uploads/'.$headerInfo[0]["image"];?>" data-holder-rendered="true">
            <div class="carousel-caption">
               <h3>About Us</h3>
               <p>We seek to touch your life with all luxuries you've dreamt of. Towards establishing all such endeavours we present an array of privileges for our esteemed members.</p>
<a class="btn btn-default" href="About">KNOW MORE</a><br/><br/><br/><br/>
            </div>
         </div>

      </div>
      <a href="#carousel-example-captions" class="left carousel-control" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a href="#carousel-example-captions" class="right carousel-control" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
   </div>
</div>
<!-- banner starts here -->


<!-- Slider Section end -->
<!-- Metrics -->
  <?php
    $colmd = count($matricsInfo);
      switch ($colmd) {
        case '1':
          $colmd = 12;
          break;
        case '2':
          $colmd = 6;
          break;
        case '3':
          $colmd = 4;
          break;
        case '4':
          $colmd = 3;
          break;
        case '5':
          $colmd = 2;
          break;

      }
  ?>
<div class="container">
	<div class="footer-auto-center">
		<div class="border-bottom">
		    <?php for($i=0; $i < count($matricsInfo); $i++){  ?>
			    <div class="block-homepage-top">
			        <div class="footer-quick-link homepage-top-thumb">
			        	<img src="<?php echo $rootUrl.'uploads/'.$matricsInfo[$i]["matrics_logo"];?>">
			        	<p><?php echo $matricsInfo[$i]["matrics_text"];?></p>
						<?php
						  	$matricsTitle = trim($matricsInfo[$i]["matrics_text"]);
						    $matricsTitle  =  strlen($matricsTitle);
							if ($matricsTitle < 19 ) {
								echo "<br>";
							}
						?>
			        	<h2><?php echo $matricsInfo[$i]["no_of_count"]?></h2>
			      	</div>
			   	</div>
		    <?php } ?>
		</div>
	</div>
</div>
<!-- Metrics end -->
<!-- Featured	projects -->
	<div class="container-fluid">
		<div class="col-md-12">
	   		<h2 class="feature-project text-center didot-font">Featured Projects</h2>
	 	</div>




    <div id="Carousel" class="carousel slide carousel-fade feature-slider col-lg-12">
            <ol class="carousel-indicators">
              <li data-target="#carousel" data-slide-to="0" class="active"></li>
              <li data-target="#carousel" data-slide-to="1"></li>
              <li data-target="#carousel" data-slide-to="2"></li>
              <li data-target="#carousel" data-slide-to="3"></li>
              <li data-target="#carousel" data-slide-to="4"></li>
            </ol>

            <div class="carousel-inner">
              <div class="item active">
    	     			<div class="container">
    	       				<div class="row">
    	         				<div class="col-md-1 col-sm-1 col-xs-1 feature-project-share-col">
    	         					<a href="#"><img src="../../img/share-icon.jpg"></a>
    	         				</div>
    	         				<div class="col-md-7 feature-project-img">
    	           					<img src="../../img/superluxuryvilla.jpg" class="img-responsive"/ >
    	         				</div>
    	         				<div class="col-md-4">
    	           					<div class="feature-project-luxury">
    	             					<div class="">
    	                 					<span><i class="fa fa-bars" aria-hidden="true"></i> LUXURY RESIDENCE</span>
    	             					</div>
    	             					<div class="">
    	               						<h3>Super Luxury Villa</h3>
    	             					</div>
    	             					<div class="feature-project-luxury-info">
    	               						<div>
    						                 	<p class="margin-bottom-none">
    						                   Project Description: we seek to touch your life with all luxuries you've dreamt of. Towards establishing all such endeavours we present an array of privileges for our esteemed members.
    						                 	</p>
    	               						</div>
    	             					</div>
    	             					<div class="feature-project-luxury-btn">
    	               						<div class="common-btn">
    	                   						<a class="btn btn-default" href="#">VIEW PROPERTY</a>
    	               						</div>
    	             					</div>
    	           					</div>
    	         				</div>
    	       				</div>
    	     			</div>
    	   			</div>
              <div class="item">
                 <div class="container">
                     <div class="row">
                       <div class="col-md-1 col-sm-1 col-xs-1 feature-project-share-col">
                         <a href="#"><i class="fa fa-share-alt fa-3x feature-project-share-icon" aria-hidden="true"></i></a>
                       </div>
                       <div class="col-md-7 feature-project-img">
                           <img src="../../img/superluxuryvilla.jpg" class="img-responsive"/>
                       </div>
                       <div class="col-md-4">
                           <div class="feature-project-luxury">
                             <div class="">
                                 <span><i class="fa fa-bars" aria-hidden="true"></i> LUXURY RESIDENCE</span>
                             </div>
                             <div class="">
                                 <h3>Super Luxury Villa</h3>
                             </div>
                             <div class="feature-project-luxury-info">
                                 <div>
                                   <p class="margin-bottom-none">
                                     Project Description: we seek to touch your life with all luxuries you've dreamt of. Towards establishing all such endeavours we present an array of privileges for our esteemed members.

                                   </p>
                                 </div>
                             </div>
                             <div class="feature-project-luxury-btn">
                                 <div class="common-btn">
                                     <a class="btn btn-default" href="#">VIEW PROPERTY</a>
                                 </div>
                             </div>
                           </div>
                       </div>
                     </div>
                  <!-- Controls -->
                 </div>
              </div>
              <div class="item">
     			    	<div class="container">
     			       		<div class="row">
     			         		<div class="col-md-1 col-sm-1 col-xs-1 feature-project-share-col">
     			         			<a href="#"><i class="fa fa-share-alt fa-3x feature-project-share-icon" aria-hidden="true"></i></a>
     			         		</div>
     			         		<div class="col-md-7 feature-project-img">
     			           			<img src="../../img/superluxuryvilla.jpg" class="img-responsive"/>
     			         		</div>
     			         		<div class="col-md-4">
     			           			<div class="feature-project-luxury">
     			             			<div>
     			                 			<span><i class="fa fa-bars" aria-hidden="true"></i> LUXURY RESIDENCE</span>
     			             			</div>
     			             			<div>
     			               				<h3>Super Luxury Villa</h3>
     			             			</div>
     			             			<div class="feature-project-luxury-info">
     			               				<div>
     			                 				<p class="margin-bottom-none">
                                     Project Description: we seek to touch your life with all luxuries you've dreamt of. Towards establishing all such endeavours we present an array of privileges for our esteemed members.

     			                 				</p>
     			               				</div>
     			             			</div>
     			             			<div class="feature-project-luxury-btn">
     			               				<div class="common-btn">
     			                   				<a class="btn btn-default" href="#">VIEW PROPERTY</a>
     			               				</div>
     			             			</div>
     			           			</div>
     			         		</div>
     			       		</div>
     			       <!-- Controls -->
     			     	</div>
     			   </div>
             <div class="item">
    			    	<div class="container">
    			       		<div class="row">
    			         		<div class="col-md-1 col-sm-1 col-xs-1 feature-project-share-col">
    			         			<a href="#"><i class="fa fa-share-alt fa-3x feature-project-share-icon" aria-hidden="true"></i></a>
    			         		</div>
    			         		<div class="col-md-7 feature-project-img">
    			           			<img src="../../img/superluxuryvilla.jpg" class="img-responsive"/>
    			         		</div>
    			         		<div class="col-md-4">
    			           			<div class="feature-project-luxury">
    			             			<div>
    			                 			<span><i class="fa fa-bars" aria-hidden="true"></i> LUXURY RESIDENCE</span>
    			             			</div>
    			             			<div>
    			               				<h3>Super Luxury Villa</h3>
    			             			</div>
    			             			<div class="feature-project-luxury-info">
    			               				<div>
    			                 				<p class="margin-bottom-none">
                                    Project Description: we seek to touch your life with all luxuries you've dreamt of. Towards establishing all such endeavours we present an array of privileges for our esteemed members.

    			                 				</p>
    			               				</div>
    			             			</div>
    			             			<div class="feature-project-luxury-btn">
    			               				<div class="common-btn">
    			                   				<a class="btn btn-default" href="#">VIEW PROPERTY</a>
    			               				</div>
    			             			</div>
    			           			</div>
    			         		</div>
    			       		</div>
    			       <!-- Controls -->
    			     	</div>
    			   </div>
             <div class="item">
    			    	<div class="container">
    			       		<div class="row">
    			         		<div class="col-md-1 col-sm-1 col-xs-1 feature-project-share-col">
    			         			<a href="#"><i class="fa fa-share-alt fa-3x feature-project-share-icon" aria-hidden="true"></i></a>
    			         		</div>
    			         		<div class="col-md-7 feature-project-img">
    			           			<img src="../../img/superluxuryvilla.jpg" class="img-responsive"/>
    			         		</div>
    			         		<div class="col-md-4">
    			           			<div class="feature-project-luxury">
    			             			<div>
    			                 			<span><i class="fa fa-bars" aria-hidden="true"></i> LUXURY RESIDENCE</span>
    			             			</div>
    			             			<div>
    			               				<h3>Super Luxury Villa</h3>
    			             			</div>
    			             			<div class="feature-project-luxury-info">
    			               				<div>
    			                 				<p class="margin-bottom-none">
                                    Project Description: we seek to touch your life with all luxuries you've dreamt of. Towards establishing all such endeavours we present an array of privileges for our esteemed members.

    			                 				</p>
    			               				</div>
    			             			</div>
    			             			<div class="feature-project-luxury-btn">
    			               				<div class="common-btn">
    			                   				<a class="btn btn-default" href="#">VIEW PROPERTY</a>
    			               				</div>
    			             			</div>
    			           			</div>
    			         		</div>
    			       		</div>
    			       <!-- Controls -->
    			     	</div>
    			   </div>
            </div>

            <a class="left carousel-control" href="#Carousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#Carousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
    </div>
</div>
<!-- Featured	projects end -->
<!-- Projects -->
<!-- <section class="projects-section">
	<div class="container">
		<div class="row">
		    <h2 class="didot-font">Projects</h2>
		    <?php for($i=0; $i < 4; $i++){ ?>
			<div class="col-md-3 col-sm-12 projects">
				<div>
          		<div class="project-img-zoom">
					  <a href="#"><img src="<?php echo $rootUrl.'uploads/'.$businessCatInfo[$i]["category_big_image"]; ?>" class="img-responsive" alt="Projects image"/></a>
         		</div>
						<h3><a href="#"><span><i class="fa fa-bars" aria-hidden="true"></i></span><?php echo $businessCatInfo[$i]['category_name']; ?></a></h3>

				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section> -->
<!-- Projects end -->
<!-- Projects -->
<section class="projects-section">
	<div class="container-fluid">
		<div class="row">
		    <h2 class="didot-font">Projects</h2>
		    			<div class="col-md-3 col-sm-12 projects">
				<div>
          		<div class="project-img-zoom">
					  <a href="../commercial/commercial.php"><img src="../../img/project_commercial.jpg" class="img-responsive" alt="Projects image"/></a>
         		</div>
						<h3><div>
                <img  src="../../img/bars.png" class="vertical-image">
                <span class="vertical-text"><a href="../commercial/commercial.php">&nbsp;&nbsp;Commercial</a></span>
            </div></h3>



				</div>
			</div>
						<div class="col-md-3 col-sm-12 projects">
				<div>
          		<div class="project-img-zoom">
					  <a href="../luxuryresidancy/luxeryresidancy.php"><img src="../../img/project_residential.jpg" class="img-responsive" alt="Projects image"/></a>
         		</div>

            <h3><div>
                <img  src="../../img/bars.png" class="vertical-image">
                <span class="vertical-text"><a href="../luxuryresidancy/luxeryresidancy.php">&nbsp;&nbsp;Residential</a></span>
            </div></h3>
				</div>
			</div>
						<div class="col-md-3 col-sm-12 projects">
				<div>
          		<div class="project-img-zoom">
					  <a href="../hospitality/hospitality.php"><img src="../../img/project_hospitality.jpg" class="img-responsive" alt="Projects image"/></a>
         		</div>

            <h3><div>
                <img  src="../../img/bars.png" class="vertical-image">
                <span class="vertical-text"><a href="../hospitality/hospitality.php">&nbsp;&nbsp;Hospitality</a></span>
            </div></h3>
				</div>
			</div>
						<div class="col-md-3 col-sm-12 projects">
				<div>
          		<div class="project-img-zoom">
					  <a href="#"><img src="../../img/project_retail.jpg" class="img-responsive" alt="Projects image"/></a>
         		</div>
            <h3><div>
                <img  src="../../img/bars.png" class="vertical-image">
                <span class="vertical-text"><a href="#">&nbsp;&nbsp;Retail</a></span>
            </div></h3>

				</div>
			</div>
					</div>
	</div>
</section>
<!-- Projects end -->
<!-- Updates -->
	<section class="updates-section">
		<div class="container">
			<div class="row">
				<h2 class="didot-font">Updates</h2>
				<div class="col-md-4">
					<a href="#"><img src="../../img/updates_1.jpg" class="project-img-zoom img-responsive" alt="Updates image"></a>
					<div class="row no-padding">
						<div class="col-sm-3 col-xs-4 NewsUpdate">
							<div style="text-align:left;">
								<p class="date didot-font">30</p>
								<p class="month">JULY</p>
							</div>
						</div>
						<div class="col-sm-9 col-xs-8 updates-content">
							<p>Panchshil unveils homes next to Mahalaxmi Race course</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<a href="#"><img src="../../img/updates_2.jpg" class="img-responsive" alt="Updates image"></a>
					<div class="row">
						<div class="col-sm-3 col-xs-4 NewsUpdate">
              <div style="text-align:left;">
								<p class="date didot-font">17</p>
								<p class="month">JULY</p>
							</div>
						</div>
						<div class="col-sm-9 col-xs-8 updates-content">
							<p>Panchshil Properties records 14% growth in net profit.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<a href="#"><img src="../../img/updates_3.jpg" class="img-responsive" alt="Updates image"></a>
					<div class="row">
						<div class="col-sm-3 col-xs-4 NewsUpdate">
              <div style="text-align:left;">
                <p class="date didot-font">26</p>
                <p class="month">JULY</p>
              </div>
						</div>
						<div class="col-sm-9 col-xs-8 updates-content">
							<p>Panchshil unveils homes next to Mahalaxmi Race course</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="common-btn">
						<a href="#" class="btn btn-default">READ ALL UPDATES</a>
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- Updates end -->
<!-- World	Family, Simpler & Agent	 -->
	<div class="container-fluid panchashil-feachers">
		<div class="row">
			<div class="col-md-4 feachers-col-padding">
				<div class="feachers-overlay project-img-zoom feachers-first">
					<img src="../../img/world-family.jpg" class="img-responsive" alt="world family image">
					<div class="feachers-overlay-text feachers-btn">
						<p>Welcome to the</p>
						<h2 class="didot-font">World<br/>Family</h2>
						<br/>
						<a href="#" class="btn btn-default"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>&nbsp;&nbsp; Register for free membership</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 feachers-col-padding">
				<div class="feachers-overlay project-img-zoom">
					<img src="../../img/simpler.jpg" class="img-responsive" alt="world family image">
					<div class="feachers-overlay-text feachers-btn">
						<p>making life</p>
						<h2>Simpler</h2>
						<br/>
						<a href="#" class="btn btn-default"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>&nbsp;&nbsp; login / sign up</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 feachers-col-padding last-section">
				<div class="feachers-overlay project-img-zoom">
					<img src="../../img/estate_agent.jpg" class="img-responsive" alt="world family image">
					<div class="feachers-overlay-text feachers-btn">
						<h2 class="feachers-3rd-text-size">are<br/>you<br/>an<br/>estate<br/>agent?</h2>
						<br/>
						<a href="../rec/rec.php" class="btn btn-default"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>&nbsp;&nbsp; look for rec details here</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- World	Family, Simpler & Agent	end -->
<?php include("../layout/footer.php");?>
