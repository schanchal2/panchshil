<?php
 	include("../layout/header.php");

 	require_once '../../admin/utilities/config.php';
	require_once "../../admin/utilities/dbUtils.php";
	require_once "../../admin/utilities/utilities.php";
	require_once "../../admin/utilities/errorMap.php";
	require_once "../../admin/model/headerContentModel.php";
	require_once "../../admin/model/matricesContentModel.php";
	require_once "../../admin/model/businessCategoryDetailsModel.php";

	$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
	if(noError($conn)){
		$conn = $conn["conn"];
		$headerInfo = getHeaderContent('home',$conn);
		$matricsInfo = getMatricsContent(NULL ,$conn);
		$businessCatInfo = getBusinessCatDetails(NULL,1,'status',$conn);

		if(noError($businessCatInfo)){
			$businessCatInfo = $businessCatInfo["response"];

		}else{
			$returnArr = $businessCatInfo;
		}

		if(noError($headerInfo)){
			$headerInfo = $headerInfo["response"];
		


		}else{
			$returnArr = $headerInfo;
		}

		if(noError($matricsInfo)){
			$matricsInfo =  $matricsInfo["response"];
		}else{
			$returnArr = $matricsInfo;
		}
	}else{
		$returnArr = $conn;
		exit;
	}
?>
<!-- Slider Section -->
	<div class="container-fluid slider-section">
		<div class="slider-content">
   			<div id="carousel-example-generic" class="carousel slide about-slider" data-ride="carousel">
   			<!-- Wrapper for slides -->
   				<div class="carousel-inner" role="listbox">
   					<?php if(count($headerInfo) <= 0 ){  ?>
              				<img class="img-responsive" src="<?php echo $rootUrl.$homeFeatureImage; ?>"/>
              				<div class="carousel-caption">
         					<div class="slider-over-text">
           						<div class="common-btn">
             						<h2 class="content-title didot-bold-font">About Panchashil</h2>
             						<p class="content-info">
               						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
             						</p>
             						<a class="btn btn-default" href="#">KNOW MORE</a>
           						</div>
         					</div>
       					</div>
            		<?php } else{ 
   					for($i=0; $i < count($headerInfo); $i++){  ?>
     				<div class="item <?php  if($i == 0) echo 'active'; ?>">
     					
         				<img class="img-responsive" src="<?php echo $rootUrl.'uploads/'.$headerInfo[$i]["image"];?>" alt="Chania1">
       					
       					<div class="carousel-caption">
         					<div class="slider-over-text">
           						<div class="common-btn">
             						<h2 class="content-title didot-bold-font"><?php echo $headerInfo[$i]["title"];?></h2>
             						<p class="content-info">
               						<?php echo $headerInfo[$i]["description"];?>
             						</p>
             						<a class="btn btn-default" href="<?php echo $headerInfo[$i]["know_more_link"]?>">KNOW MORE</a>
           						</div>
         					</div>
       					</div>
     				</div>

	     			<?php }
	     				}
	     			 ?>
   				</div>

   			<!-- Controls -->
   			<?php if(count($headerInfo) > 0 && count($headerInfo) > 1 ){  ?>
   				<div class="silder-controls">
     				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
       					<span class="glyphicon glyphicon-arrow-left silder-controls-bg-white" aria-hidden="true"></span>
       					<span class="sr-only">Previous</span>
     				</a>
     				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    					<span class="glyphicon glyphicon-arrow-right silder-controls-bg-white" aria-hidden="true"></span>
    					<span class="sr-only">Next</span>
  					</a>
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
<!-- Slider Section end -->
<!-- Metrics -->
	<?php
		$colmd = count($matricsInfo);
			switch ($colmd) {
				case '1':
					$colmd = 12;
					break;
				case '2':
					$colmd = 6;
					break;
				case '3':
					$colmd = 4;
					break;
				case '4':
					$colmd = 3;
					break;
				case '5':
					$colmd = 2;
					break;

			}
	?>
	<div class="container">
		<div class="row metrics-section">
			<?php for($i=0; $i < count($matricsInfo); $i++){  ?>
			<div class="col-md-<?php echo $colmd; ?> col-sm-12 metrics <?php  if($colmd == 2 && $i == 0) echo 'col-md-offset-1'; ?>">
				<img src="<?php echo $rootUrl.'uploads/'.$matricsInfo[$i]["matrics_logo"];?>"/>
				<p><?php echo $matricsInfo[$i]["matrics_text"]?></p>
				<h2 class="didot-font"><?php echo $matricsInfo[$i]["no_of_count"]?></h2>
			</div>
			<?php } ?>
		</div>
	</div>
<!-- Metrics end -->
<!-- Featured	projects -->
	<div class="container-fluid">
		<div class="col-md-12">
	   		<h2 class="feature-project text-center didot-font">Featured Projects</h2>

	 	</div>
	 	<div id="carousel-example-generic2" class="carousel slide feature-slider" data-ride="carousel">
	 	<!-- Indicators -->
	 		<ol class="carousel-indicators">
	   			<li data-target="#carousel-example-generic2" data-slide-to="0" class="active"></li>
	   			<li data-target="#carousel-example-generic2" data-slide-to="1"></li>
	   			<li data-target="#carousel-example-generic2" data-slide-to="2"></li>
	 		</ol>

	 	<!-- Wrapper for slides -->
	 		<div class="carousel-inner bg-white" role="listbox">
	   			<div class="item active">
	     			<div class="container">
	       				<div class="row">
	         				<div class="col-md-1 feature-project-share-col">
	         					<a href="#"><i class="fa fa-share-alt fa-3x feature-project-share-icon" aria-hidden="true"></i></a>
	         				</div>
	         				<div class="col-md-7 feature-project-img">
	           					<img src="../../img/superluxuryvilla.jpg" class="img-responsive"/ >
	         				</div>
	         				<div class="col-md-4">
	           					<div class="feature-project-luxury">
	             					<div class="">
	                 					<span><i class="fa fa-bars" aria-hidden="true"></i> LUXURY RESIDENCE</span>
	             					</div>
	             					<div class="">
	               						<h3>Super Luxury Villa</h3>
	             					</div>
	             					<div class="feature-project-luxury-info">
	               						<div>
						                 	<p class="margin-bottom-none">
						                   	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						                   	Morbi vel elit blandit, tempor orci consectetur, egestas quam.
						                  	Sed aliquet elit orci, varius fringilla erat rutrum tincidunt.
						                   	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						                   	Maecenas sed egestas leo.
						                 	</p>
	               						</div>
	             					</div>
	             					<div class="feature-project-luxury-btn">
	               						<div class="common-btn">
	                   						<a class="btn btn-default" href="#">VIEW PROPERTY</a>
	               						</div>
	             					</div>
	           					</div>
	         				</div>
	       				</div>
	     			</div>
	   			</div>

			   <div class="item">
			    	<div class="container">
			       		<div class="row">
			         		<div class="col-md-1 feature-project-share-col">
			         			<a href="#"><i class="fa fa-share-alt fa-3x feature-project-share-icon" aria-hidden="true"></i></a>
			         		</div>
			         		<div class="col-md-7 feature-project-img">
			           			<img src="../../img/superluxuryvilla.jpg" class="img-responsive"/>
			         		</div>
			         		<div class="col-md-4">
			           			<div class="feature-project-luxury">
			             			<div class="">
			                 			<span><i class="fa fa-bars" aria-hidden="true"></i> LUXURY RESIDENCE</span>
			             			</div>
			             			<div class="">
			               				<h3>Super Luxury Villa</h3>
			             			</div>
			             			<div class="feature-project-luxury-info">
			               				<div>
			                 				<p class="margin-bottom-none">
			                   				Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			                   				Morbi vel elit blandit, tempor orci consectetur, egestas quam.
			                   				Sed aliquet elit orci, varius fringilla erat rutrum tincidunt.
			                   				Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			                   				Maecenas sed egestas leo.
			                 				</p>
			               				</div>
			             			</div>
			             			<div class="feature-project-luxury-btn">
			               				<div class="common-btn">
			                   				<a class="btn btn-default" href="#">VIEW PROPERTY</a>
			               				</div>
			             			</div>
			           			</div>
			         		</div>
			       		</div>
			       <!-- Controls -->
			     	</div>
			   </div>

			   <div class="item">
			    	<div class="container">
			       		<div class="row">
			         		<div class="col-md-1 feature-project-share-col">
			         			<a href="#"><i class="fa fa-share-alt fa-3x feature-project-share-icon" aria-hidden="true"></i></a>
			         		</div>
			         		<div class="col-md-7 feature-project-img">
			           			<img src="../../img/superluxuryvilla.jpg" class="img-responsive"/>
			         		</div>
			         		<div class="col-md-4">
			           			<div class="feature-project-luxury">
			             			<div>
			                 			<span><i class="fa fa-bars" aria-hidden="true"></i> LUXURY RESIDENCE</span>
			             			</div>
			             			<div>
			               				<h3>Super Luxury Villa</h3>
			             			</div>
			             			<div class="feature-project-luxury-info">
			               				<div>
			                 				<p class="margin-bottom-none">
			                   				Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			                   				Morbi vel elit blandit, tempor orci consectetur, egestas quam.
			                   				Sed aliquet elit orci, varius fringilla erat rutrum tincidunt.
			                   				Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			                   				Maecenas sed egestas leo.
			                 				</p>
			               				</div>
			             			</div>
			             			<div class="feature-project-luxury-btn">
			               				<div class="common-btn">
			                   				<a class="btn btn-default" href="#">VIEW PROPERTY</a>
			               				</div>
			             			</div>
			           			</div>
			         		</div>
			       		</div>
			       <!-- Controls -->
			     	</div>
			   </div>

	   		</div>
	   		<a class="left carousel-control" href="#carousel-example-generic2" role="button" data-slide="prev">
	    		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    		<span class="sr-only">Previous</span>
	  		</a>
	  		<a class="right carousel-control" href="#carousel-example-generic2" role="button" data-slide="next">
	    		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	    		<span class="sr-only">Next</span>
	  		</a>
	   </div>
	</div>

<!-- Featured	projects end -->
<!-- Projects -->
<section class="projects-section">
	<div class="container">
		<div class="row">
		    <h2 class="didot-font">Projects</h2>
		    <?php for($i=0; $i < 4; $i++){ ?>
			<div class="col-md-3 col-sm-12 projects">
				<div>
          		<div class="project-img-zoom">
					  <a href="#"><img src="<?php echo $rootUrl.'uploads/'.$businessCatInfo[$i]["category_big_image"]; ?>" class="img-responsive" alt="Projects image"/></a>
         		</div>
						<h3><a href="#"><span><i class="fa fa-bars" aria-hidden="true"></i></span><?php echo $businessCatInfo[$i]['category_name']; ?></a></h3>

				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
<!-- Projects end -->
<!-- Updates -->
	<section class="updates-section">
		<div class="container">
			<div class="row">
				<h2 class="didot-font">Updates</h2>
				<div class="col-md-4">
					<a href="#"><img src="../../img/updates_1.jpg" class="img-responsive" alt="Updates image"></a>
					<div class="row">
						<div class="col-sm-3 col-xs-4 NewsUpdate">
							<div>
								<p class="date didot-font">30</p>
								<p class="month">JULY</p>
							</div>
						</div>
						<div class="col-sm-9 col-xs-8 updates-content">
							<p>Panchshil unveils homes next to Mahalaxmi Race course</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<a href="#"><img src="../../img/updates_2.jpg" class="img-responsive" alt="Updates image"></a>
					<div class="row">
						<div class="col-sm-3 col-xs-4 NewsUpdate">
							<div>
								<p class="date didot-font">17</p>
								<p class="month">JULY</p>
							</div>
						</div>
						<div class="col-sm-9 col-xs-8 updates-content">
							<p>Panchshil Properties records 14% growth in net profit.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<a href="#"><img src="../../img/updates_3.jpg" class="img-responsive" alt="Updates image"></a>
					<div class="row">
						<div class="col-sm-3 col-xs-4 NewsUpdate">
							<div>
								<p class="date didot-font">26</p>
								<p class="month">JUNE</p>
							</div>
						</div>
						<div class="col-sm-9 col-xs-8 updates-content">
							<p>Panchshil unveils homes next to Mahalaxmi Race course</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="common-btn">
						<a href="#" class="btn btn-default">READ ALL UPDATES</a>
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- Updates end -->
<!-- World	Family, Simpler & Agent	 -->
	<div class="container-fluid panchashil-feachers">
		<div class="row">
			<div class="col-md-4 feachers-col-padding">
				<div class="feachers-overlay">
					<img src="../../img/world-family.jpg" class="img-responsive" alt="world family image">
					<div class="feachers-overlay-text feachers-btn">
						<p>Welcome to the</p>
						<h2 class="didot-font">World<br/>Family</h2>
						<br/>
						<a href="#" class="btn btn-default"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>&nbsp;&nbsp; Register for free membership</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 feachers-col-padding">
				<div class="feachers-overlay">
					<img src="../../img/simpler.jpg" class="img-responsive" alt="world family image">
					<div class="feachers-overlay-text feachers-btn">
						<p>making life</p>
						<h2>Simpler</h2>
						<br/>
						<a href="#" class="btn btn-default"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>&nbsp;&nbsp; login / sign up</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 feachers-col-padding">
				<div class="feachers-overlay">
					<img src="../../img/estate_agent.jpg" class="img-responsive" alt="world family image">
					<div class="feachers-overlay-text feachers-btn">
						<h2 class="feachers-3rd-text-size">are<br/>you<br/>an<br/>estate<br/>agent?</h2>
						<br/>
						<a href="#" class="btn btn-default"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>&nbsp;&nbsp; look for rec details here</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- World	Family, Simpler & Agent	end -->
<?php include("../layout/footer.php");?>
