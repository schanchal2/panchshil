
<?php
    include("../layout/header.php");

  require_once '../../admin/utilities/config.php';
  require_once "../../admin/utilities/dbUtils.php";
  require_once "../../admin/utilities/utilities.php";
  require_once "../../admin/utilities/errorMap.php";
  require_once "../../admin/model/headerContentModel.php";
  require_once "../../admin/model/businessDetailsModel.php";
  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
    $conn = $conn["conn"];
    $headerInfo = getHeaderContent('business',$conn);
    //echo "<pre>";print_r($headerInfo); echo "</pre>";
    $businessDetails = getBusinessDetails(NULL,'status',$conn);  //echo "<pre>";print_r($businessDetails); echo "</pre>";
      if(noError($headerInfo)){
        $headerInfo = $headerInfo["response"];
        }else{
        $returnArr = $headerInfo;
      }  
      if(noError($businessDetails)){
          $businessDetails =  $businessDetails["response"];
      }else{
          $returnArr = $businessDetails;
      }
 } else{
    $returnArr = $conn;
    exit;
  }
     $businessPageNames = array(
    "manageOffering.php"    => "offering/offering.php",
    "manageServices.php"    => "businessCatagories/businessCatagories.php" );
?>
<?php if(count($headerInfo) <= 0 ){  ?>
<div class="banner-business" style="background-image: url(<?php echo $rootUrl.$businessFeatureImage ?>);">
  <div class="container-fluid nopadding">
    <ol class="breadcrumb">
      <li><a href="../home/home.php">Home</a></li>
      <li class="active">Business</li>

    </ol>
  </div><h1>Business</h1>
</div>

<section class="business-section">
  <div class="container business-container">
    <div class="row">
      <div class="col-md-12 business-text">
        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </div>
    </div>
  <?php }else{ ?>
<?php for($i=0; $i < count($headerInfo); $i++) { ?>
<div class="banner-business" style="background-image: url(<?php echo $rootUrl.'uploads/'.$headerInfo[$i]["image"];?>);">
 <div class="container-fluid nopadding">
    <ol class="breadcrumb">
    <li><a href="../home/home.php">Home</a></li>
      <li class="active">Business</li>

    </ol>
</div><br><h1><?php echo $headerInfo[$i]["page_name"];?></h1>
</div>

<section class="business-section">
  <div class="container business-container">
    <div class="row">
      <div class="col-md-12 business-text">
        <p class="text-center"><?php echo $headerInfo[$i]["description"];?></p>
      </div>
    </div><?php } } ?>

    <div class="row">
      <?php for($i=0; $i < count($businessDetails); $i++) { ?>
      <div class="<?php if($i == count($businessDetails)-1 && count($businessDetails)%2 != 0) echo "col-md-12"; else{ echo "col-md-6"; }?>">
        <div class="business-content">
          <img src="<?php echo $rootUrl.'uploads/'.$businessDetails[$i]["business_big_image"];?>" class="img-responsive"/>
          <div class="business-content-text">
            <div class="row">
              <div class="col-md-10 col-sm-10 business-content-text-padding">
                <h2><?php echo $businessDetails[$i]["business_name"];?></h2>
                <p class="block-with-text1">
                  <?php 
                  $businessCatDetails = $businessDetails[$i]["business_short_desc"];
                  $businessCatDetails = trim($businessCatDetails);
                  $businessCatLength  =  strlen($businessCatDetails);
                  if ($businessCatLength <= 150 ) {
                        echo $businessCatDetails;
                      } else {
                        echo substr($businessCatDetails,0,150);
                        echo "..."; 
                     }
                  foreach ($businessPageNames as $key => $value) {
                      if ($key == $businessDetails[$i]["pagename"]) {
                          $pageRedirection = "../".$value."?id=".$businessDetails[$i]["id"];
                          break;
                      } else {
                          $pageRedirection = "#";
                      }
                  }
                  ?>
                </p> 
              </div>
              <div class="col-md-2 col-sm-2 business-content-arrow">
                  <a href="<?php echo $pageRedirection; ?>"  class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div><!-- container -->
</section>
<?php include("../layout/footer.php");  ?>
