<?php 

    include("../layout/header.php");
    require_once '../../admin/utilities/config.php';
    require_once "../../admin/utilities/dbUtils.php";
    require_once "../../admin/utilities/utilities.php";
    require_once "../../admin/utilities/errorMap.php";
    require_once "../../admin/model/headerContentModel.php";
    require_once "../../admin/model/businessDetailsModel.php";
    require_once "../../admin/model/businessCategoryDetailsModel.php";

    $id = $_GET['id'];
    $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
    if(noError($conn)){
      $conn = $conn["conn"];
      $businessDetails = getBusinessDetails($id,'status',$conn);
      $businessCatDetails = getBusinessCatDetails(NULL, $id,'status', $conn);
        if(noError($businessCatDetails)){
        $businessCatDetails = $businessCatDetails["response"];  
 
        }else{
        $returnArr = $businessDetails;
       } 
       if(noError($businessDetails)){
        $businessDetails = $businessDetails["response"];
        }else{
        $returnArr = $businessDetails;
      }      
    }else{
      $returnArr = $conn;
      exit;
    }    
?>
<?php for($i=0; $i < count($businessDetails); $i++) { ?>
<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.'uploads/'.$businessDetails[$i]["business_big_image"];?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Business</a> / <a href="#"><?php echo $businessDetails[$i]["business_name"]; ?></a>
          </div>
          <h1><?php echo $businessDetails[$i]["business_name"]; ?></h1>
        </div>
      </div>
    </div>
  </div>
</section>
<?php } ?>

<section class="offering-container-section">
  <?php for($i=0; $i < count($businessCatDetails); $i++) { ?>
  <div class="container">
    <div class="row">
      <div class="col-md-12 offering-container-padding">
        <div class="offering-container">
          <img src="<?php echo $rootUrl.'uploads/'.$businessCatDetails[$i]["category_feature_image"];?>" class="img-responsive"/>
          <div class="offering-container-text">
            <div class="row">
              <div class="col-md-10 col-sm-10 offering-container-text-padding">
                <div class="offering-container-hover-title">
                  <span class="title"><?php echo $businessCatDetails[$i]["category_name"]?></span> <span class="property-count"> 7 PROPERTIES</span>
                </div>
                <p class="block-with-text">
                     <?php 
                         $businessDetails = $businessCatDetails[$i]["category_short_desc"];
                         $businessDetails = trim($businessDetails);
                         $businessLength  =  strlen($businessDetails);
                         if ($businessLength <= 150 ) {
                              echo $businessDetails;
                          } else {
                              echo substr($businessDetails,0,150);
                              echo "..."; 
                          }
                    ?>

                </p>
             

              </div>
              <div class="col-md-2 col-sm-2 offering-container-arrow">
                <a href="../ProjectManagement/project_management.php?id=<?php echo $businessCatDetails[$i]['id'];?>&businessId=<?php echo $id; ?>" class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
 </div>
<?php } ?>
</section>
<?php include("../layout/footer.php");  ?>
