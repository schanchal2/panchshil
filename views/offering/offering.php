<?php   include("../layout/header.php");  ?>
<?php
 	require_once '../../admin/utilities/config.php';
	require_once "../../admin/utilities/dbUtils.php";
	require_once "../../admin/utilities/utilities.php";
	require_once "../../admin/utilities/errorMap.php";
	require_once "../../admin/model/offeringDetailsModel.php";
  require_once "../../admin/model/propertyContentModel.php";

  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
    $conn = $conn["conn"];
    $pagename="manageOffering.php";
    $id=cleanXSS($_GET['id']);
    $offeringDetailsInfo = getofferingDetailsByPageName($pagename,$id,$conn);
    if(noError($offeringDetailsInfo)){
        $offeringDetailsInfo = $offeringDetailsInfo["response"];
    }else{
        $returnArr = $offeringDetailsInfo;
    }
  }else{
    $returnArr = $conn;
    exit;
  }
  //printArr($offeringDetailsInfo);
  ?>
  <?php
  if(!empty($id)){ ?>
<html>

<body class="nav-is-fixed">

  <div style="height:120px;"></div>

  <!-- header-->

<!--banner starts here-->
<div class="banner-offerings" style="background-image: url(<?php echo $rootUrl."uploads/".$offeringDetailsInfo[0]["business_feature_image"]; ?>);">
 <div class="container-fluid nopadding">
	<ol class="breadcrumb">
		<li><a href="../home/home.php">Home</a></li>
		<li><a href="../business/business.php">Business</a></li>
		<li class="active">Offerings</li>
	</ol>
</div><h1><?php echo $offeringDetailsInfo[0]['business_name']?></h1>
</div>


<section class="offering-container-section">
  <div class="container">

    <?php

      $id= $offeringDetailsInfo[0]['id'];
      $offeringCatInfo = getofferingDetailsByAllCategories($id,'active',$conn);
      if(noError($offeringCatInfo)){
          $offeringCatInfo = $offeringCatInfo["response"];
      }else{
          $returnArr = $offeringCatInfo;
      }

      for($j=0; $j < count( $offeringCatInfo); $j++) { ?>
    <div class="row">
      <div class="col-md-12 offering-container-padding">
        <div class="offering-container">
          <img src="<?php echo $rootUrl."uploads/".$offeringCatInfo[$j]["feature_image"]; ?>" class="img-responsive"/>
          <div class="offering-container-text">
      			<div class="row">
      				<div class="col-md-10 col-sm-10 offering-container-text-padding">
                <div class="offering-container-hover-title">
                  <span class="title"><?php echo $offeringCatInfo[$j]['name']?> </span> <span class="property-count">
                  <?php   $b_id= $offeringCatInfo[$j]['id'];
                    $getPropertyDetailsById = getPropertyDetailsById($b_id,$conn);
                    if(noError($getPropertyDetailsById)){
                        $getPropertyDetailsById = $getPropertyDetailsById["response"];
                    }else{
                        $returnArr = $getPropertyDetailsById;
                    }
                    echo count($getPropertyDetailsById);
                    ?>
                  </span>
                </div>
      					<p><?php echo $offeringCatInfo[$j]['shortDesc']?> </p>
      				</div>
              <?php $pagename=$offeringCatInfo[$j]['pagename'];
               if($pagename=="manageluxuryResidences.php"){     ?>
      				<div class="col-md-2 col-sm-2 offering-container-arrow">
      					<a href="../luxuryresidancy/luxeryresidancy.php?id=<?php echo urlencode($offeringCatInfo[$j]['id'])?>" class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
      				</div><?php } ?>

              <?php
               if($pagename=="manageCommercial.php"){     ?>
              <div class="col-md-2 col-sm-2 offering-container-arrow">
                <a href="../Commercial/commercial.php?id=<?php echo urlencode($offeringCatInfo[$j]['id'])?>" class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
              </div><?php } ?>

              <?php
               if($pagename=="manageRetail.php"){     ?>
              <div class="col-md-2 col-sm-2 offering-container-arrow">
                <a href="../retail/retail.php?id=<?php echo urlencode($offeringCatInfo[$j]['id'])?>" class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
              </div><?php } ?>

              <?php
               if($pagename=="manageHospitality.php"){     ?>
              <div class="col-md-2 col-sm-2 offering-container-arrow">
                <a href="../Hospitality/hospitality.php?id=<?php echo urlencode($offeringCatInfo[$j]['id'])?>" class="arrow-hover"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
              </div><?php } ?>

      			</div>
      		</div>
        </div>
      </div>
    </div>
    <?php }  ?>



  </div>
</section>
<?php include '../layout/footer.php' ?>
<?php
}else{
		header('location:../home/home.php');
} ?>
