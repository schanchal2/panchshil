<?php
 	require_once '../../admin/utilities/config.php';
	require_once "../../admin/utilities/dbUtils.php";
	require_once "../../admin/utilities/utilities.php";
	require_once "../../admin/utilities/errorMap.php";
  require_once "../../admin/model/propertyContentModel.php";
  require_once "../../admin/model/bannerContentModel.php";
  require_once "../../admin/model/galleryContentModel.php";
  require_once "../../admin/model/featureSpecificationContentModel.php";
  require_once "../../admin/model/matrixSpecificationContentModel.php";
  require_once "../../admin/model/speciTypeTitleModel.php";

  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
    $conn = $conn["conn"];
    $pagename="manageOffering.php";
    $id=cleanXSS($_GET['id']);
    $getPropertyDetails = getPropertyDetails($id,'NULL',$conn);
    if(noError($getPropertyDetails)){
        $getPropertyDetails = $getPropertyDetails["response"];
    }else{
        $returnArr = $getPropertyDetails;
    }

    $getBannerDetails = getBannerDetailsById($id,$conn);
    if(noError($getBannerDetails)){
        $getBannerDetails = $getBannerDetails["response"];
    }else{
        $returnArr = $getBannerDetails;
    }

    $getGalleryDetails = getGalleryDetailsById($id,$conn);
    if(noError($getGalleryDetails)){
        $getGalleryDetails = $getGalleryDetails["response"];
    }else{
        $returnArr = $getGalleryDetails;
    }

    $getFeatureDetails = getfeatureSpecificationDetailsById($id,'asc',$conn);
    if(noError($getFeatureDetails)){
        $getFeatureDetails = $getFeatureDetails["response"];
    }else{
        $returnArr = $getFeatureDetails;
    }


    $getMatrixDetails = getMatrixSpecificationDetailsById($id,$conn);
    if(noError($getMatrixDetails)){
        $getMatrixDetails = $getMatrixDetails["response"];
    }else{
        $returnArr = $getMatrixDetails;
    }

    $getSpeciTitleDetails = getSpeciTitleDetailsById($id,$conn);
    if(noError($getSpeciTitleDetails)){
        $getSpeciTitleDetails = $getSpeciTitleDetails["response"];
    }else{
        $returnArr = $getSpeciTitleDetails;
    }





  }else{
    $returnArr = $conn;
    exit;
  }
 //printArr($getSpeciTitleDetails);


  ?>
  <?php
  if(!empty($id)){ ?>
    <html>
    <body class="nav-is-fixed">
      <?php include '../layout/header.php' ?>
      <div style="height:120px;"></div>

<section class="property-section">
	<div class="container-fluid">
	  <div class="row">
	    <div class="col-md-12 property-slider">
	      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
              <?php if(!empty($getBannerDetails[0]["banner1"])) {?>
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li><?php } ?>
              <?php if(!empty($getBannerDetails[0]["banner2"])) {?>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li><?php } ?>
                <?php if($getBannerDetails[0]["banner3"]) {?><li data-target="#carousel-example-generic" data-slide-to="2"></li><?php } ?>
                <?php if(!empty($getBannerDetails[0]["banner4"])) {?><li data-target="#carousel-example-generic" data-slide-to="3"></li><?php } ?>
                <?php if(!empty($getBannerDetails[0]["banner5"])) {?><li data-target="#carousel-example-generic" data-slide-to="4"></li><?php } ?>
          </ol>
          <div class="carousel-inner">

            <?php if(isset($getBannerDetails[0]["banner1"])) {?>
          	<div class="item active">
              <img src="<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner1"]; ?>" class="img-responsive" alt="First slide">
              <div class="carousel-caption">
                <p class="didot-font commercial-slider-title">Express Towers</p>
								<!-- <p class="didot-font commercial-slider-subtitle">PUNE</p> -->
              </div>
            </div>
            <?php } ?>



            <?php if(isset($getBannerDetails[0]["banner2"])) {?>
            <div class="item ">
              <img src="<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner2"]; ?>" class="img-responsive" alt="First slide">
              <div class="carousel-caption">
                <p class="didot-font commercial-slider-title">Express Towers</p>
                <!-- <p class="didot-font commercial-slider-subtitle">PUNE</p> -->
              </div>
            </div>
            <?php } ?>
            <?php if(isset($getBannerDetails[0]["banner3"])) {?>
            <div class="item ">
              <img src="<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner3"]; ?>" class="img-responsive" alt="First slide">
              <div class="carousel-caption">
                <p class="didot-font commercial-slider-title">Express Towers</p>
                <!-- <p class="didot-font commercial-slider-subtitle">PUNE</p> -->
              </div>
            </div>
            <?php } ?>
            <?php if(isset($getBannerDetails[0]["banner4"])) {?>
            <div class="item ">
              <img src="<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner4"]; ?>" class="img-responsive" alt="First slide">
              <div class="carousel-caption">
                <p class="didot-font commercial-slider-title">Express Towers</p>
                <!-- <p class="didot-font commercial-slider-subtitle">PUNE</p> -->
              </div>
            </div>
            <?php } ?>

            <?php if(isset($getBannerDetails[0]["banner5"])) {?>
            <div class="item ">
              <img src="<?php echo $rootUrl."uploads/".$getBannerDetails[0]["banner5"]; ?>" class="img-responsive" alt="First slide">
              <div class="carousel-caption">
                <p class="didot-font commercial-slider-title">Express Towers</p>
                <!-- <p class="didot-font commercial-slider-subtitle">PUNE</p> -->
              </div>
            </div>
            <?php } ?>

          </div>
	    	</div>
        <div class="property-path">
					<div class="property-path-link">
	            <a href="../home/home.php">Home</a> / <a href="../offering/offering.php">Offering</a> / <a href="../commercial/commercial.php">Commercial</a> / <a href="#">Express Towers</a>
	        </div>
        </div>
	    </div>
	  </div>
	</div>
</section>

<section class="property-content-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="didot-font text-center"><?php echo $getPropertyDetails[0]["name"]?></h2>
				<br/><br/>
				<p class="text-center"><?php echo $getPropertyDetails[0]["long_desc"]?></p>
			</div>
		</div><!-- row end -->
		<br/>

		<div class="row">
			<div class="col-md-12 text-center">
				<ul class="property-details-link">
					<li><a href="" data-toggle="modal" data-target=".gallery-popup"><i class="fa fa-picture-o" aria-hidden="true"></i> &nbsp;VIEW GALLERY</a></li>
						<li><a href="http://<?php echo $getPropertyDetails[0]["website_url"];?>" target="_blank" ><i class="fa fa-external-link" aria-hidden="true"></i> &nbsp;VISIT WEBSITE</a></li>
				</ul>
			</div>
		</div><!-- row end -->
		<br/><br/>

	<!-- ============
		Gallery popup
	=================-->
	<div class="modal fade gallery-popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content gallery-modal-content">
			<!-- ======================== -->
			<div class="gallery-padding">
				<div id="gallery">
          <?php for($j=0; $j < count( $getGalleryDetails); $j++) { ?>
					<img alt="Preview Image 1"
					 src="<?php echo $rootUrl."uploads/".$getGalleryDetails[$j]["image"]; ?>"
					 data-image="<?php echo $rootUrl."uploads/".$getGalleryDetails[$j]["image"]; ?>"
					 data-description="<?php echo $getGalleryDetails[$j]["description"];?>"/>
           <?php } ?>
				</div>
			</div>
			<!-- ======================== -->
		</div>
		<p class="esc-close">ESC to close window</p>
	</div>
	</div>
	<!-- ============
		Gallery popup end
	=================-->

		<div class="row">
			<div class="col-md-12 property-img-circle">
				<div class="property-img-circle-slider">
            <?php for($j=0; $j < count( $getFeatureDetails); $j++) { ?>
					<div class="img-circle-slider">
		        <a href="#"><img src="<?php echo $rootUrl."uploads/".$getFeatureDetails[$j]["image"]; ?>" class="img-responsive img-circle" alt=""></a>
						<div class="img-overlay-count"><p class="didot-font"><?php echo $getFeatureDetails[$j]["count_id"]; ?></p></div>
						<div class="text">
							<h2 class="didot-font"><?php echo $getFeatureDetails[$j]["count_id"]; ?></h2>
							<p><?php echo $getFeatureDetails[$j]["description"]; ?></p>
						</div>
		      </div>
          <?php } ?>

				</div>
			</div>
		</div><!-- row end -->

		<div class="row">
			<div class="col-md-12 text-center img-hover-text">
				<h2 class="didot-font"><?php echo $getFeatureDetails[0]["count_id"]; ?></h2>
				<p><?php echo $getFeatureDetails[0]["description"]; ?></p>
			</div>
		</div><!-- row end -->


		<br/><br/>
		<hr class="hr-color"/>
    <!-- specifications -->
    <div class="row">
      <h2 class="text-center didot-bold-font">Specifications</h2>
      <br/><br/>

  <?php for($j=0; $j < count( $getMatrixDetails); $j++) { ?>
			<div class="col-md-3 text-center specifications specific-img-height">
        <img src="<?php echo $rootUrl."uploads/".$getMatrixDetails[$j]["image"]; ?>"/>
        <p><?php echo $getMatrixDetails[0]["title"]; ?><br/>(<?php echo $getMatrixDetails[0]["unitTitle"]; ?>)</p>
        <h3 class="didot-font"><?php echo $getMatrixDetails[0]["unitValue"]; ?></h3>
      </div>
      <?php } ?>

    </div>
    <br/><br/><br/>




    <div class="row">
      <div class="col-md-3 col-md-offset-3 text-center">
          <p><b><?php echo $getSpeciTitleDetails[0]["title1"]; ?>:</b></p>
          <p class="specifications-area"><?php echo $getSpeciTitleDetails[0]["desc1"]; ?></p>
          <p class="specifications-area"><?php echo $getSpeciTitleDetails[0]["desc2"]; ?></p>
      </div>
      <div class="col-md-3 text-center">
        <p><b><?php echo $getSpeciTitleDetails[0]["title1"]; ?>:</b></p>
        <p class="specifications-area"><?php echo $getSpeciTitleDetails[0]["desc3"]; ?></p>
        <p class="specifications-area"><?php echo $getSpeciTitleDetails[0]["desc4"]; ?></p>
      </div>
    </div>
    <br/>
    <hr class="hr-color"/>

    <!-- specifications end -->
		<!-- Locate on Map -->
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center didot-bold-font">Locate on Map</h2>
				<br/>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3774.0734989719876!2d72.82012571489886!3d18.92814058717297!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7d1e8d34ef5d7%3A0xd03369a7fcfee789!2sExpress+Towers%2C+Ramnath+Goenka+Marg%2C+Nariman+Point%2C+Mumbai%2C+Maharashtra+400021!5e0!3m2!1sen!2sin!4v1485844488210" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
		<br/><br/>
		<!-- Locate on Map end -->
  </div><!-- container -->
</section>
<?php include '../layout/footer.php' ?>
<?php
}else{
		header('location:../home/home.php');
} ?>
