<?php

  require_once '../admin/utilities/config.php';
  require_once "../admin/utilities/dbUtils.php";
  require_once "../admin/utilities/utilities.php";
  require_once "../admin/utilities/errorMap.php";
  require_once "../admin/model/pressContentModel.php";



$id= $_POST['id'];
    $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
    if(noError($conn)){
      $conn = $conn["conn"];
      $pressInfo = getPressDetails($id,NULL,$conn);
      if(noError($pressInfo)){
        $pressInfo = $pressInfo["response"][0];
      }else{
        $returnArr = $pressInfo;
      }
}
// printArr($pressInfo);
//echo $pressInfo['you_tube_url'];
//echo $rootURL;
      if (!isset($pressInfo['you_tube_url']) || empty($pressInfo['you_tube_url'])) {
    ?>
      <button type="button" class="close btn-modal-close-campexpert bg-red" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true" class="campus-expert-x">&times;</span>
      </button>
      <div class="innerAll">
      <iframe width="854" height="480" src="../../admin/uploads/<?php echo $pressInfo['pdf']; ?>" frameborder="0" allowfullscreen></iframe>
  <!-- mymodal -->
<?php } else { ?>
    <button type="button" class="close btn-modal-close-campexpert bg-red" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true" class="campus-expert-x">&times;</span>
    </button>
    <div class="innerAll">
    <iframe width="854" height="480" src="<?php echo $pressInfo['you_tube_url']; ?>" frameborder="0" allowfullscreen></iframe>
    </div>

<?php } ?>
