 <?php



  require_once '../admin/utilities/config.php'; 

  require_once "../admin/utilities/dbUtils.php";
  require_once "../admin/utilities/utilities.php";
  require_once "../admin/utilities/errorMap.php";
  require_once "../admin/model/clientDetailsModel.php";

  $offset  =   $_GET["offset"];
  if ($offset == 0) {
       $limit   =   15;
  }
  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
    $conn = $conn["conn"];
    $clientInfo = getClientDetails(NULL,client, $conn);
   // $totalClient = count($clientInfo);
    $getClientInfoWithOffset = getClientWithOffset($offset,$limit,$conn);
    
    if(noError($getClientInfoWithOffset)){
      $getClientInfoWithOffset = $getClientInfoWithOffset["response"];        
    }else{
      $returnArr = $getClientInfoWithOffset;
    }
  }else{
    $returnArr = $conn;
    exit;
  }
  // printArrs($getClientInfoWithOffset);
    for($j = 0; $j < count($getClientInfoWithOffset); $j++){
    $flag = true;
 ?>
    <div class="row">
       <?php for($i = $j; $i < ($j + 5) && $i < count($getClientInfoWithOffset); $i++ ) {?>
        <div class="col-md-2 clients-logo <?php if($flag == true){ echo 'col-md-offset-1'; $flag = false; } ?>">
        <center><div class="clients-logo-bg">
          <a href="#"><img class="img-responsive" src="<?php echo $rootUrl.'uploads/'.$getClientInfoWithOffset[$i]["clientColorImage"];?>"/></a>
        </div>
        <p class="text-center"><?php echo $getClientInfoWithOffset[$i]["clientName"];?> <br/> <span><?php echo $getClientInfoWithOffset[$i]["clientServices"];?></span></p>
        <center>
      </div>
        <?php } ?>
    </div>
    <?php 
     $j = $j +4;  
    }?>
  
   
  