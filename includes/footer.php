
<footer>
  <div class="container footer">
<div class="footer-auto-center">
    <div class="block">
      <div class="footer-quick-link">
             <h5><a href="../../views/offering/offering.php">Offerings</a></h5>
             <ul>
             <li><a href="../../views/commercial/commercial.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Commercial</a></li>
             <li><a href="../../views/luxuryresidancy/luxeryresidancy.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Residential</a></li>
             <li><a href="../../views/hospitality/hospitality.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Hospitality</a></li>
             <li><a href="../../views/retail/retail.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Retail</a></li>
              </ul>
           </div>

    </div>
  <div class="block">
       <div class="footer-quick-link">
              <h5><a href="../../views/services/services.php">Services</a></h5>
              <ul>
                       <li><a href="../../views/ProjectManagement/product-management.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Project Management</a></li>
                       <li><a href="../../views/facility_management/facility-management.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Facility Management</a></li>
                       <li><a href="../../views/fit_out_management/fit-out-management.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Fit-Out Management</a></li>
                       <li><a href="../../views/brand_licensing/brand-licensing.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Brand Licensing</a></li>
              </ul>
            </div>

     </div>

     <div class="block"> <div class="footer-quick-link">
     					<h5><a href="../../views/corporateprofile/corporate-profile.php">corporate profile</a></h5>
     					<ul>
     						<li><a href="../../views/foreward/foreward.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>foreword</a></li>
     						<li><a href="../../views/corporateprofile/corporate-profile.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>company profile</a></li>
     						<li><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>growth chronicle</a></li>
     						<li><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>group philosophy</a></li>
     					</ul>
     				</div></div>
            <div class="block"><div class="footer-quick-link">
    					<h5><a href="../../views/media/media.php">Media</a></h5>
    					<ul>
    						<li><a href="../../views/press/press.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>press</a></li>
    						<li><a href="../../views/award/award.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Awards</a></li>
    					</ul>
    					<br>
    					<h5><a href="../../views/downloads/downloads.php">downloads</a></h5>
    				</div></div>
            <div class="block"> <div class="footer-quick-link">
     					<h5><a href="../../views/career/career.php">career</a></h5>
     					<ul>
     						<li><a href="../../views/hralign/hr-align.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>HR align</a></li>
     						<li><a href="../../views/pep/pep.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>PEP</a></li>
     					</ul>
     					<br>
     					<h5><a href="../../views/leadership/leadership.php">leadership</a></h5>
     				</div></div>
       <div class="block"> <div class="footer-quick-link">
     					<h5><a href="../../views/sustainability/sustainability.php">sustainability</a></h5>
     					<ul>
     						<li><a href="../../views/inBusiness/in-business.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>in business</a></li>
     						<li><a href="../../views/csr/csr.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>CSR</a></li>
     					</ul>
     					<br>
     					<h5><a href="../../views/clients/clients.php">clients</a></h5>
     				</div></div>
</div>


	<!-- footer-quick-link end -->
	<!-- contact, location map , follow us -->
		<div class="row">		<div class="container">
			<div class="col-md-4 col-sm-12 contact padding2">
				 <h5 class="footer-header-padding">CONTACT</h5>
				 <div class="row">
           <div class="col-sm-6">
           <p class="office-location">PUNE OFFICE</p>
           <p class="office-address">Tech Park One,
           Tower E,
           191 Yerwada,
           Pune - 411 006
           INDIA </p>
           </div>
                  <div class="col-sm-6">
				 		<p class="office-location">MUMBAI OFFICE</p>
				 		<p class="office-address">PANCHSHIL REALTY
20th Floor, Express towers,
Nariman Point,
Mumbai - 400 021
INDIA</p>
				 	</div>

				 				 </div>
				 <br/><br/>
				 <div class="row">
				 				 	<div class="col-sm-6 contact-no">
				 		<p>T :0123456789 </p>
				 		<p>F : 1023045607</p>
				 	</div>
				 				 	<div class="col-sm-6 contact-no">
				 		<p>T :7894561230 </p>
				 		<p>F : 12344566778</p>
				 	</div>
				 				 </div>
			</div>
			<div class="col-md-4 col-sm-12 location-map">
				<h5 class="footer-header-padding">LOCATION ON MAP</h5>
				<div class="location-content">
					<span>Select Property: </span>
					<select class="location-dropdown-list">
						<option selected>Property Name</option>
						<option>Mumbai</option>
						<option>Pune</option>
					</select>
				</div><br>
				<img src="../../img/worldMap.png" class="img-responsive">
			</div>
			<div class="col-md-4 col-sm-12 follow-us">
				<h5 class="footer-header-padding">FOLLOW US ON</h5>
				<ul class="social-icon">
					<li><a href="" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
					<li><a href="" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
					<li><a href="" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
					<li><a href="" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
					<li><a href="" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
				</ul>
				<br/><br/>

        <div>
  <img style="vertical-align:middle" src="../../img/login-signup.png">
  <span style="font-size:14px;"> LOGIN / SIGN UP</span>
</div>
			</div>
		</div>	</div>
	<!-- contact, location map , follow us end -->
	</div><!-- container -->
	<!-- footer bottom -->

  <div class="sliderside">
      <!-- ngRepeat: cat in categories -->
      <div class="flexside">
          <div class="imgslider">
              <img src="../../img/residential.jpg">
              <!-- <div class="black-overlay"></div> -->
          </div>
          <div class="relativetxt">
              <i class="educa caticons"></i>
              <!-- <p class="ng-binding">Residential</p> -->
              <div class="relativetxt__text">
                  <p class="relativetxt__text__heading"> Luxury  Residences </p>
                  <ul class="relativetxt__text__projectList">
                      <li> <a href="../../views/trump_tower_pune/trump-towers-pune.php"> - Trump Towers Pune     </a> </li>
                      <li> <a href="../../views/yoopune_by_philippes_stark/yoopune-by-philippes-stark.php"> - yoopune By Philippes Stark     </a> </li>
                      <li> <a href="../../views/yoovillas/yoovillas.php"> - YOOVILLAS     </a> </li>
                      <li> <a href="../../views/the_address/the-address.php"> - The Address     </a> </li>
                      <li> <a href="../../views/panchshil_tower/panchshil_towers.php"> - Panchshil Towers     </a> </li>
                      <li> <a href="../../views/eon_waterfront/eon-waterfront.php"> - EON Waterfront     </a> </li>
                  </ul>
                  <div class="clearfix"></div>
              </div>
          </div>
      </div>
      <!-- end ngRepeat: cat in categories -->
      <div class="flexside">
          <div class="imgslider">
              <img src="../../img/commercial.jpg">
              <!-- <div class="black-overlay"></div> -->
          </div>
          <div class="relativetxt">
              <i class="child caticons"></i>
              <!-- <p class="ng-binding">commercial</p> -->
              <div class="relativetxt__text ">
                  <p class="relativetxt__text__heading"> Commercial </p>
                  <ul class="relativetxt__text__projectList">
                      <li> <a href="../../views/expresstowers/express-towers.php"> - Express Towers </a> </li>
                      <li> <a href="../../views/worldtradecenter/world-trade-center.php"> - Word Trade Center Pune </a> </li>
                      <li> <a href="../../views/futura/futura.php"> - Futura </a> </li>
                      <li> <a href="../../views/eon_free_zone/eon-free-zone.php"> - EON Free Zone </a> </li>
                      <li> <a href="../../views/cumins_india_campus/cumins_india_campus.php"> - Cummins India Campus </a> </li>
                      <li> <a href="../../views/suzlon_oneearth/suzlon_one_earth.php"> - Suzlon One Earth </a> </li>
                  </ul>
                  <div class="clearfix"></div>
              </div>
          </div>
      </div>
      <!-- end ngRepeat: cat in categories -->
      <div class="flexside">
          <div class="imgslider">
              <img src="../../img/hospitality-f.jpg">
              <!-- <div class="black-overlay"></div> -->
          </div>
          <div class="relativetxt">
              <i class="healt caticons"></i>
              <!-- <p class="ng-binding">hospitality</p> -->
              <div class="relativetxt__text ">
                  <p class="relativetxt__text__heading"> Hospitality </p>
                  <ul class="relativetxt__text__projectList">
                      <li> <a href="../../views/the_ritz_carlton/the_ritz_carlton.php"> - The Ritz Carlton </a> </li>
                      <li> <a href="../../views/jw_marriot_pune/jw_marriot_pune.php"> - JW Marriot Pune </a> </li>
                      <li> <a href="../../views/courtyard_by_marriot_pune_hinjewadi/courtyard_by_marriot_pune_hinjewadi.php"> - Courtyard by Marriot Pune Hinjewadi </a> </li>
                      <li> <a href="../../views/doubetree_by_hilton/doube_tree_by_hilton.php"> - DoubeTree by Hilton </a> </li>
                      <li> <a href="../../views/marriot_suites/marriot_suites.php"> - Marriot Suites </a> </li>
                      <li> <a href="../../views/oakwood_residence/oakwood_residence.php"> - Oakwood Residence  </a> </li>
                  </ul>
                  <div class="clearfix"></div>
              </div>
          </div>
      </div>
      <!-- end ngRepeat: cat in categories -->
      <div class="flexside">
          <div class="imgslider">
              <img src="../../img/retail.jpg">
              <!-- <div class="black-overlay"></div> -->
          </div>
          <div class="relativetxt">
              <i class="envir caticons"></i>
              <!-- <p class="ng-binding">retail</p> -->
              <div class="relativetxt__text ">
                  <p class="relativetxt__text__heading"> Retail </p>
                  <ul class="relativetxt__text__projectList">
                      <li> <a href="../../views/the_pavillion/the_pavillion.php"> - The Pavillion </a></li>
                      <li> <a href="#"> - Balewadi High Street </a></li>
                      <li> <a href="#"> - ICC High Street </a></li>

                  </ul>
                  <div class="clearfix"></div>
              </div>
          </div>
      </div>
  </div>

			<!-- container fluid -->
	<!-- footer bottom end -->
	<!-- copy right -->
	<div class="copy-right">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<p>Privacy Policy Terms & Conditions</p>
				</div>
				<div class="col-sm-6">
					<p class="pull-right">Site designed & maintained by <a href="#" style="text-decoration:none;color:#a9a9a9;">Neon Tree</a></p>
				</div>
			</div>
		</div>
	</div>
	<!-- copy right end -->
<!-- Footer end -->
</footer>
<!-- search div starts -->
<div id="cd-search" class="cd-search">
	<form>
		<input type="search" placeholder="Type to search here">
	</form>
</div>
<!-- search div ends-->
