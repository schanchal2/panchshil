
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" type="text/css" href="../../lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="../../lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../lib/jquery.bxslider/jquery.bxslider.css"/>

  <!-- Include Unite Gallery core files -->
	<link rel='stylesheet' type='text/css' href='../../lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='../../lib/unitegallery/themes/default/ug-theme-default.css'/>

  <link rel="stylesheet" type="text/css" href="../../css/app.css?Tuesday 27th of December 2016 03:56:31 PM"/>
  <link rel='stylesheet' type='text/css' href='../../css/easy-responsive-tabs.css'/>
  <link rel='stylesheet' type='text/css' href='../../css/protected.css'/>
  <!-- main menu with search -->
    <link rel="stylesheet" href="../../css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="../../css/style.css"> <!-- Resource style -->
  <!-- main menu with search -->

</head>
<body class="nav-is-fixed">
  <?php include '../../includes/header.html' ?>
  <div style="height:120px;"></div>

<!--banner starts here-->
<div class="banner-downloads">
 <div class="container-fluid nopadding">
	<ol class="breadcrumb">
		<li><a href="../home/home.php">Home</a></li>
		<li class="active">Downloads</li>
	</ol>
</div><h1>Downloads</h1>
</div>
<!--banner ends here-->


<!--content starts here-->

<div class="container inner-5x innerTB">
<div class="demo">

<div id="horizontalTab">
<ul class="resp-tabs-list">
<li class="tab1">all</li>
<li>corporate</li>
<li>media kit</li>
<li>property detail - residential</li>
<li>property detail - commercial</li>
</ul>
<div class="resp-tabs-container">
<div class="tab-content">
<div class="row">
    <div class="col-md-6 col-sm-6"><div>
<div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
   <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>


 <div class="maindiv">
    <i class="fa fa-zip"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
  <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>





        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
     <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






</div></div>
      <div class="col-md-6 col-sm-6"><div>
<div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
   <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>


 <div class="maindiv">
    <i class="fa fa-zip"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-word"></i><br/>
  <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>





        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
     <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






</div></div>
    </div>
</div>
<div class="tab-content">
<div class="row">
    <div class="col-md-6 col-sm-6"><div>
<div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
   <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>


 <div class="maindiv">
    <i class="fa fa-zip"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
     <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






</div></div>
      <div class="col-md-6 col-sm-6"><div>
<div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
   <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>


 <div class="maindiv">
    <i class="fa fa-zip"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-word"></i><br/>
  <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>







</div></div>
    </div>
</div>
<div class="tab-content">
<div class="row">
    <div class="col-md-6 col-sm-6"><div>
<div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
   <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>


 <div class="maindiv">
    <i class="fa fa-zip"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
  <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>





        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
     <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






</div></div>
      <div class="col-md-6 col-sm-6"><div>
<div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
   <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>


 <div class="maindiv">
    <i class="fa fa-zip"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-word"></i><br/>
  <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>





        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
     <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






</div></div>
    </div>
</div>
<div class="tab-content">
<div class="row">
    <div class="col-md-6 col-sm-6"><div>
<div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
   <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>


 <div class="maindiv">
    <i class="fa fa-zip"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
  <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>





        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
     <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






</div></div>
      <div class="col-md-6 col-sm-6"><div>
<div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
   <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>


 <div class="maindiv">
    <i class="fa fa-zip"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-word"></i><br/>
  <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>





        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
     <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






</div></div>
    </div>
</div>
<div class="tab-content">
<div class="row">
    <div class="col-md-6 col-sm-6"><div>
<div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
   <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>


 <div class="maindiv">
    <i class="fa fa-zip"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>







        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
     <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






</div></div>
      <div class="col-md-6 col-sm-6"><div>
<div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
   <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>


 <div class="maindiv">
    <i class="fa fa-zip"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>




        <div class="maindiv">
    <i class="fa fa-word"></i><br/>
  <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>








        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
    <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






        <div class="maindiv">
    <i class="fa fa-pdf"></i><br/>
     <a href="#">Document Title<br/><span>PDF</span></a>
</div><br/><br/>
        <div class="clear"></div>






</div></div>
    </div>
</div>
</div>
</div>

<!--
<div id="tabInfo">
Selected tab: <span class="tabName"></span>
</div>
-->


<div style="height: 30px; clear: both"></div>
</div>

<br/>


</div>
<!--content ends here-->

  <hr class="box-shadow">






<!--footer-->
<?php include '../../includes/footer.html' ?>

<div class="clear"></div>


<!--header-->






<!--fixed header-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="../../lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../js/app.js"></script>
<script type="text/javascript" src="../../js/easy-responsive-tabs.js"></script>


<!--tabs js-->
<script src="../../js/easy-responsive-tabs.js"></script>
<script>
$(document).ready(function () {
$('#horizontalTab').easyResponsiveTabs({
type: 'default', //Types: default, vertical, accordion
width: 'auto', //auto or any width like 600px
fit: true,   // 100% fit in a container
closed: 'accordion', // Start closed if in accordion view
activate: function(event) { // Callback function if tab is switched
var $tab = $(this);
var $info = $('#tabInfo');
var $name = $('span', $info);
$name.text($tab.text());
$info.show();
//$(".tab-content").fadeIn(1000);

}
});






});

//   $("#horizontalTab").click(function(){
//        $("#tab1_content").fadeIn();
//        $("#tab1_content1").fadeIn();
//        $("#tab1_content2").fadeIn();
//        $("#tab1_content3").fadeIn();
//        $("#tab1_content4").fadeIn();
//
//    });
</script>
<!--tabs js-->



<!--footer on hover effects-->
<script type="text/javascript">
	$(document).ready(function() {
		$(".flip1").hover(function() {
			$(".panel1").slideToggle("slow");
//             $(".flip1").find('.MT, .MTL').toggleClass('MT MTL')
		});

        $(".flip2").hover(function() {
			$(".panel2").slideToggle("slow");
//             $(".flip1").find('.MT, .MTL').toggleClass('MT MTL')
		});


        $(".flip3").hover(function() {
			$(".panel3").slideToggle("slow");
//             $(".flip1").find('.MT, .MTL').toggleClass('MT MTL')
		});

        $(".flip4").hover(function() {
			$(".panel4").slideToggle("slow");
//             $(".flip1").find('.MT, .MTL').toggleClass('MT MTL')
		});

	});
</script>
<!--footer on hover effects-->

<!--scroll top-->
<!-- <script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('.post').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeIn',
        offset: 100
       });
});

</script> -->
<!--scroll top-->
<!-- main menu with search -->
<script src="../../js/main.js"></script>
<script src="../../js/footer.js"></script>
<!-- main menu with search -->
</body>
</html>
