<?php   
  include("../layout/header.php");  
    //include necessary files
  require_once '../../admin/utilities/config.php';
  require_once "../../admin/utilities/dbUtils.php";
  require_once "../../admin/utilities/utilities.php";
  require_once "../../admin/utilities/errorMap.php";
  require_once "../../admin/model/growthCronicleModel.php";

  $id           = $_GET['id'];
  $urlId  = $_GET['urlId'];
  $returnArr    = array();

  $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
  if(noError($conn)){
      $conn = $conn["conn"];    
      $growthInfo = getGrowthDetails(NULL, $id, $conn);     
      if(noError($growthInfo)){
          $growthInfo = $growthInfo["response"];      
      }else{
          $returnArr = $growthInfo;
      }

      $growthBannerName = getGrowthCorporateDetails($id,$urlId,'status',$conn);
      if(noError($growthBannerName)){
          $growthBannerName = $growthBannerName["response"];      
      }else{
          $returnArr = $growthBannerName;
      }

  }else{
      $returnArr = $conn;
      exit;
  }
?>
<section class="hero-section hero-banner-img" style="background-image: url(<?php echo $rootUrl.'uploads/'.$growthBannerName[0]["image"];?>);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Corporate Profile</a> / <a href="#"><?php echo $growthBannerName[0]["name"];?></a>
          </div>
          <h1><?php echo ucfirst(strtolower($growthBannerName[0]["name"]));?></h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="chronicle-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="text-center didot-font">As we Grew</h2>
        <br/>
      </div>
    </div>

    <ul class="timeline">
      <?php for($i=0; $i < count($growthInfo); $i++){?>
        <li class = "<?php  if($i % 2 != 0){ echo 'timeline-inverted'; } ?>">
          <div class="timeline-badge primary"><a class="timeline-icon"><i class="glyphicon glyphicon-record glyphicon-color"></i></a></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
            <?php if(!isset($growthInfo[$i]['growthImage']) || empty($growthInfo[$i]['growthImage'])){  ?>
              <img class="img-responsive" src=""/>
            <?php } else{ ?>
             <img class="img-responsive" src="<?php echo $rootUrl.'uploads/'.$growthInfo[$i]["growthImage"];?>"/>
             <?php } ?>
            </div>
            <div class="timeline-body">
              <h2><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $growthInfo[$i]["growthYear"];?></h2>              
             <?php 
                 $desc1 ="";
                 $desc = nl2br($growthInfo[$i]["growthDescription"]);
                 $desc = explode('<br />', $desc);             
                 foreach ($desc as $key => $value) {
                     $value = trim($value);
                     if (!empty($value)) {            
                         if (count($desc) > 1) {                           
                             $desc1 .= '<i class="icon fa fa-circle" style = "margin-right:5px; font-size:8px; float:left;margin-top:7px;"></i>'.$value."<br>";
                         } else {
                             $desc1 = $value;
                         }  
                     }                                  
                 }
             ?>
              <p><?php echo $desc1;?></p>
            </div>
          </div>
        </li>
        <?php } ?>
      <li class="clearfix" style="float: none;"></li>
    </ul>
  </div><!-- container -->
</section>
<?php include("../layout/footer.php");  ?>
