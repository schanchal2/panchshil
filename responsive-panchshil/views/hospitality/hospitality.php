
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" type="text/css" href="../../lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="../../lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../lib/jquery.bxslider/jquery.bxslider.css"/>

  <!-- Include Unite Gallery core files -->
	<link rel='stylesheet' type='text/css' href='../../lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='../../lib/unitegallery/themes/default/ug-theme-default.css'/>

  <link rel="stylesheet" type="text/css" href="../../css/app.css?Tuesday 27th of December 2016 05:33:36 PM"/>
  <link rel='stylesheet' type='text/css' href='../../css/protected.css'/>

  <!-- main menu with search -->
    <link rel="stylesheet" href="../../css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="../../css/style.css"> <!-- Resource style -->
  <!-- main menu with search -->

</head>
<body class="nav-is-fixed">
  <?php include '../../includes/header.html' ?>
<div style="height:120px;"></div>
  <!-- header-->
  <!--banner starts here-->
  <div class="banner-commercial">
   <div class="container-fluid nopadding">
  	<ol class="breadcrumb">
      <li><a href="../home/home.php">Home</a></li>
      <li><a href="../offering/offering.php">Offerings</a></li>
      <li class="active">Hospitality</li>

  	</ol>
  </div><h1>Hospitality</h1>

  </div>


<section>
  <div class="container residency-container">
    <div class="row">
      <div class="col-md-4">
        <p><span class="header-residency-count didot-font">29</span> <span class="header-residency-title">PROPERTIES FOUND</span></p>
      </div>
      <div class="col-md-3" style="padding:20px 10px;">
        <span class="header-residency-title">PROPERTY TYPE</span>
        <select class="residency-property-type">
          <option selected>ALL</option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
        </select>
      </div>
      <div class="col-md-3" style="padding:20px 10px;">
        <span class="header-residency-title">PROPERTY STATUS</span>
        <select class="residency-property-type">
          <option selected>ALL</option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
        </select>
      </div>
      <div class="col-md-2" style="padding:20px 10px;">
        <span class="header-residency-title">VIEW TYPE</span>
        <span class="view-type-icon">
        <a href="#"><i class="fa fa-th" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-th-list" aria-hidden="true"></i></span></a>
      </div>

    </div>
  </div>
</section>

<section class="residency-section">
  <div class="container residency-content">
    <div class="row">
      <div class="col-md-4 mg-top">
          <div class="row">
            <div class="col-md-12 commercial-bg">
              <div class="hovereffect">
                  <img class="img-responsive" src="../../img/project-residential-1.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <a class="info" href="../the_ritz_carlton/the_ritz_carlton.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="residency-img-buttom-text">
                    <h4>The Ritz Carlton</h4>
                    <p><i class="fa fa-circle platinum" aria-hidden="true"></i> Business, Leisure & Luxury Hotels&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                    <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-4 mg-top">
          <div class="row">
            <div class="col-md-12 commercial-bg">
              <div class="hovereffect">
                  <img class="img-responsive" src="../../img/project-residential-1.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <a class="info" href="../jw_marriot_pune/jw_marriot_pune.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="residency-img-buttom-text">
                    <h4>JW Marriot Pune</h4>
                    <p><i class="fa fa-circle platinum" aria-hidden="true"></i> Business, Leisure & Luxury Hotels&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                    <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>

      <div class="col-md-4 mg-top">
          <div class="row">
            <div class="col-md-12 commercial-bg">
              <div class="hovereffect">
                  <img class="img-responsive" src="../../img/project-residential-1.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <a class="info" href="../courtyard_by_marriot_pune_hinjewadi/courtyard_by_marriot_pune_hinjewadi.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="residency-img-buttom-text">
                    <h4>Courtyard by Marriot Pune Hinjewadi</h4>
                    <p><i class="fa fa-circle platinum" aria-hidden="true"></i> Business, Leisure & Luxury Hotels&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                    <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-4 mg-top">
          <div class="row">
            <div class="col-md-12 commercial-bg">
              <div class="hovereffect">
                  <img class="img-responsive" src="../../img/project-residential-1.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <a class="info" href="../doubetree_by_hilton/doube_tree_by_hilton.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="residency-img-buttom-text">
                    <h4>DoubleTree By Hilton</h4>
                    <p><i class="fa fa-circle platinum" aria-hidden="true"></i> Business, Leisure & Luxury Hotels&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                    <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-4 mg-top">
          <div class="row">
            <div class="col-md-12 commercial-bg">
              <div class="hovereffect">
                  <img class="img-responsive" src="../../img/project-residential-1.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <a class="info" href="../marriot_suites/marriot_suites.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="residency-img-buttom-text">
                    <h4>Marriott Suites</h4>
                    <p><i class="fa fa-circle platinum" aria-hidden="true"></i> Serviced Residences&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                    <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-4 mg-top">
          <div class="row">
            <div class="col-md-12 commercial-bg">
              <div class="hovereffect">
                  <img class="img-responsive" src="../../img/project-residential-1.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <a class="info" href="../oakwood_residence/oakwood_residence.php"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br/><br/>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="residency-img-buttom-text">
                    <h4>Oakwood Residence</h4>
                    <p><i class="fa fa-circle platinum" aria-hidden="true"></i> Serviced Residences&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle construction" aria-hidden="true"></i> under construction</p>
                    <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>
    </div>


    <div class="row">
      <div class="col-md-12 text-center common-btn">
        <br/>
        <a href="#" class="btn btn-default residency-loading-btn"> LOAD MORE PROPERTIES</a>
      </div>
    </div>

  </div>
</section>

<?php include '../../includes/footer.html' ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="../../lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../js/app.js"></script>
<script type="text/javascript" src="../../lib/jquery.bxslider/jquery.bxslider.min.js"></script>

<!-- Include Unite Gallery core files -->
<script src='../../lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='../../lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
    	 var data=$(this).find(".text").html();
    		$(".img-hover-text").hide().stop().html(data).fadeIn();

    	}, function() {
    		$(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
	   jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end
   });
</script>

<script type="text/javascript" src="../../js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
	$(function () {
	var filterList = {
		init: function () {
			$('#KeyAmenities').mixItUp({
				selectors: {
					target: '.KeyAmenities-portfolio',
					filter: '.KeyAmenities-list'
				},
				load: {
					filter: '.parking, .upper_deck, .lower_deck'
				}
			});
		}
	};
	filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end
</script>
<!-- main menu with search -->
<script src="../../js/main.js"></script>
<script src="../../js/footer.js"></script>
<!-- main menu with search -->

</body>
</html>
