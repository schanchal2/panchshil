
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" type="text/css" href="../../lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="../../lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../lib/jquery.bxslider/jquery.bxslider.css"/>

  <!-- Include Unite Gallery core files -->
  <link rel='stylesheet' type='text/css' href='../../lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='../../lib/unitegallery/themes/default/ug-theme-default.css'/>

  <link rel="stylesheet" type="text/css" href="../../css/app.css?Tuesday 27th of December 2016 06:19:34 PM"/>
  <link rel='stylesheet' type='text/css' href='../../css/protected.css'/>
  <!-- main menu with search -->
    <link rel="stylesheet" href="../../css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="../../css/style.css"> <!-- Resource style -->
  <!-- main menu with search -->



</head>
<body class="nav-is-fixed">
  <?php include '../../includes/header.html' ?>
<div style="height:120px;"></div>
  <!-- header-->

<!--banner starts here-->
<div class="banner-product">
 <div class="container-fluid nopadding">
	<ol class="breadcrumb">
		<li><a href="../home/home.php">Home</a></li>
		<li><a href="../services/services.php">Services</a></li>
		<li class="active"><a href="#">Brand Licensing</a></li>
		</ol>
</div><h1>Brand Licensing</h1>
</div>
<section class="service-container-section">
  <div class="container"><p style="text-align: center; width:70%;margin:0 auto;">Fine-tuned to international norms and accepted standards,
Panchshil provides end-to-end project and facility management services and interior fit-outs. All of our projects are carefully planned around the fundamentals of time, cost, and quality. Stringent measures are used to implement systems and processes that ensure that our properties live up to our high quality standards. Using advanced system-driven technology, we provide accurate appraisals of every project through its life span. An unfailing methodology based
on experience and expertise helps us achieve a level of excellence in everything that we do.</p>


    <br/>
    <br/>
    <div class="center-block">
      <img src="../../img/project-management-icon.png"><br/><br/>
        <span class="title-bold">Persons name</span><br/><br/>
        <span class="title-02">9934571234   |   Name@gmail.com<br/><br/>Tech Park One, <br/>
Tower E, 191 Yerwada, <br/>
Pune - 411 006 </span>
      </div>
     <br/>
    <hr>  <br/>

     <div class="clients-heading">Our Clients</div>
       <br/> <br/>
     	<div class="row">
			<div class="col-md-12 property-img-circle">
				<div class="property-img-circle-slider">
					<div class="img-circle-slider">
		        <a href="#"><img src="../../img/clients-logo/deloitte.jpg" class="img-responsive img-circle" alt=""></a>
						<div class="img-overlay-count"></div>
						<div class="text">
<!--							<h2 class="didot-font">1</h2>-->
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div>
		      <div class="img-circle-slider">
		        <a href="#"><img src="../../img/clients-logo/hsbc.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"></div>
						<div class="text">
<!--							<h2 class="didot-font">2</h2>-->
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div>
		      <div class="img-circle-slider">
		        <a href="#"><img src="../../img/clients-logo/siemens.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"></div>
						<div class="text">
<!--							<h2 class="didot-font">3</h2>-->
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div>
		      <div class="img-circle-slider">
		        <a href="#"><img src="../../img/clients-logo/tata.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"></div>
						<div class="text">
<!--							<h2 class="didot-font">4</h2>-->
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div>
		      <div class="img-circle-slider">
		        <a href="#"><img src="../../img/clients-logo/wns.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"></div>
						<div class="text">
<!--							<h2 class="didot-font">5</h2>-->
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div>
		      <div class="img-circle-slider">
		      	<a href="#"><img src="../../img/clients-logo/nvidia.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"></div>
						<div class="text">
<!--							<h2 class="didot-font">6</h2>-->
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div> <div class="img-circle-slider">
		      	<a href="#"><img src="../../img/clients-logo/technology.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"></div>
						<div class="text">
<!--							<h2 class="didot-font">6</h2>-->
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div> <div class="img-circle-slider">
		      	<a href="#"><img src="../../img/clients-logo/techmahindra.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"></div>
						<div class="text">
<!--							<h2 class="didot-font">6</h2>-->
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div>
				</div>
			</div>
		</div><!-- row end -->





    </div></section>
<!-- ============
		Gallery popup
	=================-->
	<div class="modal fade gallery-popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content gallery-modal-content">
			<!-- ======================== -->
			<div class="gallery-padding">
				<div id="gallery">
					<img alt="Preview Image 1"
					 src="../../img/Property-Detai-slider-1.jpg"
					 data-image="../../img/Property-Detai-slider-1.jpg"
					 data-description="Preview Image 1 Description"/>

					<img alt="Preview Image 2"
					 src="../../img/Panchashil_Media.jpg"
					 data-image="../../img/Panchashil_Media.jpg"
					 data-description="Preview Image 2 Description"/>

					<img alt="Preview Image 1"
 					 src="../../img/Property-Detai-slider-1.jpg"
 					 data-image="../../img/Property-Detai-slider-1.jpg"
 					 data-description="Preview Image 3 Description"/>

	 				<img alt="Preview Image 2"
					 src="../../img/Panchashil_Media.jpg"
					 data-image="../../img/Panchashil_Media.jpg"
					 data-description="Preview Image 4 Description"/>

					<img alt="Preview Image 1"
					 src="../../img/Property-Detai-slider-1.jpg"
					 data-image="../../img/Property-Detai-slider-1.jpg"
					 data-description="Preview Image 5 Description"/>

	 				<img alt="Preview Image 2"
					 src="../../img/Panchashil_Media.jpg"
					 data-image="../../img/Panchashil_Media.jpg"
					 data-description="Preview Image 6 Description"/>

					<img alt="Preview Image 1"
 					 src="../../img/Property-Detai-slider-1.jpg"
 					 data-image="../../img/Property-Detai-slider-1.jpg"
 					 data-description="Preview Image 7 Description"/>

	 	 			<img alt="Preview Image 2"
					 src="../../img/Panchashil_Media.jpg"
					 data-image="../../img/Panchashil_Media.jpg"
					 data-description="Preview Image 8 Description"/>

	 				<img alt="Preview Image 1"
					 src="../../img/Property-Detai-slider-1.jpg"
					 data-image="../../img/Property-Detai-slider-1.jpg"
					 data-description="Preview Image 9 Description"/>

	 	 			<img alt="Preview Image 2"
					 src="../../img/Panchashil_Media.jpg"
					 data-image="../../img/Panchashil_Media.jpg"
					 data-description="Preview Image 10 Description"/>
				</div>
			</div>
			<!-- ======================== -->
		</div>
		<p class="esc-close">ESC to close window</p>
	</div>
	</div>
	<!-- ============
		Gallery popup end
	=================-->



  <?php include '../../includes/footer.html' ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="../../lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../js/app.js"></script>
<script type="text/javascript" src="../../lib/jquery.bxslider/jquery.bxslider.min.js"></script>

<!-- Include Unite Gallery core files -->
<script src='../../lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='../../lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
    	 var data=$(this).find(".text").html();
    		$(".img-hover-text").hide().stop().html(data).fadeIn();

    	}, function() {
    		$(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
	   jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end
   });
</script>

<script type="text/javascript" src="../../js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
	$(function () {
	var filterList = {
		init: function () {
			$('#KeyAmenities').mixItUp({
				selectors: {
					target: '.KeyAmenities-portfolio',
					filter: '.KeyAmenities-list'
				},
				load: {
					filter: '.parking, .upper_deck, .lower_deck'
				}
			});
		}
	};
	filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end
</script>
<!-- main menu with search -->
<script src="../../js/main.js"></script>
<script src="../../js/footer.js"></script>
<!-- main menu with search -->
</body>
</html>
