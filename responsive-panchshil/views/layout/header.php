<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" type="text/css" href="../../lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="../../lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../lib/jquery.bxslider/jquery.bxslider.css"/>

  <!-- Include Unite Gallery core files -->
	<link rel='stylesheet' type='text/css' href='../../lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='../../lib/unitegallery/themes/default/ug-theme-default.css'/>

  <link rel="stylesheet" type="text/css" href="../../css/app.css?<?php echo date('l jS \of F Y h:i:s A'); ?>"/>

</head>
<body>
  <header class="header-section">
  	<div class="container">
  				<nav role="navigation" class="navbar navbar-white header">
  				    <!-- Brand and toggle get grouped for better mobile display -->
  				    <div class="navbar-header">
  				        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
  				            <span class="sr-only">Toggle navigation</span>
  				            <span class="icon-bar"></span>
  				            <span class="icon-bar"></span>
  				            <span class="icon-bar"></span>
  				        </button>
                  <a href="#" class="navbar-brand" style="height:auto"><img src="../../img/logo.jpg"/></a>
  				    </div>
  				    <!-- Collection of nav links and other content for toggling -->
  				    <div id="navbarCollapse" class="collapse navbar-collapse quick-link">
  			    		<ul class="nav navbar-nav navbar-right">
  				            <li class="active"><a href="../home/home.php"><img src="../../img/home_logo.png" /></a></li>
  				            <li><a href="../about/about.php">About</a></li>
  				            <li><a href="../business/business.php">Business</a></li>
  				            <li><a href="#">Alliances</a></li>
  				            <li><a href="../sustainability/sustainability.php">Sustainability</a></li>
  				            <li><a href="../career/career.php">Career</a></li>
  				            <li><a href="../media/media.php">Media</a></li>
  				            <li><a href="../contact/contact.php">Contact</a></li>
  				            <li><a href="#" class="search_icon"><img src="../../img/search_icon.png"/></a></li>
  				        </ul>
  				    </div>
  				</nav>
      <!-- row -->
  	</div>
    <!-- container-->
  </header>
  <!-- header-->
