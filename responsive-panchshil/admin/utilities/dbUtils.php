<?php

/**
 * function name: createDbConnection()
 * parameters: $serverName(string),$dbUsername(string),$dbPassword(string),$dbName(string)
 * purpose:to create mysql database connection
 */
function createDbConnection($servername,$dbUsername,$dbPassword,$dbName){
    $returnArr = array();
    $extraParam = array();
    $conn = mysqli_connect($servername,$dbUsername,$dbPassword);

    if(!$conn){
        $returnArr = setErrorStack($returnArr,1);
    }else{
        // error reporting using array which is combination of error code n its respective message
        if(!mysqli_select_db($conn,$dbName)){
            $returnArr = setErrorStack($returnArr,2);
        }else{
            $extraParam["conn"] = $conn;
            $returnArr = setErrorStack($returnArr,-1,null,$extraParam);
        }
    }
    return $returnArr;
}

/**
 * function name: runQuery();
 * parameters: $query(string),$conn(string)
 * purpose: to execute and return sql query result .
*/
function runQuery($query, $conn){
    $returnArr = array();
    $extraArg = array();
    $result = mysqli_query($conn, $query);
    if(!$result){
        $extraArg['query']   = $query;
        $returnArr = setErrorStack($returnArr,3,null,$extraArg);
    }else{
        $extraArg['dbResource'] = $result;
        $errMsg = 'Query Successful';
        $returnArr = setErrorStack($returnArr,-1,$errMsg,$extraArg);
    }

    return $returnArr;
}

/**
 * function name: cleanQueryParameter();
 * parameters: $string(string),$conn(string)
 * purpose: to clean query attributes before executing mysql query.
 */
function cleanQueryParameter($string,$conn){
    //remove extraneous spacess
    $string = trim($string);
    // prevents duplicate backslashes
    if(get_magic_quotes_gpc()){
        $string = stripslashes($string);
    }
    //escape the string with backward compatibility
    if(phpversion() >= '4.3.0'){
        $string = mysqli_real_escape_string($conn, $string);
    }else{
        $string = mysqli_escape_string($string);
    }

    return $string;
}

?>