<?php

//global array to define all error messages
$errorArray = array(
    "-1" => "success",
    "1" => "Error in creating database connection",
    "2" => "Error in selecting database",
    "3" => "Error in mysql query",
    "4" => "Error in updating global settings",
    "5" => "Error in authenticating admin user.",
    "6" => "Error in fetching global settings",
    "7" => "All feilds are empty",
    "8" => "file is already exists",
    "9" => "File size is too large",
    "10" => "File is not an image",
    "11" => "There was an error uploading your file",
    "12" => "Error in updating header content",
    "13" => "Error in fetching header contents",
    "14" => "Empty table",
    "15" => "Error in deleting file",
    "16" => "File not exist",
    "17" => "Error in file uploading",
    "18" => "Error in Fetching contact Us details",
    "19" => "Error fetching slider content",
    "20" => "Error in deleting slider content",
    "21" => "Please Upload Image",
    "22" => "Error in deleting campus event",
    "23" => "Error in updating campus details",
    "24" => "Error in inserting Contact US details",
    "25" => "Error in inserting team details",
    "26" => "Team member is already exists",
    "27" => "Team is already exists",
    "28" => "Error in fetching client details",
    "29" => "Error in fetching team details",
    "30" => "Unknown campus name",
    "31" => "There is no participant registered with that name.",
    "32" => "Error in inserting case study details",
    "33" => "Error in Uploading Image",
    "34" => "Error in deleting Header details",
    "35" => "Error in updating Contact Form details",
    "36" => "Error in deleting Client details",
    "37" => "Error in deleting prize details",
    "38" => "Error in updating expert details",
    "39" => "Error in fetching expert details",
    "40" => "Error in deleting expert details",
    "41" => "Error in fetching case study details",
    "42" => "Error in Updatin Home header Contents",
    "43" => "Error in Creating Folder",
    "44" => "Error in Updating Matrics Details",
    "45" => "Error in Updating Enquiry Details"
);

/**
 * function name: getErrorMsg();
 * parameters: $errCode(number)
 * purpose: to getting perticuler error message from error array.
 */
function getErrMsg($errCode){
    global $errorArray;
    $errMsg = $errorArray[$errCode];
    if(!isset($errMsg) || empty($errMsg)){
        $errMsg  = "Undefined error code and error message";
    }
    return $errMsg;
}

/**
 * function name: setErrorStack();
 * parameters: $returnArr(array),$errCode(number)
 * purpose: to getting error stack.
 */
function setErrorStack($returnArr,$errCode,$errMsg,$extraArgs){
    $resArr = array();
    if(!isset($errMsg) && empty($errMsg)){
        $errMsg = getErrMsg($errCode);
    }
    $resArr["errStack"] =  $returnArr["errStack"].$errCode.":".$errMsg." ";
    $resArr["errCode"] = $errCode;
    $resArr["errMsg"] = $errMsg;
    if(isset($extraArgs) && !empty($extraArgs)){
        $resArr = array_merge($resArr,$extraArgs);
    }
    return $resArr;
}
?>