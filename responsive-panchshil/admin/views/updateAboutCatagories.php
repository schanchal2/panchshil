 maxlength="1000" <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Add New About Catagories </h4>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="businessFrom" enctype = "multipart/form-data">
		<label for="business-status">Status : </label>
		<div class="form-group">
			<label for="feature-image">Image : </label>
			<input type="file" id = "business-feature-image" class="form-control" name="business-feature-image" onchange="businessPreview('#business-feature-image', '#feature-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="feature-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$businessInfo[0]["business_feature_image"]; ?>'); <?php if (empty($businessInfo[0]['business_feature_image']) || !isset($businessInfo[0]['business_feature_image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="business-name">Name</label>
			<input type="text" class="form-control" id="business-name"  maxlength="100" name="business-name" value="<?php echo $businessInfo[0]['business_name']; ?>" >
		</div>
		<div class="form-group">
			<label for="business-services">Short Description</label>
			<textarea class="form-control" rows="3" id="business-short-desc"  maxlength="1000" name="business-short-desc"><?php echo $businessInfo[0]["business_short_desc"]; ?></textarea>
		</div>
		<div class="form-group">
			<label for="business-services">Long Description</label>
			<textarea class="form-control" rows="3" id="business-long-desc"  maxlength="10000" name="business-long-desc"><?php echo $businessInfo[0]["business_full_desc"]; ?></textarea>
		</div>


	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
