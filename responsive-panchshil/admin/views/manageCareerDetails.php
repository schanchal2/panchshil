<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/careerDetailsModel.php';

$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getCareerDetails=getCareerDetails($id,$require,$conn);
    //printArr($getCareerDetails);die;
    if(noError($getCareerDetails)){
        $getCareerDetails = $getCareerDetails["response"];
    }else{
        $returnArr = $getCareerDetails;
    }
}else{
    $returnArr = $conn;
    exit;
}
?>

<div class="col-lg-12 main-container-1">
    <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
       <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage Career Details</p>
        </div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;">
                        <th width="20%">Featured Image</th>
                        <th width="20%">Image</th>
                        <th width="25%">Name</th>
                        <th width="25%">Status</th>
                        <th width="10%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                          <div class="col-lg-3 blue-button-1" onclick="opencareerDialog();">Add New Career</div>
                    <?php
                        if (empty($getCareerDetails)) {
                            echo "<tr><td colspan = '5' style = 'text-align:center;'>No Data Found</td></tr>";
                        }
                        for ($i = 0; $i < count($getCareerDetails); $i++) {
                    ?>

                        <tr>
                            <td><div id="career-image" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$getCareerDetails[$i]["image"]; ?>');"></div>
                            </td>

                            <td><div id="career-image1" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$getCareerDetails[$i]["career_image1"]; ?>');"></div>
                            </td>
                            <td>
                                <p class = "clientDisplay"><?php echo $getCareerDetails[$i]['career_name']; ?></p>
                            </td>

                             <td>
                                 <span style="font-size: 16px;font-weight: bold; <?php if($getCareerDetails[$i]['status'] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?> "><?php echo strtoupper($getCareerDetails[$i]['status']); ?></span>
                            </td>

                            <td>
                                <a href="#" onclick="opencareerDialog(<?php echo $getCareerDetails[$i]['id']; ?>);">Edit</a><br>
                                <a href="#" onclick="confirmRemoveData(<?php echo $getCareerDetails[$i]['id']; ?>);">
                                    <span style="font-size: 16px;font-weight: bold;  ">
                                        <?php if($getCareerDetails[$i]["status"] == "Active"){ echo "Inactivate/Delete";}else{echo "Activate";} ?>
                                    </span>
                                </a><br>


                                <a href="#" onclick="openViewCareerDialog('<?php echo $getCareerDetails[$i]["pagename"];?>',<?php echo $getCareerDetails[$i]["id"];?>,'<?php echo $getCareerDetails[$i]["career_name"];?>');">Manage</a><br>

                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>

           <!-- <a href="#" onclick="openEditDialog();">Add Team</a>-->
        </div>
</div>
</div>
<script>
    $(document).ready(function() {
        //$('#example1').DataTable();
    } );

    /* TO Display Remodal on click of Add or Update Clients details */
   function opencareerDialog(id){
        $(".modal-content").load("updateCareerDetails.php?id="+id);
        $("#myModal").modal();
    }

    /* To show preview of uploaded Image */
    function careerPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        if (!files.length || !window.FileReader) return;
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();

            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdateCareerDetails() {
        $('.btn-success').prop('disable',true);
        var career_name      = $('#career_name').val(); //alert(career_name);
        var career_description       = $('#career_description').val(); //alert(career_description);
        var career_designation  = $('#career_designation').val(); //alert(career_designation);
        if (career_name == '' || career_description == '' || career_designation == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#careerfrom')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/careerController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Career Details !!!');
                        $("#load-content").load("manageCareerDetails.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Career Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    /* To remove Career Details */
    function removeCareer(id) {
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/careerController.php",
            data:{
                id:id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Career Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Career Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageCareerDetails.php");
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Career Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);
            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmRemoveData.php?id="+id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeCareer(id);
        $('#myModal .close').click();
    }


    function openViewCareerDialog(pagename,id,career_name){
      //alert(pagename);
      //alert(id);
      //alert(career_name);
        $("#load-content").load(pagename+"?id="+id+"&CareerName="+encodeURIComponent(career_name));
    }




</script>
