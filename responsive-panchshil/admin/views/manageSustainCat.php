<?php

//start session
session_start();

require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
//require_once '../model/matricesContentModel.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/sustainContentModel.php';

$returnArr = array();

$pagename1=$_GET['pagename1'];
//echo $pagename1;
$sustainids=$_GET['sustaincatId'];
//echo $sustainids;

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active redirect to the login page

    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    //$featureType = "home";
    $getSustainCatDetails = getSustainCatDetails($sustainids,NULL,$conn);
      //printArr($getSustainCatDetails);
    if(noError($getSustainCatDetails)){

            $getSustainCatDetails = $getSustainCatDetails["response"];
    }else{
            $returnArr = $getSustainCatDetails;
    }
}else{
            $returnArr = $conn;
            exit;
}
?>

<div class="col-lg-12 main-container-1">
    <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
        <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;"> Manage <?php echo $pagename1; ?></p>
        </div>
        <div class="modal-err" style="color: red"></div>
        <div class="col-lg-12 redirect">

            <form id="sustaincatform" enctype = "multipart/form-data">
                <input type="hidden" name="id" id="id" value="<?php echo $getSustainCatDetails[0]['id']; ?>">
                <input type="hidden" name="sustainids" id="sustainids" value="<?php echo $sustainids; ?>"
                <input type="hidden" name="sustain_cat_image1" value="<?php echo $getSustainCatDetails[0]['sustain_cat_image']; ?>">
              <input type="hidden" name="sustain_cat_sign_image1" value="<?php echo $getSustainCatDetails[0]['sustain_cat_sign_image']; ?>">
                <input type="hidden" name="method" value="update">

              <input type="hidden" name="forword_id" value="<?php echo $id; ?>">


              <div class="form-group">
                    <label for="feature-image1">Image : </label>
                    <input type="file" id = "sustain_cat_image" class="form-control" name="sustain_cat_image" onchange="SustainCatPreview('#sustain_cat_image', '#sustain-image-preview',null,'<?php echo $uploadDir; ?>');">
                    <div id="sustain-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getSustainCatDetails[0]["sustain_cat_image"]; ?>'); <?php if (empty($getSustainCatDetails[0]['sustain_cat_image']) || !isset($getSustainCatDetails[0]['sustain_cat_image'])) { echo 'display: none'; }?>;"></div>
                </div>

              <div class="form-group">
                    <label for="feature-image">Signature Image : </label>
                    <input type="file" id = "sustain_cat_sign_image" class="form-control" name="sustain_cat_sign_image" onchange="SustainCatPreview('#sustain_cat_sign_image', '#signcat-image-preview',null,'<?php echo $uploadDir; ?>');">
                    <div id="signcat-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getSustainCatDetails[0]["sustain_cat_sign_image"]; ?>'); <?php if (empty($getSustainCatDetails[0]['sustain_cat_sign_image']) || !isset($getSustainCatDetails[0]['sustain_cat_sign_image'])) { echo 'display: none'; }?>;"></div>
                </div>

              <hr>
              <div class="form-group">
                  <label for="top-desc">Name:</label>
                  <input type="text" class="form-control" id="sustain_cat_name" name="sustain_cat_name" value="<?php echo $getSustainCatDetails[0]["sustain_cat_name"]; ?>" maxlength="100">

                  
              </div>
              <div class="form-group">
                  <label for="know-more">Designation :</label>                  
                  <input type="text" class="form-control" id="sustain_cat_designation" name="sustain_cat_designation" value="<?php echo $getSustainCatDetails[0]["sustain_cat_designation"]; ?>" maxlength="100">
              </div>
              
              <div class="form-group">
                  <label for="top-desc-title">Description:</label>
                  <textarea class="form-control" rows="3" id="sustain_cat_long_url" name="sustain_cat_long_url" maxlength="10000"><?php echo $getSustainCatDetails[0]["sustain_cat_long_url"]; ?></textarea>

              </div>
              
              </form>
              <div class="col-lg-6"></div>
              <div class="col-lg-3 blue-button-1" onclick="addUpdatesustaincatDetails()">Submit</div>
              <!--<div class="col-lg-3 blue-button-1"  onclick="preview();">Preview</div>-->
              <div class="col-lg-3 blue-button-1" id="clear" onclick ="confirmRemoveData(<?php echo $getSustainCatDetails[0]['id']; ?>);">Clear</div>

        </div>
    </div>
</div>
<script>


    /* TO Display Remodal on click of Add or Update about details */


    /* TO add and update Clients details */
    function addUpdatesustaincatDetails(id) {
    //  alert(id);
        $('.btn-success').prop('disable',true);

        var sustain_cat_long_url = $('#sustain_cat_long_url').val(); //alert(sustain_cat_long_url);
        var sustain_cat_name = $('#sustain_cat_name').val(); //alert(sustain_cat_name);
        var sustain_cat_designation = $('#sustain_cat_designation').val(); //alert(sustain_cat_designation);

        if (sustain_cat_long_url == '' || sustain_cat_name == '' || sustain_cat_designation == '') {

            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {

          var form = $('#sustaincatform')[0];
          var formData = new FormData(form);

            //alert(JSON.stringify(formData));
           

            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/sustainCatController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                  //  alert(data);
                 console.log(data);
                  //   alert(JSON.stringify(data));
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated SustainCategory Details !!!');
                        $("#load-content").load("manageSustaincat.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating SustainCategory content!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    //to remove campus details by campus id
    function removesustaincat(id){
        var ID = id; //alert(ID);
            $.ajax({
                type: "POST",
                url: '../controller/sustainCatController.php',
                data: {
                    id:ID,
                    method: "delete"
                },
                dataType: 'json',
                success: function (data) {
                  //  console.log(data);

                    if (data.errCode == -1) {
                        $(".alert-success").show();
                        $("#success-msg").text('Successfully deleted SustainCategory details!!!');
                        setTimeout(function() { $(".alert-success").hide(); }, 5000);
                    }else{
                        $(".alert-danger").show();
                        $("#error-msg").text("Failed to Delete SustainCategory Content");
                        setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                    }
                     $("#load-content").load("manageSustainCat.php");
                },
                error: function (data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in SustainCategory details!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });

    }

    /* To show preview of uploaded Image */
    function SustainCatPreview(id,pre_id,nextSlider,uploadDir) {

        var files =  $(id)[0].files;
        var img = new Image();

        /* Checking Is any File selected */
        if (!files.length || !window.FileReader) return;

        /* Check Uploadede file is Image Only */
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();
            /* set image data as background of div */
            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#my_logo"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_logo').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_logo').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
      //alert(id);
        $(".modal-content").load("confirmChangesRemodal.php?id="+id);
        $("#myModal").modal();
    }

  /*  function confirmRemoveData(id) {
        alert(id);
        alert(1);
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/forwordController.php",
            data:{
                id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Forword Content");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Forword Content");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageSustainCat.php?id="+encodeURIComponent(id));
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Content !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);

            }
        });
    } */


    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removesustaincat(id);
        $('#myModal .close').click();
    }
    
</script>
