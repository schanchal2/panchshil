<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/leadershipContentModel.php';
require_once '../model/businessDetailsModel.php';
require_once '../model/pageNameContentModel.php';

$leaderUrlId = $_GET['id'];
//echo $leaderUrlId;
$aboutid = $_GET['pageName'];
//echo $aboutid;
$leader_name=strtoupper(strtolower(cleanXSS(urldecode($_GET['name']))));
$returnArr = array();
//printArr($returnArr);
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $leadershipInfo = getLeadershipDetailsById($leaderUrlId,$conn);
    if(noError($leadershipInfo)){
        $leadershipInfo = $leadershipInfo["response"];
    }else{
        $returnArr = $leadershipInfo;
    }


    $pageInfo = getPagename('aboutDetails',$leaderUrlId,$conn);
    if(noError($pageInfo)){
        $pageInfo = $pageInfo["response"][0];
    }else{
        $returnArr = $pageInfo;
    }

}else{
    $returnArr = $conn;
    exit;
}
// printArr($businessInfo);
?>
<input type="hidden" id="ledership_id" value="<?php echo $leaderUrlId;?>">
<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
         <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;"><?php echo strtoupper($pageInfo["name"]); ?></p>
        </div>

        <div class="col-lg-12 redirect">

            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="openViewLeadershipDialog('insert',<?php echo $leaderUrlId; ?>);;">Add Leadership</div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;">
                        <th width="20%">Image</th>
                        <th width="20%">Name</th>
                        <th width="20%">Description</th>
                        <th width="10%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (empty($leadershipInfo)) {
                                echo "<tr><td colspan = '5' style = 'text-align:center;'>No Data Found</td></tr>";
                            }
                            for ($i = 0; $i < count($leadershipInfo); $i++) {
                        ?>
                        <tr>
                            <td>
                                <div id="businessImage" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$leadershipInfo[$i]["image"]; ?>');"></div>
                            </td>
                            <td>

                                <p class = "headerDisplay" id="Name" name="Name" value = ""><?php echo $leadershipInfo[$i]['name']; ?></p>

                            </td>
                            <td>

                                 <p class = "headerDisplay" id="short-desc" name="short-desc" value = ""><?php echo $leadershipInfo[$i]['description']; ?></p>

                            </td>

                            <td>
                                <a href="#" onclick="openViewLeadershipDialog('update',<?php echo $leadershipInfo[$i]['id']; ?>);">Edit</a><br>
                                  <a href="#" onclick="confirmRemoveData(<?php echo $leadershipInfo[$i]['id']; ?>,<?php echo $leadershipInfo[$i]['about_id']; ?>);">
                                    <span style="font-size: 16px;font-weight: bold;  ">
                                        Delete
                                    </span>
                                </a><br>
                            </td>
                        </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.ckeditor.com/4.5.10/standard/ckeditor.js"></script>
<script>
  /* TO Display Remodal on click of view Growth details */
    function openViewLeadershipDialog(method,ledership_id){
        $( ".modal-content" ).load("updateLeadership.php?ledership_id="+ledership_id+'&method='+method);
        $("#myModal").modal();
    }
    /* To show preview of uploaded Image */
    function growthPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        /* Checking Is any File selected */
        if (!files.length || !window.FileReader) return;

        /* Check Uploadede file is Image Only */
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();
            /* set image data as background of div */
            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdateLeadershipDetails() {
        $('.btn-success').prop('disable',true);
          var name         = $('#name').val();
          var Description  = $('#description').val();
          var facebook     = $('#facebook').val();
          var linkedin     = $('#linkedin').val();
          var twitter      = $('#twitter').val();
          var twitter      = $('#twitter').val();
          var abt_id       = $('#abt_id').val();
          //alert(abt_id);
          var abt_id1      = $('#abt_id1').val();
          var Id           = $('#Id').val();




        if (name == '' || Description == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#leadership-form')[0];
            var formData = new FormData(form);
            console.log(formData);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/leadershipController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {

                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Leadership Details !!!');
                        if(abt_id && abt_id1)
                        {
                            $("#load-content").load("manageLeadership.php?id="+abt_id1);
                        }
                        else {
                            $("#load-content").load("manageLeadership.php?id="+abt_id);
                        }
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Leadership Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    /* To remove Growth Details */
    function removeLeadership(id,about_id) {
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/leadershipController.php",
            data:{
                deleteLeadership:id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Leadership Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Leadership Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageLeadership.php?id="+about_id);
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Leadership Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);
            }
        });
    }



    /* To display remodal on click of remove header data */
    function confirmRemoveData(id,about_id) {
        $(".modal-content").load("confirmChangesRemodalById.php?id="+id+'&award_id='+about_id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id,about_id) {
        removeLeadership(id,about_id);
        $('#myModal .close').click();
    }

</script>
