<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../model/Contact_detail_Model.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';

 $contactid = urldecode($_GET["id"]);

$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootURL."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    
    $ContactDetail=getContact_Detail($contactid,$conn);
    if(noError($ContactDetail))
    {
        $ContactDetail = $ContactDetail["response"];
            //printArr($ContactDetail);
            // die;
         
    }
    else{
        $returnArr = $ContactDetail;
    }
}else{
    $returnArr = $conn;
    exit;
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
 
  <?php if ($contactid=="undefined") {?>
	<h4 class="modal-title">Add New Contact </h4>	
  <?php }else{?>
    <h4 class="modal-title">Update Contact </h4>  
    <?php }
  ?>
</div>

<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	   <form id="contactform" class="contactform" >
        <input type="hidden" name="method" value="update">
        <input type="text" name="contactid" id="contactid" value="<?php echo $contactid; ?>" style="display: none;">
      		<div class="form-group">
      			<label for="contact-title">Title:</label>
      			<input type="text" class="form-control" id="contact-title" name="contact-title" value="<?php echo $ContactDetail[0]['location'];?>">
      		</div>

        		<div class="form-group">
        			<label for="contact-address">Address</label>
        			<input type="text" class="form-control" id="contact-address" name="contact-address" value="<?php echo $ContactDetail[0]['address'];  ?>">
        		</div>  
          		<div class="form-group input_fields_wrap"  id="dynamicInput">
          		    <div class="row">
                       <div class="col-md-8">
                            <label for="contact-phoneno">PhoneNo1</label>
          			            <input type="text" maxlength="10" class="form-control" id="contact-phoneno" name="contact-phoneno[]" value="<?php echo  $ContactDetail[0]['phoneno1']; ?>" onchange="PhoneValidation(this);">
                        </div>	
                        <?php $count=1; if($ContactDetail[0]['phoneno2']!= "")
                        {   $count=2;?>
                       <div>
                          <div class="col-md-8">
                              <label for="contact-phoneno">PhoneNo2</label>
                              <input type="text" class="form-control" id="contact-phoneno" name="contact-phoneno[]" value="<?php echo  $ContactDetail[0]['phoneno2']; ?>" >
                          </div>
                      </div>
                           <?php
                        }

                        ?>	

              <?php if($ContactDetail[0]['phoneno3']!= "")
              { $count=3;?>
                <div>
                <div class="col-md-8">
                   <label for="contact-phoneno">PhoneNo3</label>
                   <input type="text" class="form-control" id="contact-phoneno" name="contact-phoneno[]" value="<?php echo  $ContactDetail[0]['phoneno3']; ?>">
                </div>
              </div><?php
              }

              ?>  	
               <?php if($ContactDetail[0]['phoneno4']!= "")
              {?>
              <div>
                <div class="col-md-8">
                     <label for="contact-phoneno">PhoneNo4</label>
                     <input type="text" class="form-control" id="contact-phoneno" name="contact-phoneno[]" value="<?php echo  $ContactDetail[0]['phoneno4']; ?>">
                 </div>
              </div><?php
              }
              else{
           
              ?> 
		       <div class="col-md-4">	  
               <br>
		          <input type="button" value="Add Another Phone No" style="height: 38px;" class="btn btn-success" onClick="addInput('dynamicInput');">
          </div>
        </div><!--./row-->
    
<?php } ?>

  </div><!--./form-group-->

    <div class="form-group">
      <label for="contact-fax">Fax</label>
      <input type="text" class="form-control" rows="3" id="contact-fax" name="contact-fax" value="<?php  echo $ContactDetail[0]['fax']; ?>" onchange="PhoneValidation(this);" >
    </div><!--./form-group-->
 </div><!--./model-body-->
</form>	

<div class="modal-footer">
  	<button type="button" class="btn btn-success"  onclick="addupdatecontact('<?php echo $ContactDetail[0][id]?>');" >submit</button>
  	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>	
</div><!--./modal-footer-->

<script type="text/javascript">
  var  counter = <?php echo $count; ?>;
  var limit = 4;
  var wrapper         = $(".input_fields_wrap"); //Fields wrapper
  function addInput(divName){
       if (counter == limit)  {
            alert("You have reached the limit of adding " + counter + " inputs");
       }
       else {
            var newdiv = document.createElement('div');
            newdiv.innerHTML = "<div><label for='contact-phoneno'>PhoneNo</label> " + (counter + 1) + '<br><input type="text" class="form-control" id="contact-phoneno" name="contact-phoneno[]" style="width:65%" value="" onchange="PhoneValidation(this); required><a href="#" class="remove_field">Remove</a></div>';
            document.getElementById(divName).appendChild(newdiv);
            counter++;

       }
     
  }
   $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
          e.preventDefault(); $(this).parent('div').remove(); 
          counter--;
      })

        var reg = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;  
      function PhoneValidation(phoneNumber)
      {  
        var OK = reg.exec(phoneNumber.value);  
        if (!OK)  
          window.alert("phone number isn't  valid");  
        else  
          window.alert("phone number is  valid");  
      }  


</script>
