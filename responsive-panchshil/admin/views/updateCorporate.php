<?php
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/corporateDetailsModel.php';

$id = urldecode($_GET["id"]);
$corporateUrlId = urlencode($_GET['corporateUrlId']);
$uploadDir = "../uploads/corporateImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getCorporateDetails = getCorporateDetails($id,$corporateUrlId,NULL,$conn);

    if(noError($getCorporateDetails)){
        $getCorporateDetails = $getCorporateDetails["response"];

    }else{
        $returnArr = $getCorporateDetails;
    }

}else{
    $returnArr = $conn;
    exit;
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Add New Corporate Detail </h4>
</div>
<div class="modal-body">

	<div class="modal-err" style="color:red;"></div>
	<form id="corporateFrom" enctype = "multipart/form-data">
		<input type="hidden" name="id" id="id" value="<?php echo $getCorporateDetails[0]['id']; ?>">

		<input type="hidden" name="corporateUrlId" id="corporateUrlId" value="<?php if (!isset($getCorporateDetails[0]['corporateUrlId']) || empty($getCorporateDetails[0]['corporateUrlId']) || $getCorporateDetails[0]['corporateUrlId'] == 0) { echo $corporateUrlId; } else { echo $getCorporateDetails[0]['corporateUrlId']; }?>">

		<input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getCorporateDetails[0]['image']; ?>">
		<input type="hidden" name="corporateStatus" id="corporateStatus" value="<?php echo $getCorporateDetails[0]['status']; ?>">
		<input type="hidden" name="method" value="update">
		<div class="form-group">
			<label for="feature-image">Image : </label>
			<input type="file" id = "corporate-image" class="form-control" name="corporate-image" onchange="corporatePreview('#corporate-image', '#corporate-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="corporate-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getCorporateDetails[0]["image"]; ?>'); <?php if (empty($getCorporateDetails[0]['image']) || !isset($getCorporateDetails[0]['image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="corporate-name">Name</label>
			<input type="text" class="form-control" id="corporate-name" name="corporate-name" value="<?php echo $getCorporateDetails[0]['name']; ?>" >
		</div>
		<?php if (empty($getCorporateDetails[0]['id']) || !isset($getCorporateDetails[0]['id'])) {?>
		<div class="form-group">
	      	<label for="about-name">Select Theme</label>
	      	<select name="pagetype" class="form-control">
		        <option value="manageForword.php">Forword</option>
		        <option value="manageGrowthChronicle.php">Growth Cronical</option>
		        <option value="manageCompanyProfile.php">Company Profile</option>
		    </select>
    	</div>
    	 <?php } ?>
		<div class="form-group">
			<label for="corporate-services">Short Description</label>
			<textarea class="form-control" rows="3" id="corporate-shortDesc" name="corporate-shortDesc" ><?php echo $getCorporateDetails[0]['shortDesc']; ?></textarea>
		</div>
		<div class="form-group">
			<label for="corporate-services">long Description</label>
			<textarea class="form-control" rows="3" id="corporate-longDesc" name="corporate-longDesc" value=""><?php echo $getCorporateDetails[0]['longDesc']; ?></textarea>
		</div>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateCorporateDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
