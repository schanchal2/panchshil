<?php
//prepare for request
//start session
session_start();


//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/headerContentModel.php';


$featureType = strtolower($_GET['featureType']);
$returnArr = array();
$uploadDir = "../uploads/feature/";

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    if (!isset($featureType) || empty($featureType)) {
        $featureType = "home";
    }
    //$featureType = "home";
    $headerContent = getHeaderContent($featureType,$conn);
    if(noError($headerContent)){   
        $headerContent = $headerContent["response"];
        // printArr($headerContent);
    }else{
        $returnArr = $headerContent;
    }
}else{
    $returnArr = $conn;
    exit;
}

?>

<div class="col-lg-12 main-container-1">
    <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
         <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">  Manage Header Content</p>
        </div>
        <div class="modal-err" style="color: red"></div>
        <div class="col-lg-12 redirect">
            <form id="header-content">
                
                <div class="container-fluid">
                    <div class="form-group">
                        <label for="feature-type" >Select Type:</label>
                        <select class="form-control droper" name="feature-type" id="feature-type" style="">
                        </select>
                    </div>
                    
                </div>

            </form>
                    <?php 
                        if ($featureType == "home") {
                    ?>
                        <form id="header-content">
                            <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr style="color:white; background-color:#3da2da;font-size: 16px;">
                                    <th width = "25%">Feature Image</th>
                                    <th width="25%">Top Description Title</th>
                                    <th width="25%">Description</th>
                                    <th width="25%">Know More Link</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php
                            if (empty($headerContent)) {
                            echo "<tr><td colspan = '5' style = 'text-align:center;'>No Data Found</td></tr>";
                        }
                            for ($i=0; $i < count($headerContent) && $i <= 5 ; $i++) {
                        ?>
                                <tr>
                                    <td>
                                        <div id="my_image" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$headerContent[$i]["image"]; ?>'); "></div>
                                    </td>
                                    <td>
                                        <div style = "height: 130px;">
                                        <p class = "headerDisplay"><?php echo $headerContent[$i]["title"]; ?></p>
                                        <div style = "height: 130px;">
                                    </td>
                                    <td>
                                        <div style = "height: 130px;">
                                        <p class = "headerDisplay"><?php echo $headerContent[$i]["description"]; ?></p>
                                        <div style = "height: 130px;">
                                    </td>
                                    <td>
                                        <div style = "height: 130px;">
                                        <p class = "headerDisplay"><?php echo $headerContent[$i]["know_more_link"]; ?></p>
                                        <div style = "height: 130px;">
                                    </td>
                                    <td>
                                           <a href="#" onclick="updateHomeHeaderContent(<?php echo $headerContent[$i]['id']; ?>);">Edit</a><br>
                                       <a href="#" onclick = 'confirmRemoveData(<?php echo $headerContent[$i]["id"]; ?>);'>Delete</a>
                                    </td>
                                </tr> 
                        <?php 
                            } 
                        ?>
                            </tbody>
                            </table>
                            </form>
                    <div class="col-lg-6"></div>
                    <?php 
                        if (count($headerContent) < 5){ 
                    ?>
                    <div class="col-lg-3 blue-button-1"  onclick="updateHomeHeaderContent();">Add New</div>
                    
                    <?php
                        }
                    ?> 
                        </form>

                    <?php
                        } else { 
                            if (empty($headerContent)) {
                                echo "<center>No Data Found</center>";
                            }
                    ?>
                        <form id="other-header-content" enctype="multipart/form-data">
                            <input type="hidden" id="feature-image-url" name="feature-image-url" value="<?php echo $headerContent["image"]; ?>">
                            <input type="hidden" name="pageId" value="<?php echo $headerContent[0]['id']; ?>">
                            <input type="hidden" name="method" value="update">
                            <input type="hidden" name="pageName" value="<?php echo $featureType; ?>">
                            
                            <div class = "row form-group">
                                <label for="feature-image">Feature Image:</label>
                                <div id="my_image" class="col-md-9 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$headerContent[0]["image"]; ?>'); <?php if (empty($headerContent[0]['image']) || !isset($headerContent[0]['image'])) { echo 'display: none'; }?>;"></div>
                                <input type="file" class="form-control" name="feature-image" onchange="headerPreview('#feature-image','#my_image',null,'<?php echo $uploadDir; ?>');" id="feature-image" style="display: none;" />
                                <div class="col-lg-3 blue-button-1" id="upload-img">Choose file</div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="top-desc-title">Top Description Title:</label>
                                <input type="text" class="form-control" id="top-desc-title" name="top-desc-title" value="<?php echo $headerContent[0]["title"]; ?>" maxlength="100">
                            </div>
                            <div class="form-group">
                                <label for="top-desc">Top Description:</label>
                                <textarea class="form-control" rows="3" id="top-desc" name="top-desc" maxlength="10000"><?php echo $headerContent[0]["description"]; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="know-more">Know More URL :</label>
                                <input type="text" class="form-control" id="know-more" name="know-more" value="<?php echo $headerContent[0]["know_more_link"]; ?>" maxlength="100">
                            </div>
                            </form>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="addUpdateHeaderContent();">Submit</div>
                    <!--<div class="col-lg-3 blue-button-1"  onclick="preview();">Preview</div>-->
                    <div class="col-lg-3 blue-button-1" id="clear" onclick = "removeHomeHeader(<?php echo $headerContent[0]['id']; ?>)">Clear</div>
                    <?php 
                        }
                    ?> 
                </div>
            </div>
        </div>

        <?php 
        /* To detect option name and create options for select drop down */
            $pageNameArray = array('Home', 'Sustainibility', 'Business', 'About Panchshil', 'Alliances', 'Career', 'Media', 'Contact');
            $optionaValue = "<option>".ucwords($featureType)."</option>";
            for ($k = 0; $k < count($pageNameArray); $k++) {
                if ($pageNameArray[$k] != ucwords($featureType)) {
                    $optionaValue .= "<option>".$pageNameArray[$k]."</option>"; 
                }
            }
        ?>

        <script>
            $(document).ready(function() {                            
                var optionValues = '<?php echo $optionaValue; ?>';
                $("#feature-type").append(optionValues);   

                // $('#proceed').click(function(){
                //     alert('nikhil');
                //     console.log('working');
                // });                  
            });

            /* Declairing Page name globaly */
            var featurePageName = '<?php echo $featureType; ?>';

            /* To give pop up to enter Image from Client's Computer */
            $('#upload-img').click(function(){
                //alert('feature');
                $('#feature-image').click();
            });      

            /* To display remodal on click of remove header data */
            function confirmRemoveData(id) {
                $(".modal-content").load("confirmRemoveData.php?id="+id);
                $("#myModal").modal(); 
            }                   

            /* To remove header Contents from any page */
            function removeHomeHeader(headerId) {  
                $.ajax({
                    type:"POST",
                    dataType:"json",
                    url:"../controller/headerContentController.php",
                    data:{
                        headerId, method:"delete"
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.errCode == -1) {
                            $(".alert-success").show();
                            $("#success-msg").text("Successfully Deleted Header");
                            setTimeout( function() { $(".alert-success").hide(); }, 5000);
                        } else {
                            $(".alert-danger").show();
                            $("#error-msg").text("Failed to Delete Header Content");
                            setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                        }
                        $("#load-content").load("manageHeaderContent.php?featureType="+encodeURIComponent(featurePageName));
                    },
                    error: function(data) {
                        $(".alert-danger").show();
                        $("#error-msg").text('Error Deleting Content !!!');
                        setTimeout(function() { $(".alert-danger").hide(); }, 10000);

                    }
                });                
            }     

            /*It is Universal Function to Call function which delete/remove Data */
            function remodalDataDeleteCall(id) {
                removeHomeHeader(id);
                $('#myModal .close').click();
            }

            /* TO Display Remodal on click of Add or Update Home Contents or details */
            function updateHomeHeaderContent(pageId) {
                $(".modal-content").load("updateHeaderContent.php?pageId="+pageId);
                $("#myModal").modal();
            }

            /* TO add header Contents other than Home page */
            function addUpdateHeaderContent(pageId) {
                $('.btn-blue-button-1').prop('disable',true);
                var topDescTitle    = $('#top-desc-title').val();
                var topDesc         = $('#top-desc').val();
                var knowMore        = $('#know-more').val();
                if (topDescTitle == '' || topDesc == '' || knowMore == '') {
                    $('.blue-button-1').prop('disable',false);
                    $('.modal-err').text('Please Enter Mandetory Fields !!!');
                } else {
                    var form = $('#other-header-content')[0];
                    var formData = new FormData(form);
                    $.ajax({
                        type:'post',
                        dataType:'json',
                        url:'../controller/headerContentController.php',
                        data:formData,
                        contentType:false,
                        processData:false,
                        async:false,
                        success: function(data) {
                            //alert(data);
                            console.log(data);
                            if (data.errCode == -1) {
                                $('#myModal .close').click();
                                $('.alert-success').show();
                                $('#success-msg').text('Successfully Updated Header Details !!!');
                                $("#load-content").load("manageHeaderContent.php?featureType="+encodeURIComponent(featurePageName));
                                setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                            } else {
                                $('.blue-button-1').prop('disable',false);
                                $('.modal-err').text(data.errMsg);
                            }
                        },
                        error: function(data) {
                            $(".alert-danger").show();
                            $("#error-msg").text('Error in updating header content!!!');
                            setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                        }
                    });
                }                
            }

            /* TO add header Contents in only Home page */
            function addUpdateHomeHeaderContent(pageId) {
                $('.btn-success').prop('disable',true);
                var topDescTitle    = $('#top-desc-title').val();
                var topDesc         = $('#top-desc').val();
                var knowMore        = $('#know-more').val();
                if (topDescTitle == '' || topDesc == '' || knowMore == '') {
                    $('.btn-success').prop('disable',false);
                    $('.modal-err').text('Please Enter Mandetory Fields !!!');
                } else {
                    var form = $('#homePageContent')[0];
                    var formData = new FormData(form);
                    $.ajax({
                        type:'post',
                        dataType:'json',
                        url:'../controller/headerContentController.php',
                        data:formData,
                        contentType:false,
                        processData:false,
                        async:false,
                        success: function(data) {
                            //alert(data);
                            console.log(data);
                            if (data.errCode == -1) {
                                $('#myModal .close').click();
                                $('.alert-success').show();
                                $('#success-msg').text('Successfully Updated Header Details !!!');
                                $("#load-content").load("manageHeaderContent.php?featureType="+encodeURIComponent(featurePageName));
                                setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                            } else {
                                $('.btn-success').prop('disable',false);
                                $('.modal-err').text(data.errMsg);
                            }
                        },
                        error: function(data) {
                            $(".alert-danger").show();
                            $("#error-msg").text('Error in updating header content!!!');
                            setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                        }
                    });
                }                
            }

            /* To show preview of uploaded Image */
            function headerPreview(id,pre_id,nextSlider,uploadDir) {
                //alert('niks');
                var files =  $(id)[0].files;
                var img = new Image();

                /* Checking Is any File selected */
                if (!files.length || !window.FileReader) return;

                /* Check Uploadede file is Image Only */
                if (/^image/.test( files[0].type)){
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file
                    var fileType = files[0].type;
                    var fileName = files[0].name;
                    $(pre_id).show();
                    $('#slider-input-'+nextSlider).show();
                    /* set image data as background of div */
                    reader.onloadend = function(){ 
                        img.onload = function() {
                            if(id =="#feature-image"){
                                if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                                    $(id).val('');
                                    $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                                    $('#my_image').show();
                                    $(pre_id).hide();
                                }else{
                                    $('#my_image').hide();
                                    $(".modal-err").text('');
                                }
                            } else {
                                if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                                    $(id).val('');
                                    $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                                    $('#my_icon').show();
                                    $(pre_id).hide();
                                }else{
                                    $(".modal-err").text('');
                                    $('#my_icon').hide();
                                }
                            }
                        }
                        $(pre_id).css("background-image", "url("+this.result+")");
                    }
                }
            }

            /* To Show Perticular page on selection of Drop Down or Select option */
            $('select').on('change', function (e) {
                var featuretype = $("#feature-type").val();                
                $("#success-content").text('');
                $("#error-content").text('');
                $("#load-content").load("manageHeaderContent.php?featureType="+encodeURIComponent(featuretype));
            });
</script>
