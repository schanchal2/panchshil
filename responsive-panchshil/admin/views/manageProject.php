<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
      	 <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage Project Details</p>
        </div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <div class="col-lg-3 blue-button-1" onclick="openEditDialog();">Add New</div>
                    <thead>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;"> 
                        <th width="10%">Is Featured</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>                                     
                        <tr>
                            <td>  
                                <p class="pressFeature" id="" name="" style="color:Green; text-align:center;">Yes</p>                                                                     
                            </td>
                        	<td>
                                <div id="projectImage" class="col-lg-3 featureImages" style=""></div>
                            </td>                       
                            <td>
                                <div style="height: 130px;width: 100px;">
                                <p class="pressDisplay" id="name" name="name"></p>
                                <div style="height: 130px;">
                                </div>
                            </td>
                            <td>
                              <div style="height: 130px;width: 100px;">
                                <p class="pressDisplay" id="location" name="location"></p>
                                <div style="height: 130px;">
                                </div>
                            </td>
                            <td>
                                <a href="#" onclick="openEditDialog();s">Edit</a><br>
                                <a href="#" onclick="">Delete</a>
                            </td>   
                        </tr>
                    </tbody>
                </table>
            </div>
            
        </div>  
	</div>
</div>
<script>
function openEditDialog(){
    $(".modal-content").load("updateProjectContent.php");
    $("#myModal").modal();
}


</script>