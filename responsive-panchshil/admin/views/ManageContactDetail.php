<?php
//prepare for request
//start session
session_start();


//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../model/Contact_detail_Model.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';

$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootURL."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    
    $ContactDetail=getAllContact_Detail($conn);
    if(noError($ContactDetail)){

        $ContactDetail = $ContactDetail["response"];
        
    }else{
        $returnArr = $ContactDetail;
    }
}else{
    $returnArr = $conn;
    exit;
}
?>


<div class="col-lg-12 main-container-1">

    <div class="col-lg-12 sub-container">

        <div class="col-lg-12 status">
       <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage Contacts Details</p>
        </div>

        <div class="col-lg-12 redirect">

            <div class="row">

                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">

                    <thead>

                      <tr style="color:white; background-color:#3da2da;font-size: 14px; width:100%">
                  
                        <th width="20%">Location</th>

                        <th width="25%">Address</th>

                        <th width="25%">Phone No 1</th>
                     
                        <th width="20%">Fax</th>

                        <th width="10%">Action</th>

                    </tr>

                    </thead>

                    <tbody>

                    <?php foreach($ContactDetail as $value){ 

                        ?>

                        <tr style="width: 100%">
                      
                            <td> 
                            <p class = "contactdisplay" id="no-of-count" name="no-of-count" ><?php echo $value["location"];?>
                            </p></td>

                            <td> 
                            <p class = "contactdisplay" id="no-of-count" name="no-of-count" ><?php echo $value["address"]; ?></p>
                            </td>

                            <td> 
                            <p class = "contactdisplay" id="no-of-count" name="no-of-count" ><?php echo $value["phoneno1"]; ?></p>
                            </td>


                            <td>
                             <p class = "contactdisplay" id="no-of-count" name="no-of-count" >
                             <?php echo $value["fax"]; ?></p></td>

                            <td><a href="#" onclick="openEditDialog(<?php echo $value['id'];?>);">Edit</a><br>

                          <a href="#" onclick="confirmRemoveData(<?php echo $value['id']; ?>)">Delete</a></td>
 
                        </tr>

                    <?php } ?>

                    </tbody>

                </table>

            </div><!--.row-->

             <div class="col-lg-3 blue-button-1" onclick="openEditDialog();">Add New Contact</div>

        </div>  <!--./col-lg-12-->

</div><!--./sub-container-->

</div><!--./col-lg-12-->





<script>

    $(document).ready(function() {

        $('#example1').DataTable();

    } );


    //to open manage update campus dialog

   function openEditDialog(id){

        $(".modal-content").load("UpdateContactDetail.php?id="+id);

        $("#myModal").modal();

    }

    //to add or update campus details

    function addupdatecontact() {

        $('.btn-success').prop('disable',true);
        // var contactId      = $('#contactid').val();
        var contacttitle       = $('#contact-title').val();

        var contactaddress  = $('#contact-address').val();
        var phoneno =$('#contact-phoneno').val();

        var contactfax  = $('#contact-fax').val();


        if (contacttitle == '' || contactaddress == '' ||  phoneno == '' ||  contactfax == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#contactform')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/contact_detail_Controller.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                   
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated contact Details !!!');
                        $("#load-content").load("ManageContactDetail.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
              
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating contact Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }                
    }


    //to remove contact details by  id
function removeContact(id) {
//alert(id);        
        $.ajax({

            type:"POST",
            dataType:"json",
            url:"../controller/contact_detail_Controller.php",
            data:{
                DeleteContactId:id, method:"delete"
            },
            success: function(data) {
               // alert(data);
     
                console.log(data);
                if (data.errCode == -1) {

                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted contact Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Client Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("ManageContactDetail.php");
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting contact Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);
            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmRemoveData.php?id="+id);
        $("#myModal").modal(); 
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeContact(id);
        $('#myModal .close').click();
    }



</script>

