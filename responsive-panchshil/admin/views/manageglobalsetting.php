<?php

//start session
session_start();

require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php'; 
require_once '../utilities/errorMap.php';
require_once '../model/globalSettingModel.php';

$returnArr = array();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active redirect to the login page

    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    //$featureType = "home";
    $globalContent = getGlobalSettingContent(NULL,$conn);    

    if(noError($globalContent)){
              $globalContent =  $globalContent["response"];
    
    }else{
            $returnArr =  $globalContent;
    }
}else{
            $returnArr = $conn;
            exit;
}
?>
<div class="col-lg-12 main-container-1">
    <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
          <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;"> Manage Global Settings</p>

           
        </div>
        <div class="col-lg-12 redirect">
            <form id="global-setting">
                <input type="hidden" name="method" value="update">
                <input type="hidden" name="id" value="<?php echo $globalContent[0]['id']; ?>">
                <div class="form-group">
                    <label for="fb-link">Facebook Link:</label>
                    <input type="text" class="form-control" id="facebookLink" name="facebookLink" value="<?php echo $globalContent[0]['facebookLink'];  ?>">
                </div>
                <div class="form-group">
                    <label for="tw-link">Twitter Link:</label>
                    <input type="text" class="form-control" id="twitterLink"  name="twitterLink" value="<?php echo $globalContent[0]['twitterLink'];  ?>">
                </div>
                <div class="form-group">
                    <label for="li-link">LinkedIn Link:</label>
                    <input type="text" class="form-control" id="linkedinLink" name="linkedinLink" value="<?php echo $globalContent[0]['linkedinLink']; ?>">
                </div>
                <div class="form-group">
                    <label for="gp-link">Google+ Link:</label>
                    <input type="text" class="form-control" id="googleLink" name="googleLink" value="<?php echo $globalContent[0]['googleLink']; ?>">
                </div>
                <div class="form-group">
                    <label for="cs-video-link">YouTube Url:</label>
                    <input type="text" class="form-control" id="youtubeLink" name="youtubeLink" value="<?php echo $globalContent[0]['youtubeLink']; ?>">
                </div>
                <div class="form-group">
                    <label for="abt-desc">Privacy Policy Description:</label>
                    <textarea class="form-control" rows="5" id="aboutDescription" name="aboutDescription"><?php echo $globalContent[0]['aboutDescription']; ?></textarea>
                </div>
            </form>
            <div class="col-lg-6"></div>
            <div class="col-lg-3 blue-button-1" onclick="addUpdateGlobalContent();">Submit</div>
            <div class="col-lg-3 blue-button-1" id="clear" name="delete" onclick="removeGlobalSetting(<?php echo $globalContent[0]['id']; ?>);">Clear ALL</div>
        </div>
    </div>
</div>
<script>
      //to remove campus details by campus id
    function removeGlobalSetting(id){
            
          var form = $('#global-setting')[0];
          var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: '../controller/globalSettingController.php',
                data: {
                    id: id,
                    method: "delete"
                },
                dataType: 'json',

                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Deleted global setting  Details !!!');
                        $("#load-content").load("manageglobalsetting.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in Deleting global setting content!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        
    }

   /* TO add and update Global details */
    function addUpdateGlobalContent() {     
        
         var form = $('#global-setting')[0];
          var formData = new FormData(form);
           
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/globalSettingController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated global setting  Details !!!');
                        $("#load-content").load("manageglobalsetting.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating global setting content!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
                       
    }


</script>
