<?php
//prepare for request
//start session

session_start();

 //include necessary files
 require_once '../utilities/config.php';
 require_once '../utilities/dbUtils.php';
 require_once '../utilities/utilities.php';
 require_once '../utilities/errorMap.php'; 
 require_once '../controller/aboutCatagoriesController.php';  
 require_once '../model/aboutCategoriesModeldata.php'; 

$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getAboutCatDetails =  getAboutCatDetails($id,NULL,$conn);
    if(noError($getAboutCatDetails)){
        $getAboutCatDetails = $getAboutCatDetails["response"];                    
    }else{
        $returnArr = $getAboutCatDetails;
    }
}else{
    $returnArr = $conn;
    exit;
}
?>
<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
      	 <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage About Catagories
        </div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="updateViewAboutCatagoriesDialog();">Add Catagories</div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;"> 
                        <th width="25%">Image</th>
                        <th width="25%">Name</th>                        
                        <th width="25%">Description</th>                
                        <th width="25%">Status</th>
                        <th width="10%">Action</th>
                    </tr>
                    </thead>
                    <tbody>  
                    <?php for($i=0; $i <count($getAboutCatDetails); $i++) {?> 
                        <tr> 
                            <td>
                                <div id="businessImage" class="col-lg-3 featureImages" style=""></div>
                            </td>                                                       
                            <td>                            	 
                                 <p class="businessDisplay" id="name" name="name" ><?php echo $getAboutCatDetails[$i]['name']; ?></p>                                 
                            </td>                         
                            <td>                                
                                 <p class = "businessDisplay" id="shortDesc" name="shortDesc" value = ""><?php echo $getAboutCatDetails[$i]['shortDesc']; ?></p>                                
                            </td>
                            <td>                              
                                 <span style="font-size: 16px;font-weight: bold;">ACTIVE</span>                               
                            </td>
                            <td>
                                <a href="#" onclick="updateViewAboutCatagoriesDialog(<?php echo $getAboutCatDetails[$i]['id']; ?>);">Edit</a><br>
                                <a href="#" onclick="">Active/Deleted</a><br>
                                <a href="#" onclick="openViewAboutCatagoriesDialog(<?php echo "'".$getAboutCatDetails[$i]['aboutId']."','".$getAboutCatDetails[$i]['id']."'";?>);">Manage</a><br>
                            </td>   
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>  
	</div>
</div>
<script>
   function updateViewAboutCatagoriesDialog(id){
        $( ".modal-content" ).load("updateAboutCatagories.php?id="+id);
        $("#myModal").modal();
    }
  
    // add update about catagories details
    function addUpdateAboutCatDetails() {

        $('.btn-success').prop('disable',true);
        var name                 = $('#name').val();
        var shortDesc            = $('#shortDesc').val();
        var longDesc             = $('#longDesc').val();
        if (name == '' || shortDesc == '' || longDesc == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#businessFrom')[0];
            var formData = new FormData(form);
            formData.append("method","update");
            $.ajax({
                 
                type:'post',
                dataType:'json',
                url:'../controller/aboutCatagoriesController.php',

                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                   
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated about Details !!!');
                        $("#load-content").load("manageAboutContent.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating About Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }                
    }

    function openViewAboutCatagoriesDialog(){
        $("#load-content").load("manageGrowthChronicle.php");
    }
</script>
