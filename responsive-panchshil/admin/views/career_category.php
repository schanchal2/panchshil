<?php

//start session
session_start();

require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
//require_once '../model/matricesContentModel.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/careerDetailsModel.php';

$returnArr = array();

$id=$_GET['id'];
//echo $id;

$CareerName=$_GET['CareerName'];
//echo $CareerName;


if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active redirect to the login page

    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    //$featureType = "home";
    $getCareerCatDetails = getCareerCatDetails($id,$require,$conn);
     // printArr($getCareerCatDetails);
    if(noError($getCareerCatDetails)){

            $getCareerCatDetails = $getCareerCatDetails["response"];
    }else{
            $returnArr = $getCareerCatDetails;
    }
}else{
            $returnArr = $conn;
            exit;
}
?>

<div class="col-lg-12 main-container-1">
    <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
        <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;"> Manage <?php echo $CareerName;?> </p>
        </div>
        <div class="modal-err" style="color: red"></div>
        <div class="col-lg-12 redirect">

            <form id="careercatform" enctype = "multipart/form-data">
          		<input type="hidden" name="id" id="id" value="<?php echo $getCareerCatDetails[0]['id']; ?>">
          		<input type="hidden" name="career_cat_image_url" value="<?php echo $getCareerCatDetails[0]['image']; ?>">

          		<input type="hidden" name="method" value="update">

              <input type="hidden" name="career_id" value="<?php echo $id; ?>">


              <div class="form-group">
          			<label for="feature-image">Image : </label>
          			<input type="file" id = "career_cat_image" class="form-control" name="career_cat_image" onchange="careercatPreview('#career_cat_image', '#careercat-image-preview',null,'<?php echo $uploadDir; ?>');">
          			<div id="careercat-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getCareerCatDetails[0]["image"]; ?>'); <?php if (empty($getCareerCatDetails[0]['image']) || !isset($getCareerCatDetails[0]['image'])) { echo 'display: none'; }?>;"></div>
          		</div>


              <hr>
            
              <div class="form-group">
                  <label for="top-desc">Description:</label>
                  <textarea class="form-control" rows="3" id="career_cat_desc" name="career_cat_desc" maxlength="10000"><?php echo $getCareerCatDetails[0]["career_cat_desc"]; ?></textarea>
              </div>

               <div class="form-group">
                  <label for="top-desc">Name:</label>
                  <input type="text" class="form-control" id="career_cat_name" name="career_cat_name" value="<?php echo $getCareerCatDetails[0]["career_cat_name"]; ?>" maxlength="100">
              </div>

              <div class="form-group">
                  <label for="know-more">Designation :</label>
                  <input type="text" class="form-control" id="career_cat_design" name="career_cat_design" value="<?php echo $getCareerCatDetails[0]["career_cat_design"]; ?>" maxlength="100">
              </div>
              </form>
              <div class="col-lg-6"></div>
              <div class="col-lg-3 blue-button-1" onclick="addCareerCatDetails()">Submit</div>
              <!--<div class="col-lg-3 blue-button-1"  onclick="preview();">Preview</div>-->
              <div class="col-lg-3 blue-button-1" id="clear" onclick ="confirmRemoveData(<?php echo $getCareerCatDetails[0]['id']; ?>);">Clear</div>

        </div>
    </div>
</div>
<script>


    /* TO Display Remodal on click of Add or Update about details */


    /* TO add and update Clients details */
    function addCareerCatDetails(id) {
    //  alert(id);
        $('.btn-success').prop('disable',true);

        var career_cat_desc = $('#career_cat_desc').val();
        var career_cat_name = $('#career_cat_name').val();
        var career_cat_design = $('#career_cat_design').val();

        if (career_cat_desc == '' || career_cat_name == '' || career_cat_design == '') {

            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {

          var form = $('#careercatform')[0];
          var formData = new FormData(form);

            //alert(JSON.stringify(formData));
          //  console.log(formdata);

            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/careerCatController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                  //  alert(data);
                //   console.log(data);
                  //   alert(JSON.stringify(data));
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Career Category Details !!!');
                        $("#load-content").load("career_category.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Career Category content!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    //to remove campus details by campus id
    function removeCareerCat(id){
        var ID = id; //alert(ID);
            $.ajax({
                type: "POST",
                url: '../controller/careerCatController.php',
                data: {
                    id:ID,
                    method: "delete"
                },
                dataType: 'json',
                success: function (data) {
                  //  console.log(data);

                    if (data.errCode == -1) {
                        $(".alert-success").show();
                        $("#success-msg").text('Successfully deleted Career Category details!!!');
                        setTimeout(function() { $(".alert-success").hide(); }, 5000);
                    }else{
                        $(".alert-danger").show();
                        $("#error-msg").text("Failed to Delete Career Category Content");
                        setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                    }
                     $("#load-content").load("career_category.php?ID="+encodeURIComponent(ID));
                },
                error: function (data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in Career Category details!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });

    }

    /* To show preview of uploaded Image */
    function careercatPreview(id,pre_id,nextSlider,uploadDir) {

        var files =  $(id)[0].files;
        var img = new Image();

        /* Checking Is any File selected */
        if (!files.length || !window.FileReader) return;

        /* Check Uploadede file is Image Only */
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();
            /* set image data as background of div */
            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#my_logo"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_logo').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_logo').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
      //alert(id);
        $(".modal-content").load("confirmRemoveData.php?id="+id);
        $("#myModal").modal();
    }

  /*  function confirmRemoveData(id) {
        alert(id);
        alert(1);
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/forwordController.php",
            data:{
                id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Forword Content");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Forword Content");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageForword.php?id="+encodeURIComponent(id));
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Content !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);

            }
        });
    } */


    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeCareerCat(id);
        $('#myModal .close').click();
    }
</script>
