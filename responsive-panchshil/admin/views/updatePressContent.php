<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/pressContentModel.php';

$id = urldecode($_GET["id"]);
//echo $id;
$press_id = urldecode($_GET["press_id"]);
$method= urldecode($_GET["method"]);

$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    if($method!="insert"){
    $getPressDetails = getPressDetails($press_id,NULL,$conn);
    if(noError($getPressDetails)){
        $getPressDetails = $getPressDetails["response"];
    }else{
        $returnArr = $getPressDetails;
    }
  }

}else{
    $returnArr = $conn;
    exit;
}
//printArr($getPressDetails);
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if($method=="insert"){ ?>
	<h4 class="modal-title">Add New Press Details</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Press</h4>
		<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
  	<div class="modal-err1" style="color:red;"></div>
	<form id="pressForm" enctype = "multipart/form-data">
      <input type="hidden" name="press_status" id="press_status" value="<?php echo $getPressDetails[0]['status']; ?>">
  <input type="hidden" name="id" id="id" value="<?php echo $getPressDetails[0]['id']; ?>">
  <input type="hidden" name="media_id" id="media_id" value="<?php echo $press_id; ?>">
  <input type="hidden" name="media_id1" id="media_id1" value="<?php echo $getPressDetails[0]['media_id']; ?>">
  <input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getPressDetails[0]['image1']; ?>">
  <input type="hidden" name="imageUrl1" id="imageUrl1" value="<?php echo $getPressDetails[0]['image2']; ?>">
  <input type="hidden" name="pdfUrl" id="pdfUrl" value="<?php echo $getPressDetails[0]['pdf']; ?>">
  <input type="hidden" name="VideoUrl" id="VideoUrl" value="<?php echo $getPressDetails[0]['you_tube_url']; ?>">
  <input type="hidden" name="LinkUrl" id="LinkUrl" value="<?php echo $getPressDetails[0]['url']; ?>">
	<input type="hidden" name="method" value="update">

		<div class="form-group">
			<label for="feature-image">Box Image : </label>
			<input type="file" id = "press-feature-image" class="form-control" name="press-feature-image" onchange="pressPreview('#press-feature-image', '#press-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="press-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getPressDetails[0]["image1"]; ?>'); <?php if (empty($getPressDetails[0]['image1']) || !isset($getPressDetails[0]['image1'])) { echo 'display: none'; }?>;"></div>
		</div>

    <div class="form-group">
      <label for="feature-image">Feature Image : </label>
      <input type="file" id = "press-feature-image1" class="form-control" name="press-feature-image1" onchange="pressPreview('#press-feature-image1', '#press-image-preview1',null,'<?php echo $uploadDir; ?>');">
      <div id="press-image-preview1" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getPressDetails[0]["image2"]; ?>'); <?php if (empty($getPressDetails[0]['image2']) || !isset($getPressDetails[0]['image2'])) { echo 'display: none'; }?>;"></div>
    </div>


		<div class="form-group">
			<label for="about-name">Name</label>
			<input type="text" class="form-control" id="press-name" maxlength="100" name="press-name" value="<?php echo $getPressDetails[0]['name']; ?>" >
		</div>
		<div class="form-group">
			<label for="about-services">Source Name</label>
			<textarea class="form-control" rows="3" id="press-source-name" maxlength="100" name="press-source-name" ><?php echo $getPressDetails[0]['source_name']; ?></textarea>
		</div>
		<div class="form-group">
			<label for="about-services"> Description</label>
			<textarea class="form-control" rows="3" id="press-longDesc" maxlength="10000" name="press-longDesc" value=""><?php echo $getPressDetails[0]['descrption']; ?></textarea>
		</div>

    <div class="form-group">
      <label for="about-name">Select Type</label>
    </div>


    <select class="form-control droper" name="media_type" id="media_type" style="">
          <option value = "NULL"><?php echo $getPressDetails[0]['media_type']; ?></option>
          <option value = "A.php">A</option>
          <option value = "B.php">B</option>
          <option value = "C.php">C</option>
          <option value = "D.php">D</option>
          </select>




    <div class="form-group">
      <label for="date">Date:</label>
      <input type="date" class="form-control" id="press-date"  name="press-date" value="<?php echo $getPressDetails[0]['date']; ?>">
    </div>
    <div class="form-group" id="pdf_url_id">
      <label for="pdf-url">Add Pdf</label>
      <input type="file" class="form-control" id="pdf-url"  name="pdf-url" value="<?php echo $getPressDetails[0]['pdf']; ?>">
    </div>
    <div class="form-group"  id="video_url_id">
      <label for="video-url">Video Url:</label>
      <input type="text" class="form-control" id="video-url" maxlength="300" name="video-url" value="<?php echo $getPressDetails[0]['you_tube_url']; ?>">
    </div>
    <div class="form-group" id="link_url_id">
      <label for="share-link-url">Link Url:</label>
      <input type="text" class="form-control" id="link-url" maxlength="300" name="link-url" value="<?php echo $getPressDetails[0]['url']; ?>">
    </div>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdatePressDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>


<script>
var xTriggered = 0;
$( "#video-url" ).keyup(function( event ) {
  //alert(1);
   $len=$(this).val().length;
   console.log($len);
  // alert($len);
   if($len>0)
   {
    $('#link_url_id').hide();;
    $('#pdf_url_id').hide();;
   }
  else
  {
    $('#link_url_id').show();
    $('#pdf_url_id').show();;
  }
});

$( "#link-url" ).keyup(function( event ) {
  //alert(1);
   $len=$(this).val().length;
   console.log($len);
  // alert($len);
   if($len>0)
   {
    $('#video_url_id').hide();;
    $('#pdf_url_id').hide();;
   }
  else
  {
    $('#video_url_id').show();
    $('#pdf_url_id').show();;
  }
});


$(function(){
$("#pdf-url").change(function(){
  var file_name=$(this).val();
  //alert(file_name);
  if(file_name)
  {
    $('#video_url_id').hide();
    $('#link_url_id').hide();
  }
  else
  {
    $('#video_url_id').show();
    $('#link_url_id').show();
  }

})
});

</script>
