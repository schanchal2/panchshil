<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/businessSubCatagoryModel.php';


$category = cleanXSS(urldecode($_GET['subCategory']));
$businessId = cleanXSS(urldecode($_GET['businessId']));
$businessCatId = cleanXSS(urldecode($_GET['subCategoryId']));

$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];    
    $businessSubCatInfo = getBusinessSubCatDetails(NULL, $businessCatId, NULL, $conn);
    if(noError($businessSubCatInfo)){
        $businessSubCatInfo = $businessSubCatInfo["response"];                    
    }else{
        $returnArr = $businessSubCatInfo;
    }

}else{
    $returnArr = $conn;
    exit;
}
// printArr($businessSubCatInfo);
?>
<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
         <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage<?php echo " ".$category." "; ?></p>
        </div>
        <div class="col-lg-12 redirect">
            <div class="row">
            <input type="hidden" name="businessId" id="businessId" value="<?php echo $businessId;?>">
                <input type="hidden" name="businessCatId" id="businessCatId" value="<?php echo $businessCatId;?>">
                <input type="hidden" name="businessCatName" id="businessCatName" value="<?php echo $category;?>">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="updateBusinessCatagories('<?php echo $businessId;?>','<?php echo $businessCatId; ?>');">Add Catagories</div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;"> 
                        <th width="20%">Name</th>
                        <th width="20%">Number</th>
                        <th width="25%">Email</th>
                        <th width="5%">Status</th>
                        <th width="10%">Action</th>
                    </tr>
                    </thead>
                    <tbody>   
                        <?php 
                            if (empty($businessSubCatInfo)) {
                                echo "<tr><td colspan = '5' style = 'text-align:center;'>No Data Found</td></tr>";
                            }
                            for ($i = 0; $i < count($businessSubCatInfo); $i++) {
                        ?>              
                        <tr> 
                            <td>
                                <p class="businessDisplay" id="Name" name="Name"><?php echo $businessSubCatInfo[$i]['sub_cat_name']; ?></p>
                            </td>                                                        
                            <td>
                                 
                                 <p class="businessDisplay" id="Number" name="Number"><?php echo $businessSubCatInfo[$i]['sub_cat_number']; ?></p>
                                 
                            </td>
                             <td>
                               
                                 <p class="businessDisplay" id="Email" name="Email"><?php echo $businessSubCatInfo[$i]['sub_cat_email']; ?></p>
                                
                            </td>
                            <td>
                               
                                 <span style="font-size: 16px;font-weight: bold; <?php if($businessSubCatInfo[$i]["sub_cat_status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?> "><?php echo strtoupper($businessSubCatInfo[$i]["sub_cat_status"]); ?></span>
                                
                            </td>
                            <td>
                                <a href="#" onclick="updateBusinessCatagories(<?php echo "'".$businessId."','".$businessCatId."','".$businessSubCatInfo[$i]["id"]."'"; ?>);">
                                    Edit
                                </a>
                                <br>

                                <a href="#" onclick="confirmRemoveData(<?php echo $businessSubCatInfo[$i]['id']; ?>);">
                                    <span style="font-size: 16px;font-weight: bold;  ">
                                        <?php 
                                            if($businessSubCatInfo[$i]["sub_cat_status"] == "Active") { echo "Inactivate/Delete";
                                            } else {
                                                echo "Activate";
                                            } 
                                        ?>
                                    </span>
                                </a>
                                <br>

                               
                            </td>   
                        </tr>
                        <?php 
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>  
    </div>
</div>
<script type="text/javascript">
    function updateBusinessCatagories(businessId,businessCatId,businessSubCatId){


        $( ".modal-content" ).load("updateBusinessSubCatagory.php?businessId="+businessId+"&businessCatId="+businessCatId+"&businessSubCatId="+businessSubCatId);
        $("#myModal").modal();
    }

    /* To show preview of uploaded Image */
    function businessSubCatPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        /* Checking Is any File selected */
        if (!files.length || !window.FileReader) return;

        /* Check Uploadede file is Image Only */
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();
            /* set image data as background of div */
            reader.onloadend = function(){ 
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdateBusinessSubCatDetails() {
        $('.btn-success').prop('disable',true);
        var businessCatName         = encodeURIComponent($('#businessCatName').val());
        var businessCatId           = encodeURIComponent($('#businessCatId').val());
        var businessId              = encodeURIComponent($('#businessId').val());

        var businessSubCatName      = $('#businessSubCat-name').val();
        var businessSubCatNumber    = $('#businessSubCat-number').val();
        var businessSubCatEmail     = $('#businessSubCat-email').val();
        var businessSubCatAddress   = $('#businessSubCat-address').val();

        
        if (businessSubCatName == '' || businessSubCatNumber == '' || businessSubCatEmail == '' || businessSubCatAddress == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#businessSubCatForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/businessSubCatagoryController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Business Details !!!');
                        $("#load-content").load("manageBusinessSubCategory.php?subCategory="+businessCatName+"&subCategoryId="+businessCatId+"&businessId="+businessId);
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Business SUb-Category Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }                
    }

    /* To remove Client Details */
    function removeBusinessSubCat(id) {     
        var businessCatName         = encodeURIComponent($('#businessCatName').val());
        var businessCatId           = encodeURIComponent($('#businessCatId').val()); 
        var businessId              = encodeURIComponent($('#businessId').val());  
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/businessSubCatagoryController.php",
            data:{
                DeleteBusinessSubCatId:id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Business Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Business Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageBusinessSubCategory.php?subCategory="+businessCatName+"&subCategoryId="+businessCatId+"&businessId="+businessId);
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Business Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);
            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmChangesRemodal.php?id="+id);
        $("#myModal").modal(); 
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeBusinessSubCat(id);
        $('#myModal .close').click();
    }

    // function adddata(){
    //      $("#load-content").load("manageSubcategoriesInfo.php");
    // }
</script>