<?php

//start session
session_start();

require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../model/matricesContentModel.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';

$returnArr = array();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active redirect to the login page

    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    //$featureType = "home";
    $matricsContent = getMatricsContent(NULL,$conn);

    if(noError($matricsContent)){

            $matricsContent = $matricsContent["response"];
    }else{
            $returnArr = $matricsContent;
    }
}else{
            $returnArr = $conn;
            exit;
}
?>

<div class="col-lg-12 main-container-1">
    <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
            Manage Matrics Content
        </div>
        <div class="modal-err" style="color: red"></div>
        <div class="col-lg-12 redirect">
        <form id="matrics-content" enctype="multipart/form-data">                
                <div class="container-fluid">                   
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="form-group">
                                <label for="feature-img">Matrics:</label>                               
                            </div>
                        </div>
                    </div>
                                <table id="example1" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
                                <tbody><tr><th width="25%">Matrics Logo</th>
                                <th>Image Title</th>
                                <th>No Of Count</th>
                                 <th>Action</th>
                                </tr>
                                 <?php
                                    for ($i=0; $i < count($matricsContent); $i++) {
                                ?>
                                <tr>
                                    <td>
                                        <div id="my_logo" class="col-lg-3 featureImages" name="matrics-logo" style="background-image: url('<?php echo $rootUrl."uploads/".$matricsContent[$i]["matrics_logo"]; ?>');" value=""></div>
                                    </td>
                                    <td>
                                        <div style = "height: 130px;width: 100px;">
                                        <p class = "headerDisplay" id="matrics-title" name="matrics-title" ><?php echo $matricsContent[$i]['matrics_text']; ?></p>
                                        <div style = "height: 130px;">
                                       <!-- <input type="text" class="form-control" id="matrics-title" name="matrics-title" value="<?php echo $matricsContent[$i]['matrics_text']; ?>" readonly>-->
                                    </td>
                                    <td>
                                        <div style = "height: 130px;width: 100px;">
                                        <p class = "headerDisplay" id="no-of-count" name="no-of-count" ><?php echo $matricsContent[$i]['no_of_count']; ?></p>
                                        <div style = "height: 130px;">
                                        <!--<input type="text" class="form-control" id="no-of-count" name="no-of-count" value="<?php echo $matricsContent[$i]['no_of_count']; ?>" readonly>-->
                                    </td>
                                    <td>
                                         <a href="#" onclick="AddUpdatematrics(<?php echo $matricsContent[$i]['matrics_id']; ?>);">Edit</a><br>
                                         <a href="#" onclick ="confirmRemoveData(<?php echo $matricsContent[$i]['matrics_id']; ?>);">Delete</a>
                                    </td>
                                </tr> 
                                <?php
                                    }
                                ?>
                                </tbody></table>                           
                    <div class="col-lg-6"></div>
                    <?php 
                        if (count($matricsContent) < 5){ 
                    ?>
                    <div class="col-lg-3 blue-button-1" onclick="AddUpdatematrics();">Add New</div>
                    <?php 
                         }
                    ?>                   
                </div>
            </form>
        </div>
    </div>
</div>
<script>
  

    /* TO Display Remodal on click of Add or Update Matrics details */
   function AddUpdatematrics(id){
        $(".modal-content").load("updateMatrics.php?matrics_id="+id);
        $("#myModal").modal();
    }
    
    /* TO add and update Clients details */
    function addUpdateMatricsContent(id) {
        $('.btn-success').prop('disable',true);
        var matricsText     = $('#image-title').val();
        var noOfCount       = $('#no-of-count').val();        
        if (matricsText == '' || noOfCount == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else if(isNaN(noOfCount)){
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Numbers in count !!!');
        }else {
            var form = $('#matrics-content')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/matricesContentController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Header Details !!!');
                        $("#load-content").load("matrics.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating martics content!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }                
    }

    //to remove campus details by campus id
    function removeMatrics(id){
        var matricsId = id;
            $.ajax({
                type: "POST",
                url: '../controller/matricesContentController.php',
                data: {
                    matricsId:matricsId,
                    method: "delete"
                },
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if (data.errCode == -1) {
                        $(".alert-success").show();
                        $("#success-msg").text('Successfully deleted matrics details!!!');
                        setTimeout(function() { $(".alert-success").hide(); }, 5000);
                    }else{
                        $(".alert-danger").show();
                        $("#error-msg").text("Failed to Delete matrics Content");
                        setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                    }          
                     $("#load-content").load("matrics.php?matricsId="+encodeURIComponent(matricsId));
                },
                error: function (data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in deleting matrics details!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        
    }

    /* To show preview of uploaded Image */
    function headerPreview(id,pre_id,nextSlider,uploadDir) {
     
        var files =  $(id)[0].files;
        var img = new Image();

        /* Checking Is any File selected */
        if (!files.length || !window.FileReader) return;

        /* Check Uploadede file is Image Only */
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();
            /* set image data as background of div */
            reader.onloadend = function(){ 
                img.onload = function() {
                    if(id =="#my_logo"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_logo').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_logo').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmRemoveData.php?id="+id);
        $("#myModal").modal(); 
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeMatrics(id);
        $('#myModal .close').click();
    }
</script>