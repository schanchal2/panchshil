<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/clientDetailsModel.php';

$clientId 		= cleanXSS(urldecode($_GET["clientId"]));
$clientUrlId 	= cleanXSS(urldecode($_GET["clientUrlId"]));
//echo $clientUrlId;
$uploadDir 		= "../uploads/clientImages/";
$returnArr 		= array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $clientInfo = getClientDetails($clientId, $clientUrlId, $conn);
    if(noError($clientInfo)){
        $clientInfo = $clientInfo["response"];
    }else{
        $returnArr = $clientInfo;
    }

}else{
    $returnArr = $conn;
    exit;
}
//printArr($clientInfo);
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<?php if ($clientId=="undefined") {?>
	<h4 class="modal-title">Add New Client </h4>
	<?php } else{?>
	   <h4 class="modal-title">Update Client </h4>
	<?php }
	?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="clientfrom" enctype = "multipart/form-data">

        <input type="hidden" name="clientid" id="clientid" value="<?php echo $clientInfo[0]['clientId']; ?>">

        <input type="hidden" name="clientUrlId" id="clientUrlId" value="<?php echo $clientUrlId; ?>">

        <input type="hidden" name="clientColorImageUrl" id="clientColorImageUrl" value="<?php echo $clientInfo[0]['clientColorImage']; ?>">
		<input type="hidden" name="method" value="update">


		<div class="form-group">
			<label for="client-colour-photo">Image</label>
			<input type="file" id="client-colour-photo" class="form-control" name="client-colour-photo" onchange="clientPreview('#client-colour-photo', '#clientColorImagePreview',null,'<?php echo $uploadDir; ?>');">
			<div id="clientColorImagePreview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$clientInfo[0]['clientColorImage']; ?>'); <?php if (empty($clientInfo[0]['clientColorImage']) || !isset($clientInfo[0]['clientColorImage'])) { echo 'display: none'; }?>;  margin:10px 0 10px 0;"></div>
		</div>

		<div class="form-group">
			<label for="client-name">Name</label>
			<input type="text" class="form-control" id="client-name" name="client-name" value="<?php echo $clientInfo[0]['clientName'];?>" maxlength="100">
		</div>
		<div class="form-group">
			<label for="client-services">Service</label>
			<input type="text" class="form-control" rows="3" id="client-services" name="client-services" value="<?php  echo $clientInfo[0]['clientServices']; ?>" maxlength="100">
		</div>
		<div class="form-group">
			<label for="client-services">Service Belong To :</label>
			<input type="text" class="form-control" rows="3" id="client-services-belong" name="client-services-belong" value="<?php  echo $clientInfo[0]['clientServicesBelong']; ?>" maxlength="100">
		</div>

		<div class="form-group">
			<label for="client-url">URL</label>
			<input type="text" class="form-control" rows="3" id="client-url" name="client-url" value="<?php  echo $clientInfo[0]['clientUrl']; ?>" maxlength="1000">
		</div>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateClientsDetails();">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
