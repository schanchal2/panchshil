<?php
/*Start session*/
session_start();


/* Including all necessary files and model */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once "../model/matricesContentModel.php";

 $matricesId = cleanXSS(urldecode($_GET['matrics_id']));
 $uploadDir = "../uploads/matricsImages";

/* checking session is active or not */
if(!isset($_SESSION['email']) || empty($_SESSION['email'])){
	/* Session is active or not */
	print('<script>');
	print('var t = setTimeout(\'window.location="'.$rootUrl.'";\', 000);');
	print('</script>');
	die();
}


/* Declaring all necessary Variables*/
$returnArr = array();

/* Establishing connection to database */
$conn = createDbConnection($serverName, $dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
	$conn = $conn['conn'];
	if (isset($matricesId) || !empty($matricesId)) {
		$matricsInfo = getMatricsContent($matricesId,$conn);
		if (noError($matricsInfo)) {
			$matricsInfo = $matricsInfo['response'];
		} else {
			$returnArr = $matricsInfo;
		}
	}
} else {
			$returnArr = $conn;
			exit;
}

?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Add Matrics Content</h4>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="matrics-content" action="" method = "post" enctype = "multipart/form-data">
		<input type="hidden" name="matricesId" value="<?php echo $matricsInfo[0]['matrics_id']; ?>">
		<input type="hidden" name="imageUrl" value="<?php echo $matricsInfo[0]['matrics_logo']; ?>">
		<input type="hidden" name="method" value="update">

		<div class="form-group">
			<label for="image-logo"> Image Logo : </label>
			<input type="file" id = "img-logo" class="form-control" name="img-logo" onchange="headerPreview('#img-logo', '#matrics-img-preview',null,'<?php echo $uploadDir; ?>');">
		</div>
		<div id="matrics-img-preview" class="matrics-image featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$matricsInfo[0]["matrics_logo"]; ?>'); <?php if (empty($matricsInfo[0]['matrics_logo']) || !isset($matricsInfo[0]['matrics_logo'])) { echo 'display: none'; }?>"></div>
		<div class="form-group">
			<label for="image-title">Image Title:</label>
			<input type="text" class="form-control" id="image-title" name="image-title" value="<?php echo $matricsInfo[0]['matrics_text']; ?>" maxlength="100">
		</div>
		<div class="form-group">
			<label for="no-of-count">No of Count:</label>
			<input type="text" class="form-control" id="no-of-count" name="no-of-count" value="<?php echo $matricsInfo[0]['no_of_count']; ?>">
		</div>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateMatricsContent()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
