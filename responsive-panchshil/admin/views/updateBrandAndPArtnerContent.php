<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/brandAndPartnerContentModel.php';

$id = urldecode($_GET["id"]);
//echo $id;
$award_id = urldecode($_GET["award_id"]);
//echo $award_id;
$method= urldecode($_GET["method"]);
//echo $method;
$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
      if($method!="insert"){
    $getBrandAndPartnerDetails = getBrandAndPartnerDetails($id,NULL,$conn);

    if(noError($getBrandAndPartnerDetails)){
        $getBrandAndPartnerDetails = $getBrandAndPartnerDetails["response"];

    }else{
        $returnArr = $getBrandAndPartnerDetails;
    }
  }

}else{
    $returnArr = $conn;
    exit;
}
//printArr($getBrandAndPartnerDetails);
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if($method=="insert"){ ?>
	<h4 class="modal-title">Add New  Details</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Details </h4>
		<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
  	<div class="modal-err1" style="color:red;"></div>
	<form id="allianceForm" enctype = "multipart/form-data">
  <input type="hidden" name="id" id="id" value="<?php echo $getBrandAndPartnerDetails[0]['id']; ?>">
  <input type="hidden" name="alliance_id" id="alliance_id" value="<?php echo $id;?>">
    <input type="hidden" name="alliance_id1" id="alliance_id1" value="<?php echo $getBrandAndPartnerDetails[0]['alliance_id']; ?>">
  <input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getBrandAndPartnerDetails[0]['image']; ?>">
	<input type="hidden" name="method" value="update">
		<div class="form-group">
			<label for="about-name">Client Name</label>
			<input type="text" class="form-control" id="brand-name" name="brand-name" value="<?php echo $getBrandAndPartnerDetails[0]['name']; ?>" >
		</div>

		<div class="form-group">
			<label for="feature-image">Left Image : </label>
			<input type="file" id = "brand-feature-image" class="form-control" name="brand-feature-image" onchange="brandPreview('#brand-feature-image', '#brand-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="brand-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getBrandAndPartnerDetails[0]["image"]; ?>'); <?php if (empty($getBrandAndPartnerDetails[0]['image']) || !isset($getBrandAndPartnerDetails[0]['image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="about-services">Designation</label>
			<textarea class="form-control" rows="3" id="brand-source-name" maxlength="100" name="brand-source-name" ><?php echo $getBrandAndPartnerDetails[0]['desigantion']; ?></textarea>
		</div>
		<div class="form-group">
			<label for="about-services"> Description</label>
			<textarea class="form-control" rows="3" id="brand-longDesc" maxlength="10000" name="brand-longDesc" value=""><?php echo $getBrandAndPartnerDetails[0]['description']; ?></textarea>
		</div>

	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateBrandAndPartnerDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
