<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/testimonialContentModel.php';
$id = urldecode($_GET["id"]);
//echo $id;
$award_id = urldecode($_GET["award_id"]);
//echo $award_id;
$method= urldecode($_GET["method"]);
//echo $method;
$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
      if($method!="insert"){
    $getTestimonialDetails = getTestimonialDetailsByTestId($award_id,$conn);

    if(noError($getTestimonialDetails)){
        $getTestimonialDetails = $getTestimonialDetails["response"];

    }else{
        $returnArr = $getTestimonialDetails;
    }
  }

}else{
    $returnArr = $conn;
    exit;
}
//printArr($getTestimonialDetails);
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if($method=="insert"){ ?>
	<h4 class="modal-title">Add New Client Testimonial Details</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Client Testimonial </h4>
		<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
  	<div class="modal-err1" style="color:red;"></div>
	<form id="pressForm" enctype = "multipart/form-data">
      <input type="hidden" name="press_status" id="press_status" value="<?php echo $getTestimonialDetails[0]['status']; ?>">
  <input type="hidden" name="id" id="id" value="<?php echo $getTestimonialDetails[0]['id']; ?>">
  <input type="hidden" name="award_id" id="award_id" value="<?php echo $award_id;?>">
    <input type="hidden" name="award_id1" id="award_id1" value="<?php echo $getTestimonialDetails[0]['award_id']; ?>">
  <input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getTestimonialDetails[0]['image']; ?>">
	<input type="hidden" name="method" value="update">
		<div class="form-group">
			<label for="about-name">Client Name</label>
			<input type="text" class="form-control" id="press-name" maxlength="100" name="press-name" value="<?php echo $getTestimonialDetails[0]['name']; ?>" >
		</div>
		<div class="form-group">
			<label for="about-services">Designation</label>
			<textarea class="form-control" rows="3" id="press-source-name" maxlength="100" name="press-source-name" ><?php echo $getTestimonialDetails[0]['desigantion']; ?></textarea>
		</div>
		<div class="form-group">
			<label for="about-services"> Description</label>
			<textarea class="form-control" rows="3" id="press-longDesc" maxlength="10000" name="press-longDesc" value=""><?php echo $getTestimonialDetails[0]['description']; ?></textarea>
		</div>

	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateTestimonialDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
