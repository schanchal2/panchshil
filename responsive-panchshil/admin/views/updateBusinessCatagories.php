<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/businessCategoryDetailsModel.php';

$businessCatId = cleanXSS(urldecode($_GET["businessCatId"]));
$businessId = cleanXSS(urldecode($_GET["businessId"]));
$uploadDir = "../uploads/businessCategoryImages/";
$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];    
   	$businessCatInfo = getBusinessCatDetails($businessCatId,$businessId,NULL,$conn);
    if(noError($businessCatInfo)){
        $businessCatInfo = $businessCatInfo["response"];                    
    }else{
        $returnArr = $businessCatInfo;
    }

}else{
    $returnArr = $conn;
    exit;
}
// printArr($businessCatInfo);
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Update Business Category</h4>		
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="businessCatForm" enctype = "multipart/form-data">		
        <input type="hidden" name="catId" id="catId" value="<?php echo $businessCatInfo[0]['id']; ?>">
        <input type="hidden" name="bussinessId" id="bussinessId" value="<?php echo $businessId; ?>">
        <input type="hidden" name="business-category-status" id="business-category-status" value="<?php echo $businessCatInfo[0]['category_status']; ?>">
        <input type="hidden" name="business-categories-footer-image-url" id="business-categories-footer-image-url" value="<?php echo$businessCatInfo[0]['category_footer_image']; ?>">
        <input type="hidden" name="business-categories-big-image-url" id="business-categories-big-image-url" value="<?php echo $businessCatInfo[0]['category_big_image']; ?>">
        <input type="hidden" name="business-categories-feature-image-url" id="business-categories-feature-image-url" value="<?php echo $businessCatInfo[0]['category_feature_image']; ?>">
		<input type="hidden" name="method" value="update">

		<label for="business-category-status">Status : </label>	<span style="font-size: 16px;font-weight: bold; <?php if($businessCatInfo[0]["category_status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?> "><?php echo $businessCatInfo[0]["category_status"]; ?></span>
		<div class="form-group">
			<label for="feature-image">Big Image : </label>			
			<input type="file" id = "business-categories-big-image" class="form-control" name="business-categories-big-image" onchange="businessCatPreview('#business-categories-big-image', '#business-categories-big-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="business-categories-big-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$businessCatInfo[0]["category_big_image"]; ?>'); <?php if (empty($businessCatInfo[0]['category_big_image']) || !isset($businessCatInfo[0]['category_big_image'])) { echo 'display: none'; }?>;"></div>
		</div>	
		<div class="form-group">
			<label for="feature-image">Feature Image : </label>			
			<input type="file" id = "business-categories-feature-image" class="form-control" name="business-categories-feature-image" onchange="businessCatPreview('#business-categories-feature-image', '#business-categories-feature-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="business-categories-feature-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$businessCatInfo[0]["category_feature_image"]; ?>'); <?php if (empty($businessCatInfo[0]['category_feature_image']) || !isset($businessCatInfo[0]['category_feature_image'])) { echo 'display: none'; }?>;"></div>
		</div>	
		<div class="form-group">
			<label for="feature-image">Footer Image : </label>			
			<input type="file" id = "business-categories-footer-image" class="form-control" name="business-categories-footer-image" onchange="businessCatPreview('#business-categories-footer-image', '#business-categories-footer-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="business-categories-footer-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$businessCatInfo[0]["category_footer_image"]; ?>'); <?php if (empty($businessCatInfo[0]['category_footer_image']) || !isset($businessCatInfo[0]['category_footer_image'])) { echo 'display: none'; }?>;"></div>
		</div>
		<div class="form-group">
			<label for="client-name">Name</label>
			<input type="text" class="form-control" id="business-categories-name" name="business-categories-name" value="<?php echo $businessCatInfo[0]['category_name']; ?>" >
		</div>
		<div class="form-group">
			<label for="client-services">Short Description</label>
			<textarea class="form-control" rows="3" id="business-categories-short-desc" name="business-categories-short-desc"><?php echo $businessCatInfo[0]['category_short_desc']; ?></textarea>
		</div>
		<div class="form-group">
			<label for="client-services">Full Description</label>
			<textarea class="form-control" rows="3" id="business-categories-full-desc" name="business-categories-full-desc"><?php echo $businessCatInfo[0]['category_full_desc']; ?></textarea>
		</div>
		

	</form>	
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateBusinessCatDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>	
</div>