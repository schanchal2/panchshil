<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/propertyContentModel.php';

$id = urldecode($_GET["id"]);
//echo $id;
$property_id = urldecode($_GET["property_id"]);
//echo $property_id;
$method= urldecode($_GET["method"]);
//echo $method;

$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    if($method!="insert"){
    $getPropertyDetails = getPropertyDetails($property_id,NULL,$conn);
    if(noError($getPropertyDetails)){
        $getPropertyDetails = $getPropertyDetails["response"];
    }else{
        $returnArr = $getPropertyDetails;
    }
  }

}else{
    $returnArr = $conn;
    exit;
}
//printArr($getPropertyDetails);
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	 <?php if($method=="insert"){ ?>
	<h4 class="modal-title">Add New Commercial Details</h4>
	<?php }else {?>
			<h4 class="modal-title">Update Commercial</h4>
		<?php }?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
  	<div class="modal-err1" style="color:red;"></div>
	<form id="CommercialForm" enctype = "multipart/form-data">
      <input type="hidden" name="press_status" id="press_status" value="<?php echo $getPropertyDetails[0]['status']; ?>">
  <input type="hidden" name="id" id="id" value="<?php echo $getPropertyDetails[0]['id']; ?>">
  <input type="hidden" name="p_id" id="p_id" value="<?php echo $property_id; ?>">
  <input type="hidden" name="p_id1" id="p_id1" value="<?php echo $getPropertyDetails[0]['category_id']; ?>">
  <input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getPropertyDetails[0]['image']; ?>">
	<input type="hidden" name="method" value="update">

  <div class="form-group">
    <label for="feature-image">Box Image : </label>
    <input type="file" id = "commercial-feature-image" class="form-control" name="commercial-feature-image" onchange="aboutPreview('#commercial-feature-image', '#commercial-image-preview',null,'<?php echo $uploadDir; ?>');">
    <div id="commercial-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getPropertyDetails[0]["image"]; ?>'); <?php if (empty($getPropertyDetails[0]['image']) || !isset($getPropertyDetails[0]['image'])) { echo 'display: none'; }?>;"></div>
  </div>

		<div class="form-group">
			<label for="about-name">Name</label>
			<input type="text" class="form-control" id="name" name="name" value="<?php echo $getPropertyDetails[0]['name']; ?>"  maxlength="100" >
		</div>
		<div class="form-group">
			<label for="about-services">Location</label>
		<input type="text" class="form-control" id="location" name="location" value="<?php echo $getPropertyDetails[0]['location']; ?>"  maxlength="100">
		</div>
		<div class="form-group">
			<label for="about-services"> Headline</label>
		<input type="text" class="form-control" id="title" name="title" value="<?php echo $getPropertyDetails[0]['title']; ?>"  maxlength="100">
		</div>
    <div class="form-group">
      <label for="about-services"> Short Description</label>
      <textarea class="form-control" rows="3" id="short_desc" name="short_desc" maxlength="1000" value=""><?php echo $getPropertyDetails[0]['short_desc']; ?></textarea>
    </div>
    <div class="form-group">
			<label for="about-services"> Long Description</label>
			<textarea class="form-control" rows="3" id="long_desc" name="long_desc" maxlength="10000" value=""><?php echo $getPropertyDetails[0]['long_desc']; ?></textarea>
		</div>

    <div class="form-group"  id="video_url_id">
      <label for="video-url">Website Url:</label>
      <input type="text" class="form-control" id="website_url" maxlength="100"  name="website_url" value="<?php echo $getPropertyDetails[0]['website_url']; ?>">
    </div>
    <div class="form-group">
      <label for="about-services"> Latitude</label>
    <input type="text" class="form-control" id="latitude" maxlength="100"  name="latitude" value="<?php echo $getPropertyDetails[0]['latitude']; ?>" >
    </div>
    <div class="form-group">
      <label for="about-services"> Longitude</label>
    <input type="text" class="form-control" id="longitude" maxlength="100"  name="longitude" value="<?php echo $getPropertyDetails[0]['longitude']; ?>" >
    </div>

	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateCommercialDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
