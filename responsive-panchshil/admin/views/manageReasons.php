<?php
//prepare for request
//start session
session_start();


//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../model/ReasonsModel.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';

$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootURL."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    
    $getreasons=getreasons($conn);
    if(noError($getreasons)){

        $getreasons = $getreasons["response"];
        //printArr($getreasons);
    }else{
        $returnArr = $getreasons;
    }
    
    
}else{
    $returnArr = $conn;
    exit;
}

?>


<div class="col-lg-12 main-container-1">

    <div class="col-lg-12 sub-container">

        <div class="col-lg-12 status">
       <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage Reasons</p>
        </div>

        <div class="col-lg-12 redirect">

            <div class="row">

                <table id="myTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                 <div class="col-md-4">      
              
                  <input type="button" value="Add Reasons" style="height: 38px;" class="btn btn-success" onClick="AddRow(<?php echo $i; ?>);">
                </div>

                    <thead>

                      <tr style="color:white; background-color:#3da2da;font-size: 14px; width:100%">
                  
                        <th width="20%">Reasons No</th>

                        <th width="70%">Reasons</th>

                        <th width="10%">Action</th>

                    </tr>

                    </thead>

                    <tbody>

                    <?php //foreach($getreasons as $value){
                    for ($i=0; $i<count($getreasons); $i++) {

                        ?>

                  
                        <tr style="width: 100%">
                            
                            <td> 
                            <p class = "contactdisplay" id="no-of-count" name="no-of-count" ><?php echo $i+1;?>
                            </p></td>

                            <td> 
                            <p class = "contactdisplay" id="no-of-count" name="no-of-count" ><?php echo $getreasons[$i]["reasons"]; ?></p>
                            </td>

                            <td><a href="#" onclick="Editreasons(<?php echo $getreasons[$i]['id']; ?>);">Edit</a><br>

                          <a href="#" onclick="confirmRemoveData(<?php echo $getreasons[$i]['id']; ?>)">Delete</a></td>

                        </tr>
                       
                    <?php } ?>

                    </tbody>

                </table>

            </div><!--.row-->

             

        </div>  <!--./col-lg-12-->

</div><!--./sub-container-->

</div><!--./col-lg-12-->





<script>

    $(document).ready(function() {

        $('#example1').DataTable();

    } );


    //to open manage update campus dialog

   // function openEditDialog(id){

   //      $(".modal-content").load("update.php?id="+id);

   //      $("#myModal").modal();

   //  }

    //to add or update campus details

    function addupdatereasons1() {
            
        $('.btn-success').prop('disable',true);
        // var contactId      = $('#contactid').val();
        var number       = $('#number').val();
       // alert(number);

        var reasons  = $('#reasons').val();
        var method = 'update';
       // alert(reasons);

        if (number == '' || reasons == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            // var form = $('#contactform')[0];
            // var formData = new FormData(form);

            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/reasonscontroller.php',
                data: {
                number,reasons,method
                },
                
                success: function(data) {
                   
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated reasons !!!');
                        $("#load-content").load("manageReasons.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
              
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating reasons !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }                
    }


    //to remove contact details by  id
function Removereasons1(id) {
//alert(id);       
  // var method = 'delete'; 
        $.ajax({

            type:"POST",
            dataType:"json",
            url:"../controller/reasonscontroller.php",
            data:{
                DeleteReasonsid:id, method:"delete"
            },
            success: function(data) {
               // alert(data);
     
                console.log(data);
                if (data.errCode == -1) {

                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted reasons Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete reasons Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageReasons.php");
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting reasons Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);
            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmRemoveData.php?id="+id);
        $("#myModal").modal(); 
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        Removereasons1(id);
        $('#myModal .close').click();
    }


   function AddRow(i)
   {
    $('#myTable').append('<tr style="width: 100%"><td><p class="contactdisplay" id="no-of-count" name="no-of-count">'+(i+1)+'</p></td><td><input type="text" width="100%" id="reasons" name="reasons" value=""></td><td><a href="#" onclick="addupdatereasons1();">Save</a><br><a href="#" onclick="confirmRemoveData(<?php echo $value['id'];?>)">Delete</a></td></tr>');
     $('.btn-success').hide();
   }

   function Editreasons(p)
   {
    var record = "";


        <?php //foreach($getreasons as $value){ 
            for ($j = 0; $j < count($getreasons); $j++) {
        ?>
            if(p == <?php echo $getreasons[$j]['id']; ?>){
            record += '<tr style="width: 100%"><td><input type="hidden" width="100%" id="number" name="number" value="<?php echo $getreasons[$j]["id"];?>"><p class = "contactdisplay" id="no-of-count" name="no-of-count"><?php echo $j+1;?></p></td><td> <input type="text" width="100%" id="reasons" name="reasons" value="<?php echo $getreasons[$j]["reasons"]; ?>"></td><td><a href="#" onclick="addupdatereasons1();">Save</a><br><a href="#" onclick="confirmRemoveData(<?php echo $getreasons[$j]['id'];?>)">Delete</a></td></tr>'
        }else{
            record += '<tr style="width: 100%"><td><p class = "contactdisplay" id="no-of-count" name="no-of-count"><?php echo $j+1;?></p></td><td><p class = "contactdisplay" id="no-of-count" name="no-of-count" ><?php echo $getreasons[$j]["reasons"]; ?></p></td><td><a href="#" onclick="Editreasons(<?php echo $getreasons[$j]['id']; ?>);">Edit</a><br><a href="#" onclick="confirmRemoveData(<?php echo $getreasons[$j]['id']; ?>)">Delete</a></td></tr>'
        }
            <?php }?>
        
  
$('#myTable').text('');
$('#myTable').append(record);

   }

   


</script>



