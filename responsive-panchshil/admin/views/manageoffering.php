<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/offeringDetailsModel.php';

$category = cleanXSS(urldecode($_GET['pageName']));
// echo $category;
$BusinessId = cleanXSS(urldecode($_GET['id']));
// echo $BusinessId;

$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
    if(noError($conn)){
        $conn = $conn["conn"];
        $businessCatInfo = getofferingDetails($id,NULL,$conn);
        //printArr($getSustainCatDetails);
        if(noError($businessCatInfo)){
            $businessCatInfo = $businessCatInfo["response"];
        }else{
            $returnArr = $businessCatInfo;
        }

    }
// printArr($businessCatInfo);
?>
<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
      	 <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage<?php echo " ".$category." "; ?></p>
        </div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <input type="hidden" name="businessId" id="businessId" value="<?php echo $BusinessId;?>">
                <input type="hidden" name="businessName" id="businessName" value="<?php echo $category;?>">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="updateoffering('-1','<?php echo $BusinessId; ?>');">Add Category</div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;"> 
                        <th width="20%">Image</th>
                        <th width="20%">Name</th>
                        <th width="20%">Short Desc</th>                        
                        <th width="5%">Status</th>
                        <th width="10%">Action</th>
                    </tr>
                    </thead>
                    <tbody>   
                        <?php 
                            if (empty($businessCatInfo)) {
                                echo "<tr><td colspan = '5' style = 'text-align:center;'>No Data Found</td></tr>";
                            }
                            for ($i = 0; $i < count($businessCatInfo); $i++) {
                        ?>              
                        <tr> 
                            <td>
                                <div id="businessImage" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$businessCatInfo[$i]["category_big_image"]; ?>');"></div>
                            </td>                                                        
                            <td>
                            	 
                                 <p class="businessCatDisplay" id="Name" name="Name"><?php echo $businessCatInfo[$i]['category_name']; ?></p>
                                 
                            </td>

                            <td>
                                 
                                 <p class="businessCatDisplay" id="type" name="type"><?php echo $businessCatInfo[$i]['type']; ?></p>
                                 
                            </td>
                             
                            <td>
                               
                                 <span style="font-size: 16px;font-weight: bold; <?php if($businessCatInfo[$i]["category_status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?> "><?php echo strtoupper($businessCatInfo[$i]["category_status"]); ?></span>
                                
                            </td>
                            <td>
                                <a href="#" onclick="updateoffering(<?php echo "'".$businessCatInfo[$i]["id"]."','".$BusinessId."'"; ?>);">
                                    Edit
                                </a>
                                <br>

                                <a href="#" onclick="confirmRemoveData(<?php echo $businessCatInfo[$i]['id']; ?>);">
                                    <span style="font-size: 16px;font-weight: bold;  ">
                                        <?php 
                                            if($businessCatInfo[$i]["category_status"] == "Active") { echo "Inactivate/Delete";
                                            } else {
                                                echo "Activate";
                                            } 
                                        ?>
                                    </span>
                                </a>
                                <br>
                                 <a href="#" onclick="openSubCategory('<?php echo $businessCatInfo[$i]["pagename"];?>',<?php echo $businessCatInfo[$i]["business_id"];?>,<?php echo $businessCatInfo[$i]["id"];?>,'<?php echo $businessCatInfo[$i]["category_name"];?>');">Manage</a>

                            </td>   
                        </tr>
                        <?php 
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>  
	</div>
</div>
<script type="text/javascript">

    function updateoffering(id,bId){
        $( ".modal-content" ).load("updateBusinessOffering.php?businessCatId="+id+"&businessId="+bId);
        $("#myModal").modal();
    }

    /* To show preview of uploaded Image */
    function businessCatPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        /* Checking Is any File selected */
        if (!files.length || !window.FileReader) return;

        /* Check Uploadede file is Image Only */
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();
            /* set image data as background of div */
            reader.onloadend = function(){ 
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdateofferingDetails() {
        $('.btn-success').prop('disable',true);
        var businessId           = encodeURIComponent($('#businessId').val());
        var businessName         = encodeURIComponent($('#businessName').val());
        var name                 = $('#name').val();
        var shortDesc            = $('#shortDesc').val();
        var longDesc             = $('#longDesc').val();
        if (businessCatName == '' || shortDesc == '' || longDesc == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#businessCatForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/offeringDetailsController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Offering Details !!!');
                        $("#load-content").load("manageBusinessCategory.php?category="+businessName+"&id="+businessId);
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Offering Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }                
    }

    /* To remove Client Details */
    function removeBusinessCat(id) {    
        var businessId           = encodeURIComponent($('#businessId').val());
        var businessName         = encodeURIComponent($('#businessName').val());    
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/businessCategoryDetailsController.php",
            data:{
                DeleteBusinessCatId:id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Category of Offering Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Category of Offering Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageBusinessCategory.php?category="+businessName+"&id="+businessId);
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Category of Offering Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);
            }
        });
    }

    // /* To display remodal on click of remove header data */
    // function confirmRemoveData(id) {
    //     $(".modal-content").load("confirmChangesRemodal.php?id="+id);
    //     $("#myModal").modal(); 
    // }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeBusinessCat(id);
        $('#myModal .close').click();
    }

    function openSubCategory(pagename, business_id, id, category_name){
        // alert(pagename);
        // alert(business_id);
        // alert(category_name);
        // alert(id);
         $("#load-content").load(pagename+"?business_id="+business_id+"&businessCatId="+id+"&category_name="+encodeURIComponent(category_name));
    }

</script>