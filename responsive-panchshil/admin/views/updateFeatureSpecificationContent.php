<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();
//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/featureSpecificationContentModel.php';

$id = urldecode($_GET["id"]);
$uploadDir = "../uploads/mediaImages/";
$returnArr = array();
$returnArr = array();
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getFeatureSpecificationDetailsByUniqId = getFeatureSpecificationDetailsByUniqId($id,$conn);
    if(noError($getFeatureSpecificationDetailsByUniqId)){
        $getFeatureSpecificationDetailsByUniqId = $getFeatureSpecificationDetailsByUniqId["response"];
    }else{
        $returnArr = $getFeatureSpecificationDetailsByUniqId;
    }
}else{
    $returnArr = $conn;
    exit;
}
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Update Feature Specification</h4>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
  	<div class="modal-err1" style="color:red;"></div>
	<form id="galleryForm" enctype = "multipart/form-data">
      <input type="hidden" name="id" id="id" value="<?php echo $getFeatureSpecificationDetailsByUniqId[0]['id']; ?>">
  <input type="hidden" name="o_id" id="o_id" value="<?php echo $getFeatureSpecificationDetailsByUniqId[0]['o_id'] ?>"; >
  <input type="hidden" name="p_id" id="p_id" value="<?php echo $getFeatureSpecificationDetailsByUniqId[0]['p_id']; ?>">
  <input type="hidden" name="imageUrl" id="imageUrl" value="<?php echo $getFeatureSpecificationDetailsByUniqId[0]['image']; ?>">
	<input type="hidden" name="method" value="update">
		<div class="form-group">
			<label for="feature-image">Image: </label>
			<input type="file" id = "gallery-feature-image" class="form-control" name="gallery-feature-image" onchange="bannerPreview('#gallery-feature-image', '#gallery-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="gallery-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getFeatureSpecificationDetailsByUniqId[0]["image"]; ?>'); <?php if (empty($getFeatureSpecificationDetailsByUniqId[0]['image']) || !isset($getFeatureSpecificationDetailsByUniqId[0]['image'])) { echo 'display: none'; }?>;"></div>
		</div>
    <div class="form-group">
			<label for="corporate-services">Description :</label>
			<textarea class="form-control" rows="3" id="description" name="description" ><?php echo $getFeatureSpecificationDetailsByUniqId[0]['description']; ?></textarea>
		</div>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="UpdateFeatureSpecificationDetails()">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
</div>
