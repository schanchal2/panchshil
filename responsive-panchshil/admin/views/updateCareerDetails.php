<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/careerDetailsModel.php';

$id = urldecode($_GET["id"]);
//echo $id;
//echo $id;
//$clientUrlId 	= urldecode($_GET["clientUrlId"]);
$uploadDir 		= "../uploads/careerImages/";
$returnArr 		= array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];    
    $getCareerDetails = getCareerDetails($id,$require, $conn);
    //printArr($getCareerDetails);
    if(noError($getCareerDetails)){
        $getCareerDetails = $getCareerDetails["response"];                    
    }else{
        $returnArr = $getCareerDetails;
    }

}else{
    $returnArr = $conn;
    exit;
}
//printArr($getCareerDetails);
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<?php if ($id=="undefined") {?>
	<h4 class="modal-title">Add New Career </h4>	
	<?php } else{?>
	   <h4 class="modal-title">Update Career </h4>	
	<?php }
	?>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="careerfrom" enctype = "multipart/form-data">

        <input type="hidden" name="id" id="id" value="<?php echo $getCareerDetails[0]['id']; ?>">

        <input type="hidden" name="career-image-url" id="career-image-url" value="<?php echo $getCareerDetails[0]['image']; ?>">

        <input type="hidden" name="career-image-url1" id="career-image1-url" value="<?php echo $getCareerDetails[0]['career_image1']; ?>">

		<input type="hidden" name="method" value="update">


		<div class="form-group">
			<label for="career-image">Featured Image</label>
			<input type="file" id="career-image" class="form-control" name="career-image" onchange="careerPreview('#career-image', '#career-image-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="career-image-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$getCareerDetails[0]['image']; ?>'); <?php if (empty($getCareerDetails[0]['image']) || !isset($getCareerDetails[0]['image'])) { echo 'display: none'; }?>;  margin:10px 0 10px 0;"></div>
		</div>
		

		<div class="form-group">
			<label for="career-image1">Image</label>
			<input type="file" id="career-image1" class="form-control" name="career-image1" onchange="careerPreview('#career-image1', '#career-image1-preview',null,'<?php echo $uploadDir; ?>');">
			<div id="career-image1-preview" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$getCareerDetails[0]['career_image1']; ?>'); <?php if (empty($getCareerDetails[0]['career_image1']) || !isset($getCareerDetails[0]['career_image1'])) { echo 'display: none'; }?>;  margin:10px 0 10px 0;"></div>
		</div>	
		
		<div class="form-group">
			<label for="client-name">Name</label>
			<input type="text" class="form-control" id="career_name" name="career_name" value="<?php echo $getCareerDetails[0]['career_name'];?>">
		</div>
		<div class="form-group">
			<label for="client-services">Long Description</label>
			<input type="text" class="form-control" rows="3" id="career_description" name="career_description" value="<?php  echo $getCareerDetails[0]['career_description']; ?>" maxlength="100">
		</div>

		<div class="form-group">
			<label for="client-url">Designation</label>
			<input type="text" class="form-control" rows="3" id="career_designation" name="career_designation" value="<?php  echo $getCareerDetails[0]['career_designation']; ?>" maxlength="1000">
		</div>

		
	</form>	
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" onclick="addUpdateCareerDetails();">Submit</button>
	<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>	
</div>