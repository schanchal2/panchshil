<?php
//prepare for request
//start session
session_start();


//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/contactUsFormModel.php';

$returnArr = array();
$contactId = $_GET['contactId'];
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $contactUsContent = getContactUSDetails($contactId,$conn);
    if(noError($contactUsContent)){   
        $contactUsContent = $contactUsContent["response"];
   
    }else{
        $returnArr = $contactUsContent;
    }
}else{
    $returnArr = $conn;
    exit;
}

?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">View Client Details</h4>	
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<?php for($i=0; $i < count($contactUsContent); $i++) { ?>
	<form id="matrics-content" action="" method = "post" enctype = "multipart/form-data">
		<div class="form-group">
			<label>Reason:</label>
			<input type="text" class="form-control" id="reason" name="reason" value="<?php echo $contactUsContent[$i]["reason"];?>" readonly>
		</div>	
		<div class="form-group">
			<label>Name:</label>
			<input type="text" class="form-control" id="name" name="name" value="<?php echo $contactUsContent[$i]["firstName"]." ".$contactUsContent[$i]["lastName"];?>" readonly>
		</div>			
		<div class="form-group">
			<label>Phone:</label>
			<input type="text" class="form-control" id="phone" name="phone" value="<?php echo $contactUsContent[$i]["phone"];?>" readonly>
		</div>
		<div class="form-group">
			<label>Email:</label>
			<input type="text" class="form-control" id="email" name="email" value="<?php echo $contactUsContent[$i]["email"];?>" readonly>
		</div>
		<div class="form-group">
			<label>Profession:</label>
			<input type="text" class="form-control" id="profession" name="profession" value="<?php echo $contactUsContent[$i]["profession"];?>" readonly>
		</div>
		<div class="form-group">
			<label>Company:</label>
			<input type="text" class="form-control" id="company_name" name="company_name" value="<?php echo $contactUsContent[$i]["company_name"];?>" readonly>
		</div>
		<div class="form-group">
			<label>Designation:</label>
			<input type="text" class="form-control" id="designation" name="designation" value="<?php echo $contactUsContent[$i]["designation"];?>" readonly>
		</div>
		<div class="form-group">
			<label>Address:</label>
			<input type="text" class="form-control" id="address" name="address" value="<?php echo $contactUsContent[$i]["address"];?>" readonly>
		</div>
		<div class="form-group">
			<label>Country:</label>
			<input type="text" class="form-control" id="country" name="country" value="<?php echo $contactUsContent[$i]["country"];?>" readonly>
		</div>
		<div class="form-group">
			<label>City:</label>
			<input type="text" class="form-control" id="city" name="city" value="<?php echo $contactUsContent[$i]["city"];?>" readonly>
		</div>
		<div class="form-group">
			<label>Comment:</label>
			<textarea class="form-control"  id="comment" name="comment" maxlength="10000" readonly ><?php echo $contactUsContent[$i]["comment"];?> </textarea>
		</div>
	</form>	
	<?php } ?>
	<div class="modal-footer">
		<button type="button" class="btn btn-success" onclick="confirmRemoveData(<?php echo $contactUsContent[0]["id"]; ?>);">Delete</button>
		<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>	
	</div>
</div>
