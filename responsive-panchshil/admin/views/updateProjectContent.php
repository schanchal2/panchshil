<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Add New Project</h4>				
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<form id="project-form" action="" method = "post" enctype = "multipart/form-data">
		<input type="hidden" id = "projectId" class="" name="projectId" value=''>
		<input type="hidden" id = "method" class="" name="method" value="update">
		<div class="form-group">
			<label for="feature-image">Feature Image : </label>			
			<input type="file" id = "projectFeatureImage" class="form-control" name="projectFeatureImage" onchange="">
			<div id="projectFeatureImage" class="col-lg-3 featureImages" style=""></div>
		</div>	
		<div class="form-group">
			<label for="feature-image">Normal Image : </label>			
			<input type="file" id = "projectNormalImage" class="form-control" name="projectNormalImage" onchange="">
			<div id="projectNormalImage" class="col-lg-3 featureImages" style=""></div>
		</div>		
		<div class="form-group">
			<label>Offering Type:</label>
			<input type="text" class="form-control" id="offeringType" name="offeringType" value="">
		</div>
		<div class="form-group">
			<label>Catagory:</label>
			<input type="text" class="form-control" id="catagory" name="catagory" value="">
		</div>
		<div class="form-group">
			<label>Name:</label>
			<input type="text" class="form-control" id="catagory" name="catagory" value="">
		</div>
		<div class="form-group">
			<label>Location:</label>
			<input type="text" class="form-control" id="location" name="location" value="">
		</div>
		<div class="form-group">
			<label>Short Description:</label>
			<textarea class="form-control" id="shortDescription" name="shortDescription" rows = "3"></textarea>
		</div>
		<div class="form-group">
			<label>Long Description:</label>
			<textarea class="form-control" id="shortDescription" name="shortDescription" rows = "4"></textarea>
		</div>
		<div class="form-group">
			<label>Address:</label>
			<input type="text" class="form-control" id="address" name="address" value="">
		</div>
		<div class="form-group">
			<label>URL:</label>
			<input type="text" class="form-control" id="project-url" name="project-url" value="">
		</div>
		<div class="form-group">
			<label>Status:</label>
			<select style="width: 15%;">
  				<option value="volvo">Active</option>
  				<option value="saab">Inactive</option>
			</select>
		</div>
		<div class="form-group">
			<label>Area Range:</label>
			<input type="text" class="form-control" id="areaRange" name="areaRange" value="">
		</div>
		<div class="form-group">
			<label>PDF:</label>
			<input type="text" class="form-control" id="pdf" name="pdf" value="">
		</div>
		<div class="form-group">
			<label>logo:</label>
			<input type="file" id = "projectLogo" class="form-control" name="projectLogo" onchange="">
			<div id="projectLogo" class="col-lg-3 featureImages" style=""></div>
		</div>	
		<div class="form-group">
			<label>Logo Heading:</label>
			<input type="text" class="form-control" id="logo-heading" name="logo-heading" value="">
		</div>	
		<div class="form-group">
			<label for="is-feature"> Is Featured : </label><br>
			<input class="press-radio" type="radio" name="featured-press" value="No" checked=""> <span style="margin:0 15px 0 3px; color:red;">NO</span>
			 <input class="press-radio" type="radio" name="featured-press" value="yes"> <span style="margin:0 15px 0 3px; color:green;">Yes</span>  			
		</div>	
	</form>	
	<div class="modal-footer">		
		<button type="button" class="btn btn-success" onclick="">Submit</button>
		<button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>	
	</div>
</div>