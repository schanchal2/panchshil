<?php

//echo "LOLZ";

session_start();

//including necessary helpers


require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/businessDetailsModel.php';
require_once '../model/businessCategoryDetailsModel.php';
//require_once '../model/aboutCategoryDetailsModel.php';
require_once '../model/aboutContentModel.php';

require_once '../model/businessDetailsModel.php';
//require_once '../model/sustainContentModel.php';

require_once '../model/mediaContentModel.php';
require_once '../model/alliancesContentModel.php';


//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){

  //session is not active, redirect to login page
  print("<script>");
  print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
  print("</script>");
  die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];


    /* To call Business Categories */
    $getSustainDetails = getSustainDetails(NULL,NULL,$conn);
    if(noError($getSustainDetails)){
        $getSustainDetails = $getSustainDetails["response"];
    }else{
        $returnArr = $getSustainDetails;
    }
  }

    $conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];


    /* To call Business Categories */
    $businessDetailInfo = getBusinessDetails(NULL,NULL,$conn);
    if(noError($businessDetailInfo)){
        $businessDetailInfo = $businessDetailInfo["response"];
    }else{
        $returnArr = $businessDetailInfo;
    }

    /* To call About Categories */
    $getAboutDetails = getAboutDetails(NULL,NULL,$conn);
    if(noError($getAboutDetails)){
        $getAboutDetails = $getAboutDetails["response"];

    }else{
        $returnArr = $getAboutDetails;
    }


    /* To call Media Categories */
    $getMediaDetails = getMediaDetails(NULL,NULL,$conn);
    if(noError($getMediaDetails)){
        $getMediaDetails = $getMediaDetails["response"];

    }else{
        $returnArr = $getMediaDetails;
    }


    /* To call Alliances Categories */
    $getAlliancesDetails = getAlliancesDetails(NULL,NULL,$conn);
    if(noError($getAlliancesDetails)){
        $getAlliancesDetails = $getAlliancesDetails["response"];

    }else{
        $returnArr = $getAlliancesDetails;
    }

//printArr($getAlliancesDetails);
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Panchshil Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>css/protectedAdmin.css">
    <!-- <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo $rootUrl."views"; ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img class="img-responsive" src="<?php echo $rootUrl;?>/uploads/staticImages/logo.jpg" style = "background-repeat: no-repeat;"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Panchshil</b>Admin</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
         <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->

          <li class="dropdown messages-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-envelope-o"></i>
                <span class="label label-success">4</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 4 messages</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li><!-- start message -->
                      <a href="#">
                        <div class="pull-left">
                          <img src="" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                          Support Team
                          <small><i class="fa fa-clock-o"></i> 5 mins</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                      </a>
                    </li>
                    <!-- end message -->
                  </ul>
                </li>
                <li class="footer"><a href="#">See All Messages</a></li>
              </ul>
          </li>

          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">You have 10 notifications</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li>
                      <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                      </a>
                    </li>
                  </ul>
                </li>
               <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>

          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">You have 9 tasks</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Design some buttons
                            <small class="pull-right">20%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">20% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li>
                      <!-- end task item -->
                  </ul>
                </li>
                <li class="footer">
                  <a href="#">View all tasks</a>
                </li>
            </ul>
          </li>


          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $adminUser; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="" class="img-circle" alt="User Image">
                <p>
                  <?php echo $adminUser; ?>
                  <small>Panchshil</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="../controller/logoutController.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>

        </ul><!--nav navbar-nav-->
      </div><!--navbar menu-->
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->

  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
       <li class="treeview">
          <a href="#globalSettings" onclick="loadManageGlobalsettings();">
            <i class="fa fa-dashboard"></i><span>Manage Globals Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="#contact" onclick="loadGlobalSettignsContact();"><i class="fa fa-circle-o"></i>Contacts</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Dashboard v2</a></li>
          </ul>
      </li>


        <!--to load update manage header content form -->
        <li><a href="#headerContent" onclick="loadManageHeaderContent();"><i class="fa fa-header" aria-hidden="true"></i> <span>Manage Header Content</span></a></li>

        <li><a href="#matricsContent" onclick="loadMatrics();"><i class="fa fa-medium" aria-hidden="true"></i> <span>Manage Matrics</span></a></li>

        <li><a href="#clientContent" onclick="loadclient();"><i class="fa fa-users" aria-hidden="true"></i><span>Client</span></a></li>

        <li><a href="#careerContent" onclick="loadcareer();"><i class="fa fa-users" aria-hidden="true"></i><span>Careers</span></a></li>

        <li><a href="#manageContactUsContent" onclick="loadManageContactUs();"><i class="fa fa-phone" aria-hidden="true"></i><span>Manage Contact Us</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
        <ul class="treeview-menu">
            <li class="active"><a href="#reasons" onclick="loadReasons();"><i class="fa fa-circle-o"></i>Reasons</a></li>
            <li><a href="#profession" onclick="profession();"><i class="fa fa-circle-o"></i>Profession</a></li>
        </ul>
        </li>

        <li><a href="#enquiryForm" onclick="enquiryForm();"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span>Enquiry Form</span></a></li>

        <li><a href="#manageGrowthChronicle" onclick="manageGrowthChronicle();"><i class="fa fa-sign-in" aria-hidden="true"></i><span>Manage Growth Chronicle</span></a></li>

        <li><a href="#managePress" onclick="managePress();"><i class="fa fa-sign-in" aria-hidden="true"></i><span>Manage Press</span></a></li>

        <!--li><a href="#manageSustain" onclick="manageSustain();"><i class="fa fa-sign-in" aria-hidden="true"></i><span>Manage Sustainability</span></a></li-->

        <!--li><a href="#manageCorporate" onclick="manageCorporate();"><i class="fa fa-sign-in" aria-hidden="true"></i><span>Manage CorporateProfle</span></a></li-->

        <li class="treeview"><a href="#manageProject" onclick="manageProject();"><i class="fa fa-dashboard"></i> <span>Manage Project Details</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
            <ul class="treeview-menu">
                  <li class=""><a href="#manageSliderImages" onclick="manageSliderImages();"><i class="fa fa-circle-o"></i>Manage Slider Images</a></li>
                  <li><a href="#manageFeatureImages" onclick="manageFeatureImages();"><i class="fa fa-circle-o"></i>Manage Feature Images</a></li>
                  <li><a href="#layout" onclick="layout();"><i class="fa fa-circle-o"></i>Layout</a></li>
                  <li><a href="#amenities" onclick="amenities();"><i class="fa fa-circle-o"></i>Amenities</a></li>
                  <li><a href="#manageFeatureCircle" onclick="manageFeatureCircle();"><i class="fa fa-circle-o"></i>Manage Feature Circle</a></li>
                  <li><a href="#manageProjectMatices" onclick="manageProjectMatices();"><i class="fa fa-circle-o"></i>Manage Project Matrices</a></li>
                  <li><a href="#manageGallery" onclick="manageGallery();"><i class="fa fa-circle-o"></i>Manage Gallery</a></li>
            </ul>
        </li>

        <li>
          <a href="#manageBusiness" onclick="manageBusiness();">
            <i class="fa fa-sign-in" aria-hidden="true"></i>
            <span>Manage Business</span>
            <?php
              if (!empty($businessDetailInfo)) {
            ?>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            <?php
              }
            ?>
          </a>
          <?php
            if (!empty($businessDetailInfo)) {
          ?>
          <ul class="treeview-menu">
            <?php
              for ($i=0; $i < count($businessDetailInfo) ; $i++) {
            ?>
            <li class="active">
              <a href="#Businesses" onclick="loadBusiness(<?php echo "'".$businessDetailInfo[$i]['business_name']."','".$businessDetailInfo[$i]['id']."'";?>);">
                <i class="fa fa-circle-o" style = '<?php if($businessDetailInfo[$i]["business_status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?>'></i><?php echo $businessDetailInfo[$i]['business_name']?>

                <?php
                  $businessCatInfo = getBusinessCatDetails(NULL,$businessDetailInfo[$i]['id'],NULL,$conn);
                  $businessCatInfo = $businessCatInfo['response'];
                  if (!empty($businessCatInfo)) {
                ?>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                <?php
                  }
                ?>
              </a>
              <?php
                if (!empty($businessCatInfo)) {
              ?>
              <ul class = "treeview-menu">
              <?php
                  for ($j=0; $j < count($businessCatInfo) ; $j++) {
              ?>
                    <li>
                      <a href="#" onclick = "loadBusinessCategory(<?php echo "'".$businessDetailInfo[$i]['id']."','".$businessCatInfo[$j]['id']."','".$businessCatInfo[$j]['category_name']."' ";?>);">
                        <i class="fa fa-circle-o" style = '<?php if($businessCatInfo[$j]["category_status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?>'></i><?php echo $businessCatInfo[$j]['category_name']; ?>
                      </a>
                    </li>
              <?php
                  }
              ?>
              </ul>
              <?php
                  }
                }
              ?>
            </li>
            <?php } ?>
          </ul>
        </li>

        <li>
          <a href="#manageAbout" onclick="manageAbout();">
            <i class="fa fa-sign-in" aria-hidden="true"></i>
            <span>Manage About</span>
            <?php
              if (!empty($getAboutDetails)) {
            ?>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            <?php
              }
            ?>
          </a>
          <ul class="treeview-menu">
            <?php
              for ($k=0; $k < count($getAboutDetails) ; $k++) {
            ?>
              <li class="active">

                <a href="#manageAbout" onclick="loadAbout(<?php echo "'".$getAboutDetails[$k]['pagename']."','".$getAboutDetails[$k]['id']."'";?>);">

              <a href="#About" onclick="loadAbout('<?php echo $getAboutDetails[$k]['pagename'];?>')">

                  <i class="fa fa-circle-o" aria-hidden="true" style = '<?php if($getAboutDetails[$k]["status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?>'></i>
                  <span><?php echo $getAboutDetails[$k]['name']; ?></span>
                </a>
              </li>
            <?php

              }
            ?>
          </ul>
        </li>

        <li>
          <!-- <a href="#manageSustain" onclick="manageSustain();">
            <i class="fa fa-sign-in" aria-hidden="true"></i>
            <span>Manage Sustainability</span> -->
            <?php
              if (!empty($getSustainDetails)) {
            ?>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            <?php
              }
            ?>
          </a>
          <ul class="treeview-menu">
            <?php
              for ($m=0; $m < count($getSustainDetails) ; $m++) {
            ?>
              <li class="active">
                <a href="#manageSustain" onclick="loadSustain(<?php echo "'".$getSustainDetails[$m]['pagename']."','".$getSustainDetails[$m]['id']."'";?>);">
                  <i class="fa fa-circle-o" aria-hidden="true" style = '<?php if($getSustainDetails[$m]["status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?>'></i>
                  <span><?php echo $getSustainDetails[$m]['name']; ?></span>
                </a>
              </li>
            <?php

              }
            ?>
          </ul>
        </li>



         <!-- <li><a href="#manageMedia" onclick="manageMedia();"><i class="fa fa-sign-in" aria-hidden="true"></i><span>Manage Media</span></a></li> -->

        <li>
          <a href="#manageMedia" onclick="manageMedia();">
            <i class="fa fa-sign-in" aria-hidden="true"></i>
            <span>Manage Media</span>
            <?php
              if (!empty($getMediaDetails)) {
            ?>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            <?php
              }
            ?>
          </a>
          <ul class="treeview-menu">
            <?php
              for ($k=0; $k < count($getMediaDetails) ; $k++) {
            ?>
              <li class="active">
              <a href="#About" onclick="loadMedia('<?php echo $getMediaDetails[$k]['media_type'];?>',<?php echo $getMediaDetails[$k]['id'];?>)">
                  <i class="fa fa-circle-o" aria-hidden="true" style = '<?php if($getMediaDetails[$k]["status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?>'></i>
                  <span><?php echo $getMediaDetails[$k]['name']; ?></span>
                </a>
              </li>
            <?php
              }
            ?>
          </ul>
        </li>


        <li>
          <a href="#manageAlliances" onclick="manageAlliances();">
            <i class="fa fa-sign-in" aria-hidden="true"></i>
            <span>Manage Alliances</span>
            <?php
              if (!empty($getAlliancesDetails)) {
            ?>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            <?php
              }
            ?>
          </a>
          <ul class="treeview-menu">
            <?php
              for ($k=0; $k < count($getAlliancesDetails) ; $k++) {
            ?>
              <li class="active">
              <a href="#About" onclick="loadAlliance('<?php echo $getAlliancesDetails[$k]['id'];?>')">
                  <i class="fa fa-circle-o" aria-hidden="true" style = '<?php if($getAlliancesDetails[$k]["status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?>'></i>
                  <span><?php echo $getAlliancesDetails[$k]['name']; ?></span>
                </a>
              </li>
            <?php
              }
            ?>
          </ul>
        </li>


      </ul>

    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" style="width: 750px;">
          <!-- Modal content-->
          <div class="modal-content"></div>
        </div>
      </div>

      <!--to display success messages after submiting forms -->
      <div class="alert alert-success alert-dismissible" style="display: none;">
        <h4><i class="icon fa fa-check"></i> Success!!!</h4>
        <div id="success-msg"></div>
      </div>

      <!--to display failure messages after submiting forms -->
      <div class="alert alert-danger alert-dismissible" style="display: none;">
        <h4><i class="icon fa fa-ban"></i> Failure!</h4>
        <div id="error-msg"></div>
      </div>
      <div class="row">
        <!--to load all form content-->
        <div id="load-content">
          <div class="col-lg-12 main-container-1">
            <div class="col-lg-12 sub-container">
              <div class="col-lg-12 status">
                Panchshil Dashboard
              </div>

            </div>
          </div>
        </div>
      </div>
    </section>
        <!-- right col -->
  </div>
      <!-- /.row (main row) -->
    <!-- /.content -->

  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2015-2016 <a href="#">Panchshil</a>.</strong> All rights
    reserved.
  </footer>



  </div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../../lib/private/privateLte/js/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../../lib/private/privateLte/js/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Bootstrap 3.3.6 -->
<script src="../../lib/private/privateLte/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../../lib/private/privateLte/js/app.min.js"></script>
<script src="../../lib/private/privateLte/js/jquery.dataTables.min.js"></script>
<script src="../../lib/private/privateLte/js/dataTables.bootstrap.min.js"></script>

<script>
  $("li").click(function() {
    $("li").removeClass("active");
    $(this).addClass("active");
  });

  $(document).ready(function() {
    //loadManageGlobalsettings();
  });

  //to show preview of image befor upload.
  function showPrewiew(id,pre_id,nextSlider,uploadDir){
    var files =  $(id)[0].files;
    var img = new Image();

    if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
    if (/^image/.test( files[0].type)){ // only image file
      var reader = new FileReader(); // instance of the FileReader
      reader.readAsDataURL(files[0]); // read the local file
      var fileType = files[0].type;
      var fileName = files[0].name;
      $(pre_id).show();
      $(id+'-rmv').show();
      $(pre_id+'-text').show();
      $('#slider-input-'+nextSlider).show();
      reader.onloadend = function(){ // set image data as background of div
        $(pre_id).css("background-image", "url("+this.result+")");
        var base64 = this.result;
        img.src = this.result;
        img.onload = function() {
          //upload selected file
          fileUpload(base64,uploadDir,fileName,fileType,img.height,img.width);
        }
      }
    }
  }

  //touploadFileusing base64 string
  function fileUpload(base64String,uploadDir,fileName,fileType,height,width){
    $.ajax({
      type: "POST",
      url: '../controller/fileUploadController.php',
      data: {
        base64String: base64String,
        height:height,
        width:width,
        fileName:fileName,
        fileType:fileType,
        uploadDir:uploadDir,
        methode:"upload"
      },
      dataType: 'json',
      success: function (data) {
      },
      error: function (xhr, status, error) {
        $(".alert-danger").show();
        $("#error-msg").text('Error in uploading image!!!');
        setTimeout(function() { $(".alert-danger").hide(); }, 10000);
      }
    });
  }

  //to remove selected image
  function removeImage(id,pre_id,nextSlider,uploadDir){
    var fileName = $(id).val();
    $(id).val('');
    $(pre_id).hide();
    $(id+'-rmv').hide();
    $(pre_id+'-text').hide();
    deleteFile(fileName,uploadDir);
  }

  function deleteFile(fileName,uploadDir){
    $.ajax({
      type: "POST",
      url: '../controller/fileUploadController.php',
      data: {
        fileName:fileName,
        uploadDir:uploadDir,
        methode:"delete"
      },
      dataType: 'json',
      success: function (data) {
      },
      error: function (xhr, status, error) {
        $(".alert-danger").show();
        $("#error-msg").text('Error in uploading image!!!');
        setTimeout(function() { $(".alert-danger").hide(); }, 10000);
      }
    });
  }

  //to load manage gloabal setting view page
  function loadManageGlobalsettings(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageglobalsetting.php");
  }

  //to load contact in manage globalsetting
  function loadGlobalSettignsContact(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("ManageContactDetail.php");
  }
  //to load manage header content view page
  function loadManageHeaderContent(){
    //alert('niks');
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageHeaderContent.php");
  }

  //to load matrices content view page\
  function loadMatrics(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageMatricsDetails.php");
  }

  // to load the manage content view page */
  function loadManageContactUs(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageContactUs.php");
  }

  //to load the view page for media */
  function manageMedia(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageMediaContent.php");
  }


  //to load the view page for Alliances */
  function manageAlliances(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageAlliancesContent.php");
  }

  // to load the manage enquiry view page */
   function enquiryForm(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageEnquiryForm.php");
  }

  // to load the manage enquiry form view page */
  function manageGrowthChronicle(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageGrowthChronicle.php");
  }
// to load manage Press form view page */
  function managePress(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("managePressContent.php");
  }

  function manageSustain(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageSustainContent.php");
  }

  function manageCorporate(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageCorporateContent.php");
  }

// to load the manage about view page */
function manageAbout(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageAboutContent.php");
}

  // to load manage project form view page */
  function manageProject(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageProject.php");
  }

  // to load maange Business */
  function manageBusiness(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageBusiness.php");
  }

  // to load the manage Slider form view page */
  function manageSliderImages(){
      $(".alert-success").hide();
      $(".alert-danger").hide();
      $("#load-content").load("manageSliderImages.php");
  }

  //to load client content
  function loadclient(){
    $("#success-content").text('');
    $("#error-content").text('');
    $("#load-content").load("manageClientsDetails.php");
  }

  function loadcareer(){
    $("#success-content").text('');
    $("#error-content").text('');
    $("#load-content").load("manageCareerDetails.php");
  }

 //to load load reasons
  function loadReasons(){
    $(".alert-success").hide();
    $(".alert-danger").hide();
    $("#load-content").load("manageReasons.php");
  }

  function loadBusiness(category,id) {
    $("#success-content").text('');
    $("#error-content").text('');
    $("#load-content").load("manageBusinessCategory.php?category="+encodeURIComponent(category)+"&id="+encodeURIComponent(id));
  }


  function loadAbout(pagename,id) {
    //alert(pagename);
    if(pagename)
    {

    $("#success-content").text('');
    $("#error-content").text('');
    $("#load-content").load(pagename);
  }
  else
    {
      $("#load-content").load("manageBusinessCategory.php?category="+encodeURIComponent(category)+"&id="+encodeURIComponent(id));
    }

  }


  function loadAbout(category) {
    //alert(category);
    $("#success-content").text('');
    $("#error-content").text('');
    $("#load-content").load(category);
  }


  function loadMedia(pagename,id) {
    $("#success-content").text('');
    $("#error-content").text('');
    $("#load-content").load(pagename+'?&id='+id);
  }

  function loadAlliance(id) {
    //alert(category);
    $("#success-content").text('');
    $("#error-content").text('');
    $("#load-content").load("manageBrandAndPartnerContent.php?id="+id);
  }


  function loadBusinessCategory(business_id, id, category_name) {
    $("#success-content").text('');
    $("#error-content").text('');
    $("#load-content").load("manageBusinessSubCat.php?business_id="+business_id+"&businessCatId="+id+"&category_name="+encodeURIComponent(category_name));
  }

  function loadSustain(pagename,id) {
    //alert(pagename);
    if(pagename)
    {

    $("#success-content").text('');
    $("#error-content").text('');
    $("#load-content").load(pagename);
  }
  else
    {
      $("#load-content").load("manageBusinessCategory.php?category="+encodeURIComponent(category)+"&id="+encodeURIComponent(id));
    }

  }

</script>
</body>
</html>
