<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/floorPlanContentModel.php';
require_once '../model/pageNameContentModel.php';

$returnArr = array();

$o_id=$_GET['id'];
$p_id=$_GET['p_id'];

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $getFloorPlanDetails = getFloorPlanDetailsById($p_id,$conn);
    if(noError($getFloorPlanDetails)){
        $getFloorPlanDetails = $getFloorPlanDetails["response"];

    }else{
        $returnArr = $getFloorPlanDetails;
    }
  //  printArr($getFloorPlanDetails);

  $pageInfo = getPagename('offering',$o_id,$conn);
  if(noError($pageInfo)){
      $pageInfo = $pageInfo["response"][0];
  }else{
      $returnArr = $pageInfo;
  }

  $pageInfoForPropertyDetails = getPagename('property_details',$p_id,$conn);
  if(noError($pageInfoForPropertyDetails)){
      $pageInfoForPropertyDetails = $pageInfoForPropertyDetails["response"][0];
  }else{
      $returnArr = $pageInfoForPropertyDetails;
  }



}else{
    $returnArr = $conn;
    exit;
}

//printArr($getBannerDetails);

?>
<nav class="breadcrumb">
  <a class="breadcrumb-item" href="#" onclick="openViewOffering(<?php echo $getPropertyDetails[$i]['id']; ?>);" >Offering</a>
  <a class="breadcrumb-item" href="#" onclick="openViewParticulerPage(<?php echo $o_id; ?>,'<?php echo $pageInfo[pagename]; ?>');" >><?php echo ucfirst($pageInfo[name]); ?></a>
  <a class="breadcrumb-item" href="#" >><?php echo ucfirst($pageInfoForPropertyDetails[name]); ?></a>
  <a class="breadcrumb-item" href="#" >><strong>Manage Floor Plan</strong></a>
</nav>
<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
      	 <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Floor Plan</div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="updateViewBannerDialog('insert',<?php echo $o_id; ?>,<?php echo $p_id; ?>);">Add Floor Plan</div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px; ">
                        <th width="15%" style="text-align:center;">Floor Image</th>
                        <th width="15%" style="text-align:center;">Property Image</th>
                        <th width="15%" style="text-align:center;">Type</th>
                        <th width="15%" style="text-align:center;">Title</th>
                        <th width="25%" style="text-align:center;">Area</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (empty($getFloorPlanDetails)) {
                                echo "<tr><td colspan = '6' style = 'text-align:center;'>No Data Found</td></tr>";
                            }
                            for ($i = 0; $i < count($getFloorPlanDetails); $i++) {
                        ?>
                        <tr>
                            <td>
                                <div id="image" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getFloorPlanDetails[$i]["floor_img"]; ?>');"></div>
                            </td>
                            <td>
                                <div id="image" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getFloorPlanDetails[$i]["property_img"]; ?>');"></div>
                            </td>
                            <td>
                                 <p class="businessDisplay" id="source_name" name="source_name"><?php echo $getFloorPlanDetails[$i]['type']; ?></p>
                            </td>
                            <td>
                                 <p class="businessDisplay" id="source_name" name="source_name"><?php echo $getFloorPlanDetails[$i]['area']; ?></p>
                            </td>
                            <td>
                                <a href="#" onclick="updateViewBannerDialog('update',<?php echo $getFloorPlanDetails[$i]['id']; ?>);">Edit</a><br>
                                <a href="#" onclick="confirmRemoveData(<?php echo $getFloorPlanDetails[$i]['id']; ?>,<?php echo $getFloorPlanDetails[$i]['p_id']; ?>,<?php echo $getFloorPlanDetails[$i]['o_id']; ?>);">
                                    <span style="font-size: 16px;font-weight: bold;  ">Delete
                                    </span>
                                </a><br>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</div>
<script>

function openViewOffering(){
     $("#load-content").load("manageOffering.php");
 }

 function openViewParticulerPage(id,pagename){
        $("#load-content").load(pagename+'?&id='+id);
  }


   function updateViewBannerDialog(method,b_id,p_id){
        $( ".modal-content" ).load("updateFloorPlan.php?b_id="+b_id+'&method='+method+'&p_id='+p_id);
        $("#myModal").modal();
    }

    /* To show preview of uploaded Image */
    function bannerPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        if (!files.length || !window.FileReader) return;
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();

            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdateFloorPlanDetails() {
        $('.btn-success').prop('disable',true);
            var p_id                 = $('#p_id').val();
            var o_id                 = $('#o_id').val();
            var o_id1                = $('#o_id1').val();
            var p_id1                = $('#p_id1').val();
            var form = $('#floorPlanForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/floorPlanController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                  //  alert(data);``
                  //  console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Floor Plan Details !!!');
                        if(p_id && p_id1)
                        {
                            $("#load-content").load("manageFloorPlan.php?p_id="+p_id1+"&id="+o_id1);
                        }
                        else {
                          $("#load-content").load("manageFloorPlan.php?p_id="+p_id+"&id="+o_id);
                        }
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Floor Plan Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }


    /* To remove about Details */
    function removeFloorPlan(id,p_id,o_id) {
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/floorPlanController.php",
            data:{
                id:id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Floor Plan Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Amenities Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
              $("#load-content").load("manageFloorPlan.php?p_id="+p_id+"&id="+o_id);
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Floor Plan Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);

            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id,p_id,o_id) {
        $(".modal-content").load("confirmChangesRemodalByIdAndPid.php?id="+id+'&p_id='+p_id+'&o_id='+o_id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id,p_id,o_id) {
        removeFloorPlan(id,p_id,o_id);
        $('#myModal .close').click();
    }

    function openViewMediaDialog(pagename){
  // alert(pagename);
      //  $("#load-content").load(pagename);
    }

</script>
