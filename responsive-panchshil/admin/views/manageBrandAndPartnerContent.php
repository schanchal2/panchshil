<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/pageNameContentModel.php';
require_once '../model/brandAndPartnerContentModel.php';

$returnArr = array();

$id=$_GET['id'];
//echo $id;
//echo $media_id;
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);


if(noError($conn)){
    $conn = $conn["conn"];
    $getBrandAndPartnerDetails = getBrandAndPartnerDetailsById($id,$conn);
    if(noError($getBrandAndPartnerDetails)){
        $getBrandAndPartnerDetails = $getBrandAndPartnerDetails["response"];

    }else{
        $returnArr = $getBrandAndPartnerDetails;
    }

    $pageInfo = getPagename('alliancesDetails',$id,$conn);
    if(noError($pageInfo)){
        $pageInfo = $pageInfo["response"][0];
    }else{
        $returnArr = $pageInfo;
    }

}else{
    $returnArr = $conn;
    exit;
}

//printArr($pageInfo);

?>
<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
      	 <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage <?php echo strtoupper($pageInfo['name'])?></div>
        <div class="col-lg-12 redirect">
            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="updateViewMediaDialog('insert',<?php echo $id;?>);">Add Details </div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px; ">
                        <th width="30%" style="text-align:center;">Image</th>
                        <th width="25%" style="text-align:center;">Name</th>
                        <th width="25%" style="text-align:center;">Designation</th>
                          <th width="25%" style="text-align:center;">Description</th>
                        <th width="5%" style="text-align:center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (empty($getBrandAndPartnerDetails)) {
                                echo "<tr><td colspan = '6' style = 'text-align:center;'>No Data Found</td></tr>";
                            }
                            for ($i = 0; $i < count($getBrandAndPartnerDetails); $i++) {
                        ?>
                        <tr>



                            <td>
                                <div id="image" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getBrandAndPartnerDetails[$i]["image"]; ?>');"></div>
                            </td>
                            <td>
                                 <p class="businessDisplay" id="name" name="name"><?php echo $getBrandAndPartnerDetails[$i]['name']; ?></p>
                            </td>
                            <td>
                                 <p class="businessDisplay" id="source_name" name="source_name"><?php echo $getBrandAndPartnerDetails[$i]['desigantion']; ?></p>
                            </td>

                            <td>
                                 <p class="businessDisplay" id="date" name="date"><?php echo $getBrandAndPartnerDetails[$i]['description']; ?></p>
                            </td>
                            <td>
                                <a href="#" onclick="updateViewMediaDialog('update',<?php echo $getBrandAndPartnerDetails[$i]['id']; ?>);">Edit</a><br>
                                <a href="#" onclick="confirmRemoveData(<?php echo $getBrandAndPartnerDetails[$i]['id']; ?>,<?php echo $getBrandAndPartnerDetails[$i]['alliance_id']; ?>);">
                                    <span style="font-size: 16px;font-weight: bold;  ">Delete
                                    </span>
                                </a><br>

                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</div>
<script>
   function updateViewMediaDialog(method,id){
        $( ".modal-content" ).load("updateBrandAndPArtnerContent.php?id="+id+"&method="+method);
        $("#myModal").modal();
    }

    /* To show preview of uploaded Image */
    function brandPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        if (!files.length || !window.FileReader) return;
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();

            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdateBrandAndPartnerDetails() {
        $('.btn-success').prop('disable',true);

        var alliance_id          = $('#alliance_id').val();
        var alliance_id1         = $('#alliance_id1').val();
        var name                 = $('#brand-name').val();
        var shortDesc            = $('#brand-source-name').val();
        var longDesc             = $('#brand-longDesc').val();
        if (name == '' || shortDesc == '' || longDesc == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        }

        else {
            var form = $('#allianceForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/brandAndPartnerController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                   console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Brand And Partner Details !!!');
                        if( alliance_id1)
                        {
                            $("#load-content").load("manageBrandAndPartnerContent.php?id="+alliance_id1);
                        }
                        else {
                            $("#load-content").load("manageBrandAndPartnerContent.php?id="+alliance_id);
                        }

                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Brand And Partner Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    /* To remove about Details */
    function removeBrandAndPartner(id,alliance_id) {
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/brandAndPartnerController.php",
            data:{
                id:id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Brand And Partner Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Brand And Partner Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageBrandAndPartnerContent.php?id="+alliance_id);
            },
            error: function(data) {

                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Brand And Partner Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);

            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id,alliance_id) {
        $(".modal-content").load("confirmChangesRemodalById.php?id="+id+"&award_id="+alliance_id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id,alliance_id) {
        removeBrandAndPartner(id,alliance_id);
        $('#myModal .close').click();
    }

    function opentestimonial(){
        $("#load-content").load("managetestimonialContent.php");
    }

</script>
