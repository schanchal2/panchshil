<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/sustainContentModel.php';

$sustainid = cleanXSS(urldecode($_GET['id']));
//echo $sustainid;

//echo "kavita";

$name = cleanXSS(urldecode($_GET['pageName']));
//echo $name;

$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
// printArr($conn);
if(noError($conn)){
    $conn = $conn["conn"];
    $getSustainDetails = getSustainDetails($sustainid,NULL,$conn);
    //printArr($getSustainDetails);
    if(noError($getSustainDetails)){
        $getSustainDetails = $getSustainDetails["response"];

    }else{
        $returnArr = $getSustainDetails;
    }

}else{
    $returnArr = $conn;
    exit;
}
//printArr($getSustainDetails);
?>

<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
         <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage <?php echo $name;?></p>
        </div>

        <div class="col-lg-12 redirect">

            <div class="row">
            <input type="hidden" name="sustainid" id="sustainid" value="<?php echo $sustainid; ?>">
            <input type="hidden" name="name" id="name" value="<?php echo $name; ?>">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="openViewSustainDialog(<?php echo $sustainid; ?>,'insert','<?php echo $name; ?>');">Add Sustain Category</div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;">
                      <th width="25%">Image</th>
                      <th width="25%">Name</th>
                      <th width="25%">Status</th>
                      <th width="25%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (empty($getSustainDetails)) {
                                echo "<tr><td colspan = '5' style = 'text-align:center;'>No Data Found</td></tr>";
                            }
                            for ($i = 0; $i < count($getSustainDetails); $i++) {
                        ?>
                        <tr>
                            <td>
                                <div id="sustainImages" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$getSustainDetails[$i]["image"]; ?>');"></div>
                            </td>
                            <td>

                                 <p class = "headerDisplay" id="Name" name="Name" value = ""><?php echo $getSustainDetails[$i]['name']; ?></p>
                            </td>

                             <td>

                                 <span style="font-size: 16px;font-weight: bold; <?php if($getSustainDetails[$i]["status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?> "><?php echo strtoupper($getSustainDetails[$i]["status"]); ?></span>

                            </td>
                            <td>
                                <a href="#" onclick="openViewSustainDialog(<?php echo $getSustainDetails[$i]['sustainid']; ?>);">Edit</a><br>

                                <a href="#" onclick="confirmRemoveData(<?php echo $getSustainDetails[$i]['id']; ?>);">
                                    <span style="font-size: 16px;font-weight: bold;  ">
                                        <?php if($getSustainDetails[$i]["status"] == "Active"){ echo "Inactivate/Delete";}else{echo "Activate";} ?>
                                    </span>
                                </a><br>

                                <a href="#" onclick='openSusCat("<?php echo $getSustainDetails[$i]['pagename']; ?>",<?php echo $getSustainDetails[$i]['id'];?>,"<?php echo $getSustainDetails[$i]['name'];?>");'>Manage</a>
                            </td>
                        </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function openViewSustainDialog(sustainid,method,name){
       //alert(sustainid);
       //alert(name);
        $( ".modal-content" ).load("updateSustain.php?sustainid="+sustainid+'&method='+method+'&name='+encodeURIComponent(name));
        $("#myModal").modal();
    }

    /* To show preview of uploaded Image */
    function sustainPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        /* Checking Is any File selected */
        if (!files.length || !window.FileReader) return;

        /* Check Uploadede file is Image Only */
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();
            /* set image data as background of div */
            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdateSustainDetails() {
        $('.btn-success').prop('disable',true);
        var sustainid            = $('#sustainidUrl').val();
        var nameUrl              = $('#nameUrl').val();
        var name                 = $('#sustain-name').val();  
        var shortDesc            = $('#sustain-shortDesc').val(); 
        var longDesc             = $('#sustain-longDesc').val(); 
        if (name == '' || shortDesc == '' || longDesc == '') {
          
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
           
            var form = $('#sustainFrom')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/sustainDetailsController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Sustain Details !!!');
                        $("#load-content").load("manageSustainContent.php?id="+sustainid+'&pageName='+encodeURIComponent(nameUrl));
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {

                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Sustain Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    /* To remove Client Details */
    function removeSustain(id) {
      //alert(1);
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/sustainDetailsController.php",
            data:{
                id:id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Sustain Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Sustain Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageSustainDetails.php?sustainid="+sustainid);
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Sustain Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);
            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmChangesRemodal.php?id="+id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeSustain(id);
        $('#myModal .close').click();
    }

    function openSusCat(pagename,id,name){
      //alert(pagename);
      //alert(id);
      //alert(name);
            $("#load-content").load(pagename+'?&sustaincatId='+id+'&pagename1='+encodeURIComponent(name));
    }

</script>
