<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/businessDetailsModel.php';

$returnArr = array();

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $businessInfo = getBusinessDetails($businessId,NULL,$conn);
    if(noError($businessInfo)){
        $businessInfo = $businessInfo["response"];
    }else{
        $returnArr = $businessInfo;
    }

}else{
    $returnArr = $conn;
    exit;
}
// printArr($businessInfo);
?>

<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
         <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage Business</p>
        </div>

        <div class="col-lg-12 redirect">

            <div class="row">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="openViewBusinessDialog();">Add Business</div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;">
                        <th width="20%">Image</th>
                        <th width="20%">Name</th>
                        <th width="25%">Short Description</th>
                        <th width="5%">Status</th>
                        <th width="10%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (empty($businessInfo)) {
                                echo "<tr><td colspan = '5' style = 'text-align:center;'>No Data Found</td></tr>";
                            }
                            for ($i = 0; $i < count($businessInfo); $i++) {
                        ?>
                        <tr>
                            <td>
                                <div id="businessImage" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$businessInfo[$i]["business_big_image"]; ?>');"></div>
                            </td>
                            <td>

                                 <p class = "headerDisplay" id="Name" name="Name" value = ""><?php echo $businessInfo[$i]['business_name']; ?></p>

                            </td>
                            <td>

                                 <p class = "headerDisplay" id="short-desc" name="short-desc" value = ""><?php echo $businessInfo[$i]['business_short_desc']; ?></p>

                            </td>
                             <td>

                                 <span style="font-size: 16px;font-weight: bold; <?php if($businessInfo[$i]["business_status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?> "><?php echo strtoupper($businessInfo[$i]["business_status"]); ?></span>

                            </td>
                            <td>
                                <a href="#" onclick="openViewBusinessDialog(<?php echo $businessInfo[$i]['id']; ?>);">Edit</a><br>

                                <a href="#" onclick="confirmRemoveData(<?php echo $businessInfo[$i]['id']; ?>);">
                                    <span style="font-size: 16px;font-weight: bold;  ">
                                        <?php if($businessInfo[$i]["business_status"] == "Active"){ echo "Inactivate/Delete";}else{echo "Activate";} ?>
                                    </span>
                                </a><br>

                                <a href="#" onclick="openSubcategories('<?php echo $businessInfo[$i]["pagename"];?>',<?php echo $businessInfo[$i]["id"];?>,'<?php echo $businessInfo[$i]["business_name"];?>');">Manage</a>
                            </td>
                        </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function openViewBusinessDialog(id){
        $( ".modal-content" ).load("updateBusiness.php?businessId="+id);
        $("#myModal").modal();

    }

    /* To show preview of uploaded Image */
    function businessPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        /* Checking Is any File selected */
        if (!files.length || !window.FileReader) return;

        /* Check Uploadede file is Image Only */
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();
            /* set image data as background of div */
            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdateBusinessDetails() {
        $('.btn-success').prop('disable',true);
        var businessName      = $('#business-name').val();
        var businessShortDesc = $('#business-short-desc').val();
        var businessFullDesc  = $('#business-full-desc').val();
        if (businessName == '' || businessShortDesc == '' || businessFullDesc == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#businessFrom')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/businessDetailsController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Business Details !!!');
                        $("#load-content").load("manageBusiness.php");
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Business Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    /* To remove Client Details */
    function removeBusiness(id) {
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/businessDetailsController.php",
            data:{
                DeleteBusinessId:id, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Business Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Business Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageBusiness.php");
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Business Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);
            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmChangesRemodal.php?id="+id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeBusiness(id);
        $('#myModal .close').click();
    }

    function openSubcategories(pagename,id,business_name){
        $("#load-content").load(pagename+"?id="+id+"&pageName="+encodeURIComponent(business_name));
    }

</script>
