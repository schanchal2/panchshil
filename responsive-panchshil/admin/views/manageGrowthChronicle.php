<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/growthCronicleModel.php';

$returnArr = array();
$growthUrlId    = cleanXSS(urldecode($_GET['id']));
$pageName       = cleanXSS(urldecode($_GET['pageName']));

//echo $id;
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    $growthInfo = getGrowthDetails(NULL,$growthUrlId,$conn);
    if(noError($growthInfo)){
        $growthInfo = $growthInfo["response"];
    }else{
        $returnArr = $growthInfo;
    }
    // $growthName = getGrowthCorporateDetails($growthUrlId,NULL,NULL,$conn);
    // if(noError($growthName)){
    //     $growthName = $growthName["response"];
    // }else{
    //     $returnArr = $growthName;
    // }
}else{
    $returnArr = $conn;
    exit;
}
// printArr($growthName);
?>
<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
     <input type="hidden" name="pagetype" id="pagetype" value="<?php echo "$id"; ?>">
        <div class="col-lg-12 status">
      	 <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage <?php echo ucfirst(strtolower($pageName)); ?> Details</p>
        </div>
        <div class="col-lg-12 redirect">
            <div class="row">
            <input type="hidden" name="growth-Url-Id" id="growth-Url-Id" value="<?php echo $growthUrlId; ?>">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                   <div class="col-lg-3 blue-button-1" onclick="openViewGrowthChronicleDialog('-1','<?php echo $growthUrlId;?>');">Add New</div>
                    <thead>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;">
                        <th>Image</th>
                        <th>Year</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (empty($growthInfo)) {
                                echo "<tr><td colspan = '5' style = 'text-align:center;'>No Data Found</td></tr>";
                            }

                            for($i = 0; $i < count($growthInfo); $i++) {

                        ?>
                        <tr>
                        	<td>
                                <div id="growthColorImage" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$growthInfo[$i]["growthImage"]; ?>');"></div>
                            </td>
                            <td>

                                <p class = "growthYear" id="year" name="year" readonly><?php echo $growthInfo[$i]["growthYear"];?></p>

                            </td>
                             <td>
                                <?php
                                    $desc1 ="";
                                    $desc = nl2br($growthInfo[$i]["growthDescription"]);
                                    $desc = explode('<br />', $desc);
                                    foreach ($desc as $key => $value) {
                                        $value = trim($value);
                                        if (!empty($value)) {
                                            if (count($desc) > 1) {
                                                $desc1 .= '<i class="icon fa fa-circle" style = "margin-right:5px; font-size:8px; float:left;margin-top:7px;"></i>'.$value."<br>";
                                            } else {
                                                $desc1 = $value;
                                            }
                                        }
                                    }
                                ?>
                                <p class = "growthDesc" id="description" name="description" readonly><?php echo $desc1; ?></p>

                            </td>
                            <td>
                                <a href="#" onclick="openViewGrowthChronicleDialog('<?php echo $growthInfo[$i]["id"]; ?>','<?php echo $growthInfo[$i]["growthUrlId"];?>');">Edit</a><br>
                                <a href="#" onclick="confirmRemoveData(<?php echo $growthInfo[$i]["id"]; ?>);">Delete</a>
                            </td>
                        </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>

        </div>
	</div>
</div>
<script src="https://cdn.ckeditor.com/4.5.10/standard/ckeditor.js"></script>
<script>
  /* TO Display Remodal on click of view Growth details */
    function openViewGrowthChronicleDialog(id,urlId){

        $( ".modal-content" ).load("updateGrowthChronicleDetails.php?growthId="+id+'&growthUrlId='+urlId);
        $("#myModal").modal();
    }

    var pageName    = '<?php echo $pageName; ?>';
    /* To show preview of uploaded Image */
    function growthPreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        /* Checking Is any File selected */
        if (!files.length || !window.FileReader) return;

        /* Check Uploadede file is Image Only */
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();
            /* set image data as background of div */
            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdateGrowthDetails() {
        $('.btn-success').prop('disable',true);
        var growthUrlId     = $('#growth-Url-Id').val();
        var growthYear      = $('#growthYear').val();
        var growthDescription       = $('#description1').val();

        if (growthYear == '' || growthDescription == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#growth-form')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/growthCronicleController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Growth Details !!!');
                        $("#load-content").load("manageGrowthChronicle.php?id="+growthUrlId+"&pageName="+encodeURIComponent(pageName));
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Growth Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    /* To remove Growth Details */
    function removeGrowthDetails(id) {
        var growthUrlId     = $('#growth-Url-Id').val();
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/growthCronicleController.php",
            data:{
                deleteGrowthId:id,deleteGrowthUrlId:growthUrlId, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Growth Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Growth Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageGrowthChronicle.php?id="+growthUrlId+"&pageName="+encodeURIComponent(pageName));
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Growth Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);
            }
        });
    }



    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmRemoveData.php?id="+id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeGrowthDetails(id);
        $('#myModal .close').click();
    }

</script>
