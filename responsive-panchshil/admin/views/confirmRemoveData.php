<?php
$id = $_GET['id'];
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Confirmation</h4>	
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<p style="font-size:20px; font-weight:bold; text-align: center;">Sure You want to Delete This data</p>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss = "modal">No</button>	
	<button type="button" class="btn btn-success " id="proceed" onclick="remodalDataDeleteCall('<?php echo $id; ?>')">Yes</button>	
</div>