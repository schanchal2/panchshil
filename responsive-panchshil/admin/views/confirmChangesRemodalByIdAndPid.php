<?php
$id = $_GET['id'];
$p_id = $_GET['p_id'];
$o_id = $_GET['o_id'];
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Confirmation</h4>
</div>
<div class="modal-body">
	<div class="modal-err" style="color:red;"></div>
	<p style="font-size:20px; font-weight:bold; text-align: center;">Sure You Want To Do Changes</p>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss = "modal">No</button>
	<button type="button" class="btn btn-success " id="proceed" onclick="remodalDataDeleteCall(<?php echo $id; ?>,<?php echo $p_id; ?>,<?php echo $o_id; ?>)">Yes</button>
</div>
