<?php
/*Created By Kavita*/
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/propertyContentModel.php';
require_once '../model/pageNameContentModel.php';

$returnArr = array();

$p_id=$_GET['id'];
//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
   //session is not active, redirect to login page
   print("<script>");
   print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
   print("</script>");
   die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);


if(noError($conn)){
   $conn = $conn["conn"];
   $getPropertyDetails = getPropertyDetailsById($p_id,$conn);
   if(noError($getPropertyDetails)){
       $getPropertyDetails = $getPropertyDetails["response"];

   }else{
       $returnArr = $getPropertyDetails;
   }
 //  printArr($getPropertyDetails);

   $pageInfo = getPagename('offering',$p_id,$conn);
   if(noError($pageInfo)){
       $pageInfo = $pageInfo["response"][0];
   }else{
       $returnArr = $pageInfo;
   }


}else{
   $returnArr = $conn;
   exit;
}

//printArr($getPropertyDetails);

?>
<nav class="breadcrumb">
  <a class="breadcrumb-item" href="#" onclick="openViewOffering(<?php echo $getPropertyDetails[$i]['id']; ?>);" >Offering</a>
  <a class="breadcrumb-item" href="#" >><strong><?php echo " ";  echo ucfirst($pageInfo[name]); ?></strong></a>
</nav>

<div class="col-lg-12 main-container-1">
  <div class="col-lg-12 sub-container">
       <div class="col-lg-12 status">
        <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">
        Manage <?php echo " ";  echo ucfirst($pageInfo[name]); ?></div>
       <div class="col-lg-12 redirect">
           <div class="row">
               <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                   <thead>
                   <div class="col-lg-6"></div>
                   <div class="col-lg-3 blue-button-1" onclick="updateViewHospitalityDialog('insert',<?php echo $p_id; ?>);">Add Retails</div>
                   <tr style="color:white; background-color:#3da2da;font-size: 16px; ">
                       <th width="15%" style="text-align:center;">Logo Image</th>
                       <th width="15%" style="text-align:center;"> Name</th>
                       <th width="15%" style="text-align:center;">Small Desc</th>
                       <th width="10%" style="text-align:center;">Action1</th>
                       <th width="15%" style="text-align:center;">Action2</th>
                   </tr>
                   </thead>
                   <tbody>
                       <?php
                           if (empty($getPropertyDetails)) {
                               echo "<tr><td colspan = '6' style = 'text-align:center;'>No Data Found</td></tr>";
                           }
                           for ($i = 0; $i < count($getPropertyDetails); $i++) {
                       ?>
                       <tr>
                           <td>
                               <div id="image" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl."uploads/".$getPropertyDetails[$i]["image"]; ?>');"></div>
                           </td>
                           <td>
                                <p class="businessDisplay" id="source_name" name="source_name"><?php echo $getPropertyDetails[$i]['name']; ?></p>
                           </td>
                           <td>
                                <p class="businessDisplay" id="name" name="name"><?php
                                echo $getPropertyDetails[$i]['short_desc'];  ?></p>
                           </td>
                           <td>
                               <a href="#" onclick="updateViewHospitalityDialog('update',<?php echo $getPropertyDetails[$i]['id']; ?>);">Edit</a><br>
                                 <a href="#" onclick="confirmRemoveData(<?php echo $getPropertyDetails[$i]['id']; ?>,<?php echo $getPropertyDetails[$i]['o_id']; ?>);">
                                   <span style="font-size: 16px;font-weight: bold;  ">
                                       Delete
                                   </span>
                               </a><br>
                           </td>
                           <td>
                               <a href="#" onclick="openViewBannerDialog(<?php echo $getPropertyDetails[$i]['category_id']; ?>,<?php echo $getPropertyDetails[$i]['id']; ?>);">Edit Banner</a><br>
                               <a href="#" onclick="openViewGalleryDialog(<?php echo $getPropertyDetails[$i]['category_id']; ?>,<?php echo $getPropertyDetails[$i]['id']; ?>);">Edit Gallery</a><br>
                               <a href="#" onclick="openViewFeatureSpecificationDialog(<?php echo $getPropertyDetails[$i]['category_id']; ?>,<?php echo $getPropertyDetails[$i]['id']; ?>);">Edit Specifi</a><br>
                           </td>
                       </tr>
                       <?php } ?>
                   </tbody>
               </table>
           </div>
       </div>
 </div>
</div>
<script>
  function updateViewHospitalityDialog(method,property_id){
       $( ".modal-content" ).load("updateRetailContent.php?property_id="+property_id+'&method='+method);
       $("#myModal").modal();
   }
   function openViewOffering(){
        $("#load-content").load("manageOffering.php");
    }



   /* To show preview of uploaded Image */
   function hospitalityPreview(id,pre_id,nextSlider,uploadDir) {
       //alert('niks');
       var files =  $(id)[0].files;
       var img = new Image();

       if (!files.length || !window.FileReader) return;
       if (/^image/.test( files[0].type)){
           var reader = new FileReader(); // instance of the FileReader
           reader.readAsDataURL(files[0]); // read the local file
           var fileType = files[0].type;
           var fileName = files[0].name;
           $(pre_id).show();
           $('#slider-input-'+nextSlider).show();

           reader.onloadend = function(){
               img.onload = function() {
                   if(id =="#feature-image"){
                       if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                           $(id).val('');
                           $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                           $('#my_image').show();
                           $(pre_id).hide();
                       }else{
                           $('#my_image').hide();
                           $(".modal-err").text('');
                       }
                   } else {
                       if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                           $(id).val('');
                           $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                           $('#my_icon').show();
                           $(pre_id).hide();
                       }else{
                           $(".modal-err").text('');
                           $('#my_icon').hide();
                       }
                   }
               }
               $(pre_id).css("background-image", "url("+this.result+")");
           }
       }
   }

   /* TO add and update Clients details */
   function addUpdateHospitalityDetails() {
       $('.btn-success').prop('disable',true);
       var p_id                 = $('#p_id').val();//alert(p_id);
       var p_id1                 = $('#p_id1').val();
       var name                 = $('#name').val();
       var shortDesc            = $('#short_desc').val();
       var longDesc             = $('#long_desc').val();
       var website_url             = $('#website_url').val();

       if (name == '' || shortDesc == '' || longDesc == ''|| website_url == '') {
           $('.btn-success').prop('disable',false);
           $('.modal-err').text('Please Enter Mandetory Fields !!!');
       }
       else {
           var form = $('#LuxuryResidencesForm')[0];
           var formData = new FormData(form);
           $.ajax({
               type:'post',
               dataType:'json',
               url:'../controller/retailController.php',
               data:formData,
               contentType:false,
               processData:false,
               async:false,
               success: function(data) {
                 //  alert(data);
                 //  console.log(data);
                   if (data.errCode == -1) {
                       $('#myModal .close').click();
                       $('.alert-success').show();
                       $('#success-msg').text('Successfully Updated LuxuryResidences Details !!!');
                       if(p_id && p_id1)
                       {
                           $("#load-content").load("manageRetail.php?id="+p_id1);
                       }
                       else {
                             $("#load-content").load("manageRetail.php?id="+p_id);
                       }
                       setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                   } else {
                       $('.btn-success').prop('disable',false);
                       $('.modal-err').text(data.errMsg);
                   }
               },
               error: function(data) {
                   $(".alert-danger").show();
                   $("#error-msg").text('Error in updating LuxuryResidences Details !!!');
                   setTimeout(function() { $(".alert-danger").hide(); }, 10000);
               }
           });
       }
   }

   /* To remove about Details */
   function removeLuxuryResidences(id,cat_id) {
       $.ajax({
           type:"POST",
           dataType:"json",
           url:"../controller/LuxuryResidencesController.php",
           data:{
               id:id, method:"delete"
           },
           success: function(data) {
               console.log(data);
               if (data.errCode == -1) {
                   $(".alert-success").show();
                   $("#success-msg").text("Successfully Deleted LuxuryResidences Details !!!");
                   setTimeout( function() { $(".alert-success").hide(); }, 5000);
               } else {
                   $(".alert-danger").show();
                   $("#error-msg").text("Failed to Delete LuxuryResidences Details !!!");
                   setTimeout( function() { $(".alert-danger").hide(); }, 10000);
               }
               $("#load-content").load("manageLuxuryResidencesContent.php?id="+cat_id);
           },
           error: function(data) {
               $(".alert-danger").show();
               $("#error-msg").text('Error Deleting Press Details !!!');
               setTimeout(function() { $(".alert-danger").hide(); }, 10000);

           }
       });
   }

   /* To display remodal on click of remove header data */
   function confirmRemoveData(id,cat_id) {
       $(".modal-content").load("confirmChangesRemodalById.php?id="+id+'&award_id='+cat_id);
       $("#myModal").modal();
   }

   /*It is Universal Function to Call function which delete/remove Data */
   function remodalDataDeleteCall(id,cat_id) {
       removeLuxuryResidences(id,cat_id);
       $('#myModal .close').click();
   }

   function openViewBannerDialog(id,p_id){
   $("#load-content").load("manageBanner.php?id="+id+"&p_id="+p_id);
   }

   function openViewGalleryDialog(id,p_id){
   $("#load-content").load("manageGallery.php?id="+id+"&p_id="+p_id);
   }

   function openViewFeatureSpecificationDialog(id,p_id){
   $("#load-content").load("manageFeatureSpecification.php?id="+id+"&p_id="+p_id);
   }







</script>
