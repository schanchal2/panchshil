<?php

//start session
session_start();

require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
//require_once '../model/matricesContentModel.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/businessSubCatagoryModel.php';

$returnArr = array();

$category_name=$_GET['category_name'];
//echo $category_name;
$businessId=$_GET['business_id'];
//echo $businessId; echo "<br>";
$businessCatId=$_GET['businessCatId'];
//echo $businessCatId;

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active redirect to the login page

    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    //$featureType = "home";
    $getBusinessSubCatDetails = getBusinessSubCatDetails($businessId,$businessCatId,$require,$conn);
      //printArr($getBusinessSubCatDetails);
    if(noError($getBusinessSubCatDetails)){

            $getBusinessSubCatDetails = $getBusinessSubCatDetails["response"];
    }else{
            $returnArr = $getBusinessSubCatDetails;
    }
}else{
            $returnArr = $conn;
            exit;
}
?>

<div class="col-lg-12 main-container-1">
    <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
        <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;"> Manage <?php echo ucfirst(strtolower($category_name)); ?> Details</p>
        </div>
        <div class="modal-err" style="color: red"></div>
        <div class="col-lg-12 redirect">

            <form id="businessSubCatForm" enctype = "multipart/form-data">

            <input type="hidden" name="id" id="id" value="<?php echo $getBusinessSubCatDetails[0]['id']; ?>">
            
                <input type="hidden" name="method" value="update">

                <input type="hidden" name="businessSubCatName" id="businessSubCatName" value="<?php echo $category_name; ?>">

                 <input type="hidden" name="businessId" id="businessId" value="<?php echo $businessId; ?>">
                  <input type="hidden" name="businessCatId" id="businessCatId" value="<?php echo $businessCatId ?>">

    

              <div class="form-group">
                  <label for="top-desc">Name:</label>
                  <input type="text" class="form-control" rows="3" id="sub_cat_name" name="sub_cat_name" maxlength="100" value = "<?php echo $getBusinessSubCatDetails[0]["sub_cat_name"]; ?>">
              </div>

              <div class="form-group">
                  <label for="know-more">Number :</label>
                  <input type="number" class="form-control" id="sub_cat_number" name="sub_cat_number" value="<?php echo $getBusinessSubCatDetails[0]["sub_cat_number"]; ?>" maxlength="100">
              </div>
             
              <div class="form-group">
                  <label for="know-more">Email id :</label>
                  <input type="email" class="form-control" id="sub_cat_email" name="sub_cat_email" value="<?php echo $getBusinessSubCatDetails[0]["sub_cat_email"]; ?>" maxlength="100">
              </div>

               <div class="form-group">
                  <label for="top-desc-title">Address:</label>
                  <textarea type="text" class="form-control" id="sub_cat_address" rows="5" name="sub_cat_address" maxlength="10000"><?php echo $getBusinessSubCatDetails[0]["sub_cat_address"]; ?></textarea>
              </div>

              </form>
              <div class="col-lg-6"></div>
              <div class="col-lg-3 blue-button-1" onclick="addUpdateBusinessSubCatDetails()">Submit</div>
              <div class="col-lg-3 blue-button-1" id="clear" onclick ="confirmRemoveData(<?php echo $getBusinessSubCatDetails[0]['id']; ?>);">Clear</div>

        </div>
    </div>
</div>
<script>


    /* TO Display Remodal on click of Add or Update about details */

    

    /* TO add and update Clients details */
    function addUpdateBusinessSubCatDetails() {
        $('.btn-success').prop('disable',true);

        // var businessId              = $('#business_id').val(); alert(businessId);
        // var businessCatId           = $('#businessCatId').val(); alert(businessCatId);

        var businessSubCatName      = $('#sub_cat_name').val(); //alert(businessSubCatName);
        var businessSubCatNumber    = $('#sub_cat_number').val(); //alert(businessSubCatNumber);
        var businessSubCatEmail     = $('#sub_cat_email').val(); //alert(businessSubCatEmail);
        var businessSubCatAddress   = $('#sub_cat_address').val(); //alert(businessSubCatAddress);

        
        if (businessSubCatName == '' || businessSubCatNumber == '' || businessSubCatEmail == '' || businessSubCatAddress == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#businessSubCatForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/businessSubCatagoryController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Business Details !!!');
                        $("#load-content").load("manageBusinessSubCat.php?&subCategoryId="+businessCatId+"&businessId="+businessId);
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Business SUb-Category Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }                
    }

    /* To remove Client Details */

    function removeBusinessSubCat(id){
       var id = id;
        var businessCatId           = $('#businessCatId').val(); //alert(businessCatId);
    
            $.ajax({
                type: "POST",
                url: '../controller/businessSubCatagoryController.php',
                data: {
                    id:id,
                    method: "delete"
                },
                dataType: 'json',
                success: function (data) {
                  //  console.log(data);

                    if (data.errCode == -1) {
                        $(".alert-success").show();
                        $("#success-msg").text('Successfully deleted SustainibilitySubCategory details!!!');
                        setTimeout(function() { $(".alert-success").hide(); }, 5000);
                    }else{
                        $(".alert-danger").show();
                        $("#error-msg").text("Failed to Delete SustainibilitySubCategory Content");
                        setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                    }
                     $("#load-content").load("manageBusinessSubCat.php?businessCatId="+businessCatId);
                },
                error: function (data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in SustainibilitySubCategory details!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });

    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
       // alert(id);
        $(".modal-content").load("confirmChangesRemodal.php?id="+id);
        $("#myModal").modal(); 
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeBusinessSubCat(id);
        $('#myModal .close').click();
    }

</script>