<?php

session_start();

require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php'; 
//require_once '../model/professionModel.php'; 

$returnArr = array();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active redirect to the login page

    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}


$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];
    //$featureType = "home";
    // $professionContent = getProfessionContent(NULL,$conn);

    // if(noError($professionContent)){

    //         $professionContent = $professionContent["response"];
    // }else{
    //         $returnArr = $professionContent;
    // }
}else{
            $returnArr = $conn;
            exit;
}
?>


<div class="col-lg-12 main-container-1">

    <div class="col-lg-12 sub-container">

        <div class="col-lg-12 status">

            <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage Profession</p>

        </div>

        <div class="col-lg-12 redirect">
          <form id="profession-content" enctype="multipart/form-data"> 
            <div class="row">
                <div class="col-md-4">                   

                 </div>

                 <div class="col-lg-3 blue-button-1" onclick="AddUpdateProfession();">Add Profession</div>

                <table id="myTable" class="table table-striped table-bordered" cellspacing="0" width="100%">

               
                   <thead>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;">
                        <th style="width:20%">Sr.</th>
                        <th style="width:50%">Profession</th>

                        <th style="width:20%;">Action</th>
                    </tr>

                    </thead>
                    <tbody>

                    </tbody>
                

                </table>

            </div>
        </form>

        </div>

    </div>
</div>



<script>

  /* TO Display Remodal on click of Add or Update Matrics details */

   function AddUpdateProfession(id){

        $(".modal-content").load("updateProfession.php?id="+id);

        $("#myModal").modal();

    }
    function addUpdateProfessionDetails(id){
        $('.btn-success').prop('disable',true);
        var title     =   $('#profession-name').val();

        if(title == ''){
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        }else{
            var form = $('#profession-content')[0];
            var formData = new FormData(form);
   
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/professionalController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Profession Details !!!');
                        $("#load-content").load("manageProfession.php");
                        setTimeout(function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating  content!!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }    
    }
    $('#myTable').DataTable({

    "bInfo": true,
    "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
    "ajax": "../controller/professionalList.php",
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": false,
    "info": true,
    "autoWidth": true,
    "fnRowCallback" : function(nRow, aData, iDisplayIndex){
        $("td:first", nRow).html(iDisplayIndex +1);
        return nRow;
    },
        "columnDefs": [{
     "targets": -1,
     "data": null,
     "defaultContent": '<center style="float:left;"> <a href="#" class="" id="editi" data-toggle="modal" data-target="#"><span class="btn btn-xs btn-primary fa fa-edit"></span></a>&nbsp;<a href="#" id="deletei" class=""><span class="btn btn-xs btn-danger fa fa-close"></span> </a></center>'
     }]
});


$('#myTable tbody').on('click', '#deletei', function () {
    var table = $('#myTable').DataTable();
    var data = table.row($(this).parents('tr')).data();
     deleteChannel(data[0]);
});


$('#myTable tbody').on('click', '#editi', function () {
    var table = $('#myTable').DataTable();
    var data = table.row($(this).parents('tr')).data();
    editChannel(data[0]);
});

function deleteChannel(id)
{
       
}

function editChannel(id)
{
        $(".modal-content").load("updateProfession.php?id="+id);

        $("#myModal").modal();
}
</script>