<?php
//prepare for request
//start session
session_start();

//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/corporateDetailsModel.php';


$returnArr = array();
$CorporateUrlId = cleanXSS(urldecode($_GET['id']));
$pageName       = cleanXSS(urldecode($_GET['pageName']));

//checking if session is not active
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
// printArr($conn);
if(noError($conn)){
    $conn = $conn["conn"];
    $getCorporateDetails = getCorporateDetails(NULL,$CorporateUrlId,NULL,$conn);
    if(noError($getCorporateDetails)){
        $getCorporateDetails = $getCorporateDetails["response"];

    }else{
        $returnArr = $getCorporateDetails;
    }
    
    $corporateName = getAboutCorporateDetails($CorporateUrlId,NULL,$conn);
    if(noError($corporateName)){
        $corporateName = $corporateName["response"];
    }else{
        $returnArr = $corporateName;
    }

}else{
    $returnArr = $conn;
    exit;
}
// printArr($corporateName);
?>

<div class="col-lg-12 main-container-1">
   <div class="col-lg-12 sub-container">
        <div class="col-lg-12 status">
         <p style="color: #3da2da; font-size:x-large; font-weight:bold; text-align:center;">Manage <?php echo ucfirst(strtolower($pageName)); ?> Details</p>
        </div>

        <div class="col-lg-12 redirect">

            <div class="row">
                <input type="hidden" name="corporate-Url-Id" id="corporate-Url-Id" value="<?php echo $CorporateUrlId; ?>">
                <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <div class="col-lg-6"></div>
                    <div class="col-lg-3 blue-button-1" onclick="openViewCorporateDialog('-1','<?php echo $CorporateUrlId;?>');">Add Corporate Details</div>
                    <tr style="color:white; background-color:#3da2da;font-size: 16px;">
                      <th width="25%">Image</th>
                      <th width="25%">Name</th>
                      <th width="25%">Status</th>
                      <th width="25%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (empty($getCorporateDetails)) {
                                echo "<tr><td colspan = '5' style = 'text-align:center;'>No Data Found</td></tr>";
                            }
                            for ($i = 0; $i < count($getCorporateDetails); $i++) {
                        ?>
                        <tr>
                            <td>
                                <div id="CorporateImages" class="col-lg-3 featureImages" style="background-image: url('<?php echo $rootUrl.'uploads/'.$getCorporateDetails[$i]["image"]; ?>');"></div>
                            </td>
                            <td>

                                 <p class = "headerDisplay" id="Name" name="Name" value = ""><?php echo $getCorporateDetails[$i]['name']; ?></p>

                            </td>

                             <td>

                                 <span style="font-size: 16px;font-weight: bold; <?php if($getCorporateDetails[$i]["status"] == "Active"){ echo "color: green;";}else{echo "color: red;";} ?> "><?php echo strtoupper($getCorporateDetails[$i]["status"]); ?></span>

                            </td>
                            <td>
                                <a href="#" onclick="openViewCorporateDialog('<?php echo $getCorporateDetails[$i]['id']; ?>','<?php echo $getCorporateDetails[$i]['corporateUrlId']; ?>');">Edit</a><br>

                                <a href="#" onclick="confirmRemoveData(<?php echo $getCorporateDetails[$i]['id']; ?>);">
                                    <span style="font-size: 16px;font-weight: bold;  ">
                                        <?php if($getCorporateDetails[$i]["status"] == "Active"){ echo "Inactivate/Delete";}else{echo "Activate";} ?>
                                    </span>
                                </a><br>

                                <a href="#" onclick="openViewpageDialog('<?php echo $getCorporateDetails[$i]['pagename'];?>','<?php echo $getCorporateDetails[$i]['id'];?>','<?php echo $getCorporateDetails[$i]['name'];?>');">Manage</a><br>

                            </td>
                        </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function openViewCorporateDialog(id,urlId){
        $( ".modal-content" ).load("updateCorporate.php?id="+id+"&corporateUrlId="+urlId);
        $("#myModal").modal();

    }

    var pageName            = '<?php echo $pageName; ?>';

    /* To show preview of uploaded Image */
    function corporatePreview(id,pre_id,nextSlider,uploadDir) {
        //alert('niks');
        var files =  $(id)[0].files;
        var img = new Image();

        /* Checking Is any File selected */
        if (!files.length || !window.FileReader) return;

        /* Check Uploadede file is Image Only */
        if (/^image/.test( files[0].type)){
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var fileType = files[0].type;
            var fileName = files[0].name;
            $(pre_id).show();
            $('#slider-input-'+nextSlider).show();
            /* set image data as background of div */
            reader.onloadend = function(){
                img.onload = function() {
                    if(id =="#feature-image"){
                        if((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                            $('#my_image').show();
                            $(pre_id).hide();
                        }else{
                            $('#my_image').hide();
                            $(".modal-err").text('');
                        }
                    } else {
                        if((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)){
                            $(id).val('');
                            $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                            $('#my_icon').show();
                            $(pre_id).hide();
                        }else{
                            $(".modal-err").text('');
                            $('#my_icon').hide();
                        }
                    }
                }
                $(pre_id).css("background-image", "url("+this.result+")");
            }
        }
    }

    /* TO add and update Clients details */
    function addUpdateCorporateDetails() {
        $('.btn-success').prop('disable',true);
        
        var corporateUrlId     = $('#corporate-Url-Id').val();
        var name                 = $('#corporate-name').val();
        var shortDesc            = $('#corporate-shortDesc').val();
        var longDesc             = $('#corporate-longDesc').val();
        if (name == '' || shortDesc == '' || longDesc == '') {
            $('.btn-success').prop('disable',false);
            $('.modal-err').text('Please Enter Mandetory Fields !!!');
        } else {
            var form = $('#corporateFrom')[0];
            var formData = new FormData(form);
            $.ajax({
                type:'post',
                dataType:'json',
                url:'../controller/corporateDetailsController.php',
                data:formData,
                contentType:false,
                processData:false,
                async:false,
                success: function(data) {
                    //alert(data);
                    console.log(data);
                    if (data.errCode == -1) {
                        $('#myModal .close').click();
                        $('.alert-success').show();
                        $('#success-msg').text('Successfully Updated Corporate Details !!!');
                        $("#load-content").load("manageCorporateContent.php?id="+corporateUrlId+"&pageName="+pageName);
                        setTimeout( function() { $('.alert-success').hide(); }, 5000 );
                    } else {
                        $('.btn-success').prop('disable',false);
                        $('.modal-err').text(data.errMsg);
                    }
                },
                error: function(data) {
                    $(".alert-danger").show();
                    $("#error-msg").text('Error in updating Corporate Details !!!');
                    setTimeout(function() { $(".alert-danger").hide(); }, 10000);
                }
            });
        }
    }

    /* To remove Client Details */
    function removeCorporate(id) {
      //alert(1);

      var corporateUrlId     = $('#corporate-Url-Id').val();
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"../controller/corporateDetailsController.php",
            data:{
                id:id, deleteCorporateUrlId:corporateUrlId, method:"delete"
            },
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    $(".alert-success").show();
                    $("#success-msg").text("Successfully Deleted Corporate Details !!!");
                    setTimeout( function() { $(".alert-success").hide(); }, 5000);
                } else {
                    $(".alert-danger").show();
                    $("#error-msg").text("Failed to Delete Corporate Details !!!");
                    setTimeout( function() { $(".alert-danger").hide(); }, 10000);
                }
                $("#load-content").load("manageCorporateContent.php?id="+corporateUrlId+"&pageName="+pageName);
            },
            error: function(data) {
                $(".alert-danger").show();
                $("#error-msg").text('Error Deleting Corporate Details !!!');
                setTimeout(function() { $(".alert-danger").hide(); }, 10000);
            }
        });
    }

    /* To display remodal on click of remove header data */
    function confirmRemoveData(id) {
        $(".modal-content").load("confirmChangesRemodal.php?id="+id);
        $("#myModal").modal();
    }

    /*It is Universal Function to Call function which delete/remove Data */
    function remodalDataDeleteCall(id) {
        removeCorporate(id);
        $('#myModal .close').click();
    }

    function openViewpageDialog(pagename,id,name){
  //alert(pagename);
  //alert(id);
        $("#load-content").load(pagename+'?&id='+id+"&pageName="+encodeURIComponent(name));
    }

</script>
