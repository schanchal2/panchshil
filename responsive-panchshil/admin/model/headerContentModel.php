<?php

/**************************************************************************************************
    @ function name: updateHeaderContent();
    @ parameters: $pageId(integer[Auto]),$pageName(String),$featureImage(String),$pageDescTitle(String),$pageDesc(String),$pageKnowMoreUrl(string),$conn(string)
    @ purpose: to Add or update Header Content header content of page
 *************************************************************************************************/
function updateHeaderContent($pageId,$pageName,$featureImage,$pageDescTitle,$pageDesc,$pageKnowMoreUrl,$conn){
    $returnArr = array();
    if (!empty($featureImage) || $featureImage != "") {

        /* checking to Add new data or Update Existin Data */
        if (!isset($pageId) || empty($pageId)) {

            /* Checking no of Data present for each page must be lesser than 5 to insert new Data */
            $query1 = "SELECT * FROM header_contents where page_name='".cleanQueryParameter($pageName,$conn)."'";
            
            $query1Result = runQuery($query1,$conn);
            if (noError($query1Result)) {
                $res = array();
                while ($row = mysqli_fetch_assoc($query1Result["dbResource"])) {
                    $res["response"][] = $row;
                }
                    $headerRow = $res;                
                if (count($headerRow['response']) < 5) {                
                    $query = "INSERT INTO header_contents ( page_name, image,title,description,know_more_link ) VALUE ('".cleanQueryParameter($pageName,$conn)."','".cleanQueryParameter($featureImage,$conn)."','".cleanQueryParameter($pageDescTitle,$conn)."','".cleanQueryParameter($pageDesc,$conn)."','".cleanQueryParameter($pageKnowMoreUrl,$conn)."')";

                } else {
                    $returnArr = setErrorStack($query1Result, 42);
                }
            } else {
                $returnArr = setErrorStack($query1Result, 12);
            }                
        } else { 
            $query = "UPDATE header_contents SET page_name='".cleanQueryParameter($pageName,$conn)."', image='".cleanQueryParameter($featureImage,$conn)."',title='".cleanQueryParameter($pageDescTitle,$conn)."',description='".cleanQueryParameter($pageDesc,$conn)."',know_more_link='".cleanQueryParameter($pageKnowMoreUrl,$conn)."' WHERE id='".cleanQueryParameter($pageId,$conn)."'";       
        }
        $queryResult = runQuery($query,$conn);
        if(noError($queryResult)){
            $errMsg = "Successfully updated header content";
            $returnArr = setErrorStack($queryResult, -1, $errMsg);
        }else{
            $returnArr = setErrorStack($queryResult, 12);
        }
    } else {
        $returnArr = setErrorStack($queryResult, 21);
    }
    return $returnArr;
}

/***************************************************************************************************
    @ function name: getHeaderContent();
    @ parameters: $featureType(string),$conn(string)
    @ purpose: to fetch header content of page
 **************************************************************************************************/
function getHeaderContent($featureType,$conn){

    $query = "SELECT * FROM header_contents WHERE page_name ='".cleanQueryParameter($featureType,$conn)."'";

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched header content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,13);
    }
    return $returnArr;
}

/***************************************************************************************************
    @ function name: getHomeHeaderContent();
    @ parameters: $pageId(Integer),$conn(string)
    @ purpose: to fetch header content of Home Page Only
 **************************************************************************************************/
function getHomeHeaderContent($pageId,$conn){

    $query = "SELECT * FROM header_contents WHERE id ='".cleanQueryParameter($pageId,$conn)."'";

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched header content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,13);
    }
    return $returnArr;
}

/**************************************************************************************************
    @ Function Name : removeHeader()
    @ Parameters : $pageId(Integer),$conn(string)
    @ Purpose : Delete header content of page from database and upload folder 
***************************************************************************************************/
function removeHeader($pageId,$conn) {
    $returnArr = array();
    $query = "SELECT * FROM header_contents WHERE id = '".cleanQueryParameter($pageId,$conn)."'";
    $queryResult = runQuery($query, $conn);
    $res = array();
    while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
        $res["response"][] = $row;
    }
    $query1 = "DELETE FROM header_contents WHERE id = '".cleanQueryParameter($pageId,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
        unlink("../uploads/".$res["response"][0]["image"]);
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Header Details");
    } else {
        $returnArr = setErrorStack($queryResult1,34);
    }
    return $returnArr;
}

?>