<?php

/*Created By Kavita*/
/**************************************************************************************************
    @ function name: updateAwardDetails();
    @ purpose: to Add or update Award Details to Database
 *************************************************************************************************/

function updateAwardDetails($id,$media_id,$imageUrl,$imageUrl1,$imageUrl2,$name,$awardTitle,$longDesc,$conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }
    if (!isset($id) || empty($id)) {


        $query = "  INSERT INTO `awardDetails` (`id`, `media_id`,  `left_img`, `right_img`, `logo_img`,`name`, `title`, `address`, `created`) VALUES (NULL, '".cleanQueryParameter($media_id,$conn)."', '".cleanQueryParameter($imageUrl,$conn)."', '".cleanQueryParameter($imageUrl1,$conn)."','".cleanQueryParameter($imageUrl2,$conn)."', '".cleanQueryParameter($name,$conn)."', '".cleanQueryParameter($awardTitle,$conn)."', '".cleanQueryParameter($longDesc,$conn)."',  CURRENT_TIMESTAMP);";


  }
  else {
    # code...
    $query="UPDATE `awardDetails` SET `name` = '".cleanQueryParameter($name,$conn)."', `left_img` ='".cleanQueryParameter($imageUrl,$conn)."', `right_img` = '".cleanQueryParameter($imageUrl1,$conn)."', `logo_img` = '".cleanQueryParameter($imageUrl2,$conn)."', `title` = '".cleanQueryParameter($awardTitle,$conn)."', `address` = '".cleanQueryParameter($longDesc,$conn)."' WHERE `awardDetails`.`id` = '".cleanQueryParameter($id,$conn)."';";
  }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $errMsg = "Successfully updated mediaDetails content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
      $returnArr = setErrorStack($queryResult, 35);
    }

    return $returnArr;
}

/***************************************************************************************************
    @ function name: getAwardDetails();

    @ purpose: to fetch Award Details
 **************************************************************************************************/

function getAwardDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM awardDetails";
        } else {
            $query = "SELECT * FROM awardDetails ";
        }
    } else {
        if (!isset($require) || empty($require)) {
              $query = "SELECT * FROM awardDetails WHERE 	media_id ='".cleanQueryParameter($id,$conn)."'";
        } else {
               $query = "SELECT * FROM awardDetails WHERE 	media_id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Award content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

/***************************************************************************************************
    @ function name: getAwardDetailsById();

    @ purpose: to fetch Award Details By ID
 **************************************************************************************************/

function getAwardDetailsById($id,$conn){
  $query = "SELECT * FROM awardDetails WHERE media_id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Award content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

?>
