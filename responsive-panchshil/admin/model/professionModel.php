<?php
/***************************************************************************************************
    @ function name: getMatricsContent();
    @ parameters: $matricesId(Integer),$conn(string)
    @ purpose: to fetch matrics content.
 **************************************************************************************************/
function getProfessionContent($id,$conn){
    if (!isset($id) || empty($id)) {
      $query = "SELECT * FROM profession order by id";
    } else {
        $query = "SELECT * FROM profession WHERE id ='".cleanQueryParameter($id,$conn)."'";
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched header content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,45);
    }
    return $returnArr;
}

function addProfessionDetails($id, $title, $conn){
        $returnArr = array();
        if (!isset($id) || empty($id)) { 
            $query = "INSERT INTO profession (title) VALUE ('".cleanQueryParameter($title,$conn)."')";
        }else{
            $query = "UPDATE profession SET title ='".cleanQueryParameter($title,$conn)."'";
        }  
            $queryResult = runQuery($query,$conn);
            if(noError($queryResult)){
                $errMsg = "SuccessFully Added profession Details";
                $returnArr = setErrorStack($queryResult, -1, $errMsg);
            }else{
                $returnArr = setErrorStack($queryResult,24);
        }
    return $returnArr;
}
?>