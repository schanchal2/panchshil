<?php

/***************************************************************************************************
    @ function name: sendMail();
    @ parameters: $contactArray(Array),$conn(string)
    @ purpose: to Send Email on contact us Form submit.
 **************************************************************************************************/
function sendMail($enquiryArray, $conn) {

    /* Global Email Addreess */
    global $mailId;
    $returnArr = array();

    /* Creating email Body for sending */
    $body       = "Hello Admin of Panchshil, You have new Enquiry Form<br/><br/><br/>";
    $body      .= "<table border = '1'>";
    $body      .= "<tbody>";
    $body      .= "<tr><th style = 'padding:10px;'>property Name </th><td style = 'padding:10px;'>".$enquiryArray['propertyName']."</td></tr>";
    $body      .= "<tr><th style = 'padding:10px;'>Name </th><td style = 'padding:10px;'>".$enquiryArray['name']."</td></tr>";    
    $body      .= "<tr><th style = 'padding:10px;'>Email </th><td style = 'padding:10px;'>".$enquiryArray['email']."</td></tr>";
    $body      .= "<tr><th style = 'padding:10px;'>Mobile Number</th><td style = 'padding:10px;'>".$enquiryArray['mobileNumber']."</td></tr>";
    $body      .= "<tr><th style = 'padding:10px;'>Contact Date</th><td style = 'padding:10px;'>".$enquiryArray['contactDate']."</td></tr>";
    $body      .= "<tr><th style = 'padding:10px;'>Contact Time</th><td style = 'padding:10px;'>".$enquiryArray['contactTime']."</td></tr>";
    $body      .= "</tbody>";
    $body      .= "</table>"; 

    /* Email Credentials */
    $from       = $enquiryArray['email'];
    $SenderName = " ".$enquiryArray['name']." ";
    $subject    = "New Enquiry Request";

    /* Creating Header to send Email */
    $headers    = '';
    $headers    = 'MIME-Version: 1.0' . "\r\n";
    $headers   .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers   .= 'From: '.$SenderName.' <'.$from.'>' . "\r\n";

    /* Calling Mail Function to send Email */
    $mailVerify = @mail($mailId, $subject, $body, $headers);

    /* Verifying email is Sent or not */
    if ($mailVerify){
        $returnArr = setErrorStack($queryResult,-1,"Successfully Sent Mail");
    } else {
        $returnArr = setErrorStack($queryResult,1,"Failed Sent Mail");
    }
    return $returnArr;
}


/***************************************************************************************************
    @ function name: getEnquiryDetails();
    @ parameters: $Id(Integer),$conn(string)
    @ purpose: to fetch Enquiry form Details.
 **************************************************************************************************/
function getEnquiryDetails($Id,$conn){
     $returnArr = array();
	if ( !isset($Id) || empty($Id)){
		$query = "SELECT * FROM enquiry_form";
	} else {
		$query = "SELECT * FROM enquiry_form WHERE id ='".cleanQueryParameter($Id,$conn)."'";
	}   
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched enquiry content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,13);
    }
    return $returnArr;
}

/**************************************************************************************************
    @ Function Name : updateEnquiryDetails()
    @ Parameters : $Id(Integer),$conn(string)
    @ Purpose : Update enquiry content form Details from database
***************************************************************************************************/
function updateEnquiryDetails($id,$enquiryArray,$conn){
    $returnArr = array();
   $query = "UPDATE enquiry_form SET propertyName='".cleanQueryParameter($enquiryArray['propertyName'],$conn)."',name='".cleanQueryParameter($enquiryArray['name'],$conn)."',email='".cleanQueryParameter($enquiryArray['email'],$conn)."',mobileNumber='".cleanQueryParameter($enquiryArray['mobileNumber'],$conn)."',contactDate='".cleanQueryParameter($enquiryArray['contactDate'],$conn)."',contactTime='".cleanQueryParameter($enquiryArray['contactTime'],$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";  
        $queryResult1 = runQuery($query,$conn); 

         if (noError($queryResult1)) {   
           
           $sendEmail = sendMail($enquiryArray, $conn); 
           
           if(noError($sendEmail)) { 
                $returnArr = setErrorStack($queryResult1, -1, "Successfully Updated Enquiry Content Details");
            
            }else{
                  $returnArr = "Error in Send Mail";
            }
       
        } else {
                 $returnArr = setErrorStack($queryResult1,45);
        }
    return $returnArr; 
}

/**************************************************************************************************
    @ Function Name : removeEnquiryDetails()
    @ Parameters : $Id(Integer),$conn(string)
    @ Purpose : Delete enquiry content form Details from database
***************************************************************************************************/
function removeEnquiryDetails($id,$conn) {
    $returnArr = array();    
     $query1 = "DELETE FROM enquiry_form WHERE id = '".cleanQueryParameter($id,$conn)."'"; 
    $queryResult1 = runQuery($query1,$conn);
   if (noError($queryResult1)) {
       
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Enquiry Content Details");
    } else {
        $returnArr = setErrorStack($queryResult1,34);
    }
    return $returnArr;
}




?>