<?php

/*Created By Kavita*/
/**************************************************************************************************
    @ function name: updatePressDetails();
    @ purpose: to Add or update Press Details to Database
 *************************************************************************************************/

function updateMatrixSpecificationDetails($id,$o_id,$p_id,$name,$valuetit,$valueAmt,$galaryImagesArray,$conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }
    if (!isset($id) || empty($id)) {

         $query = "INSERT INTO `matrix_specification_details` (`id`, `o_id`,`p_id`, `image`, `title`,`unitTitle`,`unitValue`, `created`) VALUES (NULL, '".cleanQueryParameter($o_id,$conn)."','".cleanQueryParameter($p_id,$conn)."', '".cleanQueryParameter($galaryImagesArray,$conn)."', '".cleanQueryParameter($name,$conn)."', '".cleanQueryParameter($valuetit,$conn)."', '".cleanQueryParameter($valueAmt,$conn)."', CURRENT_TIMESTAMP);";

    } else {

    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $errMsg = "Successfully updated Press  content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
      $returnArr = setErrorStack($queryResult, 35);
    }

    return $returnArr;
}




/***************************************************************************************************
    @ function name: getMatrixSpecificationDetailsByUniqId();

    @ purpose: to fetch Matrix Specification Details By Id
 **************************************************************************************************/

function getMatrixSpecificationDetailsByUniqId($id,$conn){
  $query = "SELECT * FROM matrix_specification_details WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Matrix Specification content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

/***************************************************************************************************
    @ function name: getGalleryDetailsById();

    @ purpose: to fetch Gallery Details By Id
 **************************************************************************************************/

function getMatrixSpecificationDetailsById($id,$conn){
  $query = "SELECT * FROM matrix_specification_details WHERE p_id ='".cleanQueryParameter($id,$conn)."' ORDER BY `id` DESC ";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Gallery content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}



/***************************************************************************************************
    @ function name: updateMatrixSpecificationInfoById();

    @ purpose: to fetch Matrix Specification Details By Id
 **************************************************************************************************/

function updateMatrixSpecificationInfoById($id,$imageUrl,$title,$uTitle,$uValue,$conn)
{
       $query = "UPDATE `matrix_specification_details` SET `image` = '".cleanQueryParameter($imageUrl,$conn)."', `title` = '".cleanQueryParameter($title,$conn)."', `unitTitle` = '".cleanQueryParameter($uTitle,$conn)."', `unitValue` = '".cleanQueryParameter($uValue,$conn)."' WHERE `id` = '".cleanQueryParameter($id,$conn)."';";

      $queryResult = runQuery($query,$conn);
      if(noError($queryResult)){
      $errMsg = "Successfully updated Feature Specification Content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
      }else{
      $returnArr = setErrorStack($queryResult, 35);
      }
      return $returnArr;
}



/**************************************************************************************************
    @ Function Name : removeMatrixSpecification()
    @ Purpose : Deleteing Matrix Specification from database and Image from folder
***************************************************************************************************/



  function removeMatrixSpecification($Id,$conn) {
            $returnArr = array();
            $query = "SELECT * FROM matrix_specification_details WHERE id ='".cleanQueryParameter($Id,$conn)."'";
            $queryResult = runQuery($query, $conn);
            $res = array();
            while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
            $res["response"][] = $row;
            }
            $query1 = "DELETE FROM matrix_specification_details WHERE id = '".cleanQueryParameter($Id,$conn)."'";
            $queryResult1 = runQuery($query1,$conn);
            if (noError($queryResult1)) {
            unlink("../uploads/".$res["response"][0]["image"]);
            $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Matrix Specification Details");
            } else {
            $returnArr = setErrorStack($queryResult1,36);
            }
            return $returnArr;
  }
?>
