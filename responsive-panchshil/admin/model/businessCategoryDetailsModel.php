<?php

/**************************************************************************************************
    @ function name: updateClientDetails();
    @ parameters: $clientId(integer[Auto]),$clientName(String),$clientColorImage(String),$clientGreyImage(String),$services(String),$clientUrl(string),$conn(string)
    @ purpose: to Add or update Client Details to Database
 *************************************************************************************************/
function updateBusinessCatDetails($businessCatId, $bussinessId, $businessCatName, $businessCatStatus, $businessCatBigImage, $businessCatFeatureImage, $businessCatFooterImage, $businessCatShortDesc, $businessCatFullDesc, $conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($businessCatStatus != 'Inactive') {
        $businessCatStatus = 'Active';
    }
    if ( !empty($businessCatBigImage) && !empty($businessCatFeatureImage) && !empty($businessCatFooterImage) ) {
        /* checking for Add new data or Update Existin Data */
        if (!isset($businessCatId) || empty($businessCatId)) {
            $query = "INSERT INTO business_category_details (business_id, category_name, category_status, category_big_image, category_feature_image, category_footer_image, category_short_desc, category_full_desc, pagename) VALUE ('".cleanQueryParameter($bussinessId,$conn)."','".cleanQueryParameter($businessCatName,$conn)."','".cleanQueryParameter($businessCatStatus,$conn)."','".cleanQueryParameter($businessCatBigImage,$conn)."','".cleanQueryParameter($businessCatFeatureImage,$conn)."','".cleanQueryParameter($businessCatFooterImage,$conn)."','".cleanQueryParameter($businessCatShortDesc,$conn)."','".cleanQueryParameter($businessCatFullDesc,$conn)."','".cleanQueryParameter('manageBusinessSubCat.php',$conn)."')";
        } else {
            $query = "UPDATE business_category_details SET business_id='".cleanQueryParameter($bussinessId,$conn)."', category_name='".cleanQueryParameter($businessCatName,$conn)."',category_status='".cleanQueryParameter($businessCatStatus,$conn)."',category_big_image='".cleanQueryParameter($businessCatBigImage,$conn)."',category_feature_image='".cleanQueryParameter($businessCatFeatureImage,$conn)."',category_footer_image='".cleanQueryParameter($businessCatFooterImage,$conn)."',category_short_desc='".cleanQueryParameter($businessCatShortDesc,$conn)."',category_full_desc='".cleanQueryParameter($businessCatFullDesc,$conn)."' WHERE id='".cleanQueryParameter($businessCatId,$conn)."'";
        }
        $queryResult = runQuery($query,$conn);
        if(noError($queryResult)){
            $errMsg = "Successfully updated Category Details";
            $returnArr = setErrorStack($queryResult, -1, $errMsg);
        }else{
            $returnArr = setErrorStack($queryResult, 35);
        }
    }
    else {
        $returnArr = setErrorStack($queryResult, 21);
    }
    return $returnArr;
}

/***************************************************************************************************
    @ function name: getClientDetails();
    @ parameters: $clientId(Integer),$conn(string)
    @ purpose: to fetch Client Details
 **************************************************************************************************/
function getBusinessCatDetails($businessCatId,$BusinessId,$require,$conn){

    if (!isset($businessCatId) || empty($businessCatId)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM business_category_details WHERE business_id='".cleanQueryParameter($BusinessId,$conn)."'";
        } else {
            $query = "SELECT * FROM business_category_details WHERE business_id='".cleanQueryParameter($BusinessId,$conn)."' AND category_status='Active'";
        }

    } else {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM business_category_details WHERE id ='".cleanQueryParameter($businessCatId,$conn)."' AND business_id='".cleanQueryParameter($BusinessId,$conn)."'";
        } else {
            $query = "SELECT * FROM business_category_details WHERE id ='".cleanQueryParameter($businessCatId,$conn)."' AND business_id='".cleanQueryParameter($BusinessId,$conn)."' AND category_status='Active'";
        }

    }

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched business category content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

/**************************************************************************************************
    @ Function Name : removeClient()
    @ Parameters : $businessId(Integer),$conn(string)
    @ Purpose : Deleteing CLient Details from database and Image from folder
***************************************************************************************************/
function removeBusinessCat($DeleteBusinessCatId,$conn) {
    $returnArr = array();

    $query = "SELECT * FROM business_category_details WHERE id ='".cleanQueryParameter($DeleteBusinessCatId,$conn)."'";
    $queryResult = runQuery($query, $conn);
    $res = array();
    while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
        $res["response"][] = $row;
    }

    if ($res["response"][0]["category_status"] == "Active") {
        $status = "Inactive";
    } else {
        $status = "Active";
    }

    $query1 = "UPDATE business_category_details SET category_status='".cleanQueryParameter($status,$conn)."' WHERE id='".cleanQueryParameter($DeleteBusinessCatId,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Category Of Business Details");
    } else {
        $returnArr = setErrorStack($queryResult1,36);
    }
    return $returnArr;
}

?>
