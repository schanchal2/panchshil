<?php
/***************************************************************************************************
    @ function name: getPageNameById();

    @ purpose: to fetch Pagename from Any table
 **************************************************************************************************/
function getPagename($tablename,$id,$conn){
    $query = "SELECT * FROM $tablename WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Pagenaml content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

function getPagenameForTestimonial($tablename,$id,$conn){
   $query = "SELECT * FROM $tablename WHERE 	media_id	 ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Pagenaml content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

?>
