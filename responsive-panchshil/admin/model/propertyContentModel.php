<?php

/*Created By Kavita*/
/**************************************************************************************************
    @ function name: updateproperty_details();
    @ purpose: to Add or update Media Details to Database
 *************************************************************************************************/
function updateproperty_details($id,$category_id,$category_type	,$image,$name,$location,$title,$type,$status,$property_type,$area,$short_desc,$long_desc,$website_url,$pdf,$latitude,$longitude,$enquiry_image,$conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }
    //if ( !empty($image)) {

    /* checking for Add new data or Update Existin Data */
    if (!isset($id) || empty($id)) {
   $query = " INSERT INTO `property_details` (`id`, `category_id`, `category_type`, `image`, `name`, `location`, `title`, `type`, `status`, `property_type`, `area`, `short_desc`, `long_desc`, `website_url`, `pdf`, `latitude`, `longitude`, `enquiry_image`, `created`) VALUES (NULL, '".cleanQueryParameter($category_id,$conn)."', '".cleanQueryParameter($category_type,$conn)."', '".cleanQueryParameter($image,$conn)."', '".cleanQueryParameter($name,$conn)."', '".cleanQueryParameter($location,$conn)."', '".cleanQueryParameter($title,$conn)."', '".cleanQueryParameter($type,$conn)."', '".cleanQueryParameter($status,$conn)."', '".cleanQueryParameter($property_type,$conn)."', '".cleanQueryParameter($area,$conn)."', '".cleanQueryParameter($short_desc,$conn)."', '".cleanQueryParameter($long_desc,$conn)."', '".cleanQueryParameter($website_url,$conn)."', '".cleanQueryParameter($pdf,$conn)."', '".cleanQueryParameter($latitude,$conn)."', '".cleanQueryParameter($longitude,$conn)."', '".cleanQueryParameter($enquiry_image,$conn)."', CURRENT_TIMESTAMP);";

    } else {
    $query = "UPDATE `property_details` SET `category_type` = '".cleanQueryParameter($category_type,$conn)."', `image` = '".cleanQueryParameter($image,$conn)."', `name` = '".cleanQueryParameter($name,$conn)."', `location` = '".cleanQueryParameter($location,$conn)."', `title` = '".cleanQueryParameter($title,$conn)."', `type` = '".cleanQueryParameter($type,$conn)."',`status` = '".cleanQueryParameter($status,$conn)."', `property_type` = '".cleanQueryParameter($type,$conn)."', `area` = '".cleanQueryParameter($area,$conn)."', `short_desc` = '".cleanQueryParameter($short_desc,$conn)."', `long_desc` = '".cleanQueryParameter($long_desc,$conn)."', `website_url` = '".cleanQueryParameter($website_url,$conn)."', `pdf` = '".cleanQueryParameter($pdf,$conn)."', `latitude` = '".cleanQueryParameter($latitude,$conn)."', `longitude` = '".cleanQueryParameter($longitude,$conn)."', `enquiry_image` = '".cleanQueryParameter($enquiry_image,$conn)."' WHERE  id='".cleanQueryParameter($id,$conn)."' ";
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $errMsg = "Successfully updated property_details content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
      $returnArr = setErrorStack($queryResult, 35);
    }

    return $returnArr;
}

/***************************************************************************************************
    @ function name: getproperty_details();

    @ purpose: to fetch Media Details
 **************************************************************************************************/

function getPropertyDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM property_details";
        } else {
             $query = "SELECT * FROM property_details WHERE status='Active' ";
        }
    } else {
        if (!isset($require) || empty($require)) {
               $query = "SELECT * FROM property_details WHERE id ='".cleanQueryParameter($id,$conn)."'";
        } else {
               $query = "SELECT * FROM property_details WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Media content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}



/***************************************************************************************************
    @ function name: getproperty_detailsById();

    @ purpose: to fetch Media Details By Id
 **************************************************************************************************/

function getPropertyDetailsById($id,$conn){
    $query = "SELECT * FROM property_details WHERE 	category_id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Media content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}


/***************************************************************************************************
    @ function name: getMediaAwardDetailsById();

    @ purpose: to fetch Media Details By Id
 **************************************************************************************************/

function getMediaAwardDetailsById($id,$conn){
    $query = "SELECT * FROM property_details WHERE id =$id";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Media content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}



/**************************************************************************************************
    @ Function Name : removePress()
    @ Purpose : Deleteing Press Details from database and Image from folder
***************************************************************************************************/



  function removeProperty($Id,$conn) {
      $returnArr = array();
       $query = "SELECT * FROM property_details WHERE id ='".cleanQueryParameter($Id,$conn)."'";
      $queryResult = runQuery($query, $conn);
      $res = array();
      while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
          $res["response"][] = $row;
      }

       $query1 = "DELETE FROM property_details WHERE id = '".cleanQueryParameter($Id,$conn)."'";
      $queryResult1 = runQuery($query1,$conn);
      if (noError($queryResult1)) {
          unlink("../uploads/".$res["response"][0]["image"]);

          $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Property content");
      } else {
          $returnArr = setErrorStack($queryResult1,36);
      }
      return $returnArr;
  }
?>
