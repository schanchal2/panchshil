<?php

/*Created By Kavita*/
/**************************************************************************************************
    @ function name: addGalleryDetails();
    @ purpose: to Add Gallery Details to Database
 *************************************************************************************************/

function addGalleryDetails($id,$o_id,$p_id,$name,$galaryImagesArray,$conn){
    $returnArr = array();
    if (!isset($id) || empty($id)) {
        $query = "INSERT INTO `gallery_details` (`id`, `o_id`,`p_id`, `image`, `description`, `created`) VALUES (NULL, '".cleanQueryParameter($o_id,$conn)."','".cleanQueryParameter($p_id,$conn)."', '".cleanQueryParameter($galaryImagesArray,$conn)."', '".cleanQueryParameter($name,$conn)."', CURRENT_TIMESTAMP);";
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $errMsg = "Successfully updated Gallery Content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
      $returnArr = setErrorStack($queryResult, 35);
    }
    return $returnArr;
}

/***************************************************************************************************
    @ function name: getGalleryDetailsById();

    @ purpose: to fetch Gallery Details By Id
 **************************************************************************************************/

function updateGalleryDetailsInfoById($id,$imageUrl,$description,$conn)
{
      $query = "UPDATE `gallery_details` SET `image` = '".cleanQueryParameter($imageUrl,$conn)."', `description` = '".cleanQueryParameter($description,$conn)."' WHERE `id` = '".cleanQueryParameter($id,$conn)."';";
      $queryResult = runQuery($query,$conn);
      if(noError($queryResult)){
      $errMsg = "Successfully updated Gallery Content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
      }else{
      $returnArr = setErrorStack($queryResult, 35);
      }
      return $returnArr;
}

/***************************************************************************************************
    @ function name: getGalleryDetailsById();

    @ purpose: to fetch Gallery Details By Id
 **************************************************************************************************/

function getGalleryDetailsById($id,$conn){
  $query = "SELECT * FROM gallery_details WHERE p_id ='".cleanQueryParameter($id,$conn)."' ORDER BY `id` DESC ";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Gallery content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

/***************************************************************************************************
    @ function name: getGalleryDetailsByUniqId();

    @ purpose: to fetch Gallery Details By Id
 **************************************************************************************************/

function getGalleryDetailsByUniqId($id,$conn){
  $query = "SELECT * FROM gallery_details WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Gallery content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}


/**************************************************************************************************
    @ Function Name : removeGallery()
    @ Purpose : Deleteing Gallery Details from database and Image from folder
***************************************************************************************************/



  function removeGallery($Id,$conn) {
            $returnArr = array();
            $query = "SELECT * FROM gallery_details WHERE id ='".cleanQueryParameter($Id,$conn)."'";
            $queryResult = runQuery($query, $conn);
            $res = array();
            while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
            $res["response"][] = $row;
            }
            $query1 = "DELETE FROM gallery_details WHERE id = '".cleanQueryParameter($Id,$conn)."'";
            $queryResult1 = runQuery($query1,$conn);
            if (noError($queryResult1)) {
            unlink("../uploads/".$res["response"][0]["image"]);
            $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Gallery Details");
            } else {
            $returnArr = setErrorStack($queryResult1,36);
            }
            return $returnArr;
  }
?>
