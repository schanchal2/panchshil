<?php
/**************************************************************************************************
      @ function name: updateForwordDetails();
      @ purpose: to Add or update about Details to Database
   *************************************************************************************************/
function updateForwordDetails($id,$forword_id,$image,$long_url,$sign_image,$name,$designation,$conn){
      $returnArr = array();
    //  echo $id;

      /* Checking Image is present or not */
      if ($status != 'Inactive') {
          $status = 'Active';
      }
      //if ( !empty($image)) {

          /* checking for Add new data or Update Existin Data */
          if (!isset($id) || empty($id)) {

                $query = "INSERT INTO forword (image,forword_id, long_url, sign_image,name,designation) VALUE ('".cleanQueryParameter($image,$conn)."','".cleanQueryParameter($forword_id,$conn)."','".cleanQueryParameter($long_url,$conn)."','".cleanQueryParameter($sign_image,$conn)."','".cleanQueryParameter($name,$conn)."','".cleanQueryParameter($designation,$conn)."')";
          } else {

                $query = "UPDATE forword SET image='".cleanQueryParameter($image,$conn)."', long_url='".cleanQueryParameter($long_url,$conn)."',sign_image='".cleanQueryParameter($sign_image,$conn)."',name='".cleanQueryParameter($name,$conn)."',designation='".cleanQueryParameter($designation,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
          }
          $queryResult = runQuery($query,$conn);

          if(noError($queryResult)){
              $errMsg = "Successfully updated Forword content";
              $returnArr = setErrorStack($queryResult, -1, $errMsg);
          }else{
              $returnArr = setErrorStack($queryResult, 35);
          }
      //}
      // else {
      //     $returnArr = setErrorStack($queryResult, 21);
      // }
      return $returnArr;
  }

    /***************************************************************************************************
        @ function name: getforwordDetails();

        @ purpose: to fetch AboutDetails
     **************************************************************************************************/
    function getforwordDetails($id,$conn){

        if (!isset($id) || empty($id)) {
            if (!isset($require) || empty($require)) {
               $query = "SELECT * FROM forword";
            } else {
                $query = "SELECT * FROM forword WHERE status='Active'";
            }
        } else {
            if (!isset($require) || empty($require)) {
                 $query = "SELECT * FROM forword WHERE forword_id ='".cleanQueryParameter($id,$conn)."'";
            } else {
                  $query = "SELECT * FROM forword WHERE forword_id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
            }
        }
        $queryResult = runQuery($query,$conn);
        if(noError($queryResult)){
            $res = array();
            while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
                $res["response"][] = $row;
            $returnArr = setErrorStack($queryResult,-1,"Successfully fetched aboutDetails content",$res);
        }else{
            $returnArr = setErrorStack($queryResult,28);
        }
        return $returnArr;
    }
    /**************************************************************************************************
        @ Function Name : removeClient()
        @ Purpose : Deleteing about Details from database and Image from folder
    ***************************************************************************************************/
    function removeforword($id,$conn) {
      $returnArr = array();
      //$status = "";
     $query = "SELECT * FROM forword WHERE id ='".cleanQueryParameter($id,$conn)."'";
      $queryResult = runQuery($query, $conn);
      $res = array();
      while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
          $res["response"][] = $row;
      }
      $query1 = "DELETE from forword WHERE id ='".cleanQueryParameter($id,$conn)."'";
      $queryResult1 = runQuery($query1,$conn);
      if (noError($queryResult1)) {
        unlink("../uploads/".$res["response"][0]["image"]);
        unlink("../uploads/".$res["response"][0]["sign_image"]);
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted forword Details");
      } else {
          $returnArr = setErrorStack($queryResult1,36);
      }
      return $returnArr;
    }
?>