<?php

/*Created By Kavita*/
/**************************************************************************************************
    @ function name: updateAmenitiesDetails();
    @ purpose: to Add or update Amenities Details to Database
 *************************************************************************************************/

function updateAmenitiesDetails($id,$o_id,$p_id,$imageUrl,$imageUrl1,$type,$title,$conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }
    if (!isset($id) || empty($id)) {
        $query = "INSERT INTO `amenities_details` (`id`, `o_id`,`p_id`, `image`, `logo_img`, `type`, `title`, `created`) VALUES (NULL,'".cleanQueryParameter($o_id,$conn)."', '".cleanQueryParameter($p_id,$conn)."','".cleanQueryParameter($imageUrl,$conn)."', '".cleanQueryParameter($imageUrl1,$conn)."', '".cleanQueryParameter($type,$conn)."', '".cleanQueryParameter($title,$conn)."', CURRENT_TIMESTAMP);";
    } else {
      $query = "UPDATE `amenities_details` SET `image` = '".cleanQueryParameter($imageUrl,$conn)."', `logo_img` = '".cleanQueryParameter($imageUrl1,$conn)."', `type` = '".cleanQueryParameter($type,$conn)."', `title` = '".cleanQueryParameter($title,$conn)."' WHERE `id` = '".cleanQueryParameter($id,$conn)."';";
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $errMsg = "Successfully updated Amenities  content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
      $returnArr = setErrorStack($queryResult, 35);
    }

    return $returnArr;
}

/***************************************************************************************************
    @ function name: getAmenitiesDetails();

    @ purpose: to fetch Amenities Details By Id
 **************************************************************************************************/

function getAmenitiesDetails($id,$conn){
    $query = "SELECT * FROM amenities_details WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Amenities content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}



/***************************************************************************************************
    @ function name: getAmenitiesDetailsById();

    @ purpose: to fetch Amenities Details By property Id
 **************************************************************************************************/

function getAmenitiesDetailsById($id,$conn){
   $query = "SELECT * FROM amenities_details WHERE p_id ='".cleanQueryParameter($id,$conn)."' ORDER BY `id` DESC ";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Amenities content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}


/**************************************************************************************************
    @ Function Name : removeAmenities()
    @ Purpose : Deleteing Amenities Details from database and Image from folder
***************************************************************************************************/



  function removeAmenities($Id,$conn) {
      $returnArr = array();
       $query = "SELECT * FROM amenities_details WHERE id ='".cleanQueryParameter($Id,$conn)."'";
      $queryResult = runQuery($query, $conn);
      $res = array();
      while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
          $res["response"][] = $row;
      }
       $query1 = "DELETE FROM amenities_details WHERE id = '".cleanQueryParameter($Id,$conn)."'";
      $queryResult1 = runQuery($query1,$conn);
      if (noError($queryResult1)) {
          unlink("../uploads/".$res["response"][0]["image"]);
          unlink("../uploads/".$res["response"][0]["logo_img"]);
          $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Amenities content");
      } else {
          $returnArr = setErrorStack($queryResult1,36);
      }
      return $returnArr;
  }
?>
