<?php 
/**************************************************************************************************
    @ function name: updateGlobalContent();
    @ parameters: $Id(integer[Auto]),$facebookLink(String),$twitterLink(String),$linkedinLink(String),$googleLink(String),,$youtubeLink(String),$aboutpanchshilLink(String),$aboutDescription(String)
    @ purpose: to Add or update Global content of page
 *************************************************************************************************/
function updateGlobalContent($Id,$globalSettingArray,$conn){
	 $returnArr = array();
        /* checking for Add new data or Update Existin Data commenting test*/

        if (empty($Id)) {                          
            $query = "INSERT INTO global_setting (facebookLink,twitterLink,linkedinLink,googleLink,
                    youtubeLink,aboutDescription) VALUE ('".cleanQueryParameter($globalSettingArray['facebookLink'],$conn)."','".cleanQueryParameter($globalSettingArray['twitterLink'],$conn)."','".cleanQueryParameter($globalSettingArray['linkedinLink'],$conn)."','".cleanQueryParameter($globalSettingArray['googleLink'],$conn)."','".cleanQueryParameter($globalSettingArray['youtubeLink'],$conn)."','".cleanQueryParameter($globalSettingArray['aboutDescription'],$conn)."')";
         //echo $query;
        } else { 
            $query = "UPDATE global_setting SET facebookLink='".cleanQueryParameter($globalSettingArray['facebookLink'],$conn)."',
                    twitterLink='".cleanQueryParameter($globalSettingArray['twitterLink'],$conn)."',linkedinLink='".cleanQueryParameter($globalSettingArray['linkedinLink'],$conn)."',googleLink='".cleanQueryParameter($globalSettingArray['googleLink'],$conn)."',youtubeLink='".cleanQueryParameter($globalSettingArray['youtubeLink'],$conn)."',aboutDescription='".cleanQueryParameter($globalSettingArray['aboutDescription'],$conn)."'";      
        }
        $queryResult = runQuery($query,$conn);
        if(noError($queryResult)){
            $errMsg = "Successfully updated Global Setting content";
            $returnArr = setErrorStack($queryResult, -1, $errMsg);
        }else{
            $returnArr = setErrorStack($queryResult, 46);
        }  
  
    return $returnArr; 
}

/***************************************************************************************************
    @ function name: getGlobalSettingContent();
    @ parameters: $Id(integer[Auto]),$facebookLink(String),$twitterLink(String),$linkedinLink(String),$googleLink(String),,$youtubeLink(String),$aboutpanchshilLink(String),$aboutDescription(String)
    @ purpose: to fetch Global content.
 **************************************************************************************************/
function getGlobalSettingContent($Id,$conn){
    if (!isset($Id) || empty($Id)) {
        $query = "SELECT * FROM global_setting";
    } else {
        $query = "SELECT * FROM global_setting WHERE id ='".cleanQueryParameter($Id,$conn)."'";
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Global content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,45);
    }
    return $returnArr;
}

/**************************************************************************************************
    @ Function Name : removeGlobalSetting()
    @ Parameters : $Id(Integer),$conn(string)
    @ Purpose : Deleteing Global Settings Details from database 
***************************************************************************************************/
function removeGlobalSetting($Id,$conn) {
    $returnArr = array();
    $query1 = "DELETE FROM global_setting WHERE id = '".cleanQueryParameter($Id,$conn)."'";
    // echo $query1;
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {        
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Global Content Details");
    } else {
        $returnArr = setErrorStack($queryResult1,47);
    }
    return $returnArr;
}

?>