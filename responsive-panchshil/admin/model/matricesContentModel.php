<?php 
/**************************************************************************************************
    @ function name: updateMatricesContent();
    @ parameters: $matricesId(integer[Auto]),$featureLogo(String),$matircesText(String),$noOfCount(number),$conn(string)
    @ purpose: to Add or update matrices content of page
 *************************************************************************************************/
function updateMatricesContent($matricesId,$matrics_logo,$matircesText,$no_Of_Count,$conn){
	$returnArr = array();
    if(!empty($matrics_logo)){

        /* check to Add new data or Update Exiting Data */
        if(!isset($matricesId) || empty($matricesId)){

            /* checking the existing data present in the table */
            $query1 = "SELECT * FROM matrics_contents";
            
            $query1Result = runQuery($query1,$conn);
            if (noError($query1Result)) {
                $res = array();
                while ($row = mysqli_fetch_assoc($query1Result["dbResource"])) {
                    $res["response"][] = $row;
                }
                    $headerRow = $res;                
                if (count($headerRow['response']) < 5) {                
                     $query = "INSERT INTO matrics_contents ( matrics_logo, matrics_text,no_of_count ) VALUE ('".cleanQueryParameter($matrics_logo,$conn)."','".cleanQueryParameter($matircesText,$conn)."','".cleanQueryParameter($no_Of_Count,$conn)."')";        

                } else {
                    $returnArr = setErrorStack($query1Result, 42);
                }
            } else {
                $returnArr = setErrorStack($query1Result, 12);
            } 
        }else{
              $query = "UPDATE matrics_contents SET matrics_logo='".cleanQueryParameter($matrics_logo,$conn)."',matrics_text='".cleanQueryParameter($matircesText,$conn)."',no_of_count='".cleanQueryParameter($no_Of_Count,$conn)."' WHERE matrics_id = '".cleanQueryParameter($matricesId,$conn)."'";
        }
        $queryResult = runQuery($query,$conn);
        if(noError($queryResult)){
            $errMsg = "Successfully updated matrices content";
            $returnArr = setErrorStack($queryResult, -1, $errMsg);
        }else{
            $returnArr = setErrorStack($queryResult, 44);
        }
    }else{
        $returnArr = setErrorStack($queryResult, 21);
    }	
    return $returnArr;
}

/***************************************************************************************************
    @ function name: getMatricsContent();
    @ parameters: $matricesId(Integer),$conn(string)
    @ purpose: to fetch matrics content.
 **************************************************************************************************/
function getMatricsContent($matricesId,$conn){
    if (!isset($matricesId) || empty($matricesId)) {
        $query = "SELECT * FROM matrics_contents";
    } else {
        $query = "SELECT * FROM matrics_contents WHERE matrics_id ='".cleanQueryParameter($matricesId,$conn)."'";
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched header content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,45);
    }
    return $returnArr;
}

/***************************************************************************************************
    @ function name: removeMatrics();
    @ parameters: $matricesId(Integer),$conn(string)
    @ purpose: to delete matrics content.
 **************************************************************************************************/
function removeMatrics($matricesId,$conn){
    $returnArr = array();
    $query = "SELECT * FROM matrics_contents WHERE matrics_id ='".cleanQueryParameter($matricesId,$conn)."'";
    $queryResult = runQuery($query, $conn);
    $res = array();
    while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
        $res["response"][] = $row;
        //printArr($res);
    }
    $query1 = "DELETE FROM matrics_contents WHERE matrics_id ='".cleanQueryParameter($matricesId,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
        unlink("../uploads/".$res["response"][0]["matrics_logo"]);
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Matrics Details");
    } else {
        $returnArr = setErrorStack($queryResult1,34);
    }
    return $returnArr;
}


?>