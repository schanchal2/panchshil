<?php

/**************************************************************************************************
    @ function name: updateSustainDetails();
    @ purpose: to Add or update about Details to Database
 *************************************************************************************************/
 function updateSustainDetails($id,$sustainUrlId,$status,$image,$name,$shortDesc,$longDesc,$conn){
     $returnArr = array();

     /* Checking Image is present or not */
     if ($status != 'Inactive') {
         $status = 'Active';
     }
     //if ( !empty($image)) {

     /* checking for Add new data or Update Existin Data */
     if (!isset($id) || empty($id)) {
   $query = "INSERT INTO sustainDetails (status,sustainid,image, name,shortDesc,longDesc,pagename) VALUE ('".cleanQueryParameter($status,$conn)."',".cleanQueryParameter($sustainUrlId,$conn).",'".cleanQueryParameter($image,$conn)."','".cleanQueryParameter($name,$conn)."','".cleanQueryParameter($shortDesc,$conn)."','".cleanQueryParameter($longDesc,$conn)."','".cleanQueryParameter('manageSustainCat.php',$conn)."')";
     } else {
      $query = "UPDATE sustainDetails SET status='".cleanQueryParameter($status,$conn)."', image='".cleanQueryParameter($image,$conn)."',sustainid='".cleanQueryParameter($sustainUrlId,$conn)."',name='".cleanQueryParameter($name,$conn)."',shortDesc='".cleanQueryParameter($shortDesc,$conn)."',longDesc='".cleanQueryParameter($longDesc,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
     }
     $queryResult = runQuery($query,$conn);
     if(noError($queryResult)){
       $errMsg = "Successfully updated Sustain content";
       $returnArr = setErrorStack($queryResult, -1, $errMsg);
     }else{
       $returnArr = setErrorStack($queryResult, 35);
     }

     return $returnArr;
 }

/***************************************************************************************************
    @ function name: getSustainDetails();

    @ purpose: to fetch SustainDetails
 **************************************************************************************************/
function getSustainDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM sustainDetails";
        } else {
            $query = "SELECT * FROM sustainDetails WHERE status='Active'";
        }
    } else {
        if (!isset($require) || empty($require)) {
             $query = "SELECT * FROM sustainDetails WHERE sustainid ='".cleanQueryParameter($id,$conn)."'";
        } else {
              $query = "SELECT * FROM sustainDetails WHERE sustainid ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Sustainability content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

/**************************************************************************************************
    @ Function Name : removeSustain()
    @ Purpose : Deleteing about Details from database and Image from folder
***************************************************************************************************/
function removeSustain($id,$conn) {
      $returnArr = array();
      //$status = "";
      $query = "SELECT * FROM sustainDetails WHERE id ='".cleanQueryParameter($id,$conn)."'";
      $queryResult = runQuery($query, $conn);
      $res = array();
      while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
          $res["response"][] = $row;
      }

      if ($res["response"][0]["status"] == "Active") {
          $status = "Inactive";
      } else {
          $status = "Active";
      }

     $query1 = "UPDATE sustainDetails SET status='".cleanQueryParameter($status,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
      $queryResult1 = runQuery($query1,$conn);
      if (noError($queryResult1)) {
           $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted about Details");
      } else {
          $returnArr = setErrorStack($queryResult1,36);
      }
      return $returnArr;
    }

    /**************************************************************************************************
        @ function name: updateSustainCatDetails();
        @ purpose: to Add or update about Details to Database
     *************************************************************************************************/
     function updateSustainCatDetails($id,$sustainUrlId,$status,$sustain_cat_image,$sustain_cat_long_url,$sign_image_url,$name,$designation,$conn){
         $returnArr = array();

         /* Checking Image is present or not */
         if ($status != 'Inactive') {
             $status = 'Active';
         }
         //if ( !empty($image)) {

         /* checking for Add new data or Update Existin Data */
         if (!isset($id) || empty($id)) {
            
       $query = "INSERT INTO sustain_cat (status,sustainCatId,sustain_cat_image, sustain_cat_long_url,sustain_cat_sign_image,sustain_cat_name,sustain_cat_designation) VALUE ('".cleanQueryParameter($status,$conn)."','".cleanQueryParameter($sustainUrlId,$conn)."','".cleanQueryParameter($sustain_cat_image,$conn)."','".cleanQueryParameter($sustain_cat_long_url,$conn)."','".cleanQueryParameter($sign_image_url,$conn)."','".cleanQueryParameter($name,$conn)."','".cleanQueryParameter($designation,$conn)."')";
         } else {
          
          
          $query = "UPDATE sustain_cat SET status='".cleanQueryParameter($status,$conn)."', sustainCatId='".cleanQueryParameter($sustainUrlId,$conn)."', sustain_cat_image='".cleanQueryParameter($sustain_cat_image,$conn)."',sustain_cat_long_url='".cleanQueryParameter($sustain_cat_long_url,$conn)."',sustain_cat_sign_image='".cleanQueryParameter($sign_image_url,$conn)."',sustain_cat_name='".cleanQueryParameter($name,$conn)."',sustain_cat_designation='".cleanQueryParameter($designation,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
         }
         $queryResult = runQuery($query,$conn);
        // printArr($queryResult);
         if(noError($queryResult)){
           $errMsg = "Successfully updated Sustain content";
           $returnArr = setErrorStack($queryResult, -1, $errMsg);
         }else{
           $returnArr = setErrorStack($queryResult, 35);
         }

         return $returnArr;
     }

    /***************************************************************************************************
        @ function name: getSustainCatDetails();

        @ purpose: to fetch SustainCatDetails
     **************************************************************************************************/
    function getSustainCatDetails($sustainCatId,$require,$conn){

        if (!isset($sustainCatId) || empty($sustainCatId)) {
            if (!isset($require) || empty($require)) {
                $query = "SELECT * FROM sustain_cat";
            } else {
                $query = "SELECT * FROM sustain_cat WHERE status='Active'";
            }
        } else {
            if (!isset($require) || empty($require)) {
                 $query = "SELECT * FROM sustain_cat WHERE sustainCatId ='".cleanQueryParameter($sustainCatId,$conn)."'";
            } else {
                  $query = "SELECT * FROM sustain_cat WHERE sustainCatId ='".cleanQueryParameter($sustainCatId,$conn)."' AND status='Active'";
            }
        }
        $queryResult = runQuery($query,$conn);
        if(noError($queryResult)){
            $res = array();
            while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
                $res["response"][] = $row;
            $returnArr = setErrorStack($queryResult,-1,"Successfully fetched SustainCatDetails",$res);
        }else{
            $returnArr = setErrorStack($queryResult,28);
        }
        return $returnArr;
    }

    function removesustaincat($id,$conn) {
      $returnArr = array();
      //$status = "";
     $query = "SELECT * FROM sustain_cat WHERE id ='".cleanQueryParameter($id,$conn)."'";
      $queryResult = runQuery($query, $conn);
      $res = array();
      while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
          $res["response"][] = $row;
      }

      if ($res["response"][0]["status"] == "Active") {
          $status = "Inactive";
      } else {
          $status = "Active";
      }
          //DELETE from forword WHERE `id` = 2
          $query1 = "DELETE from sustain_cat WHERE id ='".cleanQueryParameter($id,$conn)."'";
      $queryResult1 = runQuery($query1,$conn);
      if (noError($queryResult1)) {
           $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted sustain_cat Details");
      } else {
          $returnArr = setErrorStack($queryResult1,36);
      }
      return $returnArr;
    }


   function getAboutSustainDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM aboutDetails";
        } else {
            $query = "SELECT * FROM aboutDetails WHERE status='Active'";
        }
    } else {
        if (!isset($require) || empty($require)) {
             $query = "SELECT * FROM aboutDetails WHERE id ='".cleanQueryParameter($id,$conn)."'";
        } else {
              $query = "SELECT * FROM aboutDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched aboutDetails content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}
?>
