<?php


/**
 * function name: addupdatereasons();
 * parameters: $id(id), $location(location), $phoneno1(phoneno1),$phoneno2(phoneno2),$phoneno3(phoneno3),$phoneno4(phoneno4),$conn(object)
 * purpose: to add contact by contacts id details;
 */

function getreasons($conn){

    $returnArr = array();
    $getResult = array();

   $query = "select * from reasons ";
       //$query = "SELECT * FROM reasons WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $execQuery = runQuery($query, $conn);
    if(noError($execQuery)){
        while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $res["response"][] = $row;
        }
        $returnArr = setErrorStack($execQuery, -1, "Successfully fetched record contacts details" ,$res);
    }else{
        $returnArr = setErrorStack($execQuery, 25, "Error: Retrieving contacts detail");
    }

    return $returnArr;
}


function getreasonsone($id,$conn){



    $returnArr = array();
    $getResult = array();

   //$query = "select * from reasons ";
   
      //echo $query = "SELECT * FROM reasons WHERE id =".cleanQueryParameter($id,$conn);
    $execQuery = runQuery($query, $conn);
    if(noError($execQuery)){
        while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $res["response"][] = $row;
        }
        $returnArr = setErrorStack($execQuery, -1, "Successfully fetched record contacts details" ,$res);
    }else{
        $returnArr = setErrorStack($execQuery, 25, "Error: Retrieving contacts detail");
    }

    return $returnArr;
}


function addupdatereasons($id,$reasons,$conn){

    $returnArr = array();

    /*checking to Add new data or Update Existin Data */
     if ($id=="") 
       {
         $query = "INSERT INTO reasons(reasons) VALUE ('".cleanQueryParameter($reasons,$conn)."')";    
        // $sql = "INSERT INTO `reasons` (`id`, `reasons`) VALUES (NULL, \'$reasons\')";     

       } 

     else 
      { 
          $query = "UPDATE  reasons SET  reasons='".cleanQueryParameter($reasons,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";     
      }
  

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $errMsg = "Successfully updated reasons";
        $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
        $returnArr = setErrorStack($queryResult, 12);
    }
    return $returnArr;
}



//remove the contact record in removeconatct1 function
function Removereasons($id,$conn) {
    $returnArr = array();  
    $query1 = "DELETE FROM reasons WHERE id = '".cleanQueryParameter($id,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted  reasons");
    } else {
        $returnArr = setErrorStack($queryResult1,36);
    }
    return $returnArr;
}
?>