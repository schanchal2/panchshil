<?php

/*Created By Kavita*/
/**************************************************************************************************
    @ function name: updateMediaDetails();
    @ purpose: to Add or update Media Details to Database
 *************************************************************************************************/
function updateMediaDetails($id,$status,$image,$image1,$name,$shortDesc,$longDesc,$page_type,$conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }
    //if ( !empty($image)) {

    /* checking for Add new data or Update Existin Data */
    if (!isset($id) || empty($id)) {
  // echo $query = "INSERT INTO mediaDetails (status,box_image,name,short_desc,long_desc,media_type) VALUE ('".cleanQueryParameter($status,$conn)."','".cleanQueryParameter($image,$conn)."','".cleanQueryParameter($name,$conn)."','".cleanQueryParameter($shortDesc,$conn)."','".cleanQueryParameter($longDesc,$conn)."','".cleanQueryParameter($page_type,$conn)."')";

  $query = "  INSERT INTO `mediaDetails` (`id`, `name`, `media_type`, `feature_image`, `box_image`, `short_desc`, `long_desc`, `status`, `created`) VALUES (NULL, '".cleanQueryParameter($name,$conn)."', '".cleanQueryParameter($page_type,$conn)."', '".cleanQueryParameter($image,$conn)."', '".cleanQueryParameter($image1,$conn)."',   '".cleanQueryParameter($shortDesc,$conn)."', '".cleanQueryParameter($longDesc,$conn)."', 'Active', CURRENT_TIMESTAMP);";

    } else {
   $query = "UPDATE mediaDetails SET status='".cleanQueryParameter($status,$conn)."', box_image='".cleanQueryParameter($image,$conn)."',name='".cleanQueryParameter($name,$conn)."',short_desc='".cleanQueryParameter($shortDesc,$conn)."',long_desc='".cleanQueryParameter($longDesc,$conn)."',media_type='".cleanQueryParameter($page_type,$conn)."'  WHERE id='".cleanQueryParameter($id,$conn)."'";
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $errMsg = "Successfully updated mediaDetails content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
      $returnArr = setErrorStack($queryResult, 35);
    }

    return $returnArr;
}

/***************************************************************************************************
    @ function name: getMediaDetails();

    @ purpose: to fetch Media Details
 **************************************************************************************************/

function getMediaDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM mediaDetails";
        } else {
             $query = "SELECT * FROM mediaDetails WHERE status='Active' ";
        }
    } else {
        if (!isset($require) || empty($require)) {
              $query = "SELECT * FROM mediaDetails WHERE id ='".cleanQueryParameter($id,$conn)."'";
        } else {
               $query = "SELECT * FROM mediaDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Media content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}



/***************************************************************************************************
    @ function name: getMediaDetailsById();

    @ purpose: to fetch Media Details By Id
 **************************************************************************************************/

function getMediaDetailsById($id,$conn){
    $query = "SELECT * FROM mediaDetails WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Media content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}


/***************************************************************************************************
    @ function name: getMediaAwardDetailsById();

    @ purpose: to fetch Media Details By Id
 **************************************************************************************************/

function getMediaAwardDetailsById($id,$conn){
    $query = "SELECT * FROM mediaDetails WHERE id =$id";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Media content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

/**************************************************************************************************
    @ Function Name : removeMedia()
    @ Purpose : Deleteing Media Details from database and Image from folder
***************************************************************************************************/
function removeMedia($id,$conn) {
    $returnArr = array();
    $query = "SELECT * FROM mediaDetails WHERE id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query, $conn);
    $res = array();
    while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
        $res["response"][] = $row;
    }

    if ($res["response"][0]["status"] == "Active") {
        $status = "Inactive";
    } else {
        $status = "Active";
    }

   $query1 = "UPDATE mediaDetails SET status='".cleanQueryParameter($status,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
         $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Media Details");
    } else {
        $returnArr = setErrorStack($queryResult1,36);
    }
    return $returnArr;
  }

?>
