<?php

/**************************************************************************************************
    @ function name: updateClientDetails();
    @ parameters: $clientId(integer[Auto]),$clientName(String),$clientColorImage(String),$clientGreyImage(String),$services(String),$clientUrl(string),$conn(string)
    @ purpose: to Add or update Client Details to Database
 *************************************************************************************************/
function updateClientDetails( $clientId, $clientUrlId, $clientName, $clientColorImage, $services, $clientServicesBelong, $clientUrl,$conn){
    $returnArr = array();


    /* Checking Image is present or not */
    if ( !empty($clientColorImage)) {

        /* checking for Add new data or Update Existin Data */
        if (!isset($clientId) || empty($clientId)) {                          
            $query = "INSERT INTO client (clientUrlId, clientName, clientColorImage, ClientServices, clientServicesBelong, clientUrl ) VALUE ('".cleanQueryParameter($clientUrlId,$conn)."','".cleanQueryParameter($clientName,$conn)."','".cleanQueryParameter($clientColorImage,$conn)."','".cleanQueryParameter($services,$conn)."','".cleanQueryParameter($clientServicesBelong,$conn)."','".cleanQueryParameter($clientUrl,$conn)."')";                
        } else { 
            $query = "UPDATE client SET clientName='".cleanQueryParameter($clientName,$conn)."', clientColorImage='".cleanQueryParameter($clientColorImage,$conn)."',clientServices='".cleanQueryParameter($services,$conn)."',clientServicesBelong='".cleanQueryParameter($clientServicesBelong,$conn)."',clientUrl='".cleanQueryParameter($clientUrl,$conn)."' WHERE clientId='".cleanQueryParameter($clientId,$conn)."' AND clientUrlId='".cleanQueryParameter($clientUrlId,$conn)."'";       
        }
        $queryResult = runQuery($query,$conn);
        if(noError($queryResult)){
            $errMsg = "Successfully updated client content";
            $returnArr = setErrorStack($queryResult, -1, $errMsg);
        }else{
            $returnArr = setErrorStack($queryResult, 35);
        }  
    } 
    else {
        $returnArr = setErrorStack($queryResult, 21);
    }
    return $returnArr;    
}

/***************************************************************************************************
    @ function name: getClientDetails();
    @ parameters: $clientId(Integer),$conn(string)
    @ purpose: to fetch Client Details
 **************************************************************************************************/
function getClientDetails($clientId, $clientUrlId, $conn){

    if (!isset($clientId) || empty($clientId)) {
        $query = "SELECT * FROM client WHERE clientUrlId ='".cleanQueryParameter($clientUrlId,$conn)."'";
    } else {
        $query = "SELECT * FROM client WHERE clientId ='".cleanQueryParameter($clientId,$conn)."' AND clientUrlId ='".cleanQueryParameter($clientUrlId,$conn)."'";
    }   

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched client content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}

/**************************************************************************************************
    @ Function Name : removeClient()
    @ Parameters : $clientId(Integer),$conn(string)
    @ Purpose : Deleteing CLient Details from database and Image from folder 
***************************************************************************************************/
function removeClient($clientId, $clientUrlId, $conn) {
    $returnArr = array();
    $query = "SELECT * FROM client WHERE clientId ='".cleanQueryParameter($clientId,$conn)."' AND clientUrlId ='".cleanQueryParameter($clientUrlId,$conn)."'";
    $queryResult = runQuery($query, $conn);
    $res = array();
    while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
        $res["response"][] = $row;
    }

    $query1 = "DELETE FROM client WHERE ClientId = '".cleanQueryParameter($clientId,$conn)."' AND clientUrlId ='".cleanQueryParameter($clientUrlId,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
        unlink("../uploads/".$res["response"][0]["clientColorImage"]);
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Client Details");
    } else {
        $returnArr = setErrorStack($queryResult1,36);
    }
    return $returnArr;
}
/***************************************************************************************************
    @ function name: getClientWithOffset();
    @ purpose: to fetch Client Details with Offset and limit for view All client
 **************************************************************************************************/

function getClientWithOffset($offset, $limit,$conn){
    $returnArr = array();
    if(!isset($limit)|| empty($limit) || $limit=="undefined"){
        $query = "SELECT * FROM client LIMIT 9999999999999 OFFSET ".$offset; 
    }else{
        $query = "SELECT * FROM client LIMIT ".$limit." OFFSET ".$offset;
    }                 
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"])){
            $res["response"][] = $row;
        }
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched client details",$res);

    }else{
        $returnArr = setErrorStack($queryResult,28);

    }
    return $returnArr;

}
?>