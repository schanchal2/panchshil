<?php

/**************************************************************************************************
    @ function name: updateClientDetails();
    @ parameters: $clientId(integer[Auto]),$clientName(String),$clientColorImage(String),$clientGreyImage(String),$services(String),$clientUrl(string),$conn(string)
    @ purpose: to Add or update Client Details to Database
 *************************************************************************************************/
function  updateLeadershipDetails($abt_id,$id,$Image,$name,$designation,$description,$facebook,$twitter,$linkedin,$conn)
{

    $returnArr = array();

    /* Checking Image is present or not */
    if ( !empty($Image)) {

        /* checking for Add new data or Update Existin Data commenting test*/


        if (empty($id)) {
              $query = "INSERT INTO leadership_contents (	about_id,status,image, name,designation,description,facebook,twitter,	linkedin) VALUE ('".cleanQueryParameter($abt_id,$conn)."','Active','".cleanQueryParameter($Image,$conn)."','".cleanQueryParameter($name,$conn)."','".cleanQueryParameter($designation,$conn)."','".cleanQueryParameter($description,$conn)."','".cleanQueryParameter($facebook,$conn)."','".cleanQueryParameter($twitter,$conn)."','".cleanQueryParameter($linkedin,$conn)."')";
        } else {
              $query = "UPDATE leadership_contents SET image='".cleanQueryParameter($Image,$conn)."', name='".cleanQueryParameter($name,$conn)."',description='".cleanQueryParameter($description,$conn)."',	facebook='".cleanQueryParameter($facebook,$conn)."',	twitter='".cleanQueryParameter($twitter,$conn)."',linkedin='".cleanQueryParameter($linkedin,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
        }
        $queryResult = runQuery($query,$conn);
        if(noError($queryResult)){
            $errMsg = "Successfully updated Leadership content";
            $returnArr = setErrorStack($queryResult, -1, $errMsg);
        }else{
            $returnArr = setErrorStack($queryResult, 35);
        }
    }
    else {
        $returnArr = setErrorStack($queryResult, 21);
    }
    return $returnArr;
}


/***************************************************************************************************
    @ function name: getleadership_contents();

    @ purpose: to fetch leadership_contents
 **************************************************************************************************/
function getLeadershipDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
             $query = "SELECT * FROM leadership_contents";
        } else {
            $query = "SELECT * FROM leadership_contents WHERE status='Active'";
        }
    } else {
        if (!isset($require) || empty($require)) {
             $query = "SELECT * FROM leadership_contents WHERE id ='".cleanQueryParameter($id,$conn)."'";
        } else {
              $query = "SELECT * FROM leadership_contents WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched leadership_contents content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}




/***************************************************************************************************
    @ function name: getLeadershipDetailsById();

    @ purpose: to fetch leadership_contents By Id
 **************************************************************************************************/
function getLeadershipDetailsById($id,$conn){
    $query = "SELECT * FROM leadership_contents WHERE about_id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched leadership_contents BY Id content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}
/**************************************************************************************************
    @ Function Name : removeClient()
    @ Purpose : Deleteing about Details from database and Image from folder
***************************************************************************************************/
function removeLeadership($Id,$conn) {
    $returnArr = array();
     $query = "SELECT * FROM leadership_contents WHERE id ='".cleanQueryParameter($Id,$conn)."'";
    $queryResult = runQuery($query, $conn);
    $res = array();
    while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
        $res["response"][] = $row;
    }

     $query1 = "DELETE FROM leadership_contents WHERE id = '".cleanQueryParameter($Id,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
        unlink("../uploads/".$res["response"][0]["image"]);
        $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Leadership Details");
    } else {
        $returnArr = setErrorStack($queryResult1,36);
    }
    return $returnArr;
}
?>
