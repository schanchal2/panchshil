<?php

/*Created By Kavita*/
/**************************************************************************************************
    @ function name: updateTestimonialDetails();
    @ purpose: to Add or update Testimonial Details to Database
 *************************************************************************************************/

function updateBrandAndPartnerDetails($id,$media_id,$imageUrl,$name,$designation,$longDesc,$conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }
    if (!isset($id) || empty($id)) {


       $query = "INSERT INTO `brandDetails` (`id`, `alliance_id`, `name`, `image`, `desigantion`, `description`, `created`) VALUES (NULL,'".cleanQueryParameter($media_id,$conn)."', '".cleanQueryParameter($name,$conn)."','".cleanQueryParameter($imageUrl,$conn)."', '".cleanQueryParameter($designation,$conn)."','".cleanQueryParameter($longDesc,$conn)."', CURRENT_TIMESTAMP);";


  }
  else
   {
    # code...
     $query="UPDATE `brandDetails` SET `name` = '".cleanQueryParameter($name,$conn)."', `image` = '".cleanQueryParameter($imageUrl,$conn)."', `desigantion` = '".cleanQueryParameter($designation,$conn)."', `description` = '".cleanQueryParameter($longDesc,$conn)."' WHERE id = '".cleanQueryParameter($id,$conn)."';";
  }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $errMsg = "Successfully updated mediaDetails content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
      $returnArr = setErrorStack($queryResult, 35);
    }

    return $returnArr;
}

/***************************************************************************************************
    @ function name: getTestimonialDetails();

    @ purpose: to fetch Testimonial Details
 **************************************************************************************************/

function getBrandAndPartnerDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM brandDetails";
        } else {
            $query = "SELECT * FROM brandDetails ";
        }
    } else {
        if (!isset($require) || empty($require)) {
              $query = "SELECT * FROM brandDetails WHERE id ='".cleanQueryParameter($id,$conn)."'";
        } else {
               echo $query = "SELECT * FROM brandDetails WHERE id ='".cleanQueryParameter($id,$conn)."' ";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched   Brand And Partner content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}




/***************************************************************************************************
    @ function name: getTestimonialDetailsByTestId();

    @ purpose: to fetch Testimonial Details By ID
 **************************************************************************************************/

function getBrandAndPartnerDetailsById($id,$conn){
  $query = "SELECT * FROM brandDetails WHERE 	alliance_id ='".cleanQueryParameter($id,$conn)."'";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched   Brand And Partner content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}


/**************************************************************************************************
    @ Function Name : removeBrandAndPartner()
    @ Purpose : Deleteing Brand  Details from database and Image from folder
***************************************************************************************************/



  function removeBrandAndPartner($Id,$conn) {
      $returnArr = array();
        $query = "SELECT * FROM brandDetails WHERE id ='".cleanQueryParameter($Id,$conn)."'";
      $queryResult = runQuery($query, $conn);
      $res = array();
      while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
          $res["response"][] = $row;
      }

        $query1 = "DELETE FROM brandDetails WHERE id = '".cleanQueryParameter($Id,$conn)."'";
      $queryResult1 = runQuery($query1,$conn);
      if (noError($queryResult1)) {
          unlink("../uploads/".$res["response"][0]["image"]);
          $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted  Brand And Partner Details");
      } else {
          $returnArr = setErrorStack($queryResult1,36);
      }
      return $returnArr;
  }
?>
