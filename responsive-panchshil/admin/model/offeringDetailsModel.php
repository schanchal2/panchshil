<?php

/**************************************************************************************************
    @ function name: updateClientDetails();
    @ parameters: $clientId(integer[Auto]),$clientName(String),$clientColorImage(String),$clientGreyImage(String),$services(String),$clientUrl(string),$conn(string)
    @ purpose: to Add or update Client Details to Database
 *************************************************************************************************/

function updateOfferingDetails($id, $businessId, $featureImage, $bigImage, $footerImage, $status, $name, $shortDesc, $longDesc,$page_type, $conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }
    if ( !empty($featureImage) && !empty($bigImage) && !empty($footerImage) ) {
        /* checking for Add new data or Update Existin Data */
        if (!isset($id) || empty($id)) {
            $query = "INSERT INTO offering (business_id, feature_image, big_image, footer_image, name, status, shortDesc, longDesc,pagename) VALUE ('".cleanQueryParameter($businessId,$conn)."','".cleanQueryParameter($featureImage,$conn)."','".cleanQueryParameter($bigImage,$conn)."','".cleanQueryParameter($footerImage,$conn)."','".cleanQueryParameter($name,$conn)."','".cleanQueryParameter($status,$conn)."','".cleanQueryParameter($shortDesc,$conn)."','".cleanQueryParameter($longDesc,$conn)."','".cleanQueryParameter($page_type,$conn)."')";
        } else {
            $query = "UPDATE offering SET business_id='".cleanQueryParameter($businessId,$conn)."', feature_image='".cleanQueryParameter($featureImage,$conn)."',big_image='".cleanQueryParameter($bigImage,$conn)."',footer_image='".cleanQueryParameter($footerImage,$conn)."',name='".cleanQueryParameter($name,$conn)."',status='".cleanQueryParameter($status,$conn)."',shortDesc='".cleanQueryParameter($shortDesc,$conn)."',longDesc='".cleanQueryParameter($longDesc,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."' AND business_id='".cleanQueryParameter($businessId,$conn)."'";
        }
        $queryResult = runQuery($query,$conn);
        if(noError($queryResult)){
            $errMsg = "Successfully updated Details";
            $returnArr = setErrorStack($queryResult, -1, $errMsg);
        }else{
            $returnArr = setErrorStack($queryResult, 35);
        }
    }
    else {
        $returnArr = setErrorStack($queryResult, 21);
    }
    return $returnArr;
}

/***************************************************************************************************
    @ function name: getClientDetails();
    @ parameters: $clientId(Integer),$conn(string)
    @ purpose: to fetch Client Details
 **************************************************************************************************/
function getofferingDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM offering ";
        } else {
            $query = "SELECT * FROM offering WHERE business_id='".cleanQueryParameter($id,$conn)."' AND business_status='Active'";
        }

    } else {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM offering WHERE business_id ='".cleanQueryParameter($id,$conn)."'";
        } else {
            $query = "SELECT * FROM offering WHERE business_id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
        }
    }

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched business category content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}




/***************************************************************************************************
    @ function name: getClientDetails();
    @ parameters: $clientId(Integer),$conn(string)
    @ purpose: to fetch Client Details
 **************************************************************************************************/
function getofferingDetailsByPageName($pagename,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
             $query = "SELECT * FROM business_details ";
        } else {
             $query = "SELECT * FROM business_details WHERE pagename='".cleanQueryParameter($pagename,$conn)."' AND business_status='Active'";
        }

    } else {
        if (!isset($require) || empty($require)) {
             $query = "SELECT * FROM business_details WHERE pagename ='".cleanQueryParameter($pagename,$conn)."'";
        } else {
             $query = "SELECT * FROM business_details WHERE pagename ='".cleanQueryParameter($pagename,$conn)."' AND business_status='Active'";
        }
    }

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched business category content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}





/***************************************************************************************************
    @ function name: getClientDetails();
    @ parameters: $clientId(Integer),$conn(string)
    @ purpose: to fetch Client Details
 **************************************************************************************************/
function getofferingDetailsByAllCategories($id,$status,$conn){

    if (!isset($id) || empty($id)) {
              $query = "SELECT * FROM offering ";
        } else {
              $query = "SELECT * FROM offering WHERE 		business_id='".cleanQueryParameter($id,$conn)."' AND status='Active'";
        }

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched business category content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}



// /**************************************************************************************************
//     @ Function Name : removeClient()
//     @ Parameters : $businessId(Integer),$conn(string)
//     @ Purpose : Deleteing CLient Details from database and Image from folder


  function removeBusinessCatDelete($Id,$conn) {
            $returnArr = array();
            $query = "SELECT * FROM offering WHERE id ='".cleanQueryParameter($Id,$conn)."'";
            $queryResult = runQuery($query, $conn);
            $res = array();
            while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
            $res["response"][] = $row;
            }
             $query1 = "DELETE FROM offering WHERE id = '".cleanQueryParameter($Id,$conn)."'";
            $queryResult1 = runQuery($query1,$conn);
            if (noError($queryResult1)) {
            unlink("../uploads/".$res["response"][0]["business_big_image"]);
            unlink("../uploads/".$res["response"][0]["business_feature_image"]);
            $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted offering Details");
            } else {
            $returnArr = setErrorStack($queryResult1,36);
            }
            return $returnArr;
  }




  /**************************************************************************************************
      @ Function Name : removeClient()
      @ Parameters : $businessId(Integer),$conn(string)
      @ Purpose : Deleteing CLient Details from database and Image from folder
  ***************************************************************************************************/
  function removeBusinessCatStatus($DeleteBusinessCatId,$conn) {
      $returnArr = array();

       $query = "SELECT * FROM offering WHERE id ='".cleanQueryParameter($DeleteBusinessCatId,$conn)."'";
      $queryResult = runQuery($query, $conn);
      $res = array();
      while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
          $res["response"][] = $row;
      }

      if ($res["response"][0]["status"] == "Active") {
          $status = "Inactive";
      } else {
          $status = "Active";
      }

       $query1 = "UPDATE offering SET status='".cleanQueryParameter($status,$conn)."' WHERE id='".cleanQueryParameter($DeleteBusinessCatId,$conn)."'";
      $queryResult1 = runQuery($query1,$conn);
      if (noError($queryResult1)) {
          $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Category Of Business Details");
      } else {
          $returnArr = setErrorStack($queryResult1,36);
      }
      return $returnArr;
  }


?>
