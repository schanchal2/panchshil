<?php
/**************************************************************************************************
  @ function name: updateCorporateDetails();
  @ purpose: to Add or update about Details to Database
*************************************************************************************************/
  function updateCorporateDetails($id, $corporateUrlId, $status, $image, $name, $shortDesc, $longDesc, $pagetype, $conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
     $status = 'Active';
    }
    if ( !empty($image)) {

      /* checking for Add new data or Update Existin Data */
      if (!isset($id) || empty($id)) {
      $query = "INSERT INTO corporateDetails (corporateUrlId, status, image, name, shortDesc, longDesc, pagename) VALUE ('".cleanQueryParameter($corporateUrlId,$conn)."','".cleanQueryParameter($status,$conn)."','".cleanQueryParameter($image,$conn)."','".cleanQueryParameter($name,$conn)."','".cleanQueryParameter($shortDesc,$conn)."','".cleanQueryParameter($longDesc,$conn)."','".cleanQueryParameter($pagetype,$conn)."')";
      } else {
      $query = "UPDATE corporateDetails SET corporateUrlId='".cleanQueryParameter($corporateUrlId,$conn)."', status='".cleanQueryParameter($status,$conn)."', image='".cleanQueryParameter($image,$conn)."',name='".cleanQueryParameter($name,$conn)."',shortDesc='".cleanQueryParameter($shortDesc,$conn)."',longDesc='".cleanQueryParameter($longDesc,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."'";
      }

      $queryResult = runQuery($query,$conn);
      if(noError($queryResult)){
       $errMsg = "Successfully updated Corporate Profile content";
       $returnArr = setErrorStack($queryResult, -1, $errMsg);
      }else{
       $returnArr = setErrorStack($queryResult, 35);
      }
    } else {
      $returnArr = setErrorStack($queryResult, 17);
    }

    return $returnArr;
  }

/***************************************************************************************************
  @ function name: getCorporateDetails();

  @ purpose: to fetch CorporateDetails
**************************************************************************************************/
  function getCorporateDetails($id,$CorporateUrlId,$require,$conn){

    if (!isset($id) || empty($id)) {
      if (!isset($require) || empty($require)) {
        $query = "SELECT * FROM corporateDetails WHERE corporateUrlId='".cleanQueryParameter($CorporateUrlId,$conn)."'";
      } else {
        $query = "SELECT * FROM corporateDetails WHERE status='Active' AND corporateUrlId='".cleanQueryParameter($CorporateUrlId,$conn)."'";
      }
    } else {
      if (!isset($require) || empty($require)) {
       $query = "SELECT * FROM corporateDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND corporateUrlId='".cleanQueryParameter($CorporateUrlId,$conn)."'";
      } else {
      $query = "SELECT * FROM corporateDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active' AND corporateUrlId='".cleanQueryParameter($CorporateUrlId,$conn)."'";
      }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $res = array();
      while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
        $res["response"][] = $row;
      $returnArr = setErrorStack($queryResult,-1,"Successfully fetched aboutDetails content",$res);
    }else{
      $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
  }

/**************************************************************************************************
  @ Function Name : removeCorporateDetails()
  @ Purpose : Deleteing about Details from database and Image from folder
***************************************************************************************************/
  function removeCompanyProfile($id, $corporateDeleteId, $conn) {
    $returnArr = array();
    //$status = "";
    $query = "SELECT * FROM corporateDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND corporateUrlId='".cleanQueryParameter($corporateDeleteId,$conn)."'";
    $queryResult = runQuery($query, $conn);
    $res = array();
    while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
      $res["response"][] = $row;
    }

    if ($res["response"][0]["status"] == "Active") {
      $status = "Inactive";
    } else {
      $status = "Active";
    }

    $query1 = "UPDATE corporateDetails SET status='".cleanQueryParameter($status,$conn)."' WHERE id='".cleanQueryParameter($id,$conn)."' AND corporateUrlId='".cleanQueryParameter($corporateDeleteId,$conn)."'";
    $queryResult1 = runQuery($query1,$conn);
    if (noError($queryResult1)) {
     $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted about Details");
    } else {
      $returnArr = setErrorStack($queryResult1,36);
    }
    return $returnArr;
  }

  /***************************************************************************************************
    @ function name: getAboutDetails();

    @ purpose: to fetch AboutDetails
 **************************************************************************************************/
function getAboutCorporateDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM aboutDetails";
        } else {
            $query = "SELECT * FROM aboutDetails WHERE status='Active'";
        }
    } else {
        if (!isset($require) || empty($require)) {
             $query = "SELECT * FROM aboutDetails WHERE id ='".cleanQueryParameter($id,$conn)."'";
        } else {
              $query = "SELECT * FROM aboutDetails WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched aboutDetails content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}
?>