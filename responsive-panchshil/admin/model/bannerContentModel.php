<?php

/*Created By Kavita*/
/**************************************************************************************************
    @ function name: updatePressDetails();
    @ purpose: to Add or update Press Details to Database
 *************************************************************************************************/

function updatebannerDetails($id,$o_id,$p_id,$imageUrl,$imageUrl1,$imageUrl2,$imageUrl3,$imageUrl4,$conn){
    $returnArr = array();

    /* Checking Image is present or not */
    if ($status != 'Inactive') {
        $status = 'Active';
    }
    if (!isset($id) || empty($id)) {

       $query = "INSERT INTO `banner_details` (`id`, `o_id`,`p_id`, `banner1`, `banner2`, `banner3`, `banner4`, `banner5`, `created`) VALUES (NULL,'".cleanQueryParameter($o_id,$conn)."', '".cleanQueryParameter($p_id,$conn)."','".cleanQueryParameter($imageUrl,$conn)."', '".cleanQueryParameter($imageUrl1,$conn)."', '".cleanQueryParameter($imageUrl2,$conn)."', '".cleanQueryParameter($imageUrl3,$conn)."','".cleanQueryParameter($imageUrl4,$conn)."', CURRENT_TIMESTAMP);";

    } else {
$query = "UPDATE `banner_details` SET `banner1` = '".cleanQueryParameter($imageUrl,$conn)."', `banner2` = '".cleanQueryParameter($imageUrl1,$conn)."', `banner3` = '".cleanQueryParameter($imageUrl2,$conn)."', `banner4` = '".cleanQueryParameter($imageUrl3,$conn)."', `banner5` = '".cleanQueryParameter($imageUrl4,$conn)."' WHERE `id` = '".cleanQueryParameter($id,$conn)."';";
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
      $errMsg = "Successfully updated Press  content";
      $returnArr = setErrorStack($queryResult, -1, $errMsg);
    }else{
      $returnArr = setErrorStack($queryResult, 35);
    }

    return $returnArr;
}

/***************************************************************************************************
    @ function name: getPressDetails();

    @ purpose: to fetch Press Details
 **************************************************************************************************/

function getBannerDetails($id,$require,$conn){

    if (!isset($id) || empty($id)) {
        if (!isset($require) || empty($require)) {
            $query = "SELECT * FROM banner_details";
        } else {
            $query = "SELECT * FROM banner_details ";
        }
    } else {
        if (!isset($require) || empty($require)) {
              $query = "SELECT * FROM banner_details WHERE id ='".cleanQueryParameter($id,$conn)."'";
        } else {
               $query = "SELECT * FROM banner_details WHERE id ='".cleanQueryParameter($id,$conn)."' AND status='Active'";
        }
    }
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Banner content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}



/***************************************************************************************************
    @ function name: getPressDetailsById();

    @ purpose: to fetch Press Details By Id
 **************************************************************************************************/

function getBannerDetailsById($id,$conn){
  $query = "SELECT * FROM banner_details WHERE p_id ='".cleanQueryParameter($id,$conn)."' ORDER BY `id` DESC ";
    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $res = array();
        while ($row = mysqli_fetch_assoc($queryResult["dbResource"]))
            $res["response"][] = $row;
        $returnArr = setErrorStack($queryResult,-1,"Successfully fetched Press content",$res);
    }else{
        $returnArr = setErrorStack($queryResult,28);
    }
    return $returnArr;
}


/**************************************************************************************************
    @ Function Name : removePress()
    @ Purpose : Deleteing Press Details from database and Image from folder
***************************************************************************************************/



  function removeBanner($Id,$conn) {
      $returnArr = array();
       $query = "SELECT * FROM banner_details WHERE id ='".cleanQueryParameter($Id,$conn)."'";
      $queryResult = runQuery($query, $conn);
      $res = array();
      while ($row = mysqli_fetch_assoc($queryResult["dbResource"])) {
          $res["response"][] = $row;
      }
       $query1 = "DELETE FROM banner_details WHERE id = '".cleanQueryParameter($Id,$conn)."'";
      $queryResult1 = runQuery($query1,$conn);
      if (noError($queryResult1)) {
          unlink("../uploads/".$res["response"][0]["banner1"]);
          unlink("../uploads/".$res["response"][0]["banner2"]);
          unlink("../uploads/".$res["response"][0]["banner3"]);
          unlink("../uploads/".$res["response"][0]["banner4"]);
          unlink("../uploads/".$res["response"][0]["banner5"]);
          $returnArr = setErrorStack($queryResult1, -1, "Successfully deleted Banner content");
      } else {
          $returnArr = setErrorStack($queryResult1,36);
      }
      return $returnArr;
  }
?>
