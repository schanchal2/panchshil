<?php
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/businessSubCatagoryModel.php';

/* Fetching all values form Posted Form*/

$businessId                 = cleanXSS(urldecode($_POST['businessId']));
$businessCatId              = cleanXSS(urldecode($_POST['businessCatId']));
$id                         = cleanXSS(urldecode($_POST['id']));
$businessSubCatName         = cleanXSS(urldecode($_POST['sub_cat_name']));
$businessSubCatNumber       = cleanXSS(urldecode($_POST['sub_cat_number']));
$businessSubCatEmail        = cleanXSS(urldecode($_POST['sub_cat_email']));
$businessSubCatAddress      = cleanXSS(urldecode($_POST['sub_cat_address']));

//printArr($_POST);
/* Declairing target folder to upload image and returning array */

$returnArr                  = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type 
        @ if method is update then Add new Business Details
        @ Else delete Business 
    */
    if ($_POST['method'] == "update") {
            $updateBusinessSubCatDetails = updateBusinessSubCatDetails($businessId,$businessCatId,$id,$businessSubCatName, $businessSubCatNumber, $businessSubCatEmail, $businessSubCatAddress, $conn);

            //printArr($updateBusinessSubCatDetails);

            /* Deleting Previous image if updated new Image*/
            if (noError($updateBusinessSubCatDetails)) {
                $returnArr = $updateBusinessSubCatDetails;
            } else {
                $returnArr = $updateBusinessSubCatDetails;
            }
    } elseif ($_POST['method'] == "delete") {
        $id = cleanXSS(urldecode($_POST['id']));
        $removeBusinessSubCat = removeBusinessSubCat($id,$conn);
        if (noError($removeBusinessSubCat)) {
            $returnArr = $removeBusinessSubCat;
        } else {
            $returnArr = $removeBusinessSubCat;
        }
    }
}

echo json_encode($returnArr);

?>