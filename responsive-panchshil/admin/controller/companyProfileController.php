<?php

/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/companyProfileModle.php';

/* Fetching all values form Posted Form*/
$id                         = cleanXSS(urldecode($_POST['id']));
$company_image_url          = cleanXSS(urldecode($_POST['image']));
$long_url                   = cleanXSS(urldecode($_POST['long_url']));
$company_profile_image_url  = cleanXSS(urldecode($_POST['company_profile_image_url']));
$name                       = cleanXSS(urldecode($_POST['name']));
$designation                = cleanXSS(urldecode($_POST['designation']));
$companyForwrdUrlId         = cleanXSS(urldecode($_POST['compProfileId']));

/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/companyProfile/";
$checkArr1                  = array();
$returnArr                  = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName, $dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */
    if ($_POST['method'] == "update") {

        /* To upload Color and Black white image */
        foreach ($_FILES as $key => $file) {
            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {

                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'companyProfile/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;
            }
        }
        /* Inserting Or Updating Business Details in DB */
        if (!isset($checkArr1['company_image']['name']) || empty($checkArr1['company_image']['name'])){
            $image = $company_image_url;

        } else {
            $image = $checkArr1['company_image']['name'];
        }
        if (!isset($checkArr1['company_profile_image']['name']) || empty($checkArr1['company_profile_image']['name'])) {
            $company_profile_image = $company_profile_image_url;

        } else {
            $company_profile_image = $checkArr1['company_profile_image']['name'];
        }

        if (!isset( $checkArr1['company_profile_image']['error'])) {
            $updateCompanyProfile = updateCompanyProfile($id, $companyForwrdUrlId, $image, $long_url, $company_profile_image, $name, $designation, $conn);
            /* Deleting Previous image if updated new Image*/
            if (noError($updateCompanyProfile)) {
                if (isset($checkArr1['company_image']['name'])) {
                    unlink('../uploads/'.$company_image_url);
                }
                if (isset($checkArr1['company_profile_image']['name'])) {
                    unlink('../uploads/'.$company_profile_image_url);
                }
                $returnArr = $updateCompanyProfile;
            }
        } else {
            $returnArr = setErrorStack($returnArr, 17);
        }

    } elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removeCompanyProfile = removeCompanyProfile($id,$conn);
        if (noError($removeCompanyProfile)) {
            $returnArr = $removeCompanyProfile;
        } else {
            $returnArr = $removeCompanyProfile;
        }
    }
}
echo json_encode($returnArr);
?>
