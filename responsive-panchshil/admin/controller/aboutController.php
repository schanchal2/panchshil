<?php

/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/aboutContentModel.php';

/* Fetching all values form Posted Form*/
$id                         = cleanXSS(urldecode($_POST['id']));
$status                     = cleanXSS(urldecode($_POST['aboutStatus']));
$imageUrl                   = cleanXSS(urldecode($_POST['imageUrl']));
$boxImageUrl                = cleanXSS(urldecode($_POST['boximageUrl']));
$name                       = cleanXSS(urldecode($_POST['about-name']));
$shortDesc                  = cleanXSS(urldecode($_POST['about-shortDesc']));
$longDesc                   = cleanXSS(urldecode($_POST['about-longDesc']));
$page_type                   = cleanXSS(urldecode($_POST['page_type']));


/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/aboutImages/";
$checkArr1                  = array();
$returnArr                  = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */
    if ($_POST['method'] == "update") {
        /* To upload Color and Black white image */
        foreach ($_FILES as $key => $file) {
            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);


                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'aboutImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;
            }
        }
        
        /* Inserting Or Updating DB */
        if (!isset($checkArr1['about-feature-image']['name']) || empty($checkArr1['about-feature-image']['name'])) {
                    $image = $imageUrl;
        } else {
                    $image = $checkArr1['about-feature-image']['name'];
        }
        if (!isset($checkArr1['about-box-image']['name']) || empty($checkArr1['about-box-image']['name'])) {
                    $boxImage = $boxImageUrl;
        } else {
                    $boxImage = $checkArr1['about-box-image']['name'];
        }

        if (!isset( $checkArr1['about-feature-image']['error'])) {
            $updateAboutDetails = updateAboutDetails($id,$status,$image,$boxImage,$name,$shortDesc,$longDesc,$page_type,$conn);
            /* Deleting Previous image if updated new Image*/
            if (noError($updateAboutDetails)) {
                if (isset($checkArr1['about-feature-image']['name'])) {
                    unlink('../uploads/'.$imageUrl);
                }
                $returnArr = $updateAboutDetails;
            }
        } else {


            $returnArr = setErrorStack($returnArr, 17);
        }

    } elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removeAbout = removeAbout($id,$conn);
        if (noError($removeAbout)) {
            $returnArr = $removeAbout;
        } else {
            $returnArr = $removeAbout;
        }
    }
}
echo json_encode($returnArr);
?>
