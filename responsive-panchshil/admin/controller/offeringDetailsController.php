<?php
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/offeringDetailsModel.php';


/* Fetching all values form Posted Form*/
$id                             = cleanXSS(urldecode($_POST['offeringId']));
$businessId                     = cleanXSS(urldecode($_POST['bussinessId']));
$page_type                     = cleanXSS(urldecode($_POST['page_type']));
$imageUrl                       = cleanXSS(urldecode($_POST['offering-categories-feature-image-url']));
$footerImageUrl                 = cleanXSS(urldecode($_POST['offering-categories-footer-image-url']));
$boxImageUrl                    = cleanXSS(urldecode($_POST['offering-categories-big-image-url']));
$name                           = cleanXSS(urldecode($_POST['offering-name']));
$status                         = cleanXSS(urldecode($_POST['offering-category-status']));
$shortDesc                      = cleanXSS(urldecode($_POST['offering-shortDesc']));
$longDesc                       = cleanXSS(urldecode($_POST['offering-longDesc']));

/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/offeringImages/";
$checkArr1                  = array();
$returnArr                  = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new offering Details
        @ Else delete offering
    */
    if ($_POST['method'] == "update") {

        /* To upload Color and Black white image */
        // printArr($_FILES);die();
        foreach ($_FILES as $key => $file) {

            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'offeringImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;

            }
        }

       // printArr($checkArr1);die();
        /* Inserting Or Updating offering Details in DB */
        if (!isset($checkArr1['offering-categories-feature-image']['name']) || empty($checkArr1['offering-categories-feature-image']['name'])) {
            $featureImage = $imageUrl;
        } else {
            $featureImage = $checkArr1['offering-categories-feature-image']['name'];
        }

        if (!isset($checkArr1['offering-categories-big-image']['name']) || empty($checkArr1['offering-categories-big-image']['name'])) {
            $bigImage = $imageUrl;
        } else {
            $bigImage = $checkArr1['offering-categories-big-image']['name'];
        }

        if (!isset($checkArr1['offering-categories-footer-image']['name']) || empty($checkArr1['offering-categories-footer-image']['name'])) {
            $footerImage = $imageUrl;
        } else {
            $footerImage = $checkArr1['offering-categories-footer-image']['name'];
        }

        if (!isset( $checkArr1['offering-categories-feature-image']['error']) && !isset( $checkArr1['offering-categories-big-image']['error']) && !isset( $checkArr1['offering-categories-footer-image']['error'])) {
            $updateOfferingInfo = updateOfferingDetails($id, $businessId, $featureImage, $bigImage, $footerImage, $status, $name, $shortDesc, $longDesc,$page_type, $conn);
            /* Deleting Previous image if updated new Image*/
            if (noError($updateOfferingInfo)) {
                if (isset($checkArr1['offering-categories-feature-image']['name']) || !empty($checkArr1['offering-categories-feature-image']['name'])) {
                    unlink('../uploads/'.$imageUrl);
                }
                if (isset($checkArr1['offering-categories-big-image']['name']) || !empty($checkArr1['offering-categories-big-image']['name'])) {
                    unlink('../uploads/'.$boxImageUrl);
                }
                if (isset($checkArr1['offering-categories-footer-image']['name']) || !empty($checkArr1['offering-categories-footer-image']['name'])) {
                    unlink('../uploads/'.$footerImageUrl);
                }

                $returnArr = $updateOfferingInfo;
            } else {
                $returnArr = $updateOfferingInfo;
            }
        } else {
            $errMsg = $checkArr1['offering-categories-big-image']['name']." , ".$checkArr1['offering-categories-feature-image']['name']." AND ".$checkArr1['offering-categories-footer-image']['name'];
            $returnArr = setErrorStack($returnArr, 17, $errMsg);
        }

    } else if ($_POST['method'] == "delete") {
        $DeleteofferingCatId = cleanXSS(urldecode($_POST['DeleteofferingCatId']));
        $removeofferingCat = removeofferingCat($DeleteofferingCatId,$conn);
        if (noError($removeofferingCat)) {
            $returnArr = $removeofferingCat;
        } else {
            $returnArr = $removeofferingCat;
        }
    }
}

echo json_encode($returnArr);

?>
