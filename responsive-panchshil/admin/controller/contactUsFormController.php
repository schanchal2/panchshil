<?php

//prepare for request
//start session
session_start();


//include necessary files
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
require_once '../model/contactUsFormModel.php';

/* Declairing Required Variables or Array */
$returnArr = array();
$contactArray = array();

/* 
	@ Fetching all values 
	@ Storing all fetched Values in one Associative Array.
*/
$contactArray['reason']  				= cleanXSS(urldecode($_POST['reason']));
$contactArray['firstName']	    		= cleanXSS(urldecode($_POST['firstName'])); 
$contactArray['lastName']				= cleanXSS(urldecode($_POST['lastName']));
$contactArray['phone']					= cleanXSS(urldecode($_POST['phone']));
$contactArray['email']					= cleanXSS(urldecode($_POST['email']));
$contactArray['address']				= cleanXSS(urldecode($_POST['address']));
$contactArray['country']				= cleanXSS(urldecode($_POST['country']));
$contactArray['city']					= cleanXSS(urldecode($_POST['state']));
$contactArray['profession']				= cleanXSS(urldecode($_POST['profession-name']));
$contactArray['company_name']			= cleanXSS(urldecode($_POST['company_name']));
$contactArray['designation']			= cleanXSS(urldecode($_POST['designation']));
$contactArray['comment']				= cleanXSS(urldecode($_POST['comment']));

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
	$conn = $conn["conn"];

	/* Checking Inserting or deleting contact Form */
	if ($_POST['method'] == "update") {		
		$contactFormSubmit = addContactUsDetails($contactArray,$conn);
		if (noError($contactFormSubmit)) {
			$returnArr = $contactFormSubmit;
		} else {
			$returnArr = $contactFormSubmit;
		}		
	} elseif($_POST['method'] == "delete"){
			$id = cleanXSS(urldecode($_POST['id']));
			$removeContactUsDetails = removeContactUSDetails($id,$conn);
		if (noError($removeContactUsDetails)) {
			$returnArr = $removeContactUsDetails;
		} else {
			$returnArr = $removeContactUsDetails;
		}
	}
}
 echo json_encode($returnArr);

?>