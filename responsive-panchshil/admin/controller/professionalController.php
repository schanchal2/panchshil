<?php
/*Start Session to display error on view page */
session_start();


/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once "../model/professionModel.php";


/* Fetching all values */
$id			    = cleanXSS(urldecode($_GET['id']));
$title			= cleanXSS(urldecode($_POST['profession-name']));

//echo $title;
//$checkArr1          = array();
$returnArr          = array();

$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {

    $conn = $conn["conn"];
     
        if ($_POST['method'] == "update") {
            $profession = addProfessionDetails($id, $title, $conn);
            
            if(noError($profession))
            {
                $returnArr = $profession;              
            }
            else
            {
                $returnArr = $profession;          
            }
        } 
        // elseif($_POST['method'] == "delete"){  
        //     $id = cleanXSS(urldecode($_POST["DeleteReasonsid"]));
        //     $Removereasons = Removereasons($id,$conn);
           
        //     if(noError($Removereasons)){
        //         $returnArr = $Removereasons;
        //     }else{
        //         $returnArr = $Removereasons;
        //     }
        // }
    }
?>