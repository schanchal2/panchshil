<?php
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/careerDetailsModel.php';

/* Fetching all values form Posted Form*/
$id                         = cleanXSS(urldecode($_POST['id']));
$career_image               = cleanXSS(urldecode($_POST['career-image-url']));
$career_image1              = cleanXSS(urldecode($_POST['career-image1-url1']));
$career_name                = cleanXSS(urldecode($_POST['career_name']));
$career_description         = cleanXSS(urldecode($_POST['career_description']));
$career_designation         = cleanXSS(urldecode($_POST['career_designation']));


/* Declairing target folder to upload image and returning array */
$targetDir              = "../uploads/careerImages/";
$checkArr1                  = array();
$returnArr                  = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */
    if ($_POST['method'] == "update") {

        /* To upload Color and Black white image */
        
        foreach ($_FILES as $key => $file) {

            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        // printArr($uploadStatus);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'careerImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;
                
            }
        }
       
        /* Inserting Or Updating Business Details in DB */
        
        if (!isset($checkArr1['career-image']['name']) || empty($checkArr1['career-image']['name'])){
            $careerImage = $career_image;
        } else {
            $careerImage = $checkArr1['career-image']['name'];
        }

        if (!isset($checkArr1['career-image1']['name']) || empty($checkArr1['career-image1']['name'])) {
            $careerImage1 = $career_image1;
        } else {
            $careerImage1 = $checkArr1['career-image1']['name'];
        }

        if (!isset( $checkArr1['career-image1']['error'])) {
            $updateCareerDetails = updateCareerDetails($id, $careerImage, $career_description, $careerImage1, $career_name, $career_designation,$conn);

           // printArr($updateCareerDetails);

            /* Deleting image if updated */
            if (noError($updateCareerDetails)) {
                if (isset($checkArr1['career-image']['name'])) {
                    unlink('../uploads/'.$career_image);
                }
                if (isset($checkArr1['career-image1']['name'])) {
                    unlink('../uploads/'.$career_image1);
                }
                $returnArr = $updateCareerDetails;
            }
        } else {
            $returnArr = setErrorStack($returnArr, 17);
        }

    } elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removeCareer = removeCareer($id,$conn);
        if (noError($removeCareer)) {
            $returnArr = $removeCareer;
        } else {
            $returnArr = $removeCareer;
        }
    }
}
echo json_encode($returnArr);

?>