<?php
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/offeringDetailsModel.php';


/* Fetching all values form Posted Form*/
$businessCatId                 = cleanXSS(urldecode($_POST['catId']));
$bussinessId                   = cleanXSS(urldecode($_POST['bussinessId']));
$businessCatStatus             = cleanXSS(urldecode($_POST['business-category-status']));
$businessCatBigImageUrl        = cleanXSS(urldecode($_POST['business-categories-big-image-url']));
$businessCatFooterImageUrl     = cleanXSS(urldecode($_POST['business-categories-footer-image-url']));
$businessCatFeatureImageUrl    = cleanXSS(urldecode($_POST['business-categories-feature-image-url']));
$businessCatName               = cleanXSS(urldecode($_POST['business-categories-name']));
$businessCatShortDesc          = cleanXSS(urldecode($_POST['business-categories-short-desc']));
$businessCatFullDesc           = cleanXSS(urldecode($_POST['business-categories-full-desc']));

/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/businessCatImages/";
$checkArr1                  = array();
$returnArr                  = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */
    if ($_POST['method'] == "update") {

        /* To upload Color and Black white image */
        // printArr($_FILES);die();
        foreach ($_FILES as $key => $file) {

            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'businessCatImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;

            }
        }


        /* Inserting Or Updating Business Details in DB */
        if (!isset($checkArr1['business-categories-big-image']['name']) || empty($checkArr1['business-categories-big-image']['name'])) {
            $businessCatBigImage = $businessCatBigImageUrl;
        } else {
            $businessCatBigImage = $checkArr1['business-categories-big-image']['name'];
        }

        //////////////////////////////////////////////////////////////////////////////
        if (!isset($checkArr1['business-categories-footer-image']['name']) || empty($checkArr1['business-categories-footer-image']['name'])) {
            $businessCatFooterImage = $businessCatFooterImageUrl;
        } else {
            $businessCatFooterImage = $checkArr1['business-categories-footer-image']['name'];
        }

        //////////////////////////////////////////////////////////////////////////////
        if (!isset($checkArr1['business-categories-feature-image']['name']) || empty($checkArr1['business-categories-feature-image']['name'])) {
            $businessCatFeatureImage = $businessCatFeatureImageUrl;
        } else {
            $businessCatFeatureImage = $checkArr1['business-categories-feature-image']['name'];
        }

        if (!isset( $checkArr1['business-categories-big-image']['error']) && !isset( $checkArr1['business-categories-feature-image']['error']) && !isset( $checkArr1['business-categories-footer-image']['error'])) {

            $updateBusinessDetails = updateBusinessCatDetails($businessCatId, $bussinessId, $businessCatName, $businessCatStatus, $businessCatBigImage, $businessCatFeatureImage, $businessCatFooterImage, $businessCatShortDesc, $businessCatFullDesc, $conn);
            /* Deleting Previous image if updated new Image*/
            if (noError($updateBusinessDetails)) {
                if (isset($checkArr1['business-categories-big-image']['name']) || !empty($checkArr1['business-categories-big-image']['name'])) {
                    unlink('../uploads/'.$businessCatBigImageUrl);
                }
                if (isset($checkArr1['business-categories-feature-image']['name']) || !empty($checkArr1['business-categories-feature-image']['name'])) {
                    unlink('../uploads/'.$businessCatFeatureImageUrl);
                }
                if (isset($checkArr1['business-categories-footer-image']['name']) || !empty($checkArr1['business-categories-footer-image']['name'])) {
                    unlink('../uploads/'.$businessCatFooterImageUrl);
                }

                $returnArr = $updateBusinessDetails;
            } else {
                $returnArr = $updateBusinessDetails;
            }
        } else {
            $errMsg = $checkArr1['business-categories-big-image']['name']." , ".$checkArr1['business-categories-feature-image']['name']." AND ".$checkArr1['business-categories-footer-image']['name'];
            $returnArr = setErrorStack($returnArr, 17, $errMsg);
        }

    } else if ($_POST['method'] == "delete") {
        $DeleteBusinessCatId = cleanXSS(urldecode($_POST['id']));
        $removeBusinessCat = removeBusinessCatDelete($DeleteBusinessCatId,$conn);
        if (noError($removeBusinessCat)) {
            $returnArr = $removeBusinessCat;
        } else {
            $returnArr = $removeBusinessCat;
        }
    }else if ($_POST['method'] == "status") {
        $DeleteBusinessCatId = cleanXSS(urldecode($_POST['id']));
        $removeBusinessCat = removeBusinessCatStatus($DeleteBusinessCatId,$conn);
        if (noError($removeBusinessCat)) {
            $returnArr = $removeBusinessCat;
        } else {
            $returnArr = $removeBusinessCat;
        }
    }
}

echo json_encode($returnArr);

?>
