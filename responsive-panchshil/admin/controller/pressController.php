<?php
/*press Controller Created by kavita*/
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/pressContentModel.php';

/* Fetching all values form Posted Form*/
$id                         = cleanXSS(urldecode($_POST['id']));
$imageUrl                   = cleanXSS(urldecode($_POST['imageUrl']));
$imageUrl1                  = cleanXSS(urldecode($_POST['imageUrl1']));
$name                       = cleanXSS(urldecode($_POST['press-name']));
$sourcename                 = cleanXSS(urldecode($_POST['press-source-name']));
$longDesc                   = cleanXSS(urldecode($_POST['press-longDesc']));
$press_date                 = cleanXSS(urldecode($_POST['press-date']));
$pdf_url                    = cleanXSS(urldecode($_POST['pdfUrl']));
$video_url1                  = cleanXSS(urldecode($_POST['video-url']));
$link_url                   = cleanXSS(urldecode($_POST['link-url']));
$press_status                   = cleanXSS(urldecode($_POST['press_status']));
$media_id                  = cleanXSS(urldecode($_POST['media_id']));
$media_type                  = cleanXSS(urldecode($_POST['media_type']));
$file = $_FILES['pdf-url'];
$video_url=str_replace("watch?v=","embed/","$video_url1");

/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/pressImages/";
$targetDirPDF                  = "../uploads/pressImages/";
$checkArr1                  = array();
$returnArr                  = array();
/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */
    if ($_POST['method'] == "update") {



      if($file["error"] == 0 ){
          if(end(explode(".",basename($file["name"]))) == "pdf"){
              //9999999

              if($file["size"] < 9999999) {
                  $checkArr2 = array();
                  $fileName = uniqid() . "." . end(explode(".", basename($file["name"])));
                  //$fileName = basename($file["name"]);
                  $targetFilePDF = $targetDirPDF . $fileName;
                  $uploadStatusPDF = uploadPDF($file, $type, $targetFilePDF);
                  if (noError($uploadStatusPDF)) {
                      $checkArr2["success"] = "Successfully uploaded pdf file";
                      $checkArr2["PDFfile"] = "pressImages/" . $fileName;
                  } else {
                      $checkArr2["error"] = $uploadStatusPDF["errMsg"];
                  }
              }else{
                  $checkArr2["error"] = "Please upload file up to size 10 MB only";
              }
          }else{
              $checkArr2["error"] = "Please submit pdf file only!!!";
          }
         //echo $checkArr["PDFfile"];
      }



        /* To upload Color and Black white image */
        foreach ($_FILES as $key => $file) {

            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'pressImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;

            }
        }


        /* Inserting Or Updating Client Details in DB */
        if (!isset($checkArr1['press-feature-image']['name']) || empty($checkArr1['press-feature-image']['name'])) {
            $image = $imageUrl;
        } else {
            $image = $checkArr1['press-feature-image']['name'];
        }
        if (!isset($checkArr1['press-feature-image1']['name']) || empty($checkArr1['press-feature-image1']['name'])) {
            $image1 = $imageUrl1;
        } else {
            $image1 = $checkArr1['press-feature-image1']['name'];
        }

        if (!isset($checkArr1['press-feature-image1']['name']) || empty($checkArr1['press-feature-image1']['name'])) {
            $image1 = $imageUrl1;
        } else {
            $image1 = $checkArr1['press-feature-image1']['name'];
        }

        if (!isset( $checkArr1['press-feature-image']['error']) && !isset( $checkArr1['press-feature-image1']['error'])) {
            $updateClientDetails = updatePressDetails($id,$media_id,$image,$image1,$name,$sourcename,$longDesc,$press_date,$checkArr2['PDFfile'],$video_url,$link_url,$press_status,$media_type,$conn);


            /* Deleting image if updated */
            if (noError($updateClientDetails)) {
                if (isset($checkArr1['press-feature-image']['name']) || !empty($checkArr1['press-feature-image']['name'])) {
                    unlink('../uploads/'.$clientColorImageUrl);
                }
                if (isset($checkArr1['press-feature-image1']['name']) || !empty($checkArr1['press-feature-image1']['name'])) {
                    unlink('../uploads/'.$clientBlackImageUrl);
                }
                $returnArr = $updateClientDetails;
            } else {
                $returnArr = $updateClientDetails;
            }
        } else {
            $errMsg = $checkArr1['press-feature-image']['name']." AND ".$checkArr1['press-feature-image1']['name'];
            $returnArr = setErrorStack($returnArr, 17, $errMsg);
        }

    }

    elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removePress = removePress($id,$conn);
        if (noError($removePress)) {
            $returnArr = $removePress;
        } else {
            $returnArr = $removePress;
        }
    }
}
echo json_encode($returnArr);
?>
