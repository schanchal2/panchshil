<?php

/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/careerDetailsModel.php';

/* Fetching all values form Posted Form*/
$id                     = cleanXSS(urldecode($_POST['id']));
$career_cat_image1      = cleanXSS(urldecode($_POST['career_cat_image_url']));
$career_cat_desc        = cleanXSS(urldecode($_POST['career_cat_desc']));
$career_cat_name        = cleanXSS(urldecode($_POST['career_cat_name']));
$career_cat_design      = cleanXSS(urldecode($_POST['career_cat_design']));
$career_id              = cleanXSS(urldecode($_POST['career_id']));

/* Declairing target folder to upload image and returning array */
$targetDir              = "../uploads/careerCaImages/";
$checkArr1              = array();
$returnArr              = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */
    if ($_POST['method'] == "update") {

        /* To upload Color and Black white image */
        foreach ($_FILES as $key => $file) {

            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'careerCaImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;
            }
        }
        //printArr($checkArr1);
        /* Inserting Or Updating Business Details in DB */
        
        if (!isset($checkArr1['career_cat_image']['name']) || empty($checkArr1['career_cat_image']['name'])) {
                    $image = $career_cat_image1;

        } else {
                    $image = $checkArr1['career_cat_image']['name'];
        }

        if (!isset( $checkArr1['career_cat_image']['error'])) {
            $updateCareerCatDetails = updateCareerCatDetails($id,$career_cat_desc,$career_cat_name,$career_cat_design,$image,$career_id,$conn);
            /* Deleting Previous image if updated new Image*/
            if (noError($updateCareerCatDetails)) {
               
                if (isset($checkArr1['career_cat_image']['name'])) {
                    unlink('../uploads/'.$career_cat_image1);
                }
                $returnArr = $updateCareerCatDetails;
            }
        } else {
            $returnArr = setErrorStack($returnArr, 17);
        }

    } elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removeCareerCat = removeCareerCat($id,$conn);
        if (noError($removeCareerCat)) {
            $returnArr = $removeCareerCat;
        } else {
            $returnArr = $removeCareerCat;
        }
    }
}
echo json_encode($returnArr);
?>
