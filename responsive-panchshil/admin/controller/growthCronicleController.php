<?php
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/growthCronicleModel.php';

/* Fetching all values form Posted Form*/
$growthId            = cleanXSS(urldecode($_POST['growthId']));
$growthImageUrl      = cleanXSS(urldecode($_POST['growthImageUrl']));
$growthYear          = cleanXSS(urldecode($_POST['growthYear']));
$growthDescription   = cleanXSS(urldecode($_POST['description1']));
$growthUrlId         = cleanXSS(urldecode($_POST['growthUrlId']));

/* Declairing target folder to upload image and returning array */
$targetDir           = "../uploads/growthImages/";
$checkArr1           = array();
$returnArr           = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Client Details
        @ Else delete Client
    */
    if ($_POST['method'] == "update") {

        /* To upload Color and Black white image */
        foreach ($_FILES as $key => $file) {

            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);

                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'growthImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }

                }
                $checkArr1[$key] = $checkArr;

            }
        }


        /* Inserting Or Updating Client Details in DB */
        if (!isset($checkArr1['growthFeatureImage']['name']) || empty($checkArr1['growthFeatureImage']['name'])) {
            $growthColorImage = $growthImageUrl;
        } else {
            $growthColorImage = $checkArr1['growthFeatureImage']['name'];
        }


        if (!isset( $checkArr1['growthFeatureImage']['error'])) {
            $updateGrowthDetails = updateGrowthDetails($growthId, $growthUrlId, $growthColorImage, $growthYear, $growthDescription, $conn);
            /* Deleting image if updated */

            if (noError($updateGrowthDetails)) {
                if (isset($checkArr1['growthFeatureImage']['name']) || !empty($checkArr1['growthFeatureImage']['name'])) {
                    unlink('../uploads/'.$growthImageUrl);
                }
                $returnArr = $updateGrowthDetails;
            } else {
                $returnArr = $updateGrowthDetails;
            }
        } else {
            $errMsg = $checkArr1['growthFeatureImage']['name'];
            $returnArr = setErrorStack($returnArr, 17, $errMsg);
        }

    } elseif ($_POST['method'] == "delete") {
        $DeleteGrowthId = cleanXSS(urldecode($_POST['deleteGrowthId']));
        $DeleteGrowthUrlId = cleanXSS(urldecode($_POST['deleteGrowthUrlId']));
        $removeGrowth = removeGrowth($DeleteGrowthId,$DeleteGrowthUrlId,$conn);
        if (noError($removeGrowth)) {
            $returnArr = $removeGrowth;
        } else {
            $returnArr = $removeGrowth;
        }
    }
}

echo json_encode($returnArr);

?>
