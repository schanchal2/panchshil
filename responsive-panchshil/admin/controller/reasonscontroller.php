<?php
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once "../utilities/config.php";
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once "../model/ReasonsModel.php";



/* Fetching all values */
//$id          = cleanXSS(urldecode($_POST['id']));
$number      = cleanXSS(urldecode($_POST['number']));
 //echo $number;
$reasons     = cleanXSS(urldecode($_POST['reasons']));
// echo $reasons;


$checkArr1          = array();
$returnArr          = array();


$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);


if (noError($conn)) {

    $conn = $conn["conn"];
     
        if ($_POST['method'] == "update") {
            $updatereasons = addupdatereasons($number, $reasons, $conn);
            if(noError($updatereasons))
            {
                $returnArr = $updatereasons;              
            }
            else
            {
                $returnArr = $updatereasons;          
            }
        } elseif($_POST['method'] == "delete"){  
            $id = cleanXSS(urldecode($_POST["DeleteReasonsid"]));
            $Removereasons = Removereasons($id,$conn);
           
            if(noError($Removereasons)){
                $returnArr = $Removereasons;
            }else{
                $returnArr = $Removereasons;
            }
        }
    }else{
        $returnArr = $conn;
    }

   // $returnArr = {'number':$number, 'reasons':$reasons}

    echo json_encode($returnArr);
?>

