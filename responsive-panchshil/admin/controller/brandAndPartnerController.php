<?php

/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/brandAndPartnerContentModel.php';


/* Fetching all values form Posted Form*/
$id                         = cleanXSS(urldecode($_POST['id']));
$imageUrl                   = cleanXSS(urldecode($_POST['imageUrl']));
$name                       = cleanXSS(urldecode($_POST['brand-name']));
$designation                 = cleanXSS(urldecode($_POST['brand-source-name']));
$longDesc                   = cleanXSS(urldecode($_POST['brand-longDesc']));
$alliance_id                  = cleanXSS(urldecode($_POST['alliance_id']));

/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/TestimonialImages/";
$checkArr1                  = array();
$returnArr                  = array();
/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */
    if ($_POST['method'] == "update") {





        /* To upload Color and Black white image */
        foreach ($_FILES as $key => $file) {

            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'TestimonialImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;


            }
        }


        /* Inserting Or Updating Client Details in DB */
        if (!isset($checkArr1['brand-feature-image']['name']) || empty($checkArr1['brand-feature-image']['name'])) {
            $image = $imageUrl;
        } else {
            $image = $checkArr1['brand-feature-image']['name'];
          //  printArr($image);
        }


        if (!isset( $checkArr1['brand-feature-image']['error'])) {
          //printArr($image);
            $updateClientDetails = updateBrandAndPartnerDetails($id,$alliance_id,$image,$name,$designation,$longDesc,$conn);


            /* Deleting image if updated */
            if (noError($updateClientDetails)) {
                if (isset($checkArr1['brand-feature-image']['name']) || !empty($checkArr1['brand-feature-image']['name'])) {
                    unlink('../uploads/'.$clientColorImageUrl);
                }

                $returnArr = $updateClientDetails;
            } else {
                $returnArr = $updateClientDetails;
            }
        } else {
            $errMsg = $checkArr1['brand-feature-image']['name'];
            $returnArr = setErrorStack($returnArr, 17, $errMsg);
        }

    }

    elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removebrand = removeBrandAndPartner($id,$conn);
        if (noError($removebrand)) {
            $returnArr = $removebrand;
        } else {
            $returnArr = $removebrand;
        }
    }
}
echo json_encode($returnArr);
?>
