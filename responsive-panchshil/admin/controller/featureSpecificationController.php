<?php
/*banner Controller Created by kavita*/
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/featureSpecificationContentModel.php';

/* Fetching all values form Posted Form*/
$id                         = cleanXSS(urldecode($_POST['id']));
$o_id                        = cleanXSS(urldecode($_POST['o_id']));
$p_id                        = cleanXSS(urldecode($_POST['p_id']));
// echo $o_id;
$number = count($_POST["name"]);
/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/galleryImages/";
$checkArr1                  = array();
$returnArr                  = array();
$galaryImagesArray          = array();
/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];
        if ($_POST['method'] == "update") {

          /* To upload Color and Black white image */
      //  printArr($_FILES);
          foreach ($_FILES as $key => $file) {
              if ($file['error'] == 0) {
                  $checkArr = array();
                  list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);
                    //printArr($checkArr);
                  /* To check error in File */
                  if (!isset($checkArr['error'])) {
                      $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                      $targetFile = $targetDir . $fileName;

                      /* To check folder Existance */
                      $folderExist = folderPresenceCheck($targetDir);
                      if (noError($folderExist)) {
                          /* To Upload Image in Folder */
                          $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                          if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                              $checkArr['success']    = $uploadStatus['errMsg'];
                              $checkArr['name']       = 'galleryImages/'.$fileName;
                              $checkArr['errCode']    = $uploadStatus['errCode'];
                              $galaryImagesArray[]    =  $checkArr['name'];
                          } else {
                              $checkArr['error']      = $uploadStatus['errMsg'];
                              $checkArr['name']       = $checkArr['name'];
                          }
                      } else {
                          $checkArr['error']      = "Directed Folder Mismatched";
                          $checkArr['name']       = $file['name'];
                      }
                  }
                  $checkArr1[$key] = $checkArr;

              }
          }
$b=$galaryImagesArray;
    if($number > 0)
    {
         for($i=0; $i<$number; $i++)
         {
             if(trim($_POST["name"][$i] != ''))
              {
                  $b=$galaryImagesArray;
                   $a[]= $_POST["name"][$i]; //echo "<br>";
                   $updateClientDetails = updatebannerDetails($id,$o_id,$p_id,$_POST["countPos"][$i],$_POST["name"][$i],$b[$i],$conn);
                   if (noError($updateClientDetails)) {
                       $returnArr = $updateClientDetails;
                   }
              }
         }
    }
    else {
        $returnArr = setErrorStack($returnArr, 17);
    }
  }

}
echo json_encode($returnArr);
?>
