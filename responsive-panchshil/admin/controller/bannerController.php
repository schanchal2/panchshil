<?php
/*banner Controller Created by kavita*/
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/bannerContentModel.php';

/* Fetching all values form Posted Form*/
$id                         = cleanXSS(urldecode($_POST['id']));
$o_id                        = cleanXSS(urldecode($_POST['o_id']));
$p_id                        = cleanXSS(urldecode($_POST['p_id']));
$imageUrl                   = cleanXSS(urldecode($_POST['imageUrl']));
$imageUrl1                  = cleanXSS(urldecode($_POST['imageUrl1']));
$imageUrl2                  = cleanXSS(urldecode($_POST['imageUrl2']));
$imageUrl3                  = cleanXSS(urldecode($_POST['imageUrl3']));
$imageUrl4                  = cleanXSS(urldecode($_POST['imageUrl4']));

/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/bannerImages/";
$checkArr1                  = array();
$returnArr                  = array();
/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */
    if ($_POST['method'] == "update") {

        /* To upload Color and Black white image */
        foreach ($_FILES as $key => $file) {

            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'bannerImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;

            }
        }


        /* Inserting Or Updating Client Details in DB */
        if (!isset($checkArr1['banner-feature-image']['name']) || empty($checkArr1['banner-feature-image']['name'])) {
            $image = $imageUrl;
        } else {
            $image = $checkArr1['banner-feature-image']['name'];
        }

        if (!isset($checkArr1['banner-feature-image1']['name']) || empty($checkArr1['banner-feature-image1']['name'])) {
            $image1 = $imageUrl1;
        } else {
            $image1 = $checkArr1['banner-feature-image1']['name'];
        }

        if (!isset($checkArr1['banner-feature-image2']['name']) || empty($checkArr1['banner-feature-image2']['name'])) {
            $image2 = $imageUrl2;
        } else {
            $image2 = $checkArr1['banner-feature-image2']['name'];
        }


        if (!isset($checkArr1['banner-feature-image3']['name']) || empty($checkArr1['banner-feature-image3']['name'])) {
            $image3 = $imageUrl3;
        } else {
            $image3 = $checkArr1['banner-feature-image3']['name'];
        }

        if (!isset($checkArr1['banner-feature-image4']['name']) || empty($checkArr1['banner-feature-image4']['name'])) {
            $image4 = $imageUrl4;
        } else {
            $image4 = $checkArr1['banner-feature-image4']['name'];
        }


        if (!isset( $checkArr1['banner-feature-image']['error']) && !isset( $checkArr1['banner-feature-image1']['error'])) {
            $updateClientDetails = updatebannerDetails($id,$o_id,$p_id,$image,$image1,$image2,$image3,$image4,$conn);


            /* Deleting image if updated */
            if (noError($updateClientDetails)) {
                if (isset($checkArr1['banner-feature-image']['name']) || !empty($checkArr1['banner-feature-image']['name'])) {
                    unlink('../uploads/'.$clientColorImageUrl);
                }
                if (isset($checkArr1['banner-feature-image1']['name']) || !empty($checkArr1['banner-feature-image1']['name'])) {
                    unlink('../uploads/'.$clientBlackImageUrl);
                }
                $returnArr = $updateClientDetails;
            } else {
                $returnArr = $updateClientDetails;
            }
        } else {
            $errMsg = $checkArr1['banner-feature-image']['name']." AND ".$checkArr1['banner-feature-image1']['name'];
            $returnArr = setErrorStack($returnArr, 17, $errMsg);
        }

    }

    elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removeBanner = removeBanner($id,$conn);
        if (noError($removebanner)) {
            $returnArr = $removeBanner;
        } else {
            $returnArr = $removeBanner;
        }
    }
}
echo json_encode($returnArr);
?>
