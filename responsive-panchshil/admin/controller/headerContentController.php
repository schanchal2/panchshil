<?php
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once "../model/headerContentModel.php";

/* Fetching all values */
$pageName 			= cleanXSS(urldecode($_POST['pageName']));
$pageId 			= cleanXSS(urldecode($_POST['pageId']));
$pageImageUrl 		= cleanXSS(urldecode($_POST['feature-image-url']));
$pageDescTitle 		= cleanXSS(urldecode($_POST['top-desc-title']));
$pageDesc 			= cleanXSS(urldecode($_POST['top-desc']));
$pageKnowMoreUrl 	= cleanXSS(urldecode($_POST['know-more']));

/* Declairing target folder to upload image and returning array */
$targetDir 			= "../uploads/headerImages/";
$checkArr1 			= array();
$returnArr 			= array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
	$conn = $conn["conn"];

	/*	@ Checking Method Type 
		@ if method is update then Add new header content
		@ Else delete Content 
	*/
	if ($_POST['method'] == "update") {

		/* To upload Feature image */		
		//printArr($_FILES);
		foreach ($_FILES as $key => $file) {			
			if ($file['error'] == 0) {
				$checkArr = array();
				list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

				/* To check error in File */
				if (!isset($checkArr['error'])) {					
					$fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
					$targetFile = $targetDir . $fileName;

					/* To check folder Existance */
					$folderExist = folderPresenceCheck($targetDir);
					if (noError($folderExist)) {
						/* To Upload Image in Folder */
						$uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
						if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
							$checkArr['success'] 	= $uploadStatus['errMsg'];
							$checkArr['name'] 		= 'headerImages/'.$fileName;
							$checkArr['errCode'] 	= $uploadStatus['errCode'];
						} else {
							$checkArr['error'] 		= $uploadStatus['errMsg'];
							$checkArr['name'] 		= $file['name'];
						}
					} else {
						$checkArr['error'] 		= "Directed Folder Mismatched";
						$checkArr['name'] 		= $file['name'];
					}
					/* To Upload Image in Folder */
					
				}
				$checkArr1[$key] = $checkArr;
			}
		}

		/* Inserting Or Updating Header Content in DB */
		if (!isset($checkArr1['feature-image']['name']) || empty($checkArr1['feature-image']['name'])) {
			$featureImage = $pageImageUrl;
		} else {
			$featureImage = $checkArr1['feature-image']['name'];			
		}		
		//$featureImage = $checkArr1['feature-image']['name'];
		
		if (!isset( $checkArr1['feature-image']['error'])) {
			$updateHeaderContent = updateHeaderContent($pageId,$pageName,$featureImage,$pageDescTitle,$pageDesc,$pageKnowMoreUrl,$conn);
			
			if (noError($updateHeaderContent)) {
				/* Deleting image if updated */
				if (isset($checkArr1['feature-image']['name']) || !empty($checkArr1['feature-image']['name'])) {
					unlink('../uploads/'.$pageImageUrl);
				}
				$returnArr = $updateHeaderContent;
			} else {
				$returnArr = $updateHeaderContent;
			}
		} else {
			$errMsg = $checkArr1['feature-image']['name'];
			$returnArr = setErrorStack($returnArr, 17, $errMsg);
		}

	} elseif ($_POST['method'] == "delete") {
		$pageId = cleanXSS(urldecode($_POST['headerId']));
		$removeHeader = removeHeader($pageId,$conn);
		if (noError($removeHeader)) {
			$returnArr = $removeHeader;
		} else {
			$returnArr = $removeHeader;
		}
	}
}

echo json_encode($returnArr);

?>