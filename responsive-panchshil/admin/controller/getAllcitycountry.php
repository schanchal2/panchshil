
<?php
// print_r($_POST);
require_once '../utilities/config.php';
require_once '../utilities/dbUtils.php';
require_once '../utilities/utilities.php';
require_once '../utilities/errorMap.php';
$country_id = $_POST['country_id'];
$state_id = $_POST['state_id'];
$id=$_POST['id'];
$conn = createDbConnection($serverName,$dbUserName,$dbPassword,$dbName);
if(noError($conn)){
    $conn = $conn["conn"];

    if($country_id !=""){
       $getstate=getstate($country_id,$conn);
     if(noError($getstate)){   
            $getstate = $getstate["response"];

            foreach ($getstate as $key) {
                echo '<option>'.$key['city'].'</option>';
            }
            // printArr($getstate);
            die;
        }else{
            $returnArr = $getstate;
            // print_r($returnArr);

            die;
        }
    }
    elseif ($state_id!="") {
          $getcity = getcity($state_id,$conn);
   //printArr($getstate);
    if(noError($getcity)){   
          $getcity = $getcity["response"];
           foreach ($getcity as $key) {
                echo '<option>'.$key['name'].'</option>';
            }
    }else{
        $returnArr = $getcity;
    }
         die;
       
    }
}else{
    exit;
}


 
function getcity($state_id,$conn){
 
    $returnArr = array();
    $getResult = array();

   $query = "select * from cities WHERE state_id=$state_id";

    $execQuery = runQuery($query, $conn);

    if(noError($execQuery)){
        while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $res["response"][] = $row;
            //print_r($res);
        }
        $returnArr = setErrorStack($execQuery, -1, "Successfully fetched record cities details" ,$res);
    }else{
        $returnArr = setErrorStack($execQuery, 25, "Error: Retrieving contacts detail");
    }

    return $returnArr;
}




function getstate($country_id,$conn)
{
    $returnArr = array();
    $getResult = array();

  //$query = "SELECT * FROM states WHERE country_id='".$country_id."'";
  $query="select c.name as city,c.id as cityid,co.name,s.name from countries co INNER JOIN states s ON co.id=s.country_id INNER JOIN cities c ON s.id=c.state_id WHERE co.id='".$country_id."' ORDER BY c.name ASC";
     //echo $query;
    $execQuery = runQuery($query, $conn);
    //var_dump($execQuery);
    if(noError($execQuery)){
        while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $res["response"][] = $row;
            //print_r($res);
        }
        $returnArr = setErrorStack($execQuery, -1, "Successfully fetched record  state details" ,$res);
    }else{
        $returnArr = setErrorStack($execQuery, 25, "Error: Retrieving contacts detail");
    }

    return $returnArr;
}



?>