<?php
/*Start Session to display error on view page */
session_start();

/* fetching all required Files */
require_once '../utilities/config.php';
require_once "../utilities/dbUtils.php";
require_once "../utilities/utilities.php";
require_once "../utilities/errorMap.php";
require_once '../model/propertyContentModel.php';

/* Fetching all values form Posted Form*/
$name            = cleanXSS(urldecode($_POST['name']));
$location         = cleanXSS(urldecode($_POST['location']));
$title         = cleanXSS(urldecode($_POST['title']));
$short_desc         = cleanXSS(urldecode($_POST['short_desc']));
$long_desc          = cleanXSS(urldecode($_POST['long_desc']));
$website_url           = cleanXSS(urldecode($_POST['website_url']));
$latitude             = cleanXSS(urldecode($_POST['latitude']));
$longitude           = cleanXSS(urldecode($_POST['longitude']));
$imageUrl           = cleanXSS(urldecode($_POST['imageUrl']));
$id         = cleanXSS(urldecode($_POST['id']));
$p_id         = cleanXSS(urldecode($_POST['p_id']));



/* Declairing target folder to upload image and returning array */
$targetDir                  = "../uploads/commercialImages/";
$checkArr1                  = array();
$returnArr                  = array();

/* Creating database Connection and processing */
$conn = createDbConnection($serverName,$dbUserName, $dbPassword, $dbName);
if (noError($conn)) {
    $conn = $conn["conn"];

    /*  @ Checking Method Type
        @ if method is update then Add new Business Details
        @ Else delete Business
    */

    if ($_POST['method'] == "update") {
        /* To upload Color and Black white image */

        foreach ($_FILES as $key => $file) {
            if ($file['error'] == 0) {
                $checkArr = array();
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

                /* To check error in File */
                if (!isset($checkArr['error'])) {
                    $fileName = uniqid().'.'.end(explode('.',basename($file['name'])));
                    $targetFile = $targetDir . $fileName;

                    /* To check folder Existance */
                    $folderExist = folderPresenceCheck($targetDir);
                    if (noError($folderExist)) {
                        /* To Upload Image in Folder */
                        $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                        if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                            $checkArr['success']    = $uploadStatus['errMsg'];
                            $checkArr['name']       = 'commercialImages/'.$fileName;
                            $checkArr['errCode']    = $uploadStatus['errCode'];
                        } else {
                            $checkArr['error']      = $uploadStatus['errMsg'];
                            $checkArr['name']       = $file['name'];
                        }
                    } else {
                        $checkArr['error']      = "Directed Folder Mismatched";
                        $checkArr['name']       = $file['name'];
                    }
                }
                $checkArr1[$key] = $checkArr;
            }
        }

        /* Inserting Or Updating DB */
        if (!isset($checkArr1['commercial-feature-image']['name']) || empty($checkArr1['commercial-feature-image']['name'])) {
                    $image = $imageUrl;
        } else {
                    $image = $checkArr1['commercial-feature-image']['name'];
        }
        if (!isset( $checkArr1['commercial-feature-image']['error'])) {
            $updateCommercialDetails = updateproperty_details($id,$p_id,NULL	,$image,$name,$location,$title,NULL,NULL,NULL,NULL,$short_desc,$long_desc,$website_url,NULL,$latitude,$longitude,NULL,$conn);
            /* Deleting Previous image if updated new Image*/
            if (noError($updateCommercialDetails)) {
                if (isset($checkArr1['commercial-feature-image']['name'])) {
                    unlink('../uploads/'.$imageUrl);
                }
                $returnArr = $updateCommercialDetails;
            }
        } else {

            $returnArr = setErrorStack($returnArr, 17);
        }

    } elseif ($_POST['method'] == "delete") {
        $id  =  cleanXSS(urldecode($_POST['id']));
        $removeAbout = removeProperty($id,$conn);
        if (noError($removeAbout)) {
            $returnArr = $removeAbout;
        } else {
            $returnArr = $removeAbout;
        }
    }
}
echo json_encode($returnArr);
?>
