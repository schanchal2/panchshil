# README #

This is the Panchshil Repositiory
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Developers Workin on this Repo ###

* Jagtar
* Nikhil
* Tejashree
* Dakshata
* Deepak
* Chanchal
* Kavita
* Ashwini


### Developers Guidelines ###

* Strictly Developers will work on the Develop branch
* Write Down the Fixes here which you have written on that date your name and fix message

#### 01-17-2017 ####

* Nikhil Created admin/controller/offeringDetailsController.php
* Nikhil Modified admin/model/offeringDetailsModel.php
* Nikhil Created admin/views/manageOffering.php
* Nikhil Modified admin/views/updateBusinessOffering.php
* kavita Updated modified:  admin/views/manageBanner.php
* kavita Updated modified:  admin/views/manageCommercial.php
* kavita Updated modified:  admin/views/manageGallery.php
* kavita Updated modified:  admin/views/updateBanner.php
* kavita Updated modified:  admin/views/updateCommercialContent.php
* kavita Updated modified:  admin/views/updateGallery.php
* kavita Updated modified:  admin/controller/bannerController.php
* kavita Updated modified:  admin/controller/commercialController.php
* kavita Updated modified:  admin/controller/galleryController.php
* kavita Updated modified:  admin/model/bannerContentModel.php
* kavita Updated modified:  admin/model/galleryContentModel.php
* kavita Updated modified:  admin/model/propertyContentModel.php

#### 01-05-2017 ####

* kavita worked on admin/views/updateBrandAndPArtnerContent.php 
* kavita worked on admin/views/updateAlliancesContent.php 
* kavita worked on admin/views/manageBrandAndPartnerContent.php 
* kavita worked on admin/views/manageAlliancesContent.php 
* kavita worked on admin/model/brandAndPartnerContentModel.php 
* kavita worked on admin/model/alliancesContentModel.php 
* kavita worked on admin/controller/brandAndPartnerController.php 
* kavita worked on admin/controller/alliancesController.php 
* kavita worked on views/media/media.php 
* kavita worked on admin/views/index * kavita worked on .php2
* kavita worked on admin/views/manageMediaContent.php
* Nikhil Worked on Integration bug fixes of Corporate Profile, Forward, Company Profile pages.
* Nikhil Updated views/companyProfile/companyProfile.php
* Nikhil Updated views/corporateProfile/corporateProfile.php
* Nikhil Updated views/foreward/foreward.php
* Nikhil Updated views/inBusiness/inBusiness.php
* Chanchal WOrked on Integration Part of Business and Sustainibility Module with its Categories 
* Chanchal Updated modified:   admin/model/sustainContentModel.php
* Chanchal Updated modified:   admin/views/manageSustainCat.php
* Chanchal Updated modified:   views/about/about.php
* Chanchal Updated modified:   views/inBusiness/inBusiness.php
* Chanchal Updated modified:   views/sustainability/sustainability.php
* Chanchal Updated modified:   admin/controller/businessDetailsController.php
* Chanchal Updated modified:   admin/controller/businessSubCatagoryController.php
* Chanchal Updated modified:   admin/model/aboutContentModel.php
* Chanchal Updated modified:   admin/model/businessCategoryDetailsModel.php
* Chanchal Updated modified:   admin/model/businessDetailsModel.php
* Chanchal Updated modified:   admin/model/businessSubCatagoryModel.php
* Chanchal Updated modified:   admin/views/manageBusiness.php
* Chanchal Created new file:   admin/views/manageBusinessSubCat.php
* Chanchal Created new file:   admin/views/manageServices.php
* Chanchal Updated modified:   admin/views/manageSustainCat.php
* Chanchal Updated modified:   admin/views/updateBusiness.php

#### 01-04-2017 ####

* Nikhil Worked on contact Us email and SMS sending Feature, Solved bugs from Admin About Us page.
* Nikhil Updated admin/controller/contactUsFormController.php
* Nikhil Updated admin/model/contactUsFormModel.php
* Nikhil Updated views/contact/contact.php
* Nikhil Updated views/contact/contactFormErrorMsg.php
* Chanchal WOrked on Integration Part of Sustainibility and Career Module with its Categories(Solved all bugs, Things are running properly) 
* Chanchal Update modified:   admin/views/index.php
* Chanchal Update modified:   admin/views/manageSustainContent.php
* Chanchal Update modified:   admin/views/updateSustain.php
* Chanchal Update modified:   admin/views/index.php
* Chanchal Update modified:   views/about/about.php
* Chanchal Update modified:   views/inBusiness/inBusiness.php
* Chanchal Update modified:   views/sustainability/sustainability.php
* Kavita modified:   admin/views/manageLeadership.php
* Kavita modified:   admin/views/managePressContent.php
* Kavita modified:   admin/views/manageTestimonialContent.php
* Kavita modified:   admin/views/updateLeadership.php
* Kavita modified:   admin/views/updateTestimonialContent.php
* Kavita modified:   views/award/award.php
* Kavita modified:   admin/controller/leadershipController.php
* Kavita modified:   admin/model/leadershipContentModel.php
* Kavita modified:   admin/model/mediaContentModel.php
* Kavita modified:   admin/model/testimonialContentModel.php
* Kavita modified:   admin/controller/testimonialController.php
* Kavita modified:   admin/model/awardContentModel.php 
* Kavita modified:   admin/model/testimonialContentModel.php 
* Kavita modified:   admin/views/manageMediaContent.php 
* Kavita modified:   admin/views/updateAwardContent.php 
* Kavita modified:   admin/views/updatePressContent.php 
* Kavita modified:   views/media/media.php 
* Kavita modified:   admin/views/confirmChangesRemodalById.php 
* Kavita modified:  admin/model/pageNameContentModel.php


#### 01-03-2017 ####

* Nikhil Worked on contact Us email and SMS sending Feature, Solved bugs from Admin About Us page.
* Nikhil Updated admin/views/manageClientsDetails.php
* Nikhil Updated admin/views/manageCompanyProfile.php
* Nikhil Updated admin/views/manageForword.php
* Nikhil Updated admin/views/manageGrowthChronicle.php
* Nikhil Updated admin/views/manageSustainContent.php
* Nikhil Updated admin/views/updateClientsDetails.php
* Nikhil Updated views/contact/contact.php
* Nikhil Updated admin/model/contactUsFormModel.php
* Nikhil Updated admin/views/manageAboutContent.php
* Nikhil Updated admin/views/manageCorporateContent.php
* Kavita updated admin/controller/leadershipController.php
* Kavita updated admin/model/leadershipContentModel.php
* Kavita updated admin/views/manageLeadership.php
* Kavita updated admin/views/manageMediaContent.php
* Kavita updated admin/views/updateLeadership.php
* Kavita updated admin/views/updateMediaContent.php
* Kavita updated views/media/media.php
* Chanchal Worked on backend and integration part of Sustainibility and Career page and its category pages(HR Aliances / PEP and Inbusiness / CSR)
* Chanchal Updated admin/controller/sustainCatController.php 
* Chanchal Updated admin/model/careerDetailsModel.php 
* Chanchal Updated admin/model/sustainContentModel.php 
* Chanchal Updated admin/views/career_category.php 
* Chanchal Updated admin/views/manageAboutContent.php 
* Chanchal Updated admin/views/manageSustainCat.php 
* Chanchal Updated admin/views/manageSustainContent.php 
* Chanchal Updated views/inCareer/inCareer.php 
* Chanchal Updated views/inCareer/inCarrer.php
* Chanchal Updated admin/views/manageClientsDetails.php

#### 01-02-2017 ####

* Nikhil SOlved Bugs of backend and Integration of Company Profile, Contact Us form Backend.
* Nikhil Created admin/views/manageCompanyProfile.php
* Nikhil Created admin/controller/CompanyProfileController.php
* Nikhil Created admin/model/CompanyProfileModel.php
* Nikhil Created views/companyProfile.php
* Nikhil Created views/contact/contact.php
* Nikhil Updated views/contact/contactFormErrorMsg.php
* kavita worked on press page of frontend and created the model for you tube video and pdf
* kavita worked on link direction on press page
* kavita worked on addind one more field to press module
* kavita worked on media page of add data display views
* kavita worte comment on admin/controller/pressController.php admin/model/awardContentModel.php admin/model/mediaContentModel.php
* Chanchal Worked on Sustainibility backend and integration part, and also integrated Career page and its category pages(HR Aliances and PEP)
* Chanchal Created pages for module is career.php, inCareer.php, sustainability.php and inBusiness.php, client.php.
* Chanchal create views/career/career.php
* Chanchal create views/client/client.php
* Chanchal create views/inCareer/inCareer.php
* Chanchal create views/inBusiness/inBusiness.php
* Chanchal create views/sustainability/sustainability.php
* Chanchal create views/about/about.php
* Chanchal updated controller/clientLoadEvent.php
* Chanchal updated admin/views/updateClientsDetails.php
* Chanchal updated admin/views/manageSustainContent.php
* Chanchal updated admin/views/manageSustainCat.php
* Chanchal updated admin/views/manageClientsDetails.php
* Chanchal updated admin/views/manageClientsDetails.php

#### 12-30-2016 ####

* Nikhil Worked on backend of COrporate profie, forwar, Company Profile.
* Nikhil Done Integration of Company Profile, Growth Chronicle. Solving Bugs of Integration of Company Profile.
* Nikhil Updated admin/controller/forwordController.php
* Nikhil Updated admin/controller/growthCronicleController.php
* Nikhil Updated admin/model/corporateDetailsModel.php
* Nikhil Updated admin/model/growthCronicleModel.php
* Nikhil Updated admin/views/manageCorporateContent.php
* Nikhil Updated admin/views/manageForword.php
* Nikhil Updated admin/views/manageGrowthChronicle.php
* Nikhil Updated admin/views/updateCorporate.php
* Nikhil Updated admin/views/updateGrowthChronicleDetails.php
* Nikhil Updated views/corporateProfile/corporateProfile.php
* Nikhil Updated views/foreward/foreward.php
* Nikhil Created admin/views/manageCompanyProfile.php
* Nikhil Created admin/controller/CompanyProfileController.php
* Nikhil Created admin/model/CompanyProfileModel.php
* Nikhil Created views/companyProfile.php
* Kavita Worked On Changes On Media Page, Worked On Award Page, Worked On Changes in testimonial Page
* Kavita Worked On Integrated the media Page, Integrated the press Page with specified id of press, Integrated the award Page


#### 12-29-2016 ####

* Nikhil Worked on Backend of Growth Chronicle, Corporate Profile, Forward pages.
* Nikhil Worked Integration of Growth Chronicle, Corporate Profile, Forward pages.
* Nikhil Updated admin/controller/forwordController.php
* Nikhil Updated admin/controller/growthCronicleController.php
* Nikhil Updated admin/model/corporateDetailsModel.php
* Nikhil Updated admin/model/growthCronicleModel.php
* Nikhil Updated admin/views/manageCorporateContent.php
* Nikhil Updated admin/views/manageForword.php
* Nikhil Updated admin/views/manageGrowthChronicle.php
* Nikhil Updated admin/views/updateCorporate.php
* Nikhil Updated admin/views/updateGrowthChronicleDetails.php
* Nikhil Updated views/about/about.php
* Nikhil Updated views/corporateProfile/corporateProfile.php
* Nikhil Updated views/foreward/foreward.php
* Tejashree worked on contact Us page backend And Integration.
* Tejashree Updated views/contact/contact.php
*Chanchal Created Functionality of Career, Sustainability and also its category "CSR, Inbusiness,PEP,HR aliances" (All Functionality ADD/ UPDATE, DELETE is working).
* Chanchal create admin/controller/careerCatController.php
* Chanchal create admin/controller/careerController.php
* Chanchal create admin/model/careerDetailsModel.php
* Chanchal create admin/views/career_category.php
* Chanchal create admin/views/manageCareerDetails.php
* Chanchal create admin/views/manageSustainCat.php
* Chanchal create admin/views/updateCareerDetails.php

#### 12-28-2016 ####

* Nikhil WOrked on About-Us Bug Solve, Integration of About, Corporate profile page.
* Nikhil Updated admin/controller/aboutController.php
* Nikhil Updated admin/controller/clientsDetailsController.php
* Nikhil Updated admin/model/aboutContentModel.php
* Nikhil Updated admin/model/clientDetailsModel.php
* Nikhil Updated admin/views/manageClientsDetails.php
* Nikhil Updated admin/views/updateAbout.php
* Nikhil Updated admin/views/updateClientsDetails.php
* Nikhil Deleted admin/controller/corporateProfileController.php
* Nikhil Deleted admin/model/corporateProfileModel.php
* Nikhil Deleted admin/views/manageCorporateProfile.php
* Nikhil Updated admin/views/manageLeadership.php
* Nikhil Updated admin/views/updateAbout.php
* Nikhil Updated views/about/about.php
* Nikhil Created views/corporateProfile/corporateProfile.php
* Nikhil Created views/sustainability/sustainability.php
* Tejashree Workes on the Integarted the Leadership page.
* Tejashree Updated views/leadership/chngeImg.php 
* Tejashree Updated views/leadership/leadership.php 
* Tejashree Worked on the Contact Us page.
* Tejashree Updated views/manageContactUs.php
* Tejashree Worked on the Profession module in admin
* Tejashree Updated admin/controller/professionallist.php
* Tejashree Updated admin/controller/professionalcontroller.php 
* Tejashree Updated professionalmodel.php 
* Tejashree Updated manageprofessional.php.
* Chanchal Workes on the Sustanibility, CSR and Inbusiness Module page.
* Chanchal Updated admin/controller/sustainDetailsController.php
* Chanchal Worked Update Sustainability Page.
* Chanchal Updated admin/views/updateSustain.

#### 12-27-2016 ####

* Nikhil Updated and Solved Bugs of About Us page functionality
* Nikhil Updated admin/controller/aboutController.php
* Nikhil Updated admin/model/aboutContentModel.php
* Nikhil Updated admin/views/manageAboutContent.php
* Nikhil Updated admin/views/updateAbout.php
* Tejashree Workes on the Integarted the Leadership page.
* Tejashree Updated leadership.php admin/model/aboutContentModel.php
* Tejashree Worked on the Integarated the Forward page.
* Tejashree Updated the forward.php
* Tejashree Worked on the Back End for the Profession in Admin
* Tejashree Updated the  admin/controller/professionallist.php, professionalcontroller.php professionalmodel.php,
                manageprofessional.php
* Tejashree Workes on the Integarted the About page.
* Tejashree Updated the admin/model/aboutContentModel.php views/about/about.php views/contact/contact.php
                views/leadership/leadership.php 
* Tejashree updated the controller admin/controller/getAllcitycountry.php
* Chanchal Created Functionality of Corporate Profile, Sustainability, Company Profile, Group Phylosophy(All Functionality ADD/ UPDATE, DELETE is working).
* Chanchal Created admin/controller/CompanyProfileController.php
* Chanchal Created admin/controller/GroupPhiloController.php
* Chanchal Created admin/controller/corporateDetailsController.php
* Chanchal Updated admin/controller/forwordController.php
* Chanchal Updated admin/controller/growthCronicleController.php
* Chanchal Created admin/controller/sustainDetailsController.php
* Chanchal Updated admin/model/aboutContentModel.php
* Chanchal Updated admin/model/growthCronicleModel.php
* Chanchal Created admin/views/manageCompanyProfile.php
* Chanchal Created admin/views/manageCorporateContent.php
* Chanchal Updated admin/views/manageForword.php
* Chanchal Created admin/views/manageGroupPhilo.php
* Chanchal Updated admin/views/manageGrowthChronicle.php
* Chanchal Created admin/views/manageSustainContent.php
* Chanchal Created admin/views/updateCorporate.php
* Chanchal Updated admin/views/updateGrowthChronicleDetails.php
* Chanchal Created admin/views/updateSustain.php

#### 12-26-2016 ####

* Tejashree Workes on the Backenf for the Professional page in admin.
* Tejashree Updated  admin/controller/professionallist.php, professionalcontroller.php professionalmodel.php, manageprofessional.php
* Tejashree Worked on the Integarted the forward page 
* Tejashree Updated views/forward.php 
* Tejashree Workes on the Integarted the leadership Page
* Tejashree Updated views/leadership.php
* Chanchal Worked on UI part of “Forword” in About Module
* Chanchal Worked On ADD fubctionality of About Module
* Chanchal updated AboutController.php,aboutContentModel.php,manageForword.php,updateforword.php 
* Chanchal updated Admin/controller/AboutController.php
* Chanchal updated Admin/model/aboutContentModel.php
* Chanchal updated Admin/views/manageForword.php
* Nikhil Working on Corporate profile and Clients Details in admin Dashboard.
* Nikhil created admin/model/corporateProfileModel.php
* Nikhil created admin/controller/corporateProfileController.php
* Nikhil created admin/model/corporateProfileModel.php
* Nikhil created admin/views/manageCorporateProfile.php
* Nikhil created admin/views/updateCorporateProfile.php
* Nikhil updated admin/views/manageLeadership.php
* Nikhil updated admin/controller/clientsDetailsController.php
* Nikhil updated admin/model/clientDetailsModel.php
* Nikhil updated admin/utilities/config.php
* Nikhil updated admin/views/manageClientsDetails.php
* Nikhil updated admin/views/updateClientsDetails.php

#### 12-24-2016 ####

* Chanchal Created Categories “Forword” in About Module, where in todays work task to Implement the Functionality of  “ADD/ UPDATE/ DELETE”. 
* Chanchal updated ForwordController.php,aboutContentModel.php,manageForword.php,updateforword.php 
* Chanchal updated Admin/controller/forwordController.php
* Chanchal updated Admin/model/aboutContentModel.php
* Chanchal updated Admin/views/manageForword.php
* Chanchal updated Admin/views/updateforword.php
* Kavita Created Categories “Leadership” in About Module, where in todays work task to Implement the Functionality of  “ADD/ UPDATE/ DELETE”. 
* Kavita  updated leadershipController.php, leadershipContentModel.php, manageLeadership.php, updateLeadership.php 
* Kavita updated Admin/controller/leadershipController.php
* Kavita updated Admin/model/leadershipContentModel.php
* Kavita updated Admin/views/manageForword.php
* Kavita updated Admin/views/updateLeadership.php
* Tejashree Worked on the Static data show in Integaration.
* Tejashree Updated views/business/business.php  views/businessCatagories/businessCatagories.php 
    views/contact/contact.php views/home/home.php views/media/media.php 
* Tejashree Workes on the Business page and also change the css for this page.
* Tejashree Updated  css/app.css css/protected.css lib/private/_background.scss views/business/business.php
    views/businessCatagories/businessCatagories.php 
* Tejashree Worked on the client page for view client.
* Tejashree Updated views/clients/clients.php 
* Tejashree Workes on the business and footer page
* Tejashree Updated admin/utilities/config.php admin/views/manageBusinessCategory.php views/layout/footer.php 
* Nikhil Worked on About Us in admin dashboard.
* Nikhil Updated admin/controller/aboutController.php
* Nikhil Updated admin/model/aboutContentModel.php
* Nikhil Updated admin/views/index.php
* Nikhil Updated admin/views/manageAboutContent.php
* Nikhil Updated admin/views/updateAbout.php

#### 12-23-2016 ####

* Nikhil Worked on Business, business Category and subcategory.
* Nikhil Updated admin/utilities/config.php
* Nikhil Updated admin/utilities/config2.php
* Nikhil Updated admin/views/index.php
* Nikhil Updated admin/views/manageBusinessCategory.php
* Nikhil Updated admin/views/manageBusinessSubCategory.php
* Nikhil Uploaded admin/uploads/staticImages/enquiry_banner.jpg
* Nikhil Uploaded admin/uploads/staticImages/growth_banner.jpg
* Nikhil Uploaded admin/uploads/staticImages/logo.jpg
* Nikhil Uploaded admin/uploads/staticImages/services_banner.jpg
* Tejashree Worked on the Contact Us page, Css in Admin, Backend for the About Content and also change the css
* Tejashree Updated admin/views/manageProfession.php  
* Tejashree Updated admin/views/updateProfession.php   
* Tejashree Updated admin/views/index.php 
* Tejashree Updated admin/views/manageAboutCatagories.php 
* Tejashree Updated admin/views/manageClientsDetails.php 
* Tejashree Updated admin/views/manageMatricsDetails.php 
* Tejashree Updated admin/views/managePressContent.php   
* Tejashree Updated admin/views/manageProject.php 
* Tejashree Updated admin/views/manageReasons.php 
* Tejashree Updated admin/controller/aboutController.php 
* Tejashree Updated admin/model/aboutContentModel.php    
* Tejashree Updated admin/views/manageAboutContent.php 
* Tejashree Updated admin/views/updateAbout.php 
* Tejashree Updated css/app.css 
* Tejashree Now working on the integaration part for the static image in all pages.
* Chanchal Worked on About Us Category and manageForword.
* Chanchal Updated admin/controller/aboutController.php
* Chanchal Created admin/controller/forwordController.php
* Chanchal Updated admin/model/aboutContentModel.php
* Chanchal Updated admin/views/manageAboutContent.php
* Chanchal Created admin/views/manageForword.php
* Chanchal Created admin/views/updateforword.php


#### 12-22-2016 ####

* Tejashree Worked on Backend of About us manageaboutcontent.php, manageaboutcatagories.php, updateaboutcatagories.php,updateabout.php,aboutcatagoriescontroller.php ,aboutcontroller.php, aboutcatagoriesModel.php, aboutContentmodel.php
* Tejashree  Worked on the Integaration part of the Footer Part for contact and follow us layout/footer.php


#### 12-21-2016 ####

* Tejashree Solve the bug on the footer page views/layout/footer.php 
* Tejashree Worked on Backend of About us manageaboutcontent.php, manageaboutcatagories.php, updateaboutcatagories.php,
	updateabout.php, aboutcatagoriescontroller.php ,aboutcontroller.php, aboutcatagoriesModel.php, aboutContentmodel.php
* Tejashree Worked on Backend of About us manageaboutcontent.php, manageaboutcatagories.php, updateaboutcatagories.php,
           updateabout.php,aboutcatagoriescontroller.php ,aboutcontroller.php, aboutcatagoriesModel.php, aboutContentmodel.php
* Tejashree  Worked on the Media Section in admin/views/manageMediaCatagories.php
   	  admin/views/manageMediaContent.php admin/views/updateMediaCatagories.php admin/views/updateMediaContent.php 

#### 12-20-2016 ####

* Tejashree Worked onIntegration of home page all offerings.
* Tejashree Solve the Bugs from Online Website in footer part. views/layout/footer.php  
* Tejashree Create Media Section UI in Admin Dashboard and also updated the css for this.
           admin/views/index.php  admin/views/manageMediaCatagories.php admin/views/manageMediaContent.php
          admin/views/updateMediaCatagories.php  admin/views/updateMediaContent.php  css/protected.css
          lib/private/_background.scss
* Tejashree Worked on Backend of About us manageaboutcontent.php, manageaboutcatagories.php, updateaboutcatagories.php,
           updateabout.php,aboutcatagoriescontroller.php ,aboutcontroller.php, aboutcatagoriesModel.php, aboutContentmodel.php
* Tejashree  Working on the Backend Category of About us.

#### 12-16-2016 ####

* Deepak app.css, luxeryresidancy.php, property_details.php and footer.php Include the changes.
* Tejashree Worked on Integarated Footer.
* Tejashree Updated  views/layout/footer.php  
* Tejashree worked on the UI for project in Admin 
* Tejashree updated admin/views/manageProject.php  admin/views/updateProjectContent.php 
* Tejashree Worked on the integarated project management page for client
* Tejashree Updated views/ProjectManagement/project_management.php 
* Tejashree Worked on the integarated Contact Page and also change the css for this.
* Tejashree Updated views/contact/contact.php css/app.css  
* Nikhil Worked on Admin Dashboard Integration to display business Category and sub category.
* Nikhil Updated admin/views/index.php
* Nikhil Updated admin/views/manageAboutContent.php
* Nikhil Updated admin/views/manageAboutCategory.php

#### 12-15-2016 ####
* Tejashree Worked on the about page and also integarted the about page.
* Tejashree Updated admin/controller/aboutController.php
* Tejashree Updated admin/model/aboutContentModel.php
* Tejashree Updated admin/views/manageAboutContent.php    
* Tejashree Updated admin/views/updateAbout.php    
* Tejashree worked on the update business catagories
* Tejashree updated views/businessCatagories/businessCatagories.php
* Tejashree Worked on the Project Management Subcatagories page
* Tejashree Updated views/ProjectManagement/project_management.php
* Tejashree Updated views/business/business.php
* Tejashree Updated views/businessCatagories/businessCatagories.php
* Nikhil Worked on Business category and sub category pages also done integration bug fixes for the same.
* Nikhil Updated admin/controller/businessSubCatagoryController.php
* Nikhil Updated admin/model/businessSubCatagoryModel.php
* Nikhil Updated admin/views/manageBusinessSubCategory.php
* Nikhil Updated admin/views/updateBusinessSubCatagory.php
* Nikhil Updated views/ProjectManagement/project_management.php
* Nikhil Updated views/business/business.php
* Nikhil Updated views/businessCatagories/businessCatagories.php
* Nikhil Updated views/index.php
* Nikhil Updated admin/controller/businessCatagoryController.php
* Nikhil Updated admin/model/businessCatagoryModel.php
* Nikhil Updated admin/views/manageBusinessCategory.php
* Nikhil Updated admin/views/updateBusinessCatagory.php

#### 12-14-2016 ####
* Nikhil Worked on manage Business Subcategory in admin dashboard.
* Nikhil Updated admin/controller/businessSubCatagoryController.php
* Nikhil Updated admin/model/businessSubCatagoryModel.php
* Nikhil Updated admin/views/manageBusinessSubCategory.php
* Nikhil Updated admin/views/updateBusinessSubCatagory.php
* Dakshata Worked on Contact Us page in and index page of admin. fixed all UI Bugs of Admin Index page.
* Dakshata Updated admin/views/index.php
* Dakshata Updated views/contacUs/contactUs.php
* Tejashree Integated the Social link and changes the model and controller.
* Tejashree Updated admin/controller/globalSettingController.php  admin/model/globalSettingModel.php admin/views/manageglobalsetting.php   views/layout/footer.php
* Tejashree Integarated the Footer part(Contact and Footer image)
* Tejashree Updated views/layout/footer.php
* Tejashree working on the manage about in admin
* Tejashree updated manageAbout.php
* Tejashree updated updatedAbout.php
* Tejashree created aboutContentModel.php
* Tejashree Created aboutController.php


#### 12-13-2016 ####

* Deepak app.css, property_details.php, project_management.php and clients.php Include the changes.
* Deepak add commercial.php, hospitality.php page and images.
* Deepak solved conflict clients.php
* Tejashree Integarted the client for View All Client page
* Tejashree Updated  admin/model/clientDetailsModel.php  controller/clientLoadEvent.php  views/clients/clients.php  
* Tejashree Worked manage about UI for admin
* Tejashree Updated admin/views/index.php
* Tejashree Created admin/views/manageAboutCatagories.php admin/views/manageAboutContent.php  admin/views/manageGrowthChronicle.php  admin/views/updateAbout.php admin/views/updateAboutCatagories.php
* Tejashree Worked on  business catagories
* Tejashree Updated  views/businessCatagories/businessCatagories.php
* Dakshata complete the reasons page in admin dashboard.
 and updated the
       1)admin/controller/reasonscontroller.php   
       2)admin/model/ReasonsModel.php
       3)admin/view/manageReasons.php
* Dakshata integrate the contact page in panchshil website
* Dakshata update the UI in Business page admin/views/manageBusiness.php
* Dakshata Tested the Business categary page in admin dashboard and panchshil website created the bug document.
* Nikhil Worked on Business Sub Category in admin dashboard also updated contact us page
* Nikhil updated admin/controller/contactUsFormController.php
* Nikhil updated admin/model/contactUsFormModel.php
* Nikhil updated views/contact/contact.php
* Nikhil updated admin/controller/businessSubCatagoryController.php
* Nikhil updated admin/model/businessSubCatagoryModel.php
* Nikhil Updated admin/views/manageBusinessCategory.php
* Nikhil Updated admin/views/manageBusinessSubCategory.php
* Nikhil Updated admin/views/updateBusinessCatagories.php



#### 12-12-2016 ####

* Deepak add unitegallery plugins in lib and 1 image.
* Deepak app.css, property_Details.php, footer.php and header.php Include the changes.
* Tejashree Integarted the business page
* Tejashree Updated views/business/business.php
* Tejashree Integarted the business Catagories page and its css
* Tejashree Updated  views/business/business.php views/businessCatagories/businessCatagories.php css/app.css
* Tejashree Worked on the Manage commercial UI for Admin
* Tejashree Updated admin/views/manageBusinessSubCategory.php
* Dakshata updated admin/controller/reasonscontroller.php   
* Dakshata updated admin/model/ReasonsModel.php
* Dakshata updated admin/views/manageReasons.php
* Dakshata Tested the Business page in admin dashboard and created the bug Document.
* Dakshata Tested the Business categary page in admin dashboard and created the bug document.
* Dakshata rested the bug and close the bug.
* Nikhil Working on Business SUb Category
* Nikhil Created admin/controller/businessSubCatagoryController.php
* Nikhil Created admin/model/businessSubCatagoryModel.php
* Nikhil Updated admin/views/manageBusinessCategory.php
* Nikhil Updated admin/views/manageBusinessSubCategory.php
* Nikhil Updated admin/views/updateBusinessCatagories.php
* Nikhil Updated admin/views/viewContactDetails.php
* Nikhil Renamed admin/views/updateBusinessSubcatagories.php -> admin/views/updateBusinessSubCatagory.php
* Nikhil Deleted admin/views/updateSubcatagories.php
* Nikhil Deleted admin/views/updateSubcatagoriesInfo.php


#### 12-10-2016 ####
* Deepak app.css, property_Details.php, header.php and footer.php Include the changes.
* Nikhil Worked on Manage Business Categories in Business Model and controller.
* Nikhil Created admin/controller/businessCategoryDetailsController.php
* Nikhil Created admin/model/businessCategoryDetailsModel.php
* Nikhil Created admin/views/manageBusinessCategory.php
* Nikhil Created admin/views/updateBusinessCatagories.php
* Nikhil Updated admin/views/manageBusiness.php
* Nikhil Updated admin/views/manageBusinessSubCategory.php
* Nikhil Updated admin/views/updateBusiness.php
* Nikhil Updated admin/views/updateBusinessSubcatagories.php

#### 12-09-2016 ####

* Deepak app.css, property_Details.php and footer.php Include the changes.
* Deepak add rec.php page and image.
* Tejashree Created Business UI for admin
* Tejashree Updated admin/views/index.php  admin/views/manageBusiness.php  admin/views/business.php
* Tejashree Created Subcatagories UI
* Tejashree Created  admin/views/index.php  admin/views/manageSubcatagories.php  admin/views/updateSubcatagories.php
* Tejashree Created Subcatagories Info
* Tejashree Updated  admin/views/index.php  admin/views/manageSubcatagoriesInfo.php admin/views/updateSubcatagoriesInfo.php
* Dakshata updated admin/views/manageReasons.php
* Dakshata working on admin/controller/reasonscontroller.php
* Dakshata working on model reasons admin/model/ReasonsModel.php
* Dakshata intergrate views/contact.php
* Nikhil Working on Press, business and project masnagement in Admin Dashboard.
* Nikhil Created admin/controller/businessDetailsController.php
* Nikhil Created admin/model/businessDetailsModel.php
* Nikhil Updated admin/controller/pressContentController.php
* Nikhil Updated admin/model/pressContentModel.php
* Nikhil Updated admin/views/managePressContent.php
* Nikhil Updated admin/views/updatePressContent.php
* Nikhil Updated admin/views/manageBusinessSubCategory.php
* Nikhil Updated admin/views/updateBusinessSubcatagories.php
* Nikhil Updated admin/views/updateBusiness.php
* Nikhil Updated admin/views/manageBusiness.php

#### 12-08-2016 ####

* Deepak app.css, property_Details.php and footer.php Include the changes.
* Deepak add jquery.mixitup.min.js
* Deepak add images
* Dakshata intergrate contact page in panchshil (views/conatct.php)
* Dakshata Added new table reasond in panchshil Admin Database.
* Dakshata Created reasons page UI in panchshil(admin/views/manageReasons.php)
* Dakshata Created Controller reasons(admin/controller/reasonscontroller.php)
* Dakshata Created model reasons(admin/model/ReasonsModel.php)
* Tejashree Worked on the Admin UI
* Tejashree Created Press content UI admin/views/index.php admin/views/managePressContent.php  
admin/views/updatePressContent.php  
* Tejashree created the Project UI admin/views/index.php  admin/views/manageProject.php     admin/views/updateProjectContent.php
* Tejashree Created Slider Image for admin Section admin/views/index.php  admin/views/manageSliderImages.php
* Tejashree Created UI for Gallary Images admin Section  admin/views/manageSliderImages.php admin/views/sliderForm.php
* Nikhil worked on manage press and project in admin dashboard
* Nikhil Created admin/controller/pressContentController.php
* Nikhil Created admin/model/pressContentModel.php
* Nikhil Updated admin/views/managePressContent.php
* Nikhil Updated admin/views/updatePressContent.php

#### 12-07-2016 ####

* Deepak app.css, nri.php, property_Details.php and footer.php Include the changes
* Deepak add images
* Tejashree Worked on the global setting Form.
* Tejashree Updated admin/views/manageglobalsetting.php .
* Tejashree created the model for global setting admin/model/globalSettingModel.php
* Tejashree created the controller for global setting  admin/controller/globalSettingController.php
* Tejashree completed the contact Form header integaration views/contact/contact.php
* Tejashree Updated the growth chronical page views/GrowthChronicle/growth_chronicle.php
* Tejashree Updated the Client page views/clients/clients.php
* Tejashree solve the bug in matrices and contact form.
* Tejashree Working on the Admin Manage Press admin/views/managePress.php.
* Nikhil Worked on add project details and Growth Chronicle in Admin dashboard.
* Nikhil Updated admin/model/growthCronicleModel.php
* Nikhil Updated admin/views/manageGrowthChronicle.php
* Nikhil Updated admin/views/updateGrowthChronicleDetails.php
* Dakshata Created UI Design New static page in panchshil(views/static/static.php)
* Dakshata Tested all Admin Dashboard functioning.
* Dakshata created getter and setter cities,country,state drop down list completed
* Dakshata Update css and UI for updateContactDetail.php page
* Dakshata Update css and UI for updateClientsDetail.php page
* Dakshata Update css and UI for pdateGriwthChronicleDetail.php page
* Dakshata Update css and UI for updateHeaderContent.php page
* Dakshata Update css and UI for updateMatriecs.php page
* Dakshata Update css and UI for updateMatriecs.php page



#### 12-06-2016 ####

* Deepak import jquery.bxslider.css and jquery.bxslider.js
* Deepak created nri.php and add 2 images
* Deepak app.css, app.js, home.php, footer.php and header.php Include the changes.
* Dakshata Testing Manage contact us , Manage Growth chronicle page of Admin Dashboard and created Document for testing
* Dakshata Updated updateContactDetail.php
* Dakshata created getter cities,country,state drop down list and Phone Number and reqired filed Validation using javascript in contact page.
* Tejashree Solve the bug on the Admin Enquiry Form
* Tejashree updated on admin/views/viewEnquiryDetails.php.
* Tejashree updated on admin/controller/enquiryfromController.php
* Tejashree updated on admin/model/enquirycontentModel.php
* Tejashree updated on admin/view/ManageEnquiryForm.php
* Tejashree Integarate the growth chronical page views/growthchronicle/growth_chronicle.php
* Tejashree Created mail Functionality for enquiry Details admin/model/enquiryContentModel.php
* Nikhil Worked on Growth Cronicle,  and Contact US form and Enquiry Form.
* Nikhil Updated admin/views/viewEnquiryDetails.php.
* Nikhil Updated admin/controller/enquiryfromController.php
* Nikhil Updated admin/model/enquirycontentModel.php
* Nikhil Updated admin/view/ManageEnquiryForm.php
* Nikhil Updated lib/private/_background.scss
* Nikhil Updated admin/view/ManageEnquiryForm.php
* Nikhil Updated admin/model/growthCronicleModel.php
* Nikhil Updated admin/views/manageGrowthChronicle.php
* Nikhil Updated admin/views/updateGrowthChronicleDetails.php
* Nikhil Updated admin/controller/contactusformModel.php
* Nikhil Updated admin/model/contactusformcontroller.php
* Nikhil Updated admin/views/viewContactus.php.
* Nikhil Updated admin/controller/contactusform.php
* Nikhil Updated css/protected.css
* Nikhil Updated lib/private/protected.scss



#### 12-05-2016 ####

* Deepak add new font didot
* Deepak growth_chronicle.php, foreward.php, home.php, footer.php and press.php  Include the changes
* Deepak app.css, award.php, footer.php and header.php Include the changes
* Deepak add app.js and growth_chronicle.php
* Tejashree Worked on the Admin Enquiry Form.
* Tejashree Updated admin/views/viewEnquiryDetails.php.
* Tejashree Created admin/controller/enquiryfromController.php
* Tejashree Created admin/model/enquirycontentModel.php
* Tejashree Updated admin/view/ManageEnquiryForm.php
* Tejashree Updated lib/private/_background.scss
* Tejashree Updated admin/view/ManageEnquiryForm.php
* Tejashree Updated Home Page.
* Tejashree Updated views/home/home.php
* Dakshata  Added city and Country new  table in database
* Dakshata Testing for Matrices page Admin Dashboard and create Document for testing
* Dakshata update new code contact controller (contact_detail_controller.php)
* Dakshata update new code contact Model (contact_detail_model.php)(Add Delete Function)
* Dakshata change UI contact Admin (ManageContactDetail.php)
* Dakshata Added the javascript validation in updateContactDetail.php
* Nikhil Updated COntact us form detail also integtrated contact page in public side.
* Nikhil created new COntroller and model for growth Chronicle.
* Nikhil Created admin/controller/growthCronicleController.php
* Nikhil Created admin/model/growthCronicleModel.php
* Nikhil updated admin/views/manageGrowthChronicle.php
* Nikhil updated admin/views/updateGrowthChronicleDetails.php
* Nikhil Updated admin/controller/contactusformModel.php
* Nikhil Updated admin/model/contactusformcontroller.php
* Nikhil Updated admin/views/viewContactus.php.
* Nikhil Updated admin/controller/contactusform.php


#### 12-03-2016 ####

* Deepak Include the changes- app.css, luxeryresidancy.php, project_management.php, business.php, clients.php, contact.php,      foreward.php, footer.php, header.php, leadership.php, media.php, offering.php, press.php and service.php
* Deepak add 3 images and created award.php page
* Deepak Add 5 images and created press.php page
* Dakshata Updated Contact Details in admin Dashboard
* Dakshata update  admin/controller/contact_detail_Controller.php
* Dakshata Created admin/model/Contact_detail_Model.php
* Dakshata Created new admin/views/ManageContactDetail.php
* Dakshata Created new admin/views/UpdateContactDetail.php and Added Java script Validation.
* Dakshata Tested panchshil for client and home page and update the Document
* Tejashree Worked on the Admin Contact
* Tejashree Updated admin/views/viewContactus.php.
* Tejashree Updated admin/controller/contactusform.php
* Tejashree Updated admin/model/contactusformcontroller.php
* Tejashree Updated admin/view/manageContactUs.php
* Tejashree Updated lib/private/_background.scss
* Nikhil Created Contact Us form details in admin dashboard also created controller and model for it.
* Nikhil Created admin/controller/contactUsFormController.php
* Nikhil Created admin/model/contactUsFormModel.php
* Nikhil Updated admin/views/manageContactUs.php

#### 12-02-2016 ####

* Deepak app.css and contact.php Include the changes
* Deepak Created foreward.php,leadership.php
* Deepak add 2 images
* Tejashree Worked on the Admin Matrics and also completed the integaration part for matrices content and client content.
* Tejashree Solved the bug on the matrices content in admin Dashboard.
* Tejashree Updated admin/views/matrices.php.
* Tejashree Updated admin/controller/matricsContentController.php
* Tejashree Updated admin/model/UpdateMatrics.php
* Tejashree Updated views/home/home.php
* Tejashree Updated views/clients/clients.php
* Dakshata Changed UI of contact details in Admin Dashbarod also updated MOdel and Controller of contact details in admin dashboard.
* Dakshata Added the contact Function ( page) for admin
* Dakshata Updated admin/views/ManageContactDetail.php
* Dakshata Updated admin/index.php
* Dakshata Updated admin/model/Contact_detail_Model.php
* Dakshata Updated admin/controller/contact_detail_Controller.php
* Dakshata Updated admin/index.php
* Dakshata Tested Matrices page in Admin Dashboard.(of Tejashree).
* Dakshata Tested Panchshil UI and close the bug(of Deepak).
* Dakshata Tested Home and client page closed the bug and assign the new bug(of Nikhil).
* Dakshata Updated All bugs Details Are shared in Google drive doc https://docs.google.com/a/bitstreet.in/spreadsheets/d/1Lrn7-vT1iA49pzGgrHAqaY8leiZ4gmyOyzIruR-8z_4/edit?usp=sharing
* Nikhil Updated CLient And Header Section in Admin dashboard and also updated UI For the same.
* Nikhil Solved All bugs Assigned from admin dashboard.
* Nikhil Updated admin/views/manageClientDetails.php
* Nikhil Updated admin/views/updateClientDetails.php
* Nikhil Updated admin/views/manageHeaderContent.php
* Nikhil Updated admin/views/updateHeaderContent.php
* Nikhil Updated lib/private/_background.scss
* Nikhil Updated css/protected.css

#### 12-01-2016 ####

* Tejashree Updated Admin Matrics in admin dashboard also updated Controller and model for it.
* Tejashree Updated admin/views/Matrics.php
* Tejashree Updated admin/views/matricsContentModel.php
* Tejashree Updated admin/controller/matricsContentController.php
* Tejashree Updated admin/model/UpdateMatrics.php
* Tejashree Updated admin/utilities/errorMap.php
* Dakshata Created Controller and Model to Add, update and delete contaact from dashboard.
* Dakshata Created admin/controller/contact_controller.php
* Dakshata Created admin/model/contact_model.php
* Dakshata Updated admin/views/manage_contact.php
* Dakshata Tested client page in Admin Dashboard and create Document for testing
* Nikhil Updated controller, modal and ui of client details manage in admin dashboard.
* Nikhil Updated controller, modal and ui of Header Content manage in admin dashboard.
* Nikhil Updated admin/views/manageClientDetails.php
* Nikhil Updated admin/views/updateClientDetails.php
* Nikhil Updated admin/views/manageHeaderContent.php
* Nikhil Updated admin/views/updateHeaderContent.php
* Nikhil Updated lib/private/_background.scss
* Nikhil Updated css/protected.css
* Nikhil updated admin/controller/clientsDetailsController.php
* Nikhil updated admin/model/clientDetailsModel.php
* Nikhil updated admin/model/headerContentModel.php
* Nikhil updated admin/controller/headerContentController.php
* Nikhil Updated admin/utilities/errorMap.php


#### 11-30-2016 ####

* Tejashree Updated Integration of Media page, Created Matrics page in Dashboard also Created Controller and model for Matrics.
* Tejashree Updated Media Page.
* Tejashree Updated views/media/Media.php
* Tejashree Created the Matrics.
* Tejashree Updated admin/views/Matrics.php
* Tejashree Created admin/controller/matricsContentController.php
* Tejashree Created admin/views/UpdateMatrics.php
* Tejashree Created admin/model/matricsContentModel.php
* Dakshata Updated Contact Page in Admin Dashboard also Updated its Controller and Model.
* Dakshata updated contact_model and contact_controller.php
* Dakshata Updated update_contact.php
* Nikhil Updated Client's Detail page from admin Dashboard.
* Nikhil updated admin/controller/clientsDetailsController.php
* Nikhil updated admin/model/clientDetailsModel.php
* Nikhil updated admin/views/manageClientsDetails.php
* Nikhil updated Function to check Folder Existance
* Nikhil Updated admin/utilities/utilities.php
* Nikhil Solved bug of data Insert without Image.
* Nikhil updated admin/model/headerContentModel.php
* Nikhil updated admin/controller/headerContentController.php



#### 11-29-2016 ####

* Deepak footer.php header.php Include the changes
* Deepak add clients.php, media.php & app.css
* Deepak add media images
* Deepak add new page projectmanagement.php, business.php, clients.php and changes in app.css
* Dakshata Updated Clients Detail Pages with respect to its Model And Controller.
* Dakshata updated admin/model/client_model.php
* Dakshata updated admin.controller/client_controller.php
* Dakshata updated admin/views/manage_client.php
* Dakshata Done Testing of Admin Side Header Content page Performance.
* Tejashree Updated Header Content's Css
* Tejashree Updated admin/views/manageHeaderContent.php
* Tejashree Updated css/protected.css
* Tejashree Updated lib/private/_button.scss
* Tejashree Integrated Backend with UI of Header content of home, Media, Business Pages.
* Tejashree Updated views/home/home.php
* Tejashree Updated views/business/business.php
* Tejashree Updated views/media/media.php
* Nikhil Created Model, Controller to Add Delete and update Clients Details And Renamed All files of Client Details.
* Nikhil Created/Renamed admin/controller/clientsDetailsController.php
* Nikhil Created/Renamed admin/model/clientDetailsModel.php
* Nikhil Created/Renamed admin/views/manageClientsDetails.php
* Nikhil Created Function to check Folder Existance
* Nikhil Updated admin/utilities/utilities.php



#### 11-28-2016 ####

* Deepak app.css, luxeryresidancy.php Include the changes and add 3 residency images.
* Nikhil Created Functionality to add Update and Delete page Header COntent in admin Dashboard
* Nikhil Updated admin/views/manageHeaderContent.php
* Nikhil Updated admin/views/updateHeaderContent.php
* Nikhil Updated admin/controller/headerContentController.php
* Nikhil Updated admin/model/headerContentModel.php
* Dakshata Updated Client Details in Admin dashboard.
* Dakshata updated admin/views/Manage_client.php
* Dakshata updated admin/views/update_Client.php
* Dakshata updated admin/controler/clientcontroller.php
* Dakshata updated admin/model/client_model.php
* Tejashree updated Matrics part in admin dashboard
* Tejashree updated admin/views/matrics.php
* Tejashree updated admin/views/manageheadercontent.php
* Tejashree created admin/controller/matricscontentcontroller.php
* Tejashree created admin/model/matricscontentmodel.php


#### 11-25-2016 ####

* Deepak app.css Include the changes
* Deepak service.php Include the changes
* Deepak add services-banner1.jpg image
* Deepak offering.php Include the changes
* Deepak offering page - app.css Include the changes
* Deepak app.css footer.php Include the changes
* Nikhil Updated Controller and model to ADD & EDIT Header Content in admin/model/headerContentModel.php and admin/controller/headerContentController.php.
* Tejashree Integration of Backend with UI in Home Page Header Slider 1 in views/home/home.php.
* Tejashree Updated Ui OF Admin Dashboard UI And Created Matrics Column to add matrics in Admin Dashboard in admin/views/matrics.php.
* Dakshata Created UI in Admin Dashboard for Client Details inserting in admin/views/update_Client.php.


#### 11-24-2016 ####

* Jagtar Cleaned the Repository removed the Extra files and folders.
* Nikhil Solved CSS issue of Admin Dashbord SCSS files in lib/private/*.
* Nikhil Created Add, Edit, Delete content of header in dashboard.
* Nikhil Created or Updated Admin dashboard also created javascripts in admin/views/index.php.
* Nikhil Created model and controller to add header contents in database in admin/model/headerContentModel.php and admin/controller/headerContentController.php.
* Dakshata Created Display pageof Contact details in dashboard in admin/views/index.php.
* Tejashree Created Display page of Matrics of Home page in dashbord in admin/views/index.php.
