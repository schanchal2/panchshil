
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="lib/jquery.bxslider/jquery.bxslider.css"/>
  <link rel='stylesheet' type='text/css' href='css/protected.css'/>

  <!-- Include Unite Gallery core files -->
	<link rel='stylesheet' type='text/css' href='lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='lib/unitegallery/themes/default/ug-theme-default.css'/>

  <link rel="stylesheet" type="text/css" href="css/app.css?Tuesday 27th of December 2016 03:56:31 PM"/>
  <!-- main menu with search -->
    <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
  <!-- main menu with search -->

</head>
<body class="nav-is-fixed">
  <?php include 'header.html' ?>
<div style="height:120px;"></div>
<!--banner starts here-->
<div class="banner-brands">
 <div class="container-fluid nopadding">
	<ol class="breadcrumb">
		<li><a href="#">Home</a></li>
		<li><a href="#">Library</a></li>
		<li class="active">Brands</li>
	</ol>
</div><h1>Brands</h1>
</div>
<!--banner ends here-->


<!--content starts here-->

<div class="container-fluid inner-5x innerTB">
<div class="container">
<div class="row inner-5x innerB">
    <div class="logo-content innerAll-more"><img src="img/courtyard-logo.png"></div>
    <div class="desc-content innerAll-40"><span class="title">Courtyard by Marriott</span><br/><br/><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><br/><a href="#" class="hvr-bounce-to-right">view detail</a></div>
        </div>



    <div class="row inner-5x innerB">
    <div class="logo-content innerAll-more"><img src="img/marriot-logo.png"></div>
    <div class="desc-content innerAll-40"><span class="title">JW Marriott</span><br/><br/><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><br/><a href="#" class="hvr-bounce-to-right">view detail</a></div>
        </div>


    <div class="row inner-5x innerB">
    <div class="logo-content innerAll-more"><img src="img/kelly-logo.png"></div>
    <div class="desc-content innerAll-40"><span class="title">Kelly Hoppen</span><br/><br/><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><br/><a href="#" class="hvr-bounce-to-right">view detail</a></div>
        </div>


   <div class="row inner-5x innerB">
    <div class="logo-content innerAll-more"><img src="img/courtyard-logo.png"></div>
    <div class="desc-content innerAll-40"><span class="title">Courtyard by Marriott</span><br/><br/><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><br/><a href="#" class="hvr-bounce-to-right">view detail</a></div>
        </div>



    <div class="row inner-5x innerB">
    <div class="logo-content innerAll-more"><img src="img/marriot-logo.png"></div>
    <div class="desc-content innerAll-40"><span class="title">JW Marriott</span><br/><br/><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><br/><a href="#" class="hvr-bounce-to-right">view detail</a></div>
        </div>
    </div>
</div>
<!--content ends here-->
<?php include 'footer.html' ?>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="lib/jquery.bxslider/jquery.bxslider.min.js"></script>

<!-- Include Unite Gallery core files -->
<script src='lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
    	 var data=$(this).find(".text").html();
    		$(".img-hover-text").hide().stop().html(data).fadeIn();

    	}, function() {
    		$(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
	   jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end
   });
</script>

<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
	$(function () {
	var filterList = {
		init: function () {
			$('#KeyAmenities').mixItUp({
				selectors: {
					target: '.KeyAmenities-portfolio',
					filter: '.KeyAmenities-list'
				},
				load: {
					filter: '.parking, .upper_deck, .lower_deck'
				}
			});
		}
	};
	filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end
</script>
<!-- main menu with search -->
<script src="js/main.js"></script>
<script src="js/footer.js"></script>
<!-- main menu with search -->
</body>
</html>
