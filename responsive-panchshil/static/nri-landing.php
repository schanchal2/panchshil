
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="lib/jquery.bxslider/jquery.bxslider.css"/>

  <!-- Include Unite Gallery core files -->
	<link rel='stylesheet' type='text/css' href='lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='lib/unitegallery/themes/default/ug-theme-default.css'/>

  <link rel="stylesheet" type="text/css" href="css/app.css?Tuesday 27th of December 2016 05:37:39 PM"/>
  <link rel='stylesheet' type='text/css' href='css/protected.css'/>


  <!-- main menu with search -->
    <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
  <!-- main menu with search -->

  <!-- banner -->
      <!-- Add custom CSS here -->
      <link href="css/full-slider.css" rel="stylesheet">
      <!-- banner -->

</head>
<body class="nav-is-fixed">
  <?php include 'header.html' ?>

  <!-- header-->
  <div style="height:120px;"></div>

  <!--banner starts here-->

  <!--banner starts here-->
  <div class="banner-nri">
   <div class="container-fluid nopadding">
  	<ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li class="active">NRI</li>

  	</ol>
  </div>
<div class="nri-landing-banner">
  <div class="slider-over-text">
    <div class="common-btn">
      <h2 class="content-title didot-bold-font">Headline Goes here</h2>
      <p class="content-info">
  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 					</p>
    </div>

  </div>
</div>


  </div>
  <!--banner ends here-->


<section class="nri-section bg-black" id="footer-bg-black">
  <!-- <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 nri-path-col-padding">
        <div class="nri-path-link">
            <a href="#">Home</a> / <a href="#">NRI</a> / <a href="#">Details</a>
        </div>
      </div>
    </div>
  </div> -->
  <!-- container-fluid end -->

  <div class="container-fluid nri-feachers">
    <div class="row">
      <div class="col-md-4 feachers-col-padding nopadding">
          <div class="feachers-overlay nri-feachers project-img-zoom">
            <img src="img/world-family.jpg" class="img-responsive" alt="world family image">
            <div class="feachers-overlay-text feachers-btn">

              <h2>FEACHERS</h2>
              <br>
              <a href="#" class="btn btn-default">&nbsp;&nbsp;VIEW DETAILS</a>
            </div>
          </div>
        </div>

        <div class="col-md-4 feachers-col-padding nopadding">
            <div class="feachers-overlay nri-feachers project-img-zoom">
              <img src="img/nri-property-02.jpg" class="img-responsive" alt="world family image">
              <div class="feachers-overlay-text feachers-btn">

                <h2>BENEFITS</h2>
                <br>
                <a href="#" class="btn btn-default">&nbsp;&nbsp;VIEW DETAILS</a>
              </div>
            </div>
          </div>
          <div class="col-md-4 feachers-col-padding nopadding nri-feachers-last">
              <div class="feachers-overlay nri-feachers project-img-zoom">
                <img src="img/nri-bg.jpg" class="img-responsive" alt="world family image">
                <div class="feachers-overlay-text feachers-btn">

                  <p class="nri-login-title">NRI</p>
                  <p class="nri-login-subtitle">LOGIN / SIGN UP</p>
                  <br/><br/><br/>
                  <form>
                  <input type="text" class="form-control nri-input" placeholder="USERNAME / EMAIL ID"><br/>
                  <input type="password" class="form-control nri-input" placeholder="PASSWORD"><br/>
                  <p><a href="#" class="nri-forgot-pwd">Forgot Password?</a></p>
                  <br/><br/>
                  <a href="#" class="btn btn-default nri-login-btn"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>&nbsp;&nbsp; LOGIN / SIGN UP</a>
                </form>

                </div>
              </div>
            </div>



    </div>
  </div>



<!--===================
  Related News
=====================  -->
  <div class="container">
    <!-- <div id="tabs-container">
        <ul class="tabs-menu">
            <li class="current"><a href="#tab-1">Tab 1</a></li>
            <li><a href="#tab-2">Tab 2</a></li>
            <li><a href="#tab-3">Tab 3</a></li>
            <li><a href="#tab-4">Tab 4</a></li>
        </ul>

    </div> -->

    <br/>

			<div class="row nri-related-news">
        <h3 class="nri-text-white didot-bold-font text-center">News Updates</h3>
        <br/>

				<div class="col-md-4">
					<a href="#"><img src="img/nri-news-01.jpg" class="project-img-zoom img-responsive" alt="Updates image"></a>
					<div class="row no-padding">
						<div class="col-sm-3 col-xs-4 NewsUpdate update-nrinews">
							<div style="text-align:left;">
								<p class="date didot-font">30</p>
								<p class="month">JULY</p>
							</div>
						</div>
						<div class="col-sm-9 col-xs-8 updates-content">
							<p>Panchshil unveils homes next to Mahalaxmi Race course</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<a href="#"><img src="img/nri-news-02.jpg" class="img-responsive" alt="Updates image"></a>
					<div class="row">
						<div class="col-sm-3 col-xs-4 NewsUpdate update-nrinews">
              <div style="text-align:left;">
								<p class="date didot-font">17</p>
								<p class="month">JULY</p>
							</div>
						</div>
						<div class="col-sm-9 col-xs-8 updates-content">
							<p>Panchshil Properties records 14% growth in net profit.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<a href="#"><img src="img/nri-news-03.jpg" class="img-responsive" alt="Updates image"></a>
					<div class="row">
						<div class="col-sm-3 col-xs-4 NewsUpdate update-nrinews">
              <div style="text-align:left;">
                <p class="date didot-font">26</p>
                <p class="month">JULY</p>
              </div>
						</div>
						<div class="col-sm-9 col-xs-8 updates-content">
							<p>Panchshil unveils homes next to Mahalaxmi Race course</p>
						</div>
					</div>
				</div>

        <div class="col-md-4">
          <a href="#"><img src="img/nri-news-001.jpg" class="project-img-zoom img-responsive" alt="Updates image"></a>
          <div class="row no-padding">
            <div class="col-sm-3 col-xs-4 NewsUpdate update-nrinews">
              <div style="text-align:left;">
                <p class="date didot-font">30</p>
                <p class="month">JULY</p>
              </div>
            </div>
            <div class="col-sm-9 col-xs-8 updates-content">
              <p>Panchshil unveils homes next to Mahalaxmi Race course</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <a href="#"><img src="img/nri-new.jpg" class="img-responsive" alt="Updates image"></a>
          <div class="row">
            <div class="col-sm-3 col-xs-4 NewsUpdate update-nrinews">
              <div style="text-align:left;">
                <p class="date didot-font">17</p>
                <p class="month">JULY</p>
              </div>
            </div>
            <div class="col-sm-9 col-xs-8 updates-content">
              <p>Panchshil Properties records 14% growth in net profit.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <a href="#"><img src="img/nri-new-04.jpg" class="img-responsive" alt="Updates image"></a>
          <div class="row">
            <div class="col-sm-3 col-xs-4 NewsUpdate update-nrinews">
              <div style="text-align:left;">
                <p class="date didot-font">26</p>
                <p class="month">JULY</p>
              </div>
            </div>
            <div class="col-sm-9 col-xs-8 updates-content">
              <p>Panchshil unveils homes next to Mahalaxmi Race course</p>
            </div>
          </div>
        </div>




			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="common-btn-white">
						<a href="#" class="btn btn-default">READ ALL UPDATES</a>
					</div>
				</div>
			</div>

      <div class="height-80"></div>

  </div>
  <!--===================
    Related News end
  =====================  -->


</section>

<?php include 'footer.html' ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="lib/jquery.bxslider/jquery.bxslider.min.js"></script>

<!-- Include Unite Gallery core files -->
<script src='lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
    	 var data=$(this).find(".text").html();
    		$(".img-hover-text").hide().stop().html(data).fadeIn();

    	}, function() {
    		$(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
	   jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end
   });
</script>

<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
	$(function () {
	var filterList = {
		init: function () {
			$('#KeyAmenities').mixItUp({
				selectors: {
					target: '.KeyAmenities-portfolio',
					filter: '.KeyAmenities-list'
				},
				load: {
					filter: '.parking, .upper_deck, .lower_deck'
				}
			});
		}
	};
	filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end
</script>
<script>
  $(document).ready(function(){
    var id = $('section').attr('id');
    if( id == "footer-bg-black"){
      $("footer").css("background-color", "#282828");
      $(".footer-quick-link h5 a, .footer-quick-link .fa-long-arrow-right, .contact h5, .office-location, .contact-no, .follow-us h5, .login-signup a, .location-map h5, .location-content select").css("color", "#fff");
      $(".location-content select").css("background-color", "#282828");
    }
  });
</script>
<script>
$(document).ready(function() {
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
});
</script>



<!-- main menu with search -->
<script src="js/main.js"></script>
<script src="js/footer.js"></script>
<!-- main menu with search -->
</body>
</html>
