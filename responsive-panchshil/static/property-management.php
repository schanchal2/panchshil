
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="lib/jquery.bxslider/jquery.bxslider.css"/>
  <link rel='stylesheet' type='text/css' href='css/protected.css'/>

  <!-- Include Unite Gallery core files -->
	<link rel='stylesheet' type='text/css' href='lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='lib/unitegallery/themes/default/ug-theme-default.css'/>

  <link rel="stylesheet" type="text/css" href="css/app.css?Tuesday 27th of December 2016 05:43:37 PM"/>


  <!-- main menu with search -->
    <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
  <!-- main menu with search -->


</head>
<body class="nav-is-fixed">
  <?php include 'header.html' ?>
  <div style="height:120px;"></div>

  <!-- header-->

<!--banner starts here-->
<div class="banner-property">
 <div class="container-fluid nopadding">
	<ol class="breadcrumb">
		<li><a href="#">Home</a></li>
		<li><a href="#">Business</a></li>
		<li><a href="#">Services</a></li>
		<li class="active">Property Management</li>
	</ol>
</div><h1>Property Management</h1>
</div>



<section class="project-management-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <p></p>
        <br/>
        <img class="img-responsive" style="margin: auto;" src="img/project-management-icon.png"/><br/>
        <p><b></b></p>
        <p class="project-management-grey-text"> &nbsp; | &nbsp; </p>
        <p class="project-management-grey-text"></p>
      </div>
    </div>
    <br/>
    <hr class="hr-color"/>

    <div class="row">
      <div class="col-md-12 text-center">
        <h1>Our Clients</h1>
        <br/><br/>
      </div>
    </div>
  </div>

  <div class="container project-management-client">
   <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

     <!-- Indicators -->
     <!-- <ol class="carousel-indicators">
       <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
       <li data-target="#carousel-example-generic" data-slide-to="1"></li>
       <li data-target="#carousel-example-generic" data-slide-to="2"></li>
     </ol> -->
     <!-- Controls -->
       <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
      </a>
     <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">

    <div class="item active">
      <div class="row">
                    <div class="col-md-3 clients-logo">
              <center>
                <div class="clients-logo-bg">
                  <a href="#"><img class="img-responsive" src="img/clients-logo/deloitte.jpg" style="width:205px; height:54px;"/></a>
                </div>
              </center>
            </div>  <div class="col-md-3 clients-logo">
              <center>
                <div class="clients-logo-bg">
                  <a href="#"><img class="img-responsive" src="img/clients-logo/hsbc.jpg" style="width:142px; height:120px;"/></a>
                </div>
              </center>
            </div>  <div class="col-md-3 clients-logo">
              <center>
                <div class="clients-logo-bg">
                  <a href="#"><img class="img-responsive" src="img/clients-logo/siemens.jpg" style="width:205px; height:64px;"/></a>
                </div>
              </center>
            </div>  <div class="col-md-3 clients-logo">
              <center>
                <div class="clients-logo-bg">
                  <a href="#"><img class="img-responsive" src="img/clients-logo/tata.jpg" style="width:205px; height:64px;"/></a>
                </div>
              </center>
            </div>  <div class="col-md-3 clients-logo">
              <center>
                <div class="clients-logo-bg">
                  <a href="#"><img class="img-responsive" src="img/clients-logo/wns.jpg" style="width:205px; height:64px;"/></a>
                </div>
              </center>
            </div>  <div class="col-md-3 clients-logo">
              <center>
                <div class="clients-logo-bg">
                  <a href="#"><img class="img-responsive" src="img/clients-logo/nvida.jpg" style="width:205px; height:64px;"/></a>
                </div>
              </center>
            </div>  <div class="col-md-3 clients-logo">
              <center>
                <div class="clients-logo-bg">
                  <a href="#"><img class="img-responsive" src="img/clients-logo/technology-services.jpg" style="width:205px; height:64px;"/></a>
                </div>
              </center>
            </div>  <div class="col-md-3 clients-logo">
              <center>
                <div class="clients-logo-bg">
                  <a href="#"><img class="img-responsive" src="img/clients-logo/tech-mahindra.jpg" style="width:205px; height:64px;"/></a>
                </div>
              </center>
            </div>
              </div>




    </div>

  </div>


</section>


<?php include 'footer.html' ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="lib/jquery.bxslider/jquery.bxslider.min.js"></script>

<!-- Include Unite Gallery core files -->
<script src='lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
    	 var data=$(this).find(".text").html();
    		$(".img-hover-text").hide().stop().html(data).fadeIn();

    	}, function() {
    		$(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
	   jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end
   });
</script>

<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
	$(function () {
	var filterList = {
		init: function () {
			$('#KeyAmenities').mixItUp({
				selectors: {
					target: '.KeyAmenities-portfolio',
					filter: '.KeyAmenities-list'
				},
				load: {
					filter: '.parking, .upper_deck, .lower_deck'
				}
			});
		}
	};
	filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end
</script>
<!-- main menu with search -->
<script src="js/main.js"></script>
<script src="js/footer.js"></script>
<!-- main menu with search -->
</body>
</html>
