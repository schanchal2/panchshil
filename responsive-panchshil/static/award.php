
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="lib/jquery.bxslider/jquery.bxslider.css"/>
  <link rel='stylesheet' type='text/css' href='css/protected.css'/>

  <!-- Include Unite Gallery core files -->
	<link rel='stylesheet' type='text/css' href='lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='lib/unitegallery/themes/default/ug-theme-default.css'/>

  <link rel="stylesheet" type="text/css" href="css/app.css?Tuesday 27th of December 2016 05:46:47 PM"/>

  <!-- main menu with search -->
    <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
  <!-- main menu with search -->



</head>
<body class="nav-is-fixed">

<?php include 'header.html' ?>
<div style="height:120px;"></div>
  <!-- header-->




<div class="banner-awards">
 <div class="container-fluid nopadding">
	<ol class="breadcrumb">
		<li><a href="#">Home</a></li>
		<li><a href="#">Media</a></li>
			<li class="active">Awards & Recognition</li>

	</ol>
</div><h1>Awards & Recognition</h1>
</div>


<section class="award-section">
  <div class="container award-container">
    <div class="row award-row-content award-img-margin">
      <div class="col-md-4">
        <a href="#"><img src="img/award1.png" class="img-responsive" /></a>
      </div>
      <div class="col-md-4 text-center">
        <a href="#"><img src="img/cnbc.png" class="img-responsive" /></a>
        <br/><br/><br/><br/>
        <div>
          <a href="#">
            <span class="award-title-size">BARCLAYS APPRECIATION</span><br/>
            <span class="award-subtitle-size">DELIVERING PUNE - 1<br/>BY BTCI </span>
          </a>
        </div>
        <br/><br/><br/><br/>
        <div>
          <a href="#">
            <span class="award-title-small-size">FMW GREEN INDIA AWARD</span><br/>
            <span class="award-subtitle-small-size">CONTTRIBUTION TO GREEN & CLEAN ENVIRONMENT INITIATIVES<br/>AT</span><br/>
            <span class="award-subtitle-small-size">3RD ANNUAL & 21ST CRE & GEM SUMMIT 2010, GOA</span>
          </a>
        </div>
        <br/><br/><br/><br/>
        <div>
          <a href="#"><span class="award-subtitle-size">AMICUS REAL ESTATE AWARDS 2008</span></a>
        </div>
      </div>
      <div class="col-md-4">
        <a href="#"><img src="img/award2.png" class="img-responsive" /></a>
      </div>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row">
      <br/><br/>
      <h2 class="text-center didot-font">Client's Testimonials</h2>
      <div class="col-md-12" data-wow-delay="0.2s">
        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
          <!-- Carousel Slides / Quotes -->
          <div class="carousel-inner text-center">
              <!-- Quote 1 -->
              <div class="item active">
                  <blockquote>
                      <div class="row">
                          <div class="col-sm-8 col-sm-offset-2">
                              <p class="quote-color">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."</p>
                              <br/><br/>
                              <span><b>Yohan Poonawala</b></span>
                              <small>Director Intervalve</small>
                          </div>
                      </div>
                  </blockquote>
              </div>
              <!-- Quote 2 -->
              <div class="item">
                  <blockquote>
                      <div class="row">
                          <div class="col-sm-8 col-sm-offset-2">
                              <p class="quote-color">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."</p>
                              <br/><br/>
                              <span><b>Yohan Poonawala</b></span>
                              <small>Director Intervalve</small>
                          </div>
                      </div>
                  </blockquote>
              </div>
              <!-- Quote 3 -->
              <div class="item">
                  <blockquote>
                      <div class="row">
                          <div class="col-sm-8 col-sm-offset-2">
                              <p class="quote-color">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."</p>
                              <br/><br/>
                              <span><b>Yohan Poonawala</b></span>
                              <small>Director Intervalve</small>
                          </div>
                      </div>
                  </blockquote>
              </div>
          </div>
            <!-- Carousel Buttons Next/Prev -->
            <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'footer.html' ?>

<!-- search div ends-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="lib/jquery.bxslider/jquery.bxslider.min.js"></script>

<!-- Include Unite Gallery core files -->
<script src='lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
    	 var data=$(this).find(".text").html();
    		$(".img-hover-text").hide().stop().html(data).fadeIn();

    	}, function() {
    		$(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
	   jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end
   });
</script>

<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
	$(function () {
	var filterList = {
		init: function () {
			$('#KeyAmenities').mixItUp({
				selectors: {
					target: '.KeyAmenities-portfolio',
					filter: '.KeyAmenities-list'
				},
				load: {
					filter: '.parking, .upper_deck, .lower_deck'
				}
			});
		}
	};
	filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end
</script>
<!-- main menu with search -->
<script src="js/main.js"></script>
<script src="js/footer.js"></script>

<!-- main menu with search -->
</body>
</html>
