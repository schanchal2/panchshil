
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="lib/jquery.bxslider/jquery.bxslider.css"/>

  <!-- Include Unite Gallery core files -->
	<link rel='stylesheet' type='text/css' href='lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='lib/unitegallery/themes/default/ug-theme-default.css'/>

  <link rel="stylesheet" type="text/css" href="css/app.css?Tuesday 27th of December 2016 06:08:10 PM"/>
  <link rel='stylesheet' type='text/css' href='css/protected.css'/>

  <!-- main menu with search -->
    <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
  <!-- main menu with search -->


</head>
<body class="nav-is-fixed">
  <?php include 'header.html' ?>
  <div style="height:120px;"></div>

  <!-- header-->


<!--banner starts here-->
<div class="banner-press">
 <div class="container-fluid nopadding">
	<ol class="breadcrumb">
		<li><a href="#">Home</a></li>
        <li><a href="#">Media</a></li>
		<li class="active">Press</li>
	</ol>
</div><h1>Press</h1>
</div>

<section class="press-section">
  <div class="container press-container">
    <div class="row press-row-br-bottom">
      <div class="col-md-7">
        <p><span class="press-date-filter didot-font">October, 2016</span></p>
      </div>
      <div class="col-md-3" style="padding: 15px 10px;">
        <span class="press-type">MEDIA TYPE</span>
        <select class="press-media-type">
          <option selected>ALL</option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
        </select>
      </div>
      <div class="col-md-2" style="padding: 15px 10px;">
        <span class="press-view-type">VIEW TYPE</span>
        <span class="press-view-icon">
        <a href="#"><i class="fa fa-th" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-th-list" aria-hidden="true"></i></span></a>
      </div>
    </div>
    <br/>
    <div class="row ">
      <div class="col-md-3">
        <div class="press-img">
          <a href="#"><img src="img/press01.jpg" class="img-responsive" alt="Updates image"/></a>
          <div class="press-img-overlay">
            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 col-xs-4 date-filter">
            <div>
              <p class="date didot-font">30</p>
              <p class="month didot-font">OCT</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 press-content">
            <p><b>Title come here...</b></p>
            <p class="source">source</p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="press-img">
          <a href="#"><img src="img/press02.jpg" class="img-responsive" alt="Updates image"/></a>
          <div class="press-img-overlay">
            <a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 col-xs-4 date-filter">
            <div>
              <p class="date didot-font">28</p>
              <p class="month didot-font">OCT</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 press-content">
            <p><b>Title come here...</b></p>
            <p class="source">source</p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="press-img">
          <a href="#"><img src="img/press03.jpg" class="img-responsive" alt="Updates image"/></a>
          <div class="press-img-overlay">
            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 col-xs-4 date-filter">
            <div>
              <p class="date didot-font">28</p>
              <p class="month didot-font">OCT</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 press-content">
            <p><b>Title come here...</b></p>
            <p class="source">source</p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="press-img">
          <a href="#"><img src="img/press04.jpg" class="img-responsive" alt="Updates image"/></a>
          <div class="press-img-overlay">
            <a href="#"><i class="fa fa-film" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 col-xs-4 date-filter">
            <div>
              <p class="date didot-font">25</p>
              <p class="month didot-font">OCT</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 press-content">
            <p><b>Title come here...</b></p>
            <p class="source">source</p>
          </div>
        </div>
      </div>
    </div>
    <br/>
    <div class="row ">
      <div class="col-md-3">
        <div class="press-img">
          <a href="#"><img src="img/press01.jpg" class="img-responsive" alt="Updates image"/></a>
          <div class="press-img-overlay">
            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 col-xs-4 date-filter">
            <div>
              <p class="date didot-font">30</p>
              <p class="month didot-font">OCT</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 press-content">
            <p><b>Title come here...</b></p>
            <p class="source">source</p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="press-img">
          <a href="#"><img src="img/press02.jpg" class="img-responsive" alt="Updates image"/></a>
          <div class="press-img-overlay">
            <a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 col-xs-4 date-filter">
            <div>
              <p class="date didot-font">28</p>
              <p class="month didot-font">OCT</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 press-content">
            <p><b>Title come here...</b></p>
            <p class="source">source</p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="press-img">
          <a href="#"><img src="img/press03.jpg" class="img-responsive" alt="Updates image"/></a>
          <div class="press-img-overlay">
            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 col-xs-4 date-filter">
            <div>
              <p class="date didot-font">28</p>
              <p class="month didot-font">OCT</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 press-content">
            <p><b>Title come here...</b></p>
            <p class="source">source</p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="press-img">
          <a href="#"><img src="img/press04.jpg" class="img-responsive" alt="Updates image"/></a>
          <div class="press-img-overlay">
            <a href="#"><i class="fa fa-film" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 col-xs-4 date-filter">
            <div>
              <p class="date didot-font">25</p>
              <p class="month didot-font">OCT</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 press-content">
            <p><b>Title come here...</b></p>
            <p class="source">source</p>
          </div>
        </div>
      </div>
    </div>
    <br/>
    <div class="row ">
      <div class="col-md-3">
        <div class="press-img">
          <a href="#"><img src="img/press01.jpg" class="img-responsive" alt="Updates image"/></a>
          <div class="press-img-overlay">
            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 col-xs-4 date-filter">
            <div>
              <p class="date didot-font">30</p>
              <p class="month didot-font">OCT</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 press-content">
            <p><b>Title come here...</b></p>
            <p class="source">source</p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="press-img">
          <a href="#"><img src="img/press02.jpg" class="img-responsive" alt="Updates image"/></a>
          <div class="press-img-overlay">
            <a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 col-xs-4 date-filter">
            <div>
              <p class="date didot-font">28</p>
              <p class="month didot-font">OCT</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 press-content">
            <p><b>Title come here...</b></p>
            <p class="source">source</p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="press-img">
          <a href="#"><img src="img/press03.jpg" class="img-responsive" alt="Updates image"/></a>
          <div class="press-img-overlay">
            <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 col-xs-4 date-filter">
            <div>
              <p class="date didot-font">28</p>
              <p class="month didot-font">OCT</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 press-content">
            <p><b>Title come here...</b></p>
            <p class="source">source</p>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="press-img">
          <a href="#"><img src="img/press04.jpg" class="img-responsive" alt="Updates image"/></a>
          <div class="press-img-overlay">
            <a href="#"><i class="fa fa-film" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 col-xs-4 date-filter">
            <div>
              <p class="date didot-font">25</p>
              <p class="month didot-font">OCT</p>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8 press-content">
            <p><b>Title come here...</b></p>
            <p class="source">source</p>
          </div>
        </div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-md-12 text-center common-btn">
        <a class="btn btn-default btn-press" href="#">VIEW ALL</a>
      </div>
    </div>
  </div><!-- container -->
</section>

<?php include 'footer.html' ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="lib/jquery.bxslider/jquery.bxslider.min.js"></script>

<!-- Include Unite Gallery core files -->
<script src='lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
    	 var data=$(this).find(".text").html();
    		$(".img-hover-text").hide().stop().html(data).fadeIn();

    	}, function() {
    		$(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
	   jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end
   });
</script>

<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
	$(function () {
	var filterList = {
		init: function () {
			$('#KeyAmenities').mixItUp({
				selectors: {
					target: '.KeyAmenities-portfolio',
					filter: '.KeyAmenities-list'
				},
				load: {
					filter: '.parking, .upper_deck, .lower_deck'
				}
			});
		}
	};
	filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end
</script>
<!-- main menu with search -->
<script src="js/main.js"></script>
<script src="js/footer.js"></script>
<!-- main menu with search -->

</body>
</html>
