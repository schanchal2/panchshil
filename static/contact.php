
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="lib/jquery.bxslider/jquery.bxslider.css"/>
  <link rel='stylesheet' type='text/css' href='css/protected.css'/>

  <!-- Include Unite Gallery core files -->
  <link rel='stylesheet' type='text/css' href='lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='lib/unitegallery/themes/default/ug-theme-default.css'/>
  <link rel="stylesheet" type="text/css" href="css/app.css?Tuesday 27th of December 2016 05:46:47 PM"/>

  <!-- main menu with search -->
    <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
  <!-- main menu with search -->


</head>
<body class="nav-is-fixed">
  <?php include 'header.html' ?>
<div style="height:120px;"></div>
  <!-- header-->

<!--banner starts here-->
<div class="banner-contact">
 <div class="container-fluid nopadding">
	<ol class="breadcrumb">
		<li><a href="#">Home</a></li>
		<li class="active">Contact</li>
	</ol>
</div><h1>Contact</h1>
</div>


<section class="contact-section">
  <div class="container contact-container">
    <div class="row">


      <div class="col-md-4 text-center">
        <div class="contact-box">
        <img src="img/contact-icon01.png">
          <h2>Pune</h2>
          <p class="contact-grey-text">+91(20) 66473200</p>
          <p class="contact-grey-text">Tech Park One,<br>
Tower E, 191 Yerwada,<br>
Pune - 411 006  </p>
        </div>
      </div>

      <div class="col-md-4 text-center">
        <div class="contact-box">
          <img src="img/contact-icon01.png">
          <h2>MUMBAI</h2>
          <p class="contact-grey-text">+91 22 66863939</p>
          <p class="contact-grey-text">PANCHSHIL REALTY<br>
20th Floor, Express towers,<br>
Nariman Point, Mumbai - 400 021</p>
        </div>
      </div>

      <div class="col-md-4 text-center">
        <div class="contact-box">
          <img src="img/contact-icon03.jpg">
          <h2>Pre-Sales Enquiry</h2>
          <p class="contact-grey-text">+91 22 66863939</p>
          <p class="contact-grey-text">name@gmail.com</p>
        </div>
      </div>
     </div><!-- row close -->
  <!-- contact form -->
    <div class="row contact-form">
      <div class="col-md-10 col-md-offset-1">
      <div id="errorMsg" style="   margin: 10px; text-align:center; font-size: 20px; font-weight; display:none;"></div>
        <h2 class="text-center enquiry-txt">Enquiry Form</h2>
        <br>

        <form id="contactForm" class="contact-enquiry">
        <input type="hidden" name="method" value="update">
          <div clas="row">
            <div class="form-group col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
              <select class="form-control" name="reason" id="reason">
                <option selected="" disabled="">Reason for Enquiry</option>
                                    <option value="www.google.co.in">www.google.co.in</option>
                                        <option value="2 www.google.co.in">2 www.google.co.in</option>
                                        <option value="Cunstruction">Cunstruction</option>
                                  </select><br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="First Name" name="firstName" id="firstName"><br>
                <input type="text" class="form-control" placeholder="Last Name" name="lastName" id="lastNAme"><br>
                <input type="text" class="form-control" placeholder="Phone" name="phone" id="phone" onchange="PhoneValidation(this);"><br>
                <input type="email" class="form-control" placeholder="Email" name="email" id="email" onchange="ValidateEmail(this)"><br><br>
                <select class="form-control" name="profession" id="profession">
                  <option selected="" disabled=""> Profession</option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                </select><br>
                <input type="text" class="form-control" placeholder="Company / Organzation" name="company_name" id="company_name"><br>
                <input type="text" class="form-control" placeholder="Designation" name="designation" id="designation"><br>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <textarea class="form-control textarea-address" placeholder="Address" name="address" id="address"></textarea><br>
                <select class="form-control" name="country" id="country">
                   <option value="">Select country</option>
                                        <option value="1">Afghanistan</option>
                                        <option value="2">Albania</option>
                                        <option value="3">Algeria</option>
                                        <option value="4">American Samoa</option>
                                        <option value="5">Andorra</option>
                                        <option value="6">Angola</option>
                                        <option value="7">Anguilla</option>
                                        <option value="8">Antarctica</option>
                                        <option value="9">Antigua And Barbuda</option>
                                        <option value="10">Argentina</option>
                                        <option value="11">Armenia</option>
                                        <option value="12">Aruba</option>
                                        <option value="13">Australia</option>
                                        <option value="14">Austria</option>
                                        <option value="15">Azerbaijan</option>
                                        <option value="16">Bahamas The</option>
                                        <option value="17">Bahrain</option>
                                        <option value="18">Bangladesh</option>
                                        <option value="19">Barbados</option>
                                        <option value="20">Belarus</option>
                                        <option value="21">Belgium</option>
                                        <option value="22">Belize</option>
                                        <option value="23">Benin</option>
                                        <option value="24">Bermuda</option>
                                        <option value="25">Bhutan</option>
                                        <option value="26">Bolivia</option>
                                        <option value="27">Bosnia and Herzegovina</option>
                                        <option value="28">Botswana</option>
                                        <option value="29">Bouvet Island</option>
                                        <option value="30">Brazil</option>
                                        <option value="31">British Indian Ocean Territory</option>
                                        <option value="32">Brunei</option>
                                        <option value="33">Bulgaria</option>
                                        <option value="34">Burkina Faso</option>
                                        <option value="35">Burundi</option>
                                        <option value="36">Cambodia</option>
                                        <option value="37">Cameroon</option>
                                        <option value="38">Canada</option>
                                        <option value="39">Cape Verde</option>
                                        <option value="40">Cayman Islands</option>
                                        <option value="41">Central African Republic</option>
                                        <option value="42">Chad</option>
                                        <option value="43">Chile</option>
                                        <option value="44">China</option>
                                        <option value="45">Christmas Island</option>
                                        <option value="46">Cocos (Keeling) Islands</option>
                                        <option value="47">Colombia</option>
                                        <option value="48">Comoros</option>
                                        <option value="49">Congo</option>
                                        <option value="50">Congo The Democratic Republic Of The</option>
                                        <option value="51">Cook Islands</option>
                                        <option value="52">Costa Rica</option>
                                        <option value="53">Cote D'Ivoire (Ivory Coast)</option>
                                        <option value="54">Croatia (Hrvatska)</option>
                                        <option value="55">Cuba</option>
                                        <option value="56">Cyprus</option>
                                        <option value="57">Czech Republic</option>
                                        <option value="58">Denmark</option>
                                        <option value="59">Djibouti</option>
                                        <option value="60">Dominica</option>
                                        <option value="61">Dominican Republic</option>
                                        <option value="62">East Timor</option>
                                        <option value="63">Ecuador</option>
                                        <option value="64">Egypt</option>
                                        <option value="65">El Salvador</option>
                                        <option value="66">Equatorial Guinea</option>
                                        <option value="67">Eritrea</option>
                                        <option value="68">Estonia</option>
                                        <option value="69">Ethiopia</option>
                                        <option value="70">External Territories of Australia</option>
                                        <option value="71">Falkland Islands</option>
                                        <option value="72">Faroe Islands</option>
                                        <option value="73">Fiji Islands</option>
                                        <option value="74">Finland</option>
                                        <option value="75">France</option>
                                        <option value="76">French Guiana</option>
                                        <option value="77">French Polynesia</option>
                                        <option value="78">French Southern Territories</option>
                                        <option value="79">Gabon</option>
                                        <option value="80">Gambia The</option>
                                        <option value="81">Georgia</option>
                                        <option value="82">Germany</option>
                                        <option value="83">Ghana</option>
                                        <option value="84">Gibraltar</option>
                                        <option value="85">Greece</option>
                                        <option value="86">Greenland</option>
                                        <option value="87">Grenada</option>
                                        <option value="88">Guadeloupe</option>
                                        <option value="89">Guam</option>
                                        <option value="90">Guatemala</option>
                                        <option value="91">Guernsey and Alderney</option>
                                        <option value="92">Guinea</option>
                                        <option value="93">Guinea-Bissau</option>
                                        <option value="94">Guyana</option>
                                        <option value="95">Haiti</option>
                                        <option value="96">Heard and McDonald Islands</option>
                                        <option value="97">Honduras</option>
                                        <option value="98">Hong Kong S.A.R.</option>
                                        <option value="99">Hungary</option>
                                        <option value="100">Iceland</option>
                                        <option value="101">India</option>
                                        <option value="102">Indonesia</option>
                                        <option value="103">Iran</option>
                                        <option value="104">Iraq</option>
                                        <option value="105">Ireland</option>
                                        <option value="106">Israel</option>
                                        <option value="107">Italy</option>
                                        <option value="108">Jamaica</option>
                                        <option value="109">Japan</option>
                                        <option value="110">Jersey</option>
                                        <option value="111">Jordan</option>
                                        <option value="112">Kazakhstan</option>
                                        <option value="113">Kenya</option>
                                        <option value="114">Kiribati</option>
                                        <option value="115">Korea North</option>
                                        <option value="116">Korea South</option>
                                        <option value="117">Kuwait</option>
                                        <option value="118">Kyrgyzstan</option>
                                        <option value="119">Laos</option>
                                        <option value="120">Latvia</option>
                                        <option value="121">Lebanon</option>
                                        <option value="122">Lesotho</option>
                                        <option value="123">Liberia</option>
                                        <option value="124">Libya</option>
                                        <option value="125">Liechtenstein</option>
                                        <option value="126">Lithuania</option>
                                        <option value="127">Luxembourg</option>
                                        <option value="128">Macau S.A.R.</option>
                                        <option value="129">Macedonia</option>
                                        <option value="130">Madagascar</option>
                                        <option value="131">Malawi</option>
                                        <option value="132">Malaysia</option>
                                        <option value="133">Maldives</option>
                                        <option value="134">Mali</option>
                                        <option value="135">Malta</option>
                                        <option value="136">Man (Isle of)</option>
                                        <option value="137">Marshall Islands</option>
                                        <option value="138">Martinique</option>
                                        <option value="139">Mauritania</option>
                                        <option value="140">Mauritius</option>
                                        <option value="141">Mayotte</option>
                                        <option value="142">Mexico</option>
                                        <option value="143">Micronesia</option>
                                        <option value="144">Moldova</option>
                                        <option value="145">Monaco</option>
                                        <option value="146">Mongolia</option>
                                        <option value="147">Montserrat</option>
                                        <option value="148">Morocco</option>
                                        <option value="149">Mozambique</option>
                                        <option value="150">Myanmar</option>
                                        <option value="151">Namibia</option>
                                        <option value="152">Nauru</option>
                                        <option value="153">Nepal</option>
                                        <option value="154">Netherlands Antilles</option>
                                        <option value="155">Netherlands The</option>
                                        <option value="156">New Caledonia</option>
                                        <option value="157">New Zealand</option>
                                        <option value="158">Nicaragua</option>
                                        <option value="159">Niger</option>
                                        <option value="160">Nigeria</option>
                                        <option value="161">Niue</option>
                                        <option value="162">Norfolk Island</option>
                                        <option value="163">Northern Mariana Islands</option>
                                        <option value="164">Norway</option>
                                        <option value="165">Oman</option>
                                        <option value="166">Pakistan</option>
                                        <option value="167">Palau</option>
                                        <option value="168">Palestinian Territory Occupied</option>
                                        <option value="169">Panama</option>
                                        <option value="170">Papua new Guinea</option>
                                        <option value="171">Paraguay</option>
                                        <option value="172">Peru</option>
                                        <option value="173">Philippines</option>
                                        <option value="174">Pitcairn Island</option>
                                        <option value="175">Poland</option>
                                        <option value="176">Portugal</option>
                                        <option value="177">Puerto Rico</option>
                                        <option value="178">Qatar</option>
                                        <option value="179">Reunion</option>
                                        <option value="180">Romania</option>
                                        <option value="181">Russia</option>
                                        <option value="182">Rwanda</option>
                                        <option value="183">Saint Helena</option>
                                        <option value="184">Saint Kitts And Nevis</option>
                                        <option value="185">Saint Lucia</option>
                                        <option value="186">Saint Pierre and Miquelon</option>
                                        <option value="187">Saint Vincent And The Grenadines</option>
                                        <option value="188">Samoa</option>
                                        <option value="189">San Marino</option>
                                        <option value="190">Sao Tome and Principe</option>
                                        <option value="191">Saudi Arabia</option>
                                        <option value="192">Senegal</option>
                                        <option value="193">Serbia</option>
                                        <option value="194">Seychelles</option>
                                        <option value="195">Sierra Leone</option>
                                        <option value="196">Singapore</option>
                                        <option value="197">Slovakia</option>
                                        <option value="198">Slovenia</option>
                                        <option value="199">Smaller Territories of the UK</option>
                                        <option value="200">Solomon Islands</option>
                                        <option value="201">Somalia</option>
                                        <option value="202">South Africa</option>
                                        <option value="203">South Georgia</option>
                                        <option value="204">South Sudan</option>
                                        <option value="205">Spain</option>
                                        <option value="206">Sri Lanka</option>
                                        <option value="207">Sudan</option>
                                        <option value="208">Suriname</option>
                                        <option value="209">Svalbard And Jan Mayen Islands</option>
                                        <option value="210">Swaziland</option>
                                        <option value="211">Sweden</option>
                                        <option value="212">Switzerland</option>
                                        <option value="213">Syria</option>
                                        <option value="214">Taiwan</option>
                                        <option value="215">Tajikistan</option>
                                        <option value="216">Tanzania</option>
                                        <option value="217">Thailand</option>
                                        <option value="218">Togo</option>
                                        <option value="219">Tokelau</option>
                                        <option value="220">Tonga</option>
                                        <option value="221">Trinidad And Tobago</option>
                                        <option value="222">Tunisia</option>
                                        <option value="223">Turkey</option>
                                        <option value="224">Turkmenistan</option>
                                        <option value="225">Turks And Caicos Islands</option>
                                        <option value="226">Tuvalu</option>
                                        <option value="227">Uganda</option>
                                        <option value="228">Ukraine</option>
                                        <option value="229">United Arab Emirates</option>
                                        <option value="230">United Kingdom</option>
                                        <option value="231">United States</option>
                                        <option value="232">United States Minor Outlying Islands</option>
                                        <option value="233">Uruguay</option>
                                        <option value="234">Uzbekistan</option>
                                        <option value="235">Vanuatu</option>
                                        <option value="236">Vatican City State (Holy See)</option>
                                        <option value="237">Venezuela</option>
                                        <option value="238">Vietnam</option>
                                        <option value="239">Virgin Islands (British)</option>
                                        <option value="240">Virgin Islands (US)</option>
                                        <option value="241">Wallis And Futuna Islands</option>
                                        <option value="242">Western Sahara</option>
                                        <option value="243">Yemen</option>
                                        <option value="244">Yugoslavia</option>
                                        <option value="245">Zambia</option>
                                        <option value="246">Zimbabwe</option>

                </select><br>
                  <select class="form-control" name="state" id="state">
                  <option value="">Select city</option>
                    </select><br><br>
                <textarea class="form-control textarea-comment" placeholder="Comments" name="comment" id="comment"></textarea><br>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6 text-right">
              <a href="#" class="btn btn-default btn-clear">CLEAR</a>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 text-left">
              <a class="btn btn-default btn-submit" onclick="addContactForm();">SUBMIT</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  <!-- contact form end -->
  <!-- location map -->
    <div class="row">
      <div class="col-md-12">
        <br><br>
        <h2 class="text-center enquiry-txt">Locate Property on Map</h2><br>
        <!-- <img src="img/map.jpg" class="img-responsive"/> -->

<div style="position:relative;z-index:0;">
<iframe width="100%" height="500" frameborder="0" scrolling="no" marginheight="0" src="https://maps.google.com/maps?q=panchshil tower, pune, &amp;t=p&amp;z=14&amp;ie=UTF8&amp;iwloc=&amp;output=embed" marginwidth="0">&amp;amp;lt;a class="addmaps" href="http://www.embedgooglemap.net"id="get-map-data" mce_href="http://maps.google.com/maps/api/js?sensor=false"&amp;amp;gt;bitstreet technology kurla, &amp;amp;lt;/a&amp;amp;gt;&amp;amp;lt;style&amp;amp;gt;#gmap_canvas img{max-width:none!important;background:none!important}&amp;amp;lt;/style&amp;amp;gt;
</iframe><div class="map-slider">

<div class="row">
                      <div class="col-md-12" data-wow-delay="0.2s">
                          <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                              <!-- Bottom Carousel Indicators -->

                              <!-- Carousel Slides / Quotes -->
                              <div class="carousel-inner text-center">

                                  <!-- Quote 1 -->
                                  <div class="item active">
                                    <div class="col-md-12 mg-top">
          <div class="row">
            <div class="col-md-12 platinum-bg nopadding">
              <div class="hovereffect">
                  <img class="img-responsive" src="img/project-residential-1.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <!-- <a class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br><br>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a> -->
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12 nopadding">
                  <div class="residency-img-buttom-text">
                    <h4>YooPune</h4>
                    <p><i class="fa fa-circle platinum" aria-hidden="true"></i> PLATINUM</p>
                    <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>
                                  </div>
                                  <!-- Quote 2 -->
                                  <div class="item">
                                    <div class="col-md-12 mg-top">
          <div class="row">
            <div class="col-md-12 platinum-bg nopadding">
              <div class="hovereffect">
                  <img class="img-responsive" src="img/project-residential-1.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <!-- <a class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br><br>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a> -->
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12 nopadding">
                  <div class="residency-img-buttom-text">
                    <h4>YooPune</h4>
                    <p><i class="fa fa-circle platinum" aria-hidden="true"></i> PLATINUM</p>
                    <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>
                                  </div>
                                  <!-- Quote 3 -->
                                  <div class="item">
                                    <div class="col-md-12 mg-top">
          <div class="row">
            <div class="col-md-12 platinum-bg nopadding">
              <div class="hovereffect">
                  <img class="img-responsive" src="img/project-residential-1.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
                       <!-- <a class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; View project</a><br><br>
                       <a class="info" href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp; share</a> -->
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12 nopadding">
                  <div class="residency-img-buttom-text">
                    <h4>YooPune</h4>
                    <p><i class="fa fa-circle platinum" aria-hidden="true"></i> PLATINUM</p>
                    <p>2, 3 BHK PREMIUM VILLAS 850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>
                                  </div>
                              </div>

                              <!-- Carousel Buttons Next/Prev -->
                              <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                              <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                          </div>
                      </div>
</div>



</div>
</div>

      </div>
    </div>
  <!-- location map end -->
  </div><!-- container close -->
</section>
<?php include 'footer.html' ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="lib/jquery.bxslider/jquery.bxslider.min.js"></script>

<!-- Include Unite Gallery core files -->
<script src='lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
    	 var data=$(this).find(".text").html();
    		$(".img-hover-text").hide().stop().html(data).fadeIn();

    	}, function() {
    		$(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
	   jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end
   });
</script>

<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
	$(function () {
	var filterList = {
		init: function () {
			$('#KeyAmenities').mixItUp({
				selectors: {
					target: '.KeyAmenities-portfolio',
					filter: '.KeyAmenities-list'
				},
				load: {
					filter: '.parking, .upper_deck, .lower_deck'
				}
			});
		}
	};
	filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end
</script>
<!-- main menu with search -->
<script src="js/main.js"></script>
<script src="js/footer.js"></script>
<!-- main menu with search -->
</body>
</html>
