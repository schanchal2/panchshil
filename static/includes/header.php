<?php
<header class="header-section">
  <div class="container-fluid">
        <nav role="navigation" class="navbar navbar-white header">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand" style="height:auto"><img src="img/logo.jpg"/></a>
            </div>
            <!-- Collection of nav links and other content for toggling -->
            <div id="navbarCollapse" class="collapse navbar-collapse quick-link">
              <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="../home/home.php"><img src="img/home_logo.png" /></a></li>
                    <li><a href="about.html">About</a></li>
                    <li><a href="business.html">Business</a></li>
                    <li><a href="alliances.html">Alliances</a></li>
                    <li><a href="sustainability.html">Sustainability</a></li>
                    <li><a href="career.html">Career</a></li>
                    <li><a href="media.html">Media</a></li>
                    <li><a href="contact.html">Contact</a></li>
                    <li><a href="#" class="search_icon"><img src="img/search_icon.png"/></a></li>
                </ul>
            </div>
        </nav>
    <!-- row -->
  </div>
  <!-- container-->
</header>

?>
