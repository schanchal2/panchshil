
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="lib/jquery.bxslider/jquery.bxslider.css"/>

  <!-- Include Unite Gallery core files -->
	<link rel='stylesheet' type='text/css' href='lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='lib/unitegallery/themes/default/ug-theme-default.css'/>
  <link rel='stylesheet' type='text/css' href='css/protected.css'/>

  <link rel="stylesheet" type="text/css" href="css/app.css?Tuesday 27th of December 2016 05:50:06 PM"/>

</head>
<body class="nav-is-fixed">
  <header class="header-section">
  	<div class="container-fluid">
  				<nav role="navigation" class="navbar navbar-white header">
  				    <!-- Brand and toggle get grouped for better mobile display -->
  				    <div class="navbar-header">
  				        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
  				            <span class="sr-only">Toggle navigation</span>
  				            <span class="icon-bar"></span>
  				            <span class="icon-bar"></span>
  				            <span class="icon-bar"></span>
  				        </button>
                  <a href="#" class="navbar-brand" style="height:auto"><img src="img/logo.jpg"/></a>
  				    </div>
  				    <!-- Collection of nav links and other content for toggling -->
  				    <div id="navbarCollapse" class="collapse navbar-collapse quick-link">
  			    		<ul class="nav navbar-nav navbar-right">
  				            <li class="active"><a href="index.html"><img src="img/home_logo.png" /></a></li>
  				            <li><a href="about.html">About</a></li>
  				            <li><a href="business.html">Business</a></li>
  				            <li><a href="alliances.html">Alliances</a></li>
  				            <li><a href="sustainability.html">Sustainability</a></li>
  				            <li><a href="career.html">Career</a></li>
  				            <li><a href="media.html">Media</a></li>
  				            <li><a href="contact.html">Contact</a></li>
  				            <li><a href="#" class="search_icon"><img src="img/search_icon.png"/></a></li>
  				        </ul>
  				    </div>
  				</nav>
      <!-- row -->
  	</div>
    <!-- container-->
  </header>
  <!-- header-->
<section class="hero-section hero-banner-img" style="background-image: url(http://localhost/panchshil_local/admin/uploads/businessImages/585293d0d0a9e.jpg);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Business</a> / <a href="#">Offerings123</a>
          </div>
          <h1>Offerings123</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="hero-section hero-banner-img" style="background-image: url(http://localhost/panchshil_local/admin/uploads/businessImages/585293f921bb4.jpg);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Business</a> / <a href="#">Services</a>
          </div>
          <h1>Services</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="hero-section hero-banner-img" style="background-image: url(http://localhost/panchshil_local/admin/uploads/businessImages/5853b1da4e828.png);">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-section-col-padding">
        <div class="hero-banner-text">
          <div class="hero-banner-link">
              <a href="#">Home</a> / <a href="#">Business</a> / <a href="#">Jagtar</a>
          </div>
          <h1>Jagtar</h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="offering-container-section">
  </section>
  <footer>
    <div class="container footer">
  	<!-- footer-quick-link -->
  		<div class="row footer-quick-link-hr-border">
  						<div class="col-md-2 col-sm-12">
  				<div class="footer-quick-link">
  					<h5><a href="../businessCatagories/businessCatagories.php?id=1">Offerings</a></h5>
  					<ul>
  					<li><a href="commercial.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Commercial</a></li>
  					<li><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Residential</a></li>
  					<li><a href="hospitality.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Hospitality</a></li>
  					<li><a href="retail.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Retail</a></li>
            </ul>
  				</div>

  			</div>
  						<div class="col-md-2 col-sm-12">
  				<div class="footer-quick-link">
  					<h5><a href="../businessCatagories/businessCatagories.php?id=2">Services</a></h5>
  					<ul>
                    <li><a href="commercial.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Project Management</a></li>


  											</ul>
  				</div>

  			</div>
  						<div class="col-md-2 col-sm-12">
  				<div class="footer-quick-link">
  					<h5><a href="#">corporate profile</a></h5>
  					<ul>
  						<li><a href="../foreward/foreward.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> foreword</a></li>
  						<li><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> company profile</a></li>
  						<li><a href="../GrowthChronicle/growth_chronicle.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> growth chronicle</a></li>
  						<li><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> group philosophy</a></li>
  					</ul>
  				</div>
  			</div>
  			<div class="col-md-2 col-sm-12">
  				<div class="footer-quick-link">
  					<h5><a href="../media/media.php">Media</a></h5>
  					<ul>
  						<li><a href="../press/press.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> press</a></li>
  						<li><a href="../award/award.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Awards</a></li>
  					</ul>
  					<br/>
  					<h5><a href="#">downloads</a></h5>
  				</div>
  			</div>
  			<div class="col-md-2 col-sm-12">
  				<div class="footer-quick-link">
  					<h5><a href="#">career</a></h5>
  					<ul>
  						<li><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> HR align</a></li>
  						<li><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> PEP</a></li>
  					</ul>
  					<br/>
  					<h5><a href="../leadership/leadership.php">leadership</a></h5>
  				</div>
  			</div>
  			<div class="col-md-2 col-sm-12">
  				<div class="footer-quick-link">
  					<h5><a href="#">sustainability</a></h5>
  					<ul>
  						<li><a href="../business/business.php"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> in business</a></li>
  						<li><a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> CSR</a></li>
  					</ul>
  					<br/>
  					<h5><a href="../clients/clients.php">clients</a></h5>
  				</div>
  			</div>
  		</div>
  	<!-- footer-quick-link end -->
  	<!-- contact, location map , follow us -->
  		<div class="row">
  			<div class="col-md-4 col-sm-12 contact padding2">
  				 <h5 class="footer-header-padding">CONTACT</h5>
  				 <div class="row">
  				 				 	<div class="col-sm-6">
  				 		<p class="office-location">MUMBAI</p>
  				 		<p class="office-address">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel elit blandit, tempor orci consectetur, egestas quam. Sed aliquet elit orci.</p>
  				 	</div>
  				 				 	<div class="col-sm-6">
  				 		<p class="office-location">Pune</p>
  				 		<p class="office-address">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel elit blandit, tempor orci consectetur,</p>
  				 	</div>
  				 				 </div>
  				 <br/><br/>
  				 <div class="row">
  				 				 	<div class="col-sm-6 contact-no">
  				 		<p>T :0123456789 </p>
  				 		<p>F : 1023045607</p>
  				 	</div>
  				 				 	<div class="col-sm-6 contact-no">
  				 		<p>T :7894561230 </p>
  				 		<p>F : 12344566778</p>
  				 	</div>
  				 				 </div>
  			</div>
  			<div class="col-md-4 col-sm-12 location-map">
  				<h5 class="footer-header-padding">LOCATION ON MAP</h5>
  				<div class="location-content">
  					<span>Select Property: </span>
  					<select class="location-dropdown-list">
  						<option selected>Property Name</option>
  						<option>Mumbai</option>
  						<option>Pune</option>
  					</select>
  				</div>
  				<img src="img/worldMap.png" class="img-responsive">
  			</div>
  			<div class="col-md-4 col-sm-12 follow-us">
  				<h5 class="footer-header-padding">FOLLOW US ON</h5>
  				<ul class="social-icon">
  					<li><a href="" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
  					<li><a href="" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
  					<li><a href="" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
  					<li><a href="" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
  					<li><a href="" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
  				</ul>
  				<br/>
  				<div class="login-signup">
  					<a href="#"><img src="img/login-signup.png"/> &nbsp; LOGIN / SIGN UP</a>
  				</div>
  			</div>
  		</div>
  	<!-- contact, location map , follow us end -->
  	</div><!-- container -->
  	<!-- footer bottom -->

          <div class="sliderside">
              <!-- ngRepeat: cat in categories --><div class="flexside ng-scope" ng-repeat="cat in categories">
                  <div class="imgslider">
                      <img src="img/residential.jpg">
                      <!-- <div class="black-overlay"></div> -->
                  </div>

                  <div class="relativetxt" ng-click="toexplore(cat)">
                      <i class="educa caticons"></i>
                      <p class="ng-binding">Residential</p>
                  </div>
              </div><!-- end ngRepeat: cat in categories --><div class="flexside ng-scope" ng-repeat="cat in categories">
                  <div class="imgslider">
                        <img src="img/commercial.jpg">
                      <!-- <div class="black-overlay"></div> -->
                  </div>

                  <div class="relativetxt" ng-click="toexplore(cat)">
                      <i class="child caticons"></i>
                      <p class="ng-binding">commercial</p>
                  </div>
              </div><!-- end ngRepeat: cat in categories --><div class="flexside ng-scope" ng-repeat="cat in categories">
                  <div class="imgslider">
                    <img src="img/hospitality.jpg">
                      <!-- <div class="black-overlay"></div> -->
                  </div>

                  <div class="relativetxt" ng-click="toexplore(cat)">
                      <i class="healt caticons"></i>
                      <p class="ng-binding">hospitality</p>
                  </div>
              </div><!-- end ngRepeat: cat in categories --><div class="flexside ng-scope" ng-repeat="cat in categories">
                  <div class="imgslider">
                      <img src="img/retail.jpg">
                      <!-- <div class="black-overlay"></div> -->
                  </div>

                  <div class="relativetxt" ng-click="toexplore(cat)">
                      <i class="envir caticons"></i>
                      <p class="ng-binding">retail</p>
                  </div>
              </div>
          </div>

  			<!-- container fluid -->
  	<!-- footer bottom end -->
  	<!-- copy right -->
  	<div class="copy-right">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-6 col-sm-6 col-xs-12">
  					<p>Privacy Policy Terms & Conditions</p>
  				</div>
  				<div class="col-sm-6">
  					<p class="pull-right">Site designed & maintained by <a href="#" style="color: #fff; text-decoration: none;">Neon Tree</a></p>
  				</div>
  			</div>
  		</div>
  	</div>
  	<!-- copy right end -->
  <!-- Footer end -->
  </footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="lib/jquery.bxslider/jquery.bxslider.min.js"></script>

<!-- Include Unite Gallery core files -->
<script src='lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
    	 var data=$(this).find(".text").html();
    		$(".img-hover-text").hide().stop().html(data).fadeIn();

    	}, function() {
    		$(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
	   jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end
   });
</script>

<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
	$(function () {
	var filterList = {
		init: function () {
			$('#KeyAmenities').mixItUp({
				selectors: {
					target: '.KeyAmenities-portfolio',
					filter: '.KeyAmenities-list'
				},
				load: {
					filter: '.parking, .upper_deck, .lower_deck'
				}
			});
		}
	};
	filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end
</script>

</body>
</html>
