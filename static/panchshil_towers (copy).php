
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


  <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="lib/jquery.bxslider/jquery.bxslider.css"/>

  <!-- Include Unite Gallery core files -->
	<link rel='stylesheet' type='text/css' href='lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='lib/unitegallery/themes/default/ug-theme-default.css'/>

  <link rel="stylesheet" type="text/css" href="css/app.css?Tuesday 27th of December 2016 05:39:27 PM"/>
  <link rel='stylesheet' type='text/css' href='css/protected.css'/>
  <link href="css/jquery-gallery.css" rel="stylesheet" type="text/css">

  <!-- main menu with search -->
  <link rel="stylesheet" href="css/reset.css">
  <!-- CSS reset -->
  <link rel="stylesheet" href="css/style.css">
   <!-- Resource style -->
<!-- main menu with search -->



</head>
<body class="nav-is-fixed">
  <?php include 'header.html' ?>
  <div style="height:120px;"></div>

  <!-- header-->
<section class="property-section">
	<div class="container-fluid">
	  <div class="row">
	    <div class="col-md-12 property-slider">
	      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
              <li data-target="#carousel-example-generic" data-slide-to="3"></li>
              <li data-target="#carousel-example-generic" data-slide-to="4"></li>
          </ol>
          <div class="carousel-inner">
          	<div class="item active">
              <img src="img/panchshil_banner.jpg" class="img-responsive" alt="First slide">
              <div class="carousel-caption">
                <p class="didot-font property-slider-title">panchshil</p>
								<p class="didot-font property-slider-subtitle">towers</p>
              </div>
            </div>
            <div class="item">
              <img src="img/panchshil_banner.jpg" class="img-responsive" alt="Second slide">
              <div class="carousel-caption">
								<p class="didot-font property-slider-title">panchshil</p>
								<p class="didot-font property-slider-subtitle">towers</p>
              </div>
            </div>
            <div class="item">
              <img src="img/panchshil_banner.jpg" class="img-responsive" alt="Third slide">
              <div class="carousel-caption">
								<p class="didot-font property-slider-title">panchshil</p>
								<p class="didot-font property-slider-subtitle">towers</p>
              </div>
            </div>
            <div class="item">
              <img src="img/panchshil_banner.jpg" class="img-responsive" alt="Third slide">
              <div class="carousel-caption">
								<p class="didot-font property-slider-title">panchshil</p>
								<p class="didot-font property-slider-subtitle">towers</p>
              </div>
            </div>
            <div class="item">
              <img src="img/panchshil_banner.jpg" class="img-responsive" alt="Third slide">
              <div class="carousel-caption">
								<p class="didot-font property-slider-title">panchshil</p>
								<p class="didot-font property-slider-subtitle">towers</p>
              </div>
            </div>
          </div>
	    	</div>
        <div class="property-path">
					<div class="property-path-link">
	            <a href="#">Home</a> / <a href="#">Offering</a> / <a href="#">Residence</a> / <a href="#">Panchshil Towers</a>
	        </div>
        </div>
				<div class="property-enquire-btn">
		      <a data-toggle="modal" data-target="#enquire_now" class="btn btn-default enquire-btn"><i class="fa fa-envelope-o" aria-hidden="true"></i> &nbsp;&nbsp;ENQUIRE NOW</a>
        </div>
	    </div>
	  </div>
	</div>
</section>

<!-- ====================
	Enquiry now popup
=========================-->
<div class="modal fade" id="enquire_now" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content enquire_modal-content">
      <div class="modal-header enquire_modal-header">
				<img src="img/enquiry-form-banner.jpg" class="img-responsive"/>
				<div class="enquire_modal-header-overlay">
					<p class="didot-bold-font enquire_modal-header-1st-title">panchshil</p>
					<p class="didot-bold-font enquire_modal-header-2nd-title">towers</p>
				</div>
      </div>
      <div class="modal-body">
        <h3 class="text-center didot-font">Enquiry Form</h3><br/>
				<div class="enquire_now-form">
          <form>
          <input type="text" class="form-control enquire-input" placeholder="NAME"><br/>
          <input type="email" class="form-control enquire-input" placeholder="EMAIL"><br/>
					<input type="text" class="form-control enquire-input" placeholder="MOBILE / PHONE"><br/>

					<h4 class="didot-font">When should we contact you?</h4><br/>
          <div class="row">
						<div class="col-xs-6">
							<input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control enquire-input" placeholder="DATE">
						</div>
						<div class="col-xs-6">
							<input type="text" onfocus="(this.type='time')" onblur="(this.type='text')" class="form-control enquire-input" placeholder="TIME">
						</div>
					</div>
          <br/>
					<input type="reset" value="RESET" class="enquire-reset-btn">
          <a href="#" class="btn btn-default enquire-submit-btn">SUBMIT NOW</a>
        </from>
        </div>
      </div>
    </div>
		<p class="esc-close">ESC to close window</p>
  </div>
</div>
<!-- ====================
	Enquiry now popup end
=========================-->

<section class="property-content-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 para">
				<h2 class="didot-font text-center">Headline Goes here..</h2>
				<br/>
				<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
			</div>
		</div><!-- row end -->
		<br/>

		<div class="row">
			<div class="col-md-12 text-center">
				<ul class="property-details-link">
					<li><a href="#"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp;GOLD PROPERTY</a></li>
					<li><a href="#"><i class="fa fa-check-circle" aria-hidden="true"></i> &nbsp;UNDER DEVELOPMENT</a></li>
					<li><a href="" data-toggle="modal" data-target=".gallery-popup"><i class="fa fa-picture-o" aria-hidden="true"></i> &nbsp;VIEW GALLERY</a></li>
					<li><a href="#"><i class="fa fa-external-link" aria-hidden="true"></i> &nbsp;VISIT WEBSITE</a></li>
					<li><a href="#"><i class="fa fa-download" aria-hidden="true"></i> &nbsp;DOWNLOAD PDF</a></li>
				</ul>
			</div>
		</div><!-- row end -->
		<br/><br/>

	<!-- ============
		Gallery popup
	=================-->
	<div class="modal fade gallery-popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content gallery-modal-content">
			<!-- ======================== -->
			<div class="gallery-padding">
				<div id="gallery">
					<img alt="Preview Image 1"
					 src="img/Property-Detai-slider-1.jpg"
					 data-image="img/Property-Detai-slider-1.jpg"
					 data-description="Preview Image 1 Description"/>

					<img alt="Preview Image 2"
					 src="img/Panchashil_Media.jpg"
					 data-image="img/Panchashil_Media.jpg"
					 data-description="Preview Image 2 Description"/>

					<img alt="Preview Image 1"
 					 src="img/Property-Detai-slider-1.jpg"
 					 data-image="img/Property-Detai-slider-1.jpg"
 					 data-description="Preview Image 3 Description"/>

	 				<img alt="Preview Image 2"
					 src="img/Panchashil_Media.jpg"
					 data-image="img/Panchashil_Media.jpg"
					 data-description="Preview Image 4 Description"/>

					<img alt="Preview Image 1"
					 src="img/Property-Detai-slider-1.jpg"
					 data-image="img/Property-Detai-slider-1.jpg"
					 data-description="Preview Image 5 Description"/>

	 				<img alt="Preview Image 2"
					 src="img/Panchashil_Media.jpg"
					 data-image="img/Panchashil_Media.jpg"
					 data-description="Preview Image 6 Description"/>

					<img alt="Preview Image 1"
 					 src="img/Property-Detai-slider-1.jpg"
 					 data-image="img/Property-Detai-slider-1.jpg"
 					 data-description="Preview Image 7 Description"/>

	 	 			<img alt="Preview Image 2"
					 src="img/Panchashil_Media.jpg"
					 data-image="img/Panchashil_Media.jpg"
					 data-description="Preview Image 8 Description"/>

	 				<img alt="Preview Image 1"
					 src="img/Property-Detai-slider-1.jpg"
					 data-image="img/Property-Detai-slider-1.jpg"
					 data-description="Preview Image 9 Description"/>

	 	 			<img alt="Preview Image 2"
					 src="img/Panchashil_Media.jpg"
					 data-image="img/Panchashil_Media.jpg"
					 data-description="Preview Image 10 Description"/>
				</div>
			</div>
			<!-- ======================== -->
		</div>
		<p class="esc-close">ESC to close window</p>
	</div>
	</div>
	<!-- ============
		Gallery popup end
	=================-->

		<div class="row">
			<div class="col-md-12 property-img-circle">
				<div class="property-img-circle-slider">
					<div class="img-circle-slider">
		        <a href="#"><img src="img/panchshil_clients.jpg" class="img-responsive img-circle" alt=""></a>
						<div class="img-overlay-count"><p class="didot-font">1</p></div>
						<div class="text para">
							<h2 class="didot-font">1</h2>
              <p>Wide roads, uninterupted sidewalks and zero-traffic promenades reveal the scenic charm of the surroundings</p>

						</div>
		      </div>
		      <div class="img-circle-slider">
		        <a href="#"><img src="img/panchshil_clients1.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"><p class="didot-font">2</p></div>
						<div class="text para">
							<h2 class="didot-font">2</h2>
              <p>Wide roads, uninterupted sidewalks and zero-traffic promenades reveal the scenic charm of the surroundings</p>

						</div>
		      </div>
		      <div class="img-circle-slider">
		        <a href="#"><img src="img/panchshil_clients2.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"><p class="didot-font">3</p></div>
						<div class="text para">
							<h2 class="didot-font">3</h2>
              <p>Wide roads, uninterupted sidewalks and zero-traffic promenades reveal the scenic charm of the surroundings</p>

						</div>
		      </div>
		      <div class="img-circle-slider">
		        <a href="#"><img src="img/panchshil_clients3.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"><p class="didot-font">4</p></div>
						<div class="text para">
							<h2 class="didot-font">4</h2>
              <p>Wide roads, uninterupted sidewalks and zero-traffic promenades reveal the scenic charm of the surroundings</p>

						</div>
		      </div>
          <div class="img-circle-slider">
            <a href="#"><img src="img/panchshil_clients.jpg" class="img-responsive img-circle" alt=""></a>
            <div class="img-overlay-count"><p class="didot-font">1</p></div>
            <div class="text para">
              <h2 class="didot-font">5</h2>
              <p>Wide roads, uninterupted sidewalks and zero-traffic promenades reveal the scenic charm of the surroundings</p>

            </div>
          </div>
          <div class="img-circle-slider">
            <a href="#"><img src="img/panchshil_clients1.jpg" class="img-responsive img-circle" alt="Updates image"></a>
            <div class="img-overlay-count"><p class="didot-font">2</p></div>
            <div class="text para">
              <h2 class="didot-font">6</h2>
              <p>Wide roads, uninterupted sidewalks and zero-traffic promenades reveal the scenic charm of the surroundings</p>

            </div>
          </div>
          <div class="img-circle-slider">
            <a href="#"><img src="img/panchshil_clients2.jpg" class="img-responsive img-circle" alt="Updates image"></a>
            <div class="img-overlay-count"><p class="didot-font">3</p></div>
            <div class="text">
              <h2 class="didot-font">7</h2>
              <p>Wide roads, uninterupted sidewalks and zero-traffic promenades reveal the scenic charm of the surroundings</p>

            </div>
          </div>
          <div class="img-circle-slider">
            <a href="#"><img src="img/panchshil_clients3.jpg" class="img-responsive img-circle" alt="Updates image"></a>
            <div class="img-overlay-count"><p class="didot-font">4</p></div>
            <div class="text para">
              <h2 class="didot-font">8</h2>
              <p>Wide roads, uninterupted sidewalks and zero-traffic promenades reveal the scenic charm of the surroundings</p>
            </div>
          </div>
				</div>
			</div>
		</div><!-- row end -->

		<div class="row">
			<div class="col-md-12 text-center img-hover-text">
				<h2 class="didot-font">1</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
			</div>
		</div><!-- row end -->
		<br/><br/>
		<hr class="hr-color"/>

		<div class="row">
			<div class="col-md-12 text-center">
				<h2 class="text-center didot-bold-font">Key Amenities</h2>
				<br/>
				<ul class="filter-nav" id="filters">
          <li><a class="KeyAmenities-list active" data-filter=".parking, .upper_deck, .lower_deck">All</a></li>
          <li><a class="KeyAmenities-list" data-filter=".parking">Parking</a></li>
          <li><a class="KeyAmenities-list" data-filter=".upper_deck">Upper Deck</a></li>
          <li><a class="KeyAmenities-list" data-filter=".lower_deck">Lower Deck</a></li>
        </ul>
				<br/>
			</div>
		</div>
		<div class="row">
      <div class="coverimg">
      				<!-- <a href="#"><img src="http://lorempixel.com/1024/728/business/2" data-gallery="first-gallery" alt=""/></a>
      				<a href="#"><img src="http://lorempixel.com/1024/728/sports/2" data-gallery="first-gallery" alt="" /></a>
      				<a href="#"><img src="http://lorempixel.com/1024/728/animals/3" data-gallery="first-gallery" alt="" /></a>
      				<a href="#"><img src="http://lorempixel.com/1024/728/cats/1" data-gallery="first-gallery" alt="" /></a>
      				<a href="#"><img src="http://lorempixel.com/1024/728/nature/2" data-gallery="first-gallery" alt="" /></a> -->
      		</div>
          <div id="KeyAmenities">
              <div class="col-md-4 col-padding">
                  <div class="KeyAmenities-portfolio parking" data-cat="parking">
                      <div class="portfolio-wrapper">
                          <!-- <img src="img/property-detail/amenities-img-01.jpg" alt="" class="KeyAmenities-col-1and3-img" /> -->
                          <a href="#"><img src="img/property-detail/amenities-img-01.jpg" data-info="about img 1" data-gallery="first-gallery" alt="" class="KeyAmenities-col-1and3-img" /></a>
                          <div class="portfolio-wrapper-overlay-icon">
                              <img src="img/property-detail/amenities-01.png" />
                          </div>
                          <div class="portfolio-wrapper-overlay-text">
                              <p>swiming pool</p>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-4 col-padding">
                  <div class="row" style="margin: 0;">
                      <div class="col-md-12 col-padding">
                          <div class="KeyAmenities-portfolio parking" data-cat="parking">
                              <div class="portfolio-wrapper">
                                  <!-- <img src="img/property-detail/amenities-img-02.jpg" alt="" class="KeyAmenities-col-1and3-img" /> -->
                                  <a href="#"><img src="img/property-detail/amenities-img-02.jpg" data-info="about img 2" data-gallery="first-gallery" alt="" class="KeyAmenities-col-1and3-img" /></a>
                                  <div class="portfolio-wrapper-overlay-icon">
                                      <img src="img/property-detail/amenities-03.png" />
                                  </div>
                                  <div class="portfolio-wrapper-overlay-text">
                                      <p>kids playground</p>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-12">
                          <div class="row">
                              <div class="col-md-6 col-padding">
                                  <div class="KeyAmenities-portfolio upper_deck" data-cat="upper_deck">
                                      <div class="portfolio-wrapper">
                                          <!-- <img src="img/property-detail/amenities-img-03.jpg" alt="" class="KeyAmenities-col-2-img" /> -->
                                          <a href="#"><img src="img/property-detail/amenities-img-03.jpg" data-info="about img03" data-gallery="first-gallery" alt="" class="KeyAmenities-col-1and3-img" /></a>
                                          <div class="portfolio-wrapper-overlay-icon">
                                              <img src="img/property-detail/amenities-05.png" />
                                          </div>
                                          <div class="portfolio-wrapper-overlay-text">
                                              <p>gym</p>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6 col-padding">
                                  <div class="KeyAmenities-portfolio upper_deck" data-cat="upper_deck">
                                      <div class="portfolio-wrapper">
                                          <!-- <img src="img/property-detail/amenities-img-04.jpg" alt="" class="KeyAmenities-col-2-img" /> -->
                                          <a href="#"><img src="img/property-detail/amenities-img-04.jpg" data-info="about img04" data-gallery="first-gallery" alt="" class="KeyAmenities-col-1and3-img" /></a>
                                          <div class="portfolio-wrapper-indoor-game-icon">
                                              <img src="img/property-detail/amenities-02.png" />
                                          </div>
                                          <div class="portfolio-wrapper-indoor-game-text">
                                              <p>indoor game</p>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-4 col-padding">
                  <div class="KeyAmenities-portfolio lower_deck" data-cat="lower_deck">
                      <div class="portfolio-wrapper">
                          <!-- <img src="img/property-detail/amenities-img-05.jpg" alt="" class="KeyAmenities-col-1and3-img" /> -->
                          <a href="#"><img src="img/property-detail/amenities-img-05.jpg" data-info="about img05" data-gallery="first-gallery" alt="" class="KeyAmenities-col-1and3-img" /></a>
                          <div class="portfolio-wrapper-overlay-icon">
                              <img src="img/property-detail/amenities-04.png" />
                          </div>
                          <div class="portfolio-wrapper-overlay-text">
                              <p>community hall</p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
			<!-- <div id="KeyAmenities">
				<div class="col-md-4 col-sm-4 col-padding ">
					<div class="KeyAmenities-portfolio parking" data-cat="parking">
						<div class="portfolio-wrapper">
							<a href="#"><img src="img/property-detail/amenities-img-01.jpg" class="KeyAmenities-col-1and3-img"/></a>
              <a href="#"><img src="img/property-detail/amenities-img-01.jpg" data-gallery="first-gallery" alt="" class="KeyAmenities-col-1and3-img" /></a>

							<div class="portfolio-wrapper-overlay-icon">
								<img src="img/property-detail/amenities-01.png"/>
							</div>
							<div class="portfolio-wrapper-overlay-text">
								<p>swiming pool</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-padding">
					<div class="row" style="margin: 0;">
						<div class="col-md-12 col-padding">
							<div class="KeyAmenities-portfolio parking" data-cat="parking">
								<div class="portfolio-wrapper">
									<img src="img/property-detail/amenities-img-02.jpg" alt="" class="KeyAmenities-col-2-img"/>
									<div class="portfolio-wrapper-overlay-icon">
										<img src="img/property-detail/amenities-03.png"/>
									</div>
									<div class="portfolio-wrapper-overlay-text">
										<p>kids playground</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6 col-sm-6 col-padding">
									<div class="KeyAmenities-portfolio upper_deck" data-cat="upper_deck">
										<div class="portfolio-wrapper">
											<img src="img/property-detail/amenities-img-03.jpg" alt="" class="KeyAmenities-col-2-img"/>
											<div class="portfolio-wrapper-overlay-icon">
												<img src="img/property-detail/amenities-05.png"/>
											</div>
											<div class="portfolio-wrapper-overlay-text">
												<p>gym</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6  col-sm-6 col-padding">
									<div class="KeyAmenities-portfolio upper_deck" data-cat="upper_deck">
										<div class="portfolio-wrapper">
											<img src="img/property-detail/amenities-img-04.jpg" alt="" class="KeyAmenities-col-2-img"/>
											<div class="portfolio-wrapper-indoor-game-icon">
												<img src="img/property-detail/amenities-02.png"/>
											</div>
											<div class="portfolio-wrapper-indoor-game-text">
												<p>indoor game</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-padding">
					<div class="KeyAmenities-portfolio lower_deck" data-cat="lower_deck">
						<div class="portfolio-wrapper">
							<img src="img/property-detail/amenities-img-01.jpg" alt="" class="KeyAmenities-col-1and3-img"/>
							<div class="portfolio-wrapper-overlay-icon">
								<img src="img/property-detail/amenities-04.png"/>
							</div>
							<div class="portfolio-wrapper-overlay-text">
								<p>community hall</p>
							</div>
						</div>
					</div>
				</div>
			</div> -->
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<br/><br/>
        <div class="common-btn"><a class="btn btn-default" href="#">VIEW ALL AMENITIES</a></div>

			</div>
		</div>


		<!-- Property layout -->
		<div class="row">
			<div class="col-md-12 text-center">
				<br/><br/><br/>
				<h2 class="text-center didot-bold-font">Property Layout & Floor Plans</h2>
				<br/>
				<ul class="filter-nav" id="filters">
          <li><a class="PropertyLayout-list active" data-filter=".3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse">All</a></li>
          <li><a class="PropertyLayout-list" data-filter=".3bhk">3 BHK</a></li>
          <li><a class="PropertyLayout-list" data-filter=".4bhk">4 BHK</a></li>
          <li><a class="PropertyLayout-list" data-filter=".5bhk-podium-villa">5 BHK - Podium villa</a></li>
					<li><a class="PropertyLayout-list" data-filter=".5bhk-penthouse">5 BHK - Penthouse</a></li>
        </ul>
				<br/><br/>
			</div>
		</div>

		<div class="row">
			<div id="PropertyLayout">
				<div class="propertylayout-slider">
					<div class="propertylayout-img">
						<div class="PropertyLayout-portfolio 3bhk" data-cat="3bhk">
							<div class="portfolio-wrapper" data-toggle="modal" data-target="#floor_plansModal">
								<img src="img/floor-plan/floor-plan-01.png" alt=""/>
								<p>Property Layout</p>
							</div>
						</div>
					</div>
					<div class="propertylayout-img">
						<div class="PropertyLayout-portfolio 4bhk" data-cat="4bhk">
							<div class="portfolio-wrapper" data-toggle="modal" data-target="#floor_plansModal">
								<img src="img/floor-plan/floor-plan-01.png" alt=""/>
								<p>3BHK</p>
							</div>
						</div>
					</div>
					<div class="propertylayout-img">
						<div class="PropertyLayout-portfolio 5bhk-podium-villa" data-cat="5bhk-podium-villa">
							<div class="portfolio-wrapper" data-toggle="modal" data-target="#floor_plansModal">
								<img src="img/floor-plan/floor-plan-01.png" alt=""/>
								<p>4BHK</p>
							</div>
						</div>
					</div>
					<div class="propertylayout-img">
						<div class="PropertyLayout-portfolio 5bhk-penthouse" data-cat="5bhk-penthouse">
							<div class="portfolio-wrapper" data-toggle="modal" data-target="#floor_plansModal">
								<img src="img/floor-plan/floor-plan-01.png" alt=""/>
								<p>5 BHK - Podium Villa</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- ==========================================
		  Property Layout and floor plan popup Modal
		=============================================== -->
		<div class="modal fade" id="floor_plansModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div class="floor_plans-padding">
							<div id="floor_plans">
								<img alt="Preview Image 1"
								 src="img/floor-plan/floor-plan-01.png"
								 data-image="img/floor-plan/floor-plan-zoom-01.jpg"
								 data-description="Preview Image 1 Description"/>
							</div>
						</div>
					</div>
				</div>
				<p class="esc-close">ESC to close window</p>
			</div>
		</div>

		<!-- ===========================================
		  Property Layout and floor plan popup Modal end
		=============================================== -->

		<!-- Property layout end -->

		<!-- Locate on Map -->
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center didot-bold-font">Locate on Map</h2>
				<br/>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.482543630902!2d73.96005811489322!3d18.55227158739056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c3b8407218ff%3A0x23125fe713d028d3!2sPanchshil+Towers+Kharadi!5e0!3m2!1sen!2sin!4v1484387555486" width="100%" height="360" frameborder="0" style="border:0" allowfullscreen></iframe>
				<!-- <img src="img/locate_on_map.jpg" class="img-responsive"/> -->
			</div>
		</div>
		<br/><br/>
		<!-- Locate on Map end -->

		<!-- ===================================== -->

		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center didot-bold-font">Property Type</h2><br/>
			</div>
		</div>

		<div class="row PropertyType">
      <div class="col-md-4 mg-top">
          <div class="row">
            <div class="col-md-12">
              <div class="property-hovereffect">
                  <img class="img-responsive" src="img/panchshil_property_type01.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
											 <a data-toggle="modal" data-target="#enquire_now" class="info" href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp; enquire now</a><br/><br/>
                       <a data-toggle="modal" data-target="#floor_plansModal" class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; floor plan</a><br/><br/>
											 <a class="info" data-toggle="modal" data-target=".specifications-popup"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp; Specifications</a><br/><br/>
                       <a class="info" href="#"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp; know more</a>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="property-img-buttom-text">
                    <h4>3 BHK</h4>
                    <p>Panchshil Towers</p>
                    <p>850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="property-hovereffect">
                <img class="img-responsive" src="img/panchshil_property_type02.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
										<a data-toggle="modal" data-target="#enquire_now" class="info" href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp; enquire now</a><br/><br/>
										<a data-toggle="modal" data-target="#floor_plansModal" class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; floor plan</a><br/><br/>
										<a class="info" data-toggle="modal" data-target=".specifications-popup"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp; Specifications</a><br/><br/>
										<a class="info" href="#"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp; know more</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="property-img-buttom-text">
                  <h4>4 BHK</h4>
                  <p>Panchshil Towers</p>
                  <p>850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4 mg-top">
        <div class="row">
          <div class="col-md-12">
            <div class="property-hovereffect">
                <img class="img-responsive" src="img/panchshil_property_type03.jpg" alt="">
                <div class="overlay">
                  <div class="overlay-margin-top">
										<a data-toggle="modal" data-target="#enquire_now" class="info" href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp; enquire now</a><br/><br/>
										<a data-toggle="modal" data-target="#floor_plansModal" class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; floor plan</a><br/><br/>
										<a class="info" data-toggle="modal" data-target=".specifications-popup"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp; Specifications</a><br/><br/>
										<a class="info" href="#"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp; know more</a>
                 </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="property-img-buttom-text">
                  <h4>5 BHK - Podium villa</h4>
                  <p>Panchshil Towers</p>
                  <p>850 - 1250 SQ.FT.</p>
                </div>
            </div>
        </div>
      </div>
    </div>

		<div class="row PropertyType">
      <div class="col-md-4 mg-top">
          <div class="row">
            <div class="col-md-12">
              <div class="property-hovereffect">
                  <img class="img-responsive" src="img/panchshil_property_type04.jpg" alt="">
                  <div class="overlay">
                    <div class="overlay-margin-top">
											<a data-toggle="modal" data-target="#enquire_now" class="info" href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp; enquire now</a><br/><br/>
											<a data-toggle="modal" data-target="#floor_plansModal" class="info" href="#"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; floor plan</a><br/><br/>
											<a class="info" data-toggle="modal" data-target=".specifications-popup"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp; Specifications</a><br/><br/>
											<a class="info" href="#"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp; know more</a>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="property-img-buttom-text">
                    <h4>5 BHK - Penthouse</h4>
                    <p>Panchshil Towers</p>
                    <p>850 - 1250 SQ.FT.</p>
                  </div>
              </div>
          </div>
      </div>

			<div class="col-md-4 feachers-col-padding mg-top">
				<div class="property-feachers-overlay project-img-zoom panchshil_btn">
          <img src="img/panchshil_property_type05.jpg" class="img-responsive hidden-sm" alt="">
              <img src="img/panchshil_property_type05-tab.jpg" class="img-responsive visible-sm" alt="">
					<div class="property-feachers-overlay-text">
						<p>LOOKING FOR</p>
						<h2 class="">SOMETHING<br/>ELSE?</h2>
						<br/>
						<a href="#" class="btn btn-default"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>&nbsp;&nbsp; TRY PROPERTY FINDER</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 feachers-col-padding mg-top">
				<div class="property-feachers-overlay project-img-zoom panchshil_estate_agent">
          <img src="img/panchshil_property_type06.jpg" class="img-responsive hidden-sm" alt="">
              <img src="img/panchshil_property_type06-tab.jpg" class="img-responsive visible-sm" alt="">
					<div class="property-feachers-overlay-text property-feachers-3rd-col">
						<h2 class="feachers-3rd-text-size">ABOUT<br/>DEVELOPER</h2>
						<a href="#"><i class="fa fa-long-arrow-right fa-1x" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
    </div>
		<!-- ==================
		Specifications popup
		======================= -->
		<div class="modal fade specifications-popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content specifications-modal-content">
		      <div class="row">
						<div class="col-md-5">
							<img class="img-responsive" src="img/hospitality.jpg" alt="">
						</div>
						<div class="col-md-7 specifications-pd-right">
							<h2 class="didot-font">3 BHK</h2>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-12">
									<h3 class="didot-font">Level 01</h3>
									<ul class="specifications-popup-list">
										<li>list 01</li>
										<li>list 02</li>
										<li>list 03</li>
										<li>list 04</li>
										<li>list 05</li>
										<li>list 06</li>
										<li>list 07</li>
										<li>list 08</li>
										<li>list 09</li>
										<li>list 10</li>
									</ul>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-12">
									<h3 class="didot-font">Level 02</h3>
									<ul class="specifications-popup-list">
										<li>list 01</li>
										<li>list 02</li>
										<li>list 03</li>
										<li>list 04</li>
										<li>list 05</li>
										<li>list 06</li>
										<li>list 07</li>
										<li>list 08</li>
									</ul>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-12">
									<h3 class="didot-font">Level 03</h3>
									<ul class="specifications-popup-list">
										<li>list 01</li>
										<li>list 02</li>
										<li>list 03</li>
										<li>list 04</li>
										<li>list 05</li>
									</ul>
								</div>
							</div><!-- row end -->
							<br/><br/>
							<div class="row">
								<div class="col-md-12">
									<ol class="specifications-popup-list">
										<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor </li>
										<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor </li>
									</ol>
								</div>
							</div>
						</div>
					</div>
		    </div>
		  </div>
		</div>
		<!-- ==================
		Specifications popup end
		======================= -->
	</div><!-- container -->
</section>



<?php include 'footer.html' ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="lib/jquery.bxslider/jquery.bxslider.min.js"></script>
<script src="js/jquery-gallery.js"></script>

<!-- Include Unite Gallery core files -->
<script src='lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
    	 var data=$(this).find(".text").html();
    		$(".img-hover-text").hide().stop().html(data).fadeIn();

    	}, function() {
    		$(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
	   jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end
   });
</script>

<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
	$(function () {
	var filterList = {
		init: function () {
			$('#KeyAmenities').mixItUp({
				selectors: {
					target: '.KeyAmenities-portfolio',
					filter: '.KeyAmenities-list'
				},
				load: {
					filter: '.parking, .upper_deck, .lower_deck'
				}
			});
		}
	};
	filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end






</script>
<script>
// pop gallery

$(document).jquerygallery({

// displays a thumbnails navigation
'coverImgOverlay' : true,

// CSS classes
'imgActive' : "imgActive",
'thumbnail' : "coverImgOverlay",
'overlay' : "overlay",

// the height of the thumbnails
'thumbnailHeight' : 120,

// custom navigation controls.
// requires Font Awesome
'imgNext' : "<i class='fa fa-angle-right'></i>",
'imgPrev' : "<i class='fa fa-angle-left'></i>",
'imgClose' : "<i class='fa fa-times'></i>",

// animation speed
'speed' : 300

});
</script>
<!-- main menu with search -->
<script src="js/main.js"></script>
<script src="js/footer.js"></script>
<!-- main menu with search -->
</body>
</html>
