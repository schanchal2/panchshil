(function($) {
    "use strict"
    $.fn.multiCarousal = function(options) {

        // Defaults
        var settings = $.extend({
            'left': $('.left.carousel-control'),
            'right': $('.right.carousel-control')
        }, options);
        // console.log(this);
        // this.on('click', function(event) {
        // 	event.preventDefault();
        // 	/* Act on the event */

        // 	console.log(event);
        // });

        settings.left.on('click', function(event) {
            event.preventDefault();
            // console.log(settings.left);

            // settings.left.each(function(index, el) {
            //     this.click();
            // });
            $('.carousel.slide').carousel('prev');
        });

        // $('#carousel-id').on('slide.bs.carousel', function() {
        //     // $('.carousel.slide').
        //     // carousel('prev');
        //     // settings.left.each(function(index, el) {
        //     //     this.click();
        //     // });
        //     console.log(direction);
        // })
        settings.right.on('click', function(event) {
            event.preventDefault();
            // console.log(settings.right);

            // settings.right.each(function(index, el) {
            //     this.click();
            // });
            $('.carousel.slide').carousel('next');
        });


        this.keydown(function(event) {

            /* Act on the event */

            var code = event.keyCode || event.which;
            switch (code) {
                case 37:
                    $('.carousel.slide').carousel('prev');
                    break
                case 39:
                    $('.carousel.slide').carousel('next');
                    break
                default:
                    return
            }

        });
    };
})(jQuery);
