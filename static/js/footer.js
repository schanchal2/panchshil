jQuery(document).ready(function($) {
    /*==============================
    =            footer            =
    ==============================*/
    $('.flexside').hover(function() {
        $(this).parents('.sliderside').find('.relativetxt__text').css('display', 'none');
        $(this).find('.relativetxt__text').css('display', 'block');
        $(this).find('.relativetxt__text').addClass('isHover');
    }, function() {
        $(this).parents('.sliderside').find('.relativetxt__text').css('display', 'block');
        $(this).find('.relativetxt__text').removeClass('isHover');
    });

    /*=====  End of footer  ======*/

});
