<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Panchshil</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- main menu with search -->
    <link rel="stylesheet" href="css/reset.css">
    <!-- CSS reset -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Resource style -->
    <!-- main menu with search -->
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="lib/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="lib/jquery.bxslider/jquery.bxslider.css" />
    <!-- Include Unite Gallery core files -->
    <link rel='stylesheet' type='text/css' href='lib/unitegallery/css/unite-gallery.css' />
    <link rel='stylesheet' type='text/css' href='lib/unitegallery/themes/default/ug-theme-default.css' />
    <link rel="stylesheet" type="text/css" href="css/app.css?Tuesday 27th of December 2016 05:39:27 PM" />
    <link rel='stylesheet' type='text/css' href='css/protected.css' />
    <!-- <link href="css/jquery-gallery.css" rel="stylesheet" type="text/css"> -->
    <style>
    .coverimg {
        margin: 10px 0
    }    
    .coverimg a {
        display: inline-block;
        /*    width: 113px;
        height: 113px;*/
        overflow: hidden;
        position: relative
    }
    
    .coverimg img {
        max-height: 100%;
        min-width: 100%;
        width: auto;
        left: 50%;
        -o-transform: translateX(-50%);
        -moz-transform: translateX(-50%);
        -webkit-transform: translateX(-50%);
        transform: translateX(-50%);
        position: absolute
    }
    
    img[data-gallery] {
        cursor: pointer
    }
    
    .overlay {
        background: rgba(0, 0, 0, 0.5);
        display: none;
        height: 100%;
        position: fixed;
        width: 100%;
        top: 0;
        z-index: 999
    }
    
    .imgActive img {
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        position: absolute;
        top: 50%;
        left: 50%;
        max-width: 100%;
        max-height: 90%
    }
    
    .prev,
    .next,
    .close {
        transition: 0.3s ease all;
        display: inline-block;
        color: white;
        font-size: 6rem;
        opacity: 0.2;
        z-index: 1000
    }
    
    .prev:hover,
    .next:hover,
    .close:hover {
        cursor: pointer;
        opacity: 1
    }
    
    .prev,
    .next {
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        position: absolute;
        top:90%;
        /*bottom: 100px;*/
         color: red;
    }
    
    .prev {
        left: 50px;
       
    }
    
    .next {
        right:50px;
    }
    
    .close {
        font-size: 4rem;
        position: absolute;
        top: 20px;
        right: 20px
    }
    
    .coverImgOverlay {
        bottom: 20px;
        height: 100px;
        left: 50%;
        overflow: hidden;
        position: absolute;
        width: 3000%;
        z-index: 1000
    }
    
    .coverImgOverlay span {
        display: inline-block;
        height: 100px;
        margin: 5px;
        overflow: hidden;
        position: relative;
        width: 100px
    }
    
    .coverImgOverlay img {
        transition: 0.3s ease all;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        position: absolute;
        top: 50%;
        left: 50%;
        max-height: 100%;
        min-width: 100%;
        /*opacity: 0.2;*/
        width: auto
    }
    
    .coverImgOverlay img:hover {
        opacity: 1 !important
    }
    .overlay{
        padding: 10%;

    }
    .stage0{

        background: #fff;
        padding: 20px;
        position: relative;


    }
    .stage {
        background: #fff;
        width: 100%;
        height: 100%;
        position: relative;
        overflow: hidden;
    }
    </style>
</head>

<body>
    <?php /*include 'header.html'*/ ?>
    <!-- header-->
    <!-- ====================
    Enquiry now popup
=========================-->
    <!-- ====================
    Enquiry now popup end
=========================-->
    <section class="property-content-section">
        <div class="container">
            <div class="row">
                <!-- ============
        Gallery popup
    =================-->
                <!-- ============
        Gallery popup end
    =================-->
                <!-- row end -->
                <!-- row end -->
                <hr class="hr-color" />
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="text-center didot-bold-font">Key Amenities</h2>
                        <br/>
                        <ul class="filter-nav" id="filters">
                            <li><a class="KeyAmenities-list active" data-filter=".parking, .upper_deck, .lower_deck">All</a></li>
                            <li><a class="KeyAmenities-list" data-filter=".parking">Parking</a></li>
                            <li><a class="KeyAmenities-list" data-filter=".upper_deck">Upper Deck</a></li>
                            <li><a class="KeyAmenities-list" data-filter=".lower_deck">Lower Deck</a></li>
                        </ul>
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="coverimg">
                        <!-- <a href="#"><img src="img/property-detail/amenities-img-01.jpg" data-gallery="first-gallery" alt="" /></a> -->
                        <!-- <a href="#"><img src="http://lorempixel.com/1024/728/sports/2" data-gallery="first-gallery" alt="" /></a> -->
                        <!-- <a href="#"><img src="http://lorempixel.com/1024/728/animals/3" data-gallery="first-gallery" alt="" /></a> -->
                        <!-- <a href="#"><img src="http://lorempixel.com/1024/728/cats/1" data-gallery="first-gallery" alt="" /></a> -->
                        <!-- <a href="#"><img src="http://lorempixel.com/1024/728/nature/2" data-gallery="first-gallery" alt="" /></a> -->
                    </div>
                    <div id="KeyAmenities">
                        <div class="col-md-4 col-padding">
                            <div class="KeyAmenities-portfolio parking" data-cat="parking">
                                <div class="portfolio-wrapper">
                                    <!-- <img src="img/property-detail/amenities-img-01.jpg" alt="" class="KeyAmenities-col-1and3-img" /> -->
                                    <a href="#"><img src="img/property-detail/amenities-img-01.jpg" data-info="about img 1" data-gallery="first-gallery" alt="" class="KeyAmenities-col-1and3-img" /></a>
                                    <div class="portfolio-wrapper-overlay-icon">
                                        <img src="img/property-detail/amenities-01.png" />
                                    </div>
                                    <div class="portfolio-wrapper-overlay-text">
                                        <p>swiming pool</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-padding">
                            <div class="row" style="margin: 0;">
                                <div class="col-md-12 col-padding">
                                    <div class="KeyAmenities-portfolio parking" data-cat="parking">
                                        <div class="portfolio-wrapper">
                                            <!-- <img src="img/property-detail/amenities-img-02.jpg" alt="" class="KeyAmenities-col-1and3-img" /> -->
                                            <a href="#"><img src="img/property-detail/amenities-img-02.jpg" data-info="about img 2" data-gallery="first-gallery" alt="" class="KeyAmenities-col-1and3-img" /></a>
                                            <div class="portfolio-wrapper-overlay-icon">
                                                <img src="img/property-detail/amenities-03.png" />
                                            </div>
                                            <div class="portfolio-wrapper-overlay-text">
                                                <p>kids playground</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6 col-padding">
                                            <div class="KeyAmenities-portfolio upper_deck" data-cat="upper_deck">
                                                <div class="portfolio-wrapper">
                                                    <!-- <img src="img/property-detail/amenities-img-03.jpg" alt="" class="KeyAmenities-col-2-img" /> -->
                                                    <a href="#"><img src="img/property-detail/amenities-img-03.jpg" data-info="about img03" data-gallery="first-gallery" alt="" class="KeyAmenities-col-1and3-img" /></a>
                                                    <div class="portfolio-wrapper-overlay-icon">
                                                        <img src="img/property-detail/amenities-05.png" />
                                                    </div>
                                                    <div class="portfolio-wrapper-overlay-text">
                                                        <p>gym</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-padding">
                                            <div class="KeyAmenities-portfolio upper_deck" data-cat="upper_deck">
                                                <div class="portfolio-wrapper">
                                                    <!-- <img src="img/property-detail/amenities-img-04.jpg" alt="" class="KeyAmenities-col-2-img" /> -->
                                                    <a href="#"><img src="img/property-detail/amenities-img-04.jpg" data-info="about img04" data-gallery="first-gallery" alt="" class="KeyAmenities-col-1and3-img" /></a>
                                                    <div class="portfolio-wrapper-indoor-game-icon">
                                                        <img src="img/property-detail/amenities-02.png" />
                                                    </div>
                                                    <div class="portfolio-wrapper-indoor-game-text">
                                                        <p>indoor game</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-padding">
                            <div class="KeyAmenities-portfolio lower_deck" data-cat="lower_deck">
                                <div class="portfolio-wrapper">
                                    <!-- <img src="img/property-detail/amenities-img-05.jpg" alt="" class="KeyAmenities-col-1and3-img" /> -->
                                    <a href="#"><img src="img/property-detail/amenities-img-05.jpg" data-info="about img05" data-gallery="first-gallery" alt="" class="KeyAmenities-col-1and3-img" /></a>
                                    <div class="portfolio-wrapper-overlay-icon">
                                        <img src="img/property-detail/amenities-04.png" />
                                    </div>
                                    <div class="portfolio-wrapper-overlay-text">
                                        <p>community hall</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <br/>
                        <br/>
                        <div class="common-btn"><a class="btn btn-default" href="#">VIEW ALL AMENITIES</a></div>
                    </div>
                </div>
                <!-- Property layout -->
                <!-- ==========================================
          Property Layout and floor plan popup Modal
        =============================================== -->
                <!-- ===========================================
          Property Layout and floor plan popup Modal end
        =============================================== -->
                <!-- Property layout end -->
                <!-- Locate on Map -->
                <br/>
                <br/>
                <!-- Locate on Map end -->
                <!-- ===================================== -->
                <div class="row"></div>
                <div class="row PropertyType"></div>
                <!-- ==================
        Specifications popup
        ======================= -->
                <!-- ==================
        Specifications popup end
        ======================= -->
            </div>
            <!-- container -->
    </section>
    <?php /*include 'footer.html' */?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript" src="lib/jquery.bxslider/jquery.bxslider.min.js"></script>
    <script src="js/jquery-gallery.js"></script>
    <!-- Include Unite Gallery core files -->
    <script src='lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
    <script src='lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
    <script type="text/javascript">
    jQuery(document).ready(function() {
        /// on image hover show text js
        $(".img-circle-slider").hover(function() {
            var data = $(this).find(".text").html();
            $(".img-hover-text").hide().stop().html(data).fadeIn();

        }, function() {
            $(".img-hover-text").fadeOut();
        });
        /// on image hover show text js end

        // popup image gallery js
        jQuery("#gallery").unitegallery({
            slider_control_zoom: false,
        });
        // popup image gallery js end

        // popup floor plans js
        jQuery("#floor_plans").unitegallery({
            theme_enable_text_panel: false,
            theme_enable_play_button: false,
            theme_enable_fullscreen_button: false,
            slider_enable_progress_indicator: false,
            slider_control_swipe: false,
            slider_scale_mode: "down",
            strippanel_enable_buttons: true,
        });
        // popup floor plans js end

        //footer animation effect
        $(".footer-bottom-img").mouseover(function() {
            $(this).find(".plus-icon-change").addClass("fa-minus");
        });

        $(".footer-bottom-img").mouseout(function() {
            $(this).find(".plus-icon-change").removeClass("fa-minus");
        });
        //footer animation effect end
    });
    </script>
    <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
    <script>
    // NRI news slider js
    $('.nri-news-slider').bxSlider({
        slideWidth: 500,
        minSlides: 2,
        maxSlides: 3,
        moveSlides: 1,
        slideMargin: 10
    });
    // NRI news slider js end

    $('.property-img-circle-slider').bxSlider({
        slideWidth: 500,
        minSlides: 2,
        maxSlides: 4,
        moveSlides: 1,
        slideMargin: 10
    });

    //Key Amenities filter list js
    $(function() {
        var filterList = {
            init: function() {
                $('#KeyAmenities').mixItUp({
                    selectors: {
                        target: '.KeyAmenities-portfolio',
                        filter: '.KeyAmenities-list'
                    },
                    load: {
                        filter: '.parking, .upper_deck, .lower_deck'
                    }
                });
            }
        };
        filterList.init();
    });
    //Key Amenities filter list js end

    // Property layout and floor plan fliter js
    $(function() {
        var filterList = {
            init: function() {
                $('#PropertyLayout').mixItUp({
                    selectors: {
                        target: '.PropertyLayout-portfolio',
                        filter: '.PropertyLayout-list'
                    },
                    load: {
                        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
                    }
                });
            }
        };
        filterList.init();
    });
    // Property layout and floor plan fliter js end

    // Property layout and floor plan slider js
    $('.propertylayout-slider').bxSlider({
        slideWidth: 500,
        minSlides: 2,
        maxSlides: 4,
        moveSlides: 1,
        slideMargin: 10
    });
    // Property layout and floor plan slider js end
    </script>
    
    <script>
    $(document).jquerygallery({

        // displays a thumbnails navigation
        'coverImgOverlay': true,

        // CSS classes
        'imgActive': "imgActive",
        'thumbnail': "coverImgOverlay",
        'overlay': "overlay",

        // the height of the thumbnails
        'thumbnailHeight': 120,

        // custom navigation controls. 
        // requires Font Awesome
        'imgNext': "<i class='fa fa-angle-right'></i>",
        'imgPrev': "<i class='fa fa-angle-left'></i>",
        'imgClose': " ",

        // animation speed
        'speed': 300

    });
    </script>
    <!-- main menu with search -->
    <script src="js/main.js"></script>
    <script src="js/footer.js"></script>
    <!-- main menu with search -->
</body>

</html>
