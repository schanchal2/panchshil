
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panchshil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="lib/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="lib/jquery.bxslider/jquery.bxslider.css"/>
  <link rel='stylesheet' type='text/css' href='css/protected.css'/>

  <!-- Include Unite Gallery core files -->
	<link rel='stylesheet' type='text/css' href='lib/unitegallery/css/unite-gallery.css'/>
  <link rel='stylesheet' type='text/css' href='lib/unitegallery/themes/default/ug-theme-default.css'/>

  <link rel="stylesheet" type="text/css" href="css/app.css?Tuesday 27th of December 2016 05:20:19 PM"/>
  <!-- main menu with search -->
    <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
  <!-- main menu with search -->



</head>
<body class="nav-is-fixed">
  <?php include 'header.html' ?>
<div style="height:120px;"></div>
  <!-- header-->

<section class="property-section">
	<div class="container-fluid">
	  <div class="row">
	    <div class="col-md-12 property-slider">
	      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner">
          	<div class="item active">
              <img src="img/Property-Detai-slider-1.jpg" class="img-responsive" alt="First slide">
              <div class="carousel-caption">
                <p class="didot-font commercial-slider-title">chambers</p>
								<!-- <p class="didot-font commercial-slider-subtitle">PUNE</p> -->
              </div>
            </div>
            <div class="item">
              <img src="img/Property-Detai-slider-1.jpg" class="img-responsive" alt="Second slide">
              <div class="carousel-caption">
								<p class="didot-font commercial-slider-title">chambers</p>
								<!-- <p class="didot-font commercial-slider-subtitle">PUNE</p> -->
              </div>
            </div>
            <div class="item">
              <img src="img/Property-Detai-slider-1.jpg" class="img-responsive" alt="Third slide">
              <div class="carousel-caption">
								<p class="didot-font commercial-slider-title">chambers</p>
								<!-- <p class="didot-font commercial-slider-subtitle">PUNE</p> -->
              </div>
            </div>
          </div>
	    	</div>
        <div class="property-path">
					<div class="property-path-link">
	            <a href="#">Home</a> / <a href="#">Offering</a> / <a href="#">Commercial</a> / <a href="#">Chambers</a>
	        </div>
        </div>
	    </div>
	  </div>
	</div>
</section>

<section class="property-content-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="didot-font text-center">Headline Goes here..</h2>
				<br/><br/>
				<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
			</div>
		</div><!-- row end -->
		<br/>

		<div class="row">
			<div class="col-md-12 text-center">
				<ul class="property-details-link">
					<li><a href="#"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp;GOLD PROPERTY</a></li>
					<li><a href="#"><i class="fa fa-check-circle" aria-hidden="true"></i> &nbsp;UNDER CONSTRUCTION</a></li>
					<li><a href="" data-toggle="modal" data-target=".gallery-popup"><i class="fa fa-picture-o" aria-hidden="true"></i> &nbsp;VIEW GALLERY</a></li>
					<li><a href="#"><i class="fa fa-external-link" aria-hidden="true"></i> &nbsp;VISIT WEBSITE</a></li>
					<li><a href="#"><i class="fa fa-download" aria-hidden="true"></i> &nbsp;DOWNLOAD PDF</a></li>
				</ul>
			</div>
		</div><!-- row end -->
		<br/><br/>

	<!-- ============
		Gallery popup
	=================-->
	<div class="modal fade gallery-popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content gallery-modal-content">
			<!-- ======================== -->
			<div class="gallery-padding">
				<div id="gallery">
					<img alt="Preview Image 1"
					 src="img/Property-Detai-slider-1.jpg"
					 data-image="img/Property-Detai-slider-1.jpg"
					 data-description="Preview Image 1 Description"/>

					<img alt="Preview Image 2"
					 src="img/Panchashil_Media.jpg"
					 data-image="img/Panchashil_Media.jpg"
					 data-description="Preview Image 2 Description"/>

					<img alt="Preview Image 1"
 					 src="img/Property-Detai-slider-1.jpg"
 					 data-image="img/Property-Detai-slider-1.jpg"
 					 data-description="Preview Image 3 Description"/>

	 				<img alt="Preview Image 2"
					 src="img/Panchashil_Media.jpg"
					 data-image="img/Panchashil_Media.jpg"
					 data-description="Preview Image 4 Description"/>

					<img alt="Preview Image 1"
					 src="img/Property-Detai-slider-1.jpg"
					 data-image="img/Property-Detai-slider-1.jpg"
					 data-description="Preview Image 5 Description"/>

	 				<img alt="Preview Image 2"
					 src="img/Panchashil_Media.jpg"
					 data-image="img/Panchashil_Media.jpg"
					 data-description="Preview Image 6 Description"/>

					<img alt="Preview Image 1"
 					 src="img/Property-Detai-slider-1.jpg"
 					 data-image="img/Property-Detai-slider-1.jpg"
 					 data-description="Preview Image 7 Description"/>

	 	 			<img alt="Preview Image 2"
					 src="img/Panchashil_Media.jpg"
					 data-image="img/Panchashil_Media.jpg"
					 data-description="Preview Image 8 Description"/>

	 				<img alt="Preview Image 1"
					 src="img/Property-Detai-slider-1.jpg"
					 data-image="img/Property-Detai-slider-1.jpg"
					 data-description="Preview Image 9 Description"/>

	 	 			<img alt="Preview Image 2"
					 src="img/Panchashil_Media.jpg"
					 data-image="img/Panchashil_Media.jpg"
					 data-description="Preview Image 10 Description"/>
				</div>
			</div>
			<!-- ======================== -->
		</div>
		<p class="esc-close">ESC to close window</p>
	</div>
	</div>
	<!-- ============
		Gallery popup end
	=================-->

		<div class="row">
			<div class="col-md-12 property-img-circle">
				<div class="property-img-circle-slider">
					<div class="img-circle-slider">
		        <a href="#"><img src="img/updates_1.jpg" class="img-responsive img-circle" alt=""></a>
						<div class="img-overlay-count"><p class="didot-font">1</p></div>
						<div class="text">
							<h2 class="didot-font">1</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div>
		      <div class="img-circle-slider">
		        <a href="#"><img src="img/updates_2.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"><p class="didot-font">2</p></div>
						<div class="text">
							<h2 class="didot-font">2</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div>
		      <div class="img-circle-slider">
		        <a href="#"><img src="img/updates_3.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"><p class="didot-font">3</p></div>
						<div class="text">
							<h2 class="didot-font">3</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div>
		      <div class="img-circle-slider">
		        <a href="#"><img src="img/updates_1.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"><p class="didot-font">4</p></div>
						<div class="text">
							<h2 class="didot-font">4</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div>
		      <div class="img-circle-slider">
		        <a href="#"><img src="img/updates_2.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"><p class="didot-font">5</p></div>
						<div class="text">
							<h2 class="didot-font">5</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div>
		      <div class="img-circle-slider">
		      	<a href="#"><img src="img/updates_3.jpg" class="img-responsive img-circle" alt="Updates image"></a>
						<div class="img-overlay-count"><p class="didot-font">6</p></div>
						<div class="text">
							<h2 class="didot-font">6</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
						</div>
		      </div>
				</div>
			</div>
		</div><!-- row end -->

		<div class="row">
			<div class="col-md-12 text-center img-hover-text">
				<h2 class="didot-font">1</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br/>incididunt ut labore et dolore magna aliqua.</p>
			</div>
		</div><!-- row end -->
		<br/><br/>
		<hr class="hr-color"/>
    <!-- specifications -->
    <div class="row">
      <h2 class="text-center didot-bold-font">Specifications</h2>
      <br/><br/>
			<div class="col-md-3 text-center specifications">
        <img src="img/Commercial-icon/01.png"/>
        <p>Campus Spread<br/>(acres)</p>
        <h3 class="didot-font">47</h3>
      </div>
      <div class="col-md-3 text-center specifications">
        <img src="img/Commercial-icon/04.png"/>
        <p>Total built-up area.<br/>Completed.(millions sq.ft)</p>
        <h3 class="didot-font">4.5</h3>
      </div>
      <div class="col-md-3 text-center specifications">
        <img src="img/Commercial-icon/03.png"/>
        <p>Under Construction<br/>(millions sq.ft)</p>
        <h3 class="didot-font">2</h3>
      </div>
      <div class="col-md-3 text-center specifications">
        <img src="img/Commercial-icon/01.png"/>
        <p>Development Status<br/>(clusters completed)</p>
        <h3 class="didot-font">5</h3>
      </div>
    </div>
    <br/><br/><br/>
    <div class="row">
      <div class="col-md-3 col-md-offset-3 text-center">
          <p><b>Cluster Break-up:</b></p>
          <p class="specifications-area">Wings 1 & 4: 190,000 sq. ft. each</p>
          <p class="specifications-area">Wings 2 & 3: 290,000 sq. ft. each</p>
      </div>
      <div class="col-md-3 text-center">
        <p><b>Floor Plate Size:</b></p>
        <p class="specifications-area">Wings 1 & 4: 33,000 sq. ft. (approx.)</p>
        <p class="specifications-area">Wings 2 & 3: 39,000 sq. ft. (approx.)</p>
      </div>
    </div>
    <br/>
    <hr class="hr-color"/>

    <!-- specifications end -->
		<!-- Locate on Map -->
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center didot-bold-font">Locate on Map</h2>
				<br/>
				<img src="img/locate_on_map.jpg" class="img-responsive"/>
			</div>
		</div>
		<br/><br/>
		<!-- Locate on Map end -->
  </div><!-- container -->
</section>

<?php include 'footer.html' ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="lib/jquery.bxslider/jquery.bxslider.min.js"></script>

<!-- Include Unite Gallery core files -->
<script src='lib/unitegallery/js/unitegallery.min.js' type='text/javascript'></script>
<script src='lib/unitegallery/themes/default/ug-theme-default.js' type='text/javascript'></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  /// on image hover show text js
    $(".img-circle-slider").hover(function() {
    	 var data=$(this).find(".text").html();
    		$(".img-hover-text").hide().stop().html(data).fadeIn();

    	}, function() {
    		$(".img-hover-text").fadeOut();
    });
  /// on image hover show text js end

  // popup image gallery js
	   jQuery("#gallery").unitegallery({
       slider_control_zoom: false,
     });
  // popup image gallery js end

  // popup floor plans js
     jQuery("#floor_plans").unitegallery({
       theme_enable_text_panel: false,
       theme_enable_play_button: false,
       theme_enable_fullscreen_button: false,
       slider_enable_progress_indicator: false,
       slider_control_swipe:false,
       slider_scale_mode: "down",
       strippanel_enable_buttons: true,
     });
  // popup floor plans js end

  //footer animation effect
      $(".footer-bottom-img").mouseover(function(){
        $(this).find(".plus-icon-change").addClass( "fa-minus" );
      });

      $(".footer-bottom-img").mouseout(function(){
        $(this).find(".plus-icon-change").removeClass( "fa-minus" );
      });
  //footer animation effect end
   });
</script>

<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
<script>
// NRI news slider js
  $('.nri-news-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });
// NRI news slider js end

  $('.property-img-circle-slider').bxSlider({
    slideWidth: 500,
    minSlides: 2,
    maxSlides: 4,
    moveSlides: 1,
    slideMargin: 10
  });

//Key Amenities filter list js
	$(function () {
	var filterList = {
		init: function () {
			$('#KeyAmenities').mixItUp({
				selectors: {
					target: '.KeyAmenities-portfolio',
					filter: '.KeyAmenities-list'
				},
				load: {
					filter: '.parking, .upper_deck, .lower_deck'
				}
			});
		}
	};
	filterList.init();
});
//Key Amenities filter list js end

// Property layout and floor plan fliter js
$(function () {
var filterList = {
  init: function () {
    $('#PropertyLayout').mixItUp({
      selectors: {
        target: '.PropertyLayout-portfolio',
        filter: '.PropertyLayout-list'
      },
      load: {
        filter: '.3bhk, .4bhk, .5bhk-podium-villa, .5bhk-penthouse'
      }
    });
  }
};
filterList.init();
});
// Property layout and floor plan fliter js end

// Property layout and floor plan slider js
$('.propertylayout-slider').bxSlider({
  slideWidth: 500,
  minSlides: 2,
  maxSlides: 4,
  moveSlides: 1,
  slideMargin: 10
});
// Property layout and floor plan slider js end
</script>
<!-- main menu with search -->
<script src="js/main.js"></script>
<script src="js/footer.js"></script>
<!-- main menu with search -->
</body>
</html>
